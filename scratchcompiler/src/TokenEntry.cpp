#include "TokenEntry.hpp"

TokenEntry::TokenEntry(std::string key) :
    key(key) {}

TokenEntry::TokenEntry(std::string key, std::string re) :
    key(key),
    re(re) {}

bool TokenEntry::operator==(const TokenEntry &b) {
    return key == b.key;
}

bool TokenEntry::operator!=(const TokenEntry &b) {
    return !(*this == b);
}

