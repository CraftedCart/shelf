#include "Generator.hpp"
#include <sstream>
#include <iostream>
#include <algorithm>
#include <iterator>
#include <iomanip>

std::string Generator::generate(Node *tree) {
    std::stringstream outStr;

    if (RootNode *node = dynamic_cast<RootNode*>(tree)) {
        outStr << generateRootNode(*node);
    } else if (DefNode *node = dynamic_cast<DefNode*>(tree)) {
        outStr << generateDefNode(*node);
    } else if (AssignNode *node = dynamic_cast<AssignNode*>(tree)) {
        outStr << generateAssignNode(*node);
    } else if (CallNode *node = dynamic_cast<CallNode*>(tree)) {
        outStr << generateCallNode(*node);
    } else if (VarRefNode *node = dynamic_cast<VarRefNode*>(tree)) {
        outStr << generateVarRefNode(*node);
    } else if (IntegerNode *node = dynamic_cast<IntegerNode*>(tree)) {
        outStr << generateIntegerNode(*node);
    } else if (FloatNode *node = dynamic_cast<FloatNode*>(tree)) {
        outStr << generateFloatNode(*node);
    } else if (StringNode *node = dynamic_cast<StringNode*>(tree)) {
        outStr << generateStringNode(*node);
    } else if (VarNode *node = dynamic_cast<VarNode*>(tree)) {
        outStr << generateVarNode(*node);
    } else {
        throw std::runtime_error(std::string("Unexpected node type: ") + typeid(*tree).name());
    }

    return outStr.str();
}

std::string Generator::generateRootNode(RootNode &node) {
    std::stringstream outStr;
    for (Node *node : node.nodes) outStr << generate(node);
    return outStr.str();
}

std::string Generator::generateDefNode(DefNode &node) {
    std::stringstream outStr;

    outStr << "function " << node.name << "(" << stringVecToCommaSep(node.argNames) << "){";

    for (Node *bodyNode : node.body) {
        outStr << generate(bodyNode) << ";";
    }

    outStr << "};";
    return outStr.str();
}

std::string Generator::generateAssignNode(AssignNode &node) {
    return node.identifier + "=" + generate(node.expr);
}

std::string Generator::generateCallNode(CallNode &node) {
    return node.name + "(" + exprVecToCommaSep(node.argExprs) + ")";
}

std::string Generator::generateVarRefNode(VarRefNode &node) {
    return node.identifier;
}

std::string Generator::generateIntegerNode(IntegerNode &node) {
    return std::to_string(node.value);
}

std::string Generator::generateFloatNode(FloatNode &node) {
    std::stringstream str;
    str << std::setprecision(99999) << node.value;

    return str.str();
}

std::string Generator::generateStringNode(StringNode &node) {
    return "\"" + node.str + "\"";
}

std::string Generator::generateVarNode(VarNode &node) {
    return "let " + node.name;
}

std::string Generator::stringVecToCommaSep(std::vector<std::string> &vec) {
    std::stringstream commaSepStr;

    for (auto iter = vec.begin(); iter != vec.end(); iter++) {
        if (iter != vec.begin()) commaSepStr << ",";
        commaSepStr << *iter;
    }

    return commaSepStr.str();
}

std::string Generator::exprVecToCommaSep(std::vector<Node*> &vec) {
    std::stringstream commaSepStr;

    for (auto iter = vec.begin(); iter != vec.end(); iter++) {
        if (iter != vec.begin()) commaSepStr << ",";

        commaSepStr << generate(*iter);
    }

    return commaSepStr.str();
}

