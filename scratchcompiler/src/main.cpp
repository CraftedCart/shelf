#include <fstream>
#include <iostream>
#include <sstream>
#include "Tokenizer.hpp"
#include "Parser.hpp"
#include "Generator.hpp"

int main(int argc, char *argv[]) {
    std::ifstream inFile(argv[1]);

    if (!inFile) {
        std::cerr << "Failed to open file" << std::endl;
        exit(1);
    }

    std::stringstream buffer;
    buffer << inFile.rdbuf();
    std::string str = buffer.str();

    Tokenizer tokenizer(str);
    std::vector<Token> tokens = tokenizer.tokenize();

    Parser parser(tokens);
    Node* node = parser.parse();

    Generator generator;
    std::string genCode = generator.generate(node);

    static const std::string RUNTIME = "function add(a,b){return a+b;};function print(a){console.log(a);};main();";
    genCode += RUNTIME;

    std::cout << genCode << std::endl;

    //TODO: delete the node and implement destructors

    return 0;
}

