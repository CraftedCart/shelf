#include "Parser.hpp"
#include <stdexcept>

Parser::Parser(std::vector<Token> &tokens) : tokens(tokens) {}

RootNode* Parser::parse() {
    std::vector<Node*> nodes;

    while (!tokens.empty()) {
        nodes.push_back(parseDef());
    }

    return new RootNode(nodes);
}

DefNode* Parser::parseDef() {
    consume("def");

    std::string name = consume("identifier").str;
    std::vector<std::string> argNames = parseArgNames();

    consume("beginBlock");

    std::vector<Node*> body;
    while (!peek("endBlock")) {
        body.push_back(parseExpr());
        consume("terminator");
    }

    consume("endBlock");

    return new DefNode(name, argNames, body);
}

std::vector<std::string> Parser::parseArgNames() {
    std::vector<std::string> argNames;

    consume("oparen");

    //Get function args (if any)
    if (peek("identifier")) {
        argNames.push_back(consume("identifier").str);

        while (peek("comma")) {
            consume("comma");
            argNames.push_back(consume("identifier").str);
        }
    }

    consume("cparen");

    return argNames;
}

Node* Parser::parseExpr() {
    if (peek("integer")) {
        return parseInteger();
    } else if (peek("float")) {
        return parseFloat();
    } else if (peek("string")){
        return parseString();
    } else if (peek("identifier") && peek("oparen", 1)) {
        return parseCall();
    } else if (peek("identifier") && peek("assign", 1)) {
        return parseAssign();
    } else if (peek("var")) {
        return parseVar();
    } else {
        return parseVarRef();
    }
}

IntegerNode* Parser::parseInteger() {
    int integer = std::stoi(consume("integer").str);
    return new IntegerNode(integer);
}

FloatNode* Parser::parseFloat() {
    float fl = std::stof(consume("float").str);
    return new FloatNode(fl);
}

StringNode* Parser::parseString() {
    std::string str = consume("string").str;
    str = str.substr(1, str.length() - 2); //Remove the first and last chars (The quote marks)
    return new StringNode(str);
}

CallNode* Parser::parseCall() {
    std::string name = consume("identifier").str;
    std::vector<Node*> argExprs = parseArgExprs();

    return new CallNode(name, argExprs);
}

AssignNode* Parser::parseAssign() {
    std::string identifier = consume("identifier").str;
    consume("assign");
    Node *expr = parseExpr();

    return new AssignNode(identifier, expr);
}

VarNode* Parser::parseVar() {
    consume("var");
    std::string name = consume("identifier").str;

    return new VarNode(name);
}

std::vector<Node*> Parser::parseArgExprs() {
    std::vector<Node*> argExprs;

    consume("oparen");

    if (!peek("cparen")) {
        argExprs.push_back(parseExpr());

        while (peek("comma")) {
            consume("comma");
            argExprs.push_back(parseExpr());
        }
    }

    consume("cparen");

    return argExprs;
}

VarRefNode* Parser::parseVarRef() {
    std::string identifier = consume("identifier").str;

    return new VarRefNode(identifier);
}

Token Parser::consume(std::string expectedType) {
    Token token = tokens[0];
    tokens.erase(tokens.begin()); //Remove the first entry

    if (token.type.key == expectedType) {
        return token;
    } else {
        throw std::runtime_error("Expected token type \"" + expectedType + "\" but got \"" + token.type.key + "\"");
    }
}

bool Parser::peek(std::string expectedType, unsigned int offset) {
    return tokens[offset].type.key == expectedType;
}

