#include "DefNode.hpp"

DefNode::DefNode(std::string name, std::vector<std::string> argNames, std::vector<Node*> body) :
    name(name),
    argNames(argNames),
    body(body) {}

