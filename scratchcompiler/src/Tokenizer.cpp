#include "Tokenizer.hpp"

Tokenizer::Tokenizer(std::string code) : code(code) {}

std::vector<Token> Tokenizer::tokenize() {
    boost::trim_right(code); //Remove trailing whitespace

    std::vector<Token> tokens;

    while (!code.empty()) {
        tokens.push_back(tokenizeOne());

        //Strip leading whitespace
        boost::trim_left(code);
    }

    return tokens;
}

Token Tokenizer::tokenizeOne() {
    for (TokenEntry token : TOKEN_TYPES) {
        boost::regex re("\\A(" + token.re + ")", boost::regex::perl);

        boost::cmatch match;
        if (boost::regex_search(code.c_str(), match, re)) {
            std::string matchedTokenEntry = std::string(match[1].first, match[1].second);

            code.erase(0, matchedTokenEntry.length());
            return Token(token, matchedTokenEntry);
        }
    }

    throw std::runtime_error("Couldn't match token on the code:\n" + code + "\n");
}

