#ifndef SCRATCHCOMPILER_TOKEN_HPP
#define SCRATCHCOMPILER_TOKEN_HPP

#include "TokenEntry.hpp"

class Token {
    public:
        TokenEntry type;
        std::string str;

    public:
        Token(TokenEntry type, std::string str);
};

#endif

