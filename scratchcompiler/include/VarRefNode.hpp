#ifndef SCRATCHCOMPILER_VARREFNODE_HPP
#define SCRATCHCOMPILER_VARREFNODE_HPP

#include "Node.hpp"
#include <string>

class VarRefNode : public Node {
    public:
        std::string identifier;

    public:
        VarRefNode(std::string identifier);
};

#endif

