#ifndef SCRATCHCOMPILER_GENERATOR_HPP
#define SCRATCHCOMPILER_GENERATOR_HPP

#include "RootNode.hpp"
#include "DefNode.hpp"
#include "AssignNode.hpp"
#include "CallNode.hpp"
#include "VarRefNode.hpp"
#include "IntegerNode.hpp"
#include "FloatNode.hpp"
#include "StringNode.hpp"
#include "VarNode.hpp"
#include <string>

class Generator {
    protected:
        std::string generateRootNode(RootNode &node);
        std::string generateDefNode(DefNode &node);
        std::string generateAssignNode(AssignNode &node);
        std::string generateCallNode(CallNode &node);
        std::string generateVarRefNode(VarRefNode &node);
        std::string generateIntegerNode(IntegerNode &node);
        std::string generateFloatNode(FloatNode &node);
        std::string generateStringNode(StringNode &node);
        std::string generateVarNode(VarNode &node);

        std::string stringVecToCommaSep(std::vector<std::string> &vec);
        std::string exprVecToCommaSep(std::vector<Node*> &vec);

    public:
        std::string generate(Node *tree);
};

#endif

