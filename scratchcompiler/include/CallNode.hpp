#ifndef SCRATCHCOMPILER
#define SCRATCHCOMPILER

#include "Node.hpp"
#include <vector>
#include <string>

class CallNode : public Node {
    public:
        std::string name;
        std::vector<Node*> argExprs;

    public:
        CallNode(std::string name, std::vector<Node*> argExprs);
};

#endif

