#ifndef SCRATCHCOMPILER_PARSER_HPP
#define SCRATCHCOMPILER_PARSER_HPP

#include "Token.hpp"
#include "RootNode.hpp"
#include "DefNode.hpp"
#include "AssignNode.hpp"
#include "IntegerNode.hpp"
#include "FloatNode.hpp"
#include "StringNode.hpp"
#include "CallNode.hpp"
#include "VarNode.hpp"
#include "VarRefNode.hpp"
#include <vector>

class Parser {
    protected:
        std::vector<Token> &tokens;

    public:
        Parser(std::vector<Token> &tokens);

        RootNode* parse();
        DefNode* parseDef();
        std::vector<std::string> parseArgNames();
        Node* parseExpr();
        IntegerNode* parseInteger();
        FloatNode* parseFloat();
        StringNode* parseString();
        CallNode* parseCall();
        AssignNode* parseAssign();
        VarNode* parseVar();
        std::vector<Node*> parseArgExprs();
        VarRefNode* parseVarRef();

        Token consume(std::string expectedType);
        bool peek(std::string expectedType, unsigned int offset = 0);
};

#endif

