#ifndef SCRATCHCOMPILER_DEFNODE_HPP
#define SCRATCHCOMPILER_DEFNODE_HPP

#include "Node.hpp"
#include "IntegerNode.hpp"
#include <vector>
#include <string>

class DefNode : public Node {
    public:
        std::string name;
        std::vector<std::string> argNames;
        std::vector<Node*> body;

    public:
        DefNode(std::string name, std::vector<std::string> argNames, std::vector<Node*> body);
};

#endif

