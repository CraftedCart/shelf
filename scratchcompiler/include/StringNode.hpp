#ifndef SCRATCHCOMPILER_STRINGNODE_HPP
#define SCRATCHCOMPILER_STRINGNODE_HPP

#include "Node.hpp"
#include <string>

class StringNode : public Node {
    public:
        std::string str;

    public:
        StringNode(std::string str);
};

#endif

