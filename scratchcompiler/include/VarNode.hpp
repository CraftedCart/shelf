#ifndef SCRATCHCOMPILER_VARNODE_HPP
#define SCRATCHCOMPILER_VARNODE_HPP

#include "Node.hpp"
#include <string>

class VarNode : public Node {
    public:
        std::string name;

    public:
        VarNode(std::string name);
};

#endif

