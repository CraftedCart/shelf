#ifndef SCRATCHCOMPILER_NODE_HPP
#define SCRATCHCOMPILER_NODE_HPP

class Node {
    public:
        virtual ~Node() = default;
};

#endif

