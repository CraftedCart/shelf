#ifndef SCRATCHCOMPILER_ROOTNODE_HPP
#define SCRATCHCOMPILER_ROOTNODE_HPP

#include "Node.hpp"
#include <vector>

class RootNode : public Node {
    public:
        std::vector<Node*> nodes;

    public:
        RootNode(std::vector<Node*> nodes);
};

#endif

