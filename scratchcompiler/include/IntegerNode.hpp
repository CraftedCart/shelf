#ifndef SCRATCHCOMPILER_INTEGERNODE
#define SCRATCHCOMPILER_INTEGERNODE

#include "Node.hpp"

class IntegerNode : public Node {
    public:
        int value;

    public:
        IntegerNode(int value);
};

#endif

