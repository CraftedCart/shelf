#ifndef SCRATCHCOMPILER_FLOATNODE_HPP
#define SCRATCHCOMPILER_FLOATNODE_HPP

#include "Node.hpp"

class FloatNode : public Node {
    public:
        float value;

    public:
        FloatNode(float value);
};

#endif

