#ifndef SCRATCHCOMPILER_TOKENENTRY_HPP
#define SCRATCHCOMPILER_TOKENENTRY_HPP

#include <string>

class TokenEntry {
    public:
        std::string key;
        std::string re;

    public:
        TokenEntry(std::string key);
        TokenEntry(std::string key, std::string re);

        bool operator==(const TokenEntry &b);
        bool operator!=(const TokenEntry &b);
};

#endif

