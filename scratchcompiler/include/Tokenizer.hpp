#ifndef SCRATCHCOMPILER_TOKENIZER_HPP
#define SCRATCHCOMPILER_TOKENIZER_HPP

#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>
#include <array>
#include <vector>
#include "TokenEntry.hpp"
#include "Token.hpp"

class Tokenizer {
    protected:
        std::string code;

        const std::array<TokenEntry, 13> TOKEN_TYPES = {{
            TokenEntry("def", "\\bdef\\b"),
            TokenEntry("var", "\\bvar\\b"),
            TokenEntry("beginBlock", "{"),
            TokenEntry("endBlock", "}"),
            TokenEntry("identifier", "\\b[a-zA-Z]+\\b"),
            TokenEntry("float", "\\b[0-9]+\\.[0-9]+f\\b"),
            TokenEntry("integer", "\\b[0-9]+\\b"),
            TokenEntry("oparen", "\\("),
            TokenEntry("cparen", "\\)"),
            TokenEntry("string", "\".*\""),
            TokenEntry("assign", "="),
            TokenEntry("comma", ","),
            TokenEntry("terminator", ";"),
        }};

    public:
        Tokenizer(std::string code);

        std::vector<Token> tokenize();

        Token tokenizeOne();
};

#endif

