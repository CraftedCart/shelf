#ifndef SCRATCHCOMPILER_ASSIGNNODE_HPP
#define SCRATCHCOMPILER_ASSIGNNODE_HPP

#include "Node.hpp"
#include <string>

class AssignNode : public Node {
    public:
        std::string identifier;
        Node *expr;

    public:
        AssignNode(std::string identifier, Node *expr);
};

#endif

