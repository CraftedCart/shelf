module Typeclasses where

data Trivial = Trivial

instance Eq Trivial where
  Trivial == Trivial = True
  -- (==) Trivial Trivial = True -- Means the same as the line above

------------------------------------------------------------------------------------------------------------------------

data DayOfWeek = Mon | Tue | Wed | Thu | Fri | Sat | Sun
    deriving (Show, Ord)

instance Eq DayOfWeek where
  (==) Mon Mon = True
  (==) Tue Tue = True
  (==) Wed Wed = True
  (==) Thu Thu = True
  (==) Fri Fri = True
  (==) Sat Sat = True
  (==) Sun Sun = True
  (==) _ _ = False

-- Day of week and numerical day of month
data Date = Date DayOfWeek Int
    deriving Show

instance Eq Date where
  (==) (Date weekday dayOfMonth) (Date weekday' dayOfMonth') =
      weekday == weekday' &&
      dayOfMonth == dayOfMonth'

------------------------------------------------------------------------------------------------------------------------

data Identity a = Identity a

instance Eq a => Eq (Identity a) where
  (==) (Identity val) (Identity val') = val == val'

------------------------------------------------------------------------------------------------------------------------

data TisAnInteger = TisAn Integer
data TwoIntegers = Two Integer Integer
data StringOrInt = TisAnInt Int | TisAString String
data Pair a = Pair a a
data Tuple a b = Tuple a b
data Which a = ThisOne a | ThatOne a
data EitherOr a b = Hello a | Goodbye b

instance Eq TisAnInteger where
  (==) (TisAn int) (TisAn int') = int == int'

instance Eq TwoIntegers where
  (==) (Two intA intB) (Two intA' intB') = intA == intA' && intB == intB'

instance Eq StringOrInt where
  (==) (TisAnInt int) (TisAnInt int') = int == int'
  (==) (TisAString str) (TisAString str') = str == str'
  (==) _ _ = False

instance Eq a => Eq (Pair a) where
  (==) (Pair a b) (Pair a' b') = a == a' && b == b'

instance (Eq a, Eq b) => Eq (Tuple a b) where
  (==) (Tuple a b) (Tuple a' b') = a == a' && b == b'

instance Eq a => Eq (Which a) where
  (==) (ThisOne val) (ThisOne val') = val == val'
  (==) (ThatOne val) (ThatOne val') = val == val'
  (==) _ _ = False

instance (Eq a, Eq b) => Eq (EitherOr a b) where
  (==) (Hello a) (Hello a') = a == a'
  (==) (Goodbye b) (Goodbye b') = b == b'
  (==) _ _ = False
