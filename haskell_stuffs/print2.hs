module Print2 where

helloGreeting :: String
helloGreeting = "Hello, " ++ "world!"

hello :: String
hello = "Hello"

world :: String
world = "world!"

main :: IO ()
main = do
    putStrLn helloGreeting
    putStrLn secondGreeting
        where
            secondGreeting = concat [hello, ", ", world]
