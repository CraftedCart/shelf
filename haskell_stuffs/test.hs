sayHello :: String -> IO ()
sayHello whom =
    putStrLn ("Hello, " ++ whom ++ "!")

hi = x * 3 + y
  where
    x = 3
    y = 1000

hi2 = x * 5
  where
    y = 100
    x = 10 * 5 + y

hi3 = z / x + y
  where
    x = 7
    y = negate x
    z = y * 10

waxOn = x * 5
  where
    x = y ^ 2
    y = z + 8
    z = 7

waxOff val = triple val - 10

triple val = val * 3

thirdChar :: [a] -> a
thirdChar val = val !! 2

rvrs :: String
rvrs = (drop 9 str) ++ " " ++ (take 2 $ drop 6 str) ++ " " ++ (take 5 str)
  where
    str = "Curry is awesome"

data Mood = Woot | Bleh deriving Show

changeMood :: Mood -> Mood
changeMood Woot = Bleh
changeMood Bleh = Woot

mrowl :: String -> IO ()
mrowl hi =
    if yay
       then putStrLn "nya~"
       else putStrLn "aw"

  where
    yay = hi == "nya"

isPalindrome :: Eq a => [a] -> Bool
isPalindrome val = val == (reverse val)

myAbs :: (Num a, Ord a) => a -> a
myAbs val = if val < 0
               then negate val
               else val

tupleStuff :: (a, b) -> (c, d) -> ((b, d), (a, c))
tupleStuff left right = ((snd left, snd right), (fst left, fst right))
-- tupleStuff (a, b) (c, d) = ((b, d), (a, c))

factorial :: Integer -> Integer
factorial 0 = 1
factorial n = n * factorial (n - 1)

over9000 :: Num a => a -> a
over9000 = (+9000)

-- VVVVV Typechecking tests below VVVVV --

functionH :: [a] -> a
functionH (x:_) = x

functionC :: Ord a => a -> a -> Bool
functionC x y =
    if (x > y) then True else False

functionS :: (x, y) -> y
functionS (x, y) = y

i :: a -> a
i a = a

c :: a -> b -> a
c a b = a

c' :: a -> b -> b
c' a b = b

r :: [a] -> [a]
r a = a

co :: (b -> c) -> (a -> b) -> a -> c
co bToC aToB a = (bToC (aToB a))

a :: (a -> c) -> a -> a
a aToC a = a

a' :: (a -> b) -> a -> b
a' aToB a = aToB a
