-- |Fibbonachi
fibs :: Num a => [a]
fibs = 0 : 1 : [ a + b | (a, b) <- zip fibs (tail fibs) ]

calcTimeRemaining :: Num a => a -> a -> a
calcTimeRemaining totalTime timeElapsed = totalTime - timeElapsed

calcTimeRemainingFrom60 :: Num a => a -> a
calcTimeRemainingFrom60 = calcTimeRemaining 60

sumAll :: Num a => [a] -> a
sumAll (x:xs) = x + sumAll xs
sumAll [] = 0

sumAll2 :: Num a => [a] -> a
sumAll2 = foldr (+) 0

showTime :: Int -> Int -> String
showTime hours mins
  | hours == 0 = "12:" ++ (show2figures mins) ++ " am"
  | hours < 12 = (show2figures hours) ++ ":" ++ (show2figures mins) ++ " am"
  | hours == 12 = "12:" ++ (show2figures mins) ++ " pm"
  | otherwise = (show2figures (hours - 12)) ++ ":" ++ (show2figures mins) ++ " pm"
  where
  show2figures value
    | value < 10 = "0" ++ (show value)
    | otherwise = show value

showLen :: [a] -> String
showLen [x] = "1 item"
showLen list = (show (length list)) ++ " items"

main = do
  putStr "1> "
  a <- getLine
  putStr "2> "
  b <- getLine
  putStrLn (show a ++ show b)

ascendingNumbers :: Num a => a -> [a]
ascendingNumbers from = from : ascendingNumbers (from + 1)
