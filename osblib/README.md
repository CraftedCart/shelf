osblib
======

The idea was to make a library/program of sorts to make making [osu!](https://osu.ppy.sh) storyboards easier - in the
end I only ever used it for a triangle-fire effect that's hardcoded in there.
