package craftedcart.osblib;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class OSBLib {

    public static void main(String[] args) {
        List<String> sb = new ArrayList<>();
        Random rand = new Random();

        sb.add("[Events]");
        //Luna BG
        sb.add("Sprite,Background,Centre,\"background2.jpg\",320,240");
        sb.add(" F,0,63754,64754,1,0");
        sb.add(" S,0,63754,64754,0.52,0.5");

        for (int i = 0; i < 900; i++) {
            sb.add("Sprite,Foreground,Centre,\"sb\\triangle.png\",320,240");
            sb.add(" L,63754,12");

            int startTime = rand.nextInt(5001);
            int endTime = startTime + 5000;

            //Fade
            float fadeStart = rand.nextFloat();
            sb.add(String.format("  F,0,%d,%d,%f,0", startTime, endTime, fadeStart));

            //Vector Scale
            float scaleStart = rand.nextFloat() * 0.6f;
            float scaleEndX = ((rand.nextFloat() - 0.5f) * 2.0f) * scaleStart;
            float scaleEndY = ((rand.nextFloat() - 0.5f) * 2.0f) * scaleStart;
            sb.add(String.format("  V,0,%d,%d,%f,%f,%f,%f", startTime, endTime, scaleStart, scaleStart, scaleEndX, scaleEndY));

            //Rotation
            float rotStart = (rand.nextFloat() - 0.5f) * 4.0f;
            float rotEnd = (rand.nextFloat() - 0.5f) * 4.0f;
            sb.add(String.format("  R,0,%d,%d,%f,%f", startTime, endTime, rotStart, rotEnd));

            //Position
            float posStartX = rand.nextInt(640);
            float posEndX = posStartX + rand.nextInt(120) - 60;
            float posEndY = rand.nextInt(480);
            sb.add(String.format("  M,0,%d,%d,%f,550,%f,%f", startTime, endTime, posStartX, posEndX, posEndY));

            //Color
            int startR = rand.nextInt(64);
            int startG = rand.nextInt(128) + 100;
            int startB = rand.nextInt(64) + 128 + 64;
            sb.add(String.format("  C,0,%d,%d,%d,%d,%d,64,200,128", startTime, endTime, startR, startG, startB));
        }

        //Copy the sb to the clipboard
        StringBuilder builder = new StringBuilder();
        for (String str : sb) {
            builder.append(str).append('\n');
        }
        builder.append('\n');

        String str = builder.toString();
        Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
        c.setContents(new StringSelection(str), null);

        System.out.println("Copying storyboard to the clipboard");
        System.out.println("Now sleeping for 5s in case you are using X11, because the program must stay alive to copy");

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
