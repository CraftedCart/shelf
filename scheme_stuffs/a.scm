
(define hello-str "Hello, world!")
(define (say-hello name)
  (display (string-append "Hello, " name "!"))
  (newline)
  )
(define (absolutely val)
  (if (< val 0)
      (* val -1)
      val
      )
  )
(define (recp val)
  (if (= val 0)
      #f
      (/ 1 val)
      )
  )

(define (asciify val)
  (if (< val 33)
      #f
      (if (> val 126)
          #f
          (integer->char val)
          )
      )
  )

(define (fact n)
  (if (= n 1)
      1
      (* n (fact (- n 1)))
      )
  )

(define-syntax nil!
  (syntax-rules () ((_ x)
     (set! x '())
     )
    )
  )

;; This is standard anyway
(define-syntax when
  (syntax-rules () ((_ pred b1 ...)
                    (if pred (begin b1 ...))
                    )
                )
  )

(define (a b c)
  (+ 2 2)
  )

(define (main argv)
  (define x 99)

  (say-hello "Alice")

  (display (absolutely 24)) (newline)
  (display (absolutely 70)) (newline)
  (display (absolutely -3)) (newline)
  (display (absolutely -81)) (newline)
  (display (absolutely 0)) (newline)

  (display (recp 4)) (newline)
  (display (recp 17)) (newline)
  (display (recp -6)) (newline)
  (display (recp 0)) (newline)

  (display (asciify 7)) (newline)
  (display (asciify 42)) (newline)
  (display (asciify 74)) (newline)
  (display (asciify 999)) (newline)

  (let ((a 7))
    (display(+ a a)) (newline)
    )

  (display (fact 5)) (newline)

  (let loop ((i 20))
    (display (string-append "Loop: " (number->string i))) (newline)

    (if (> i 0)
        (loop (- i 1))
        )
    )

  (set! x 24)
  (display x) (newline)
  (nil! x)
  (display x) (newline)

  (when (< 4 5)
    (display "4 is ")
    (display "less than 5")
    (newline)
    )

  (if (< 4 5)
      (begin
        (display "4 is ")
        (display "less than 5")
        (newline)
        )
    )

  (when (< 5 4)
    (display "5 is ")
    (display "less than 4")
    (newline)
    )

  (display (a 'time 7)) (newline)
  )

