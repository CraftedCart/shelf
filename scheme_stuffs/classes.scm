(define (vec2 x y)
  (let ((x x)
        (y y)
        )

    (define (get-x) x)
    (define (set-x new-x) (set! x new-x))
    (define (get-y) y)
    (define (set-y new-y) (set! y new-y))

    (define (self command . params)
      (cond ((eqv? command 'get-x) (apply get-x params))
            ((eqv? command 'set-x) (apply set-x params))
            ((eqv? command 'get-y) (apply get-y params))
            ((eqv? command 'set-y) (apply set-y params))
            (else (error "Invalid command"))
            )
      )

    self
    )
  )

(define (main argv)
  (define point-a (vec2 4 8))
  (define point-b (vec2 -2 5))

  (display (point-a 'get-x)) (newline)
  (point-a 'set-x 39)
  (display (point-a 'get-x)) (newline)

  (display (point-b 'get-y)) (newline)
  (point-b 'set-y 48)
  (display (point-b 'get-y)) (newline)
  )

