;; The hash-table that will contain the procedures and their associated
;; documentation strings as well as their code for later use.
(define *documentation-hash-table* (make-hash-table))

;; Documented define er-macro
;; (define-syntax (define* form r c)
;;   (let* ((args-form (second form))     ; (proc args)
;;          (proc-name (first args-form)) ;  proc
;;          (doc (third form))            ; "The docstring"
;;          (body (drop form 3)))         ; (everything else)
;;     (hash-table-set! *documentation-hash-table* proc-name (list doc body)) ; insert the doc and code body
;;     `(,(r 'define) ,args-form ,@body)  ; Define the procedure as normal
;;     )
;;   )

;; Example of a documented procedure, will print a nice-ish list of documented procedures
;; (define* (print-docs)
;;   "Print a list of documented procedures"
;;   (hash-table-walk *documentation-hash-table*
;;                    (lambda (key val)
;;                      (printf "~a => ~a~n  ~a~n~n" key (first val) (second val))
;;                      )
;;                    )
;;   )

;; (define* (main argv)
;;   "Hey"
;;   (print-docs)
;;   )

(define-syntax nil!
  (syntax-rules ()
    ((_ x)
     (begin
       (set! x '())))))

(define a 28)
(nil! a)

;; define with docstring
(define-syntax define*
  (syntax-rules ()
    ((_ form docstring contents ...)
     (begin
       (display docstring) (newline)
       (define form contents ...)))))

;; Example of a documented procedure, will print a nice-ish list of documented procedures
(define* (print-docs)
  "Print a list of documented procedures"
  ;; (hash-table-walk *documentation-hash-table*
  ;;                  (lambda (key val)
  ;;                    (printf "~a => ~a~n  ~a~n~n" key (first val) (second val))))
  (display "Hai") (newline)
  (display "Hai") (newline)
  (display "Hai") (newline)
  (display "Hai") (newline)
  )

(define* (print-some-stuffs stuffs)
  "Prints... something. Whatever you tell it to"

  (display stuffs) (newline)
  (display stuffs) (newline)
  (display stuffs) (newline)
  (display stuffs) (newline)
  )

(print-some-stuffs "haiii")
;; (display (hash-map *documentation-hash-table*))
