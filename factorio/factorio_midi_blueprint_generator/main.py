#!/usr/bin/env python3

import base64
import json
import math
import sys

import zlib
from typing import List, Dict

import mido
# noinspection PyProtectedMember
from mido import MidiFile

GAME_TICKRATE = 60
MIDI_PPQ = 480


class NoteStorage:
    def __init__(self, game_tick: int, pitch: int):
        self.game_tick = int(game_tick)
        self.pitch = int(pitch)


class TrackStorage:
    def __init__(self):
        self.notes: List[NoteStorage] = []
        self.instrument = "piano"
        self.channel = None


def to_blueprint(tracks: List[TrackStorage]) -> str:
    for track in tracks:
        # Check for negative notes, convert to bass, and bring up the pitch
        lowest_pitch = 0
        for note in track.notes:
            if note.pitch <= 0:
                track.instrument = "bass"

                if note.pitch < lowest_pitch:
                    lowest_pitch = note.pitch

        if track.instrument == "bass":
            for note in track.notes:
                note.pitch += 12 * math.floor((abs(lowest_pitch) + 1) / 12)

        # Check for too high notes, and bring down the pitch
        highest_pitch = 52
        too_high = False
        for note in track.notes:
            if note.pitch > 52:
                too_high = True

                if note.pitch > highest_pitch:
                    highest_pitch = note.pitch

        if too_high:
            for note in track.notes:
                note.pitch -= 12 * math.floor((highest_pitch - 52) / 12)

    out = {
        "blueprint": {
            "icons": [
                {
                    "signal": {
                        "type": "item",
                        "name": "decider-combinator"
                    },
                    "index": 1
                },
                {
                    "signal": {
                        "type": "item",
                        "name": "programmable-speaker"
                    },
                    "index": 2
                }
            ],

            "entities": [],

            "item": "blueprint",
            "version": 68722819072
        }
    }

    entity_num = 1

    track_index = 0
    column = 0
    first_deciders: List[Dict] = []  # Index: Column, Value: Entity dict
    for track in tracks:
        prev_note_time = -1
        prev_note_signal = "A"
        max_simultaneous_notes = 0
        current_simultaneous_notes = 0
        row = 0
        for note in track.notes:
            if prev_note_time == note.game_tick:
                next_letter = chr(ord(prev_note_signal) + 1)
                note_signal = "signal-" + next_letter
                current_simultaneous_notes += 1

                prev_note_signal = next_letter
            else:
                note_signal = "signal-A"
                prev_note_signal = "A"
                current_simultaneous_notes = 1

            if current_simultaneous_notes > max_simultaneous_notes:
                max_simultaneous_notes = current_simultaneous_notes

            const_ent = {
                "entity_number": entity_num,
                "name": "constant-combinator",
                "position": {
                    "x": -2 + column,
                    "y": row
                },
                "direction": 2,
                "control_behavior": {
                    "filters": [
                        {
                            "signal": {  # Note signal
                                "type": "virtual",
                                "name": note_signal
                            },
                            "count": note.pitch,
                            "index": 1
                        },
                        {
                            "signal": {  # Time signal
                                "type": "virtual",
                                "name": "signal-T"
                            },
                            "count": note.game_tick,
                            "index": 18
                        }
                    ]
                },
                "connections": {
                    "1": {
                        "red": [
                            {
                                "entity_id": entity_num + 1,
                                "circuit_id": 1
                            }
                        ]
                    }
                },
            }
            entity_num += 1
            out["blueprint"]["entities"].append(const_ent)

            decider_ent = {
                "entity_number": entity_num,
                "name": "decider-combinator",
                "position": {
                    "x": 0.5 + column,
                    "y": row
                },
                "direction": 2,
                "control_behavior": {
                    "decider_conditions": {
                        "first_signal": {
                            "type": "virtual",
                            "name": "signal-T"
                        },
                        "second_signal": {
                            "type": "virtual",
                            "name": "signal-1"
                        },
                        "comparator": "=",
                        "output_signal": {
                            "type": "virtual",
                            "name": note_signal
                        },
                        "copy_count_from_input": True
                    }
                },
                "connections": {
                    "1": {
                        "red": [
                            {
                                "entity_id": entity_num - 1
                            }
                        ],
                    },
                    "2": {
                        "green": [
                            {
                                "entity_id": entity_num + 1
                            }
                        ]
                    }
                },
            }

            if len(first_deciders) < track_index + 1:
                first_deciders.append(decider_ent)

            # Add connections to the next decider if there is another one
            if row < len(track.notes) - 1:
                decider_ent["connections"]["1"]["green"] = [
                    {
                        "entity_id": entity_num + 3,
                        "circuit_id": 1
                    }
                ]

                decider_ent["connections"]["2"]["red"] = [
                    {
                        "entity_id": entity_num + 3,
                        "circuit_id": 2
                    }
                ]

            # Add connections to the previous decider if there is one
            if row > 0:
                decider_ent["connections"]["1"]["green"] = [
                    {
                        "entity_id": entity_num - 3,
                        "circuit_id": 1
                    }
                ]

                decider_ent["connections"]["2"]["red"] = [
                    {
                        "entity_id": entity_num - 3,
                        "circuit_id": 2
                    }
                ]

            entity_num += 1
            out["blueprint"]["entities"].append(decider_ent)

            lamp_ent = {
                "entity_number": entity_num,
                "name": "small-lamp",
                "position": {
                    "x": 2 + column,
                    "y": row
                },
                "direction": 2,
                "control_behavior": {
                    "circuit_condition": {
                        "first_signal": {
                            "type": "virtual",
                            "name": "signal-anything"
                        },
                        "constant": 0,
                        "comparator": ">"
                    }
                },
                "connections": {
                    "1": {
                        "green": [
                            {
                                "entity_id": entity_num - 1,
                                "circuit_id": 2
                            }
                        ]
                    }
                },
            }
            entity_num += 1
            out["blueprint"]["entities"].append(lamp_ent)

            prev_note_time = note.game_tick
            row += 1

        # Add electric poles
        for i in range(-max_simultaneous_notes, len(track.notes) + 7, 7):
            pole_ent = {
                "entity_number": entity_num,
                "name": "medium-electric-pole",
                "position": {
                    "x": -1 + column,
                    "y": i
                }
            }
            entity_num += 1
            out["blueprint"]["entities"].append(pole_ent)

        # Add speakers
        for i in range(0, max_simultaneous_notes):
            signal = "signal-" + chr(ord("A") + i)

            if track.instrument == "bass":
                instrument_id = 4
                volume = 0.5
            else:
                # Fall back to piano
                instrument_id = 3
                volume = 1

            speaker_ent = {
                "entity_number": entity_num,
                "name": "programmable-speaker",
                "position": {
                    "x": 2 + column,
                    "y": -1 - i
                },
                "control_behavior": {
                    "circuit_condition": {
                        "first_signal": {
                            "type": "virtual",
                            "name": signal
                        },
                        "constant": 0,
                        "comparator": "<"
                    },
                    "circuit_parameters": {
                        "signal_value_is_pitch": True,
                        "instrument_id": instrument_id,
                        "note_id": 0
                    }
                },
                "connections": {
                    "1": {
                        "red": []
                    }
                },
                "parameters": {
                    "playback_volume": volume,
                    "playback_globally": True,
                    "allow_polyphony": True
                },
                "alert_parameters": {
                    "show_alert": False,
                    "show_on_map": True,
                    "alert_message": ""
                }
            }

            # If this isn't the last speaker in the column, connect it to the next one
            if i < max_simultaneous_notes - 1:
                speaker_ent["connections"]["1"]["red"].append({
                    "entity_id": entity_num + 1
                })

            # If this isn't the first speaker in the column, connect it to the prev one
            if i > 0:
                speaker_ent["connections"]["1"]["red"].append({
                    "entity_id": entity_num - 1
                })
            else:  # If this is the first speaker in the column, connect it to the first decider
                speaker_ent["connections"]["1"]["red"].append({
                    "entity_id": first_deciders[track_index]["entity_number"],
                    "circuit_id": 2
                })

                # And also connect the first decider to it
                first_deciders[track_index]["connections"]["2"]["red"].append(
                    {
                        "entity_id": entity_num
                    }
                )

            entity_num += 1
            out["blueprint"]["entities"].append(speaker_ent)

        column += 5
        track_index += 1

    json_str = json.dumps(out)
    compressed = zlib.compress(json_str.encode("utf8"), 9)
    encoded = base64.b64encode(compressed)
    return "0" + encoded.decode("utf8")


def main():
    mid = MidiFile(sys.argv[1], ticks_per_beat=MIDI_PPQ)

    tracks: List[TrackStorage] = []
    channel_track_map: Dict[int, TrackStorage] = {}

    tempo = 0

    for i, track in enumerate(mid.tracks):
        # print('Track {}: {}'.format(i, track.name))
        current_tick = 0
        notes: List[NoteStorage] = []

        track_store = TrackStorage()

        for msg in track:
            current_tick += msg.time

            if msg.type == "set_tempo":
                tempo = msg.tempo
            elif msg.type == "note_on":
                if track_store.channel is None:
                    track_store.channel = msg.channel

                if msg.velocity == 0:
                    continue

                second = mido.tick2second(current_tick, MIDI_PPQ, tempo)
                game_tick = second * GAME_TICKRATE / 0.25 + 1
                game_pitch = msg.note - 52

                if msg.channel == track_store.channel:
                    notes.append(NoteStorage(game_tick, game_pitch))
                else:
                    if msg.channel in channel_track_map:
                        channel_track_map[msg.channel].notes.append(NoteStorage(game_tick, game_pitch))
                    else:
                        channel_track_map[msg.channel] = TrackStorage()
                        channel_track_map[msg.channel].notes.append(NoteStorage(game_tick, game_pitch))
                        tracks.append(channel_track_map[msg.channel])

        track_store.notes = notes
        tracks.append(track_store)

    print(to_blueprint(tracks))


if __name__ == "__main__":
    main()
