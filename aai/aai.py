#!/usr/bin/env python3

import subprocess
import sys
import os


PACKAGE_LIST = [
    # Base
    "base",
    "base-devel",

    # Bootloader
    "grub",
    "efibootmgr",

    # Display
    "xorg-server",  # X11
    "xorg-xinit",
    "lightdm",  # Display manager
    "lightdm-gtk-greeter",  # Display manager login screen
    "light-locker",  # Session locker
    "compton",  # Compositor
    "awesome",  # Window manager

    # Theming
    "lxappearance",  # Configure themes
    "adapta-gtk-theme",  # GTK+ theme
    "adapta-kde",  # Qt theme
    "papirus-icon-theme",  # Icon theme
    "qt5ct",  # Qt 5 config utility
    "kvantum-qt5",  # Qt 5 theme engine

    # GUI apps
    "tilix",  # Terminal
    "firefox",  # Browser
    "pcmanfm-gtk3",  # File manager
    "mpv",  # Media player

    # Audio
    "pulseaudio",
    "pulseaudio-alsa",
    "pavucontrol",

    # Editors
    "neovim",
    "python-neovim",
    "emacs",

    # Shell
    "zsh",

    # Developer tools
    "git",
    "cmake",
    "clang",
    "python",

    # Command line tools
    "maim",  # Screenshot tool
    "elinks",  # CLI web browser

    # Fonts
    "ttf-liberation",
    "terminus-font",
]

AUR_PACKAGE_LIST = [
    "yay",
    "bibata-cursor-theme",  # Cursor
    "ttf-google-fonts-git",
    "yadm-git",  # Yet Another Dotfiles Manager
]

AUR_PACKAGE_MAKE_DEPS = [
    "go",
    "inkscape",
    "gtk-engine-murrine",
    "xorg-xcursorgen",
]

AUR_PACKAGE_RUNTIME_DEPS = [
    "noto-fonts",
    "noto-fonts-extra",
    "ttf-fira-sans",
    "ttf-fira-mono",
    "ttf-ubuntu-font-family",
    "ttf-croscore",
    "ttf-roboto",
    "ttf-inconsolata",
    "ttf-merriweather",
    "ttf-merriweather-sans",
    "ttf-opensans",
    "ttf-oswald",
    "ttf-quintessential",
    "ttf-signika",
]


def main():
    log_info("Checking network connection")
    try_command_or_fatal(["ping", "-c", "1", "archlinux.org"], "Failed to find a network connection")

    log_info("Checking boot directory exists")
    check_isdir_or_fatal("/mnt/boot", "Failed to find mounted boot directory")

    hostname = ask_user_not_empty("Machine hostname")
    username = ask_user_not_empty("Your username")
    password = ask_user_not_empty("Your password")

    if not ask_user_yes_no("Proceed with installation?"):
        log_fatal("Installation canceled by user")

    log_info("Installing packages")
    try_command_or_fatal(["pacstrap", "/mnt"] + PACKAGE_LIST, "Failed to install packages")

    log_info("Generating fstab")
    try_command_or_fatal(["bash", "-c", "genfstab -U /mnt >> /mnt/etc/fstab"], "Failed to generate fstab")

    log_info("Chrooting")
    try_command_or_fatal(["bash", "-c", f"""
cat << EOF | arch-chroot /mnt
ln -s /usr/share/zoneinfo/Europe/London /etc/localtime
hwclock --systohc

sed -i 's/#en_US.UTF-8/en_US.UTF-8/' /etc/locale.gen
locale-gen
echo 'LANG=en_US.UTF-8' > /etc/locale.conf

echo '{hostname}' > /etc/hostname
echo '127.0.0.1\\tlocalhost' >> /etc/hosts
echo '::1\\tlocalhost' >> /etc/hosts
echo '127.0.1.1\\t{hostname}.localhost\\t{hostname}' >> /etc/hosts

grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable lightdm.service
systemctl enable dhcpcd.service

useradd -m -G wheel -s /bin/zsh {username}
echo '{username}:{password}' | chpasswd # Yes I know this is bad
sed -i 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/locale.gen
EOF
    """], "Failed to perform chroot commands")

    log_info("Installing AUR packages")
    install_aur_packages(username, AUR_PACKAGE_LIST, AUR_PACKAGE_MAKE_DEPS, AUR_PACKAGE_RUNTIME_DEPS)

    log_info("Chrooting 2: Electric Boogaloo")
    try_command_or_fatal(["bash", "-c", f"""
cat << EOF | arch-chroot /mnt
su {username} <<EOSU
yadm clone https://gitlab.com/CraftedCart/dotfiles.git
yadm submodule update --init --recursive
EOSU
EOF
    """], "Failed to perform chroot commands")

    log_info("Done!")

    if ask_user_yes_no("Reboot now?"):
        log_info("Rebooting")
        try_command_or_fatal(["umount", "-R", "/mnt"], "Failed to unmount /mnt recursively")
        try_command_or_fatal(["reboot"], "Failed to unmount /mnt recursively")


def install_aur_packages(username, packages, make_deps, runtime_deps):
    try_command_or_fatal(["bash", "-c", f"""
cat << EOF | arch-chroot /mnt
pacman --noconfirm -S --asdep {' '.join(make_deps)} {' '.join(runtime_deps)}
EOF
    """], f"Failed to install AUR package dependencies: {', '.join(make_deps)} {', '.join(runtime_deps)}")

    for package in packages:
        install_aur_package(username, package)

    try_command_or_fatal(["bash", "-c", f"""
cat << EOF | arch-chroot /mnt
pacman --noconfirm -Rs {' '.join(make_deps)}
EOF
    """], f"Failed to uninstall AUR make dependencies: {', '.join(make_deps)}")


def install_aur_package(username, package):
    try_command_or_fatal(["bash", "-c", f"""
cat << EOF | arch-chroot /mnt
su {username} <<EOSU
pushd

cd /tmp
git clone https://aur.archlinux.org/{package}.git
cd {package}
makepkg

popd
EOSU

pacman --noconfirm -U /tmp/{package}/*.pkg.tar.xz
rm -rf /tmp/{package}
EOF
    """], f"Failed to install AUR package '{package}'")


def ask_user(message):
    return input(f"\033[36m[QUERY] {message} \033[0m")


def ask_user_not_empty(message):
    ret = ""
    while ret == "":
        ret = input(f"\033[36m[QUERY] {message} \033[0m")

    return ret


def ask_user_yes_no(message):
    ret = ""
    while not (ret == "y" or ret == "n"):
        ret = ask_user(message + " [y/n]")

    return ret == "y"


def try_command_or_fatal(cmd, error_msg=None):
    ret = subprocess.call(cmd)
    if ret != 0:
        if error_msg:
            log_error(error_msg)
        log_fatal(f"Command '{' '.join(cmd)}' failed")


def check_isdir_or_fatal(path, error_msg=None):
    if not os.path.isdir(path):
        if error_msg:
            log_error(error_msg)
        log_fatal(f"Path '{path}' does not exist")


def log_info(*args):
    print("\033[34m[INFO] ", *args, "\033[0m")


def log_warning(*args):
    print("\033[33m[WARN] ", *args, "\033[0m")


def log_error(*args):
    print("\033[31m[ERROR]", *args, "\033[0m")


def log_fatal(*args):
    print("\033[31m[FATAL]", *args, "\033[0m")
    sys.exit(1)


if __name__ == "__main__":
    main()
