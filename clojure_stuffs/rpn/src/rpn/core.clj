(ns rpn.core
  (:gen-class)
  (:require [clojure.string :as string]))

(def ops {"+" {:func + :arity 2}
          "-" {:func - :arity 2}
          "*" {:func * :arity 2}
          "/" {:func / :arity 2}
          "abs" {:func #(Math/abs %) :arity 1}})

(def stack '())

(defn run-token
  "Returns the new state of the stack"
  [stack token]
  (if-let [op (get ops token)]
    (cons (apply (:func op) (reverse (take (:arity op) stack)))
          (drop (:arity op) stack))
    (try
      (conj stack (Double/parseDouble token))
      (catch NumberFormatException e
        (println (str "Could not parse " token))
        stack))))

(defn -main [& args]
  (doseq [line (repeatedly read-line) :while (some? line)] ; For each line..
    (when-not (empty? line)
      (doseq [token (string/split line #"\s+")] ; Split up each line into tokens
        (alter-var-root #'stack run-token token)))
    (prn stack)))
