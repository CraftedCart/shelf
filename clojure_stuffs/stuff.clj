(require '[clojure.inspector :as ins])

(+ 1 2)
(str "Clojure is " (- 2021 2007) " years old")
(inc 1)
(map inc [1 2 3 4 5])
(map (partial * 10) [1 2 3 4 5])
(clojure.string/join ", " (map str [1 2 3 4 5]))
(filter odd? (range 11))
(remove odd? (range 11))

(clojure.repl/doc clojure.repl/doc)
(clojure.repl/doc meta)
(:doc (meta #'clojure.repl/doc))

(= (reduce + [1 2 3 4 5]) 15)

(defn square-of
  "Calculates the square of the given number"
  [n]
  (* n n))

(square-of 5)
(map square-of [1 2 3 4 5])
(map #(* % %) [1 2 3 4 5])

(let [data (range 20, 4932)
      sum (reduce + data)
      length (count data)]
  (str "Average value: " (float (/ sum length))))

(defn factorial
  "Computes the factorial of the given number"
  [n]
  (if (= n 1)
    1
    (* n (factorial (- n 1)))))

(factorial 5)

(defn fibbonaci-seq
  "Lazyily generates a fibbonaci sequence"
  [current-number next-number]
  (lazy-seq
    (cons current-number
          (fibbonaci-seq next-number (+ current-number next-number)))))

(ins/inspect (take 30 (fibbonaci-seq 0 1)))

(name :earth/here)
(str *ns*)

(ins/inspect-table (take 100 (ns-publics 'clojure.core)))

(do
  (defn random-func
    "Returns the name and documentation for a random function from clojure.core"
    []
    (let [func-meta (meta (rand-nth (vals (ns-publics 'clojure.core))))]
      (str (:name func-meta) "\n\n" (:doc func-meta))))

  (print (random-func)))
