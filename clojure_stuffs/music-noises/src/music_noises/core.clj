(ns music-noises.core)

(defn foo
  "I don't do a whole lot."
  [x]
  (println x "Hello, World!"))

(comment
  (stop)

  (definst foo [] (saw 220))
  (foo)
  (kill foo)

  (definst bar [freq 220] (saw freq))

  (bar 110)
  (ctl bar :freq 160)
  (kill bar)

  (definst trem [freq 440 depth 10 rate 6 length 3]
    (* 0.3
       (line:kr 0 1 length FREE)
       (saw (+ freq (* depth (sin-osc:kr rate))))))

  (trem)
  (trem 200 60 0.8)
  (kill trem))
