(ns hello-seesaw.core)

; (defn foo
  ; "I don't do a whole lot."
  ; [x]
  ; (println x "Hello, World!"))

(use 'clojure.repl)
(use '[seesaw.core :as ssc])

;; Now before we create any UI stuff, tell Seesaw to try to make things look as
;; native as possible. This puts the menubar in the right place on Mac, etc.
(ssc/native!)

;; Make a frame
(def f (ssc/frame :title "Heya Seesaw!"))

;; Auto-size it and show it
(-> f pack! show!)

;; Get the title
(ssc/config f :title)

;; Set the title
(ssc/config! f :title "owo what's this?")

;; Add some content
(def lbl (ssc/label "Heya mew~"))
(ssc/config! f :content lbl)

;; Pack it again, so I can *see* the window (it was 1x1 pixel before)
(-> f pack!)

;; Opens dialogs and blocks
(ssc/alert f "Heya~")
(ssc/input f "nya?")

(ssc/config! f :content (ssc/button
                          :text "Pushable button!"
                          :listen [:action #(ssc/alert % "woah!")]))
