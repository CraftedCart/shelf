local function get_upvalue_by_name(fn, search_name)
  local idx = 1
  while true do
    local name, val = debug.getupvalue(fn, idx)
    if not name then break end
    if name == search_name then
      return idx, val
    end
    idx = idx + 1
  end
end

local function superify(cls, parent)
  for k, v in pairs(cls) do
    if type(v) == "function" then
      local _, env = get_upvalue_by_name(v, "_ENV")
      env["super"] = parent.base[k]
    end
  end
end

local function class(name)
  return function(args)
    if type(args) == "string" then
      -- Derived class
      local parent = _G[args]
      return function(child_args)
        setmetatable(child_args, {
          __index = parent.base,
        })
        superify(child_args, parent)

        _G[name] = {
          base = child_args,
          new = function()
            local t = {}
            setmetatable(t, {
              __index = child_args
            })

            return t
          end
        }
      end

    else
      -- Base class
      _G[name] = {
        base = args,
        new = function()
          local t = {}
          setmetatable(t, {
            __index = args
          })

          return t
        end
      }
    end
  end
end

--------------------------------------------------

class "Vec3" {
  x = 0,
  y = 0,
  z = 0,

  set_y = function(self, y)
    self.y = y
  end,
}

local vec = Vec3.new()
vec.x = 4.2
vec:set_y(7)

print(vec.x, vec.y, vec.z)

--------------------------------------------------

class "Vec4" ("Vec3") {
  w = 0,

  log = function(self)
    print(self.x, self.y, self.z, self.w)
  end,
}

local v4 = Vec4.new()
v4.w = 2
v4.x = 9.2
v4:set_y(-8)
v4:log()

--------------------------------------------------

-- Multiple levels of inheritance
class "Vec5" ("Vec4") {
  v = 27,

  log = function(self)
    super(self)
    print(self.v)
  end
}

local v5 = Vec5.new()
v5:set_y(3)
v5.x = 4
v5:log()
