local BaseType = {}
BaseType._members = {
  __type = BaseType,
  __init = function(self, ...) end,
}

local function get_upvalue_by_name(fn, search_name)
  local idx = 1
  while true do
    local name, val = debug.getupvalue(fn, idx)
    if not name then break end
    if name == search_name then
      return idx, val
    end
    idx = idx + 1
  end
end

-- Adds super(...) to the function's environment
local function superify(fn, inheritance_locator)
  local env
  if _ENV then
    local _, uv_env = get_upvalue_by_name(fn, "_ENV")
    if not uv_env then return end
    env = uv_env
  else
    env = getfenv(fn)
  end

  local t = {}
  setmetatable(t, {
    __index = inheritance_locator,
  })
  env["super"] = t
end

local function new_inherited_class(extends)
  return function(members)
    -- Inheritance locator is a function for __index, to locate a value for a given key
    local inheritance_locator
    if #extends == 1 then
      inheritance_locator = extends[1]._members
    else
      inheritance_locator = function(_, k)
        for _, i_v in pairs(extends) do
          local val = i_v._members[k]
          if val then return val end
        end
      end
    end

    -- Enable inheritance
    setmetatable(members, {__index = inheritance_locator})

    local obj_metatable = {__index = members}

    for _, v in pairs(extends) do
      if v._members.__mt then
        for mt_k, mt_v in pairs(v._members.__mt) do obj_metatable[mt_k] = mt_v end
      end
    end

    -- Add super(...) to function environments
    -- Also check for any metatable stuff
    for k, v in pairs(members) do
      if type(v) == "function" then superify(v, inheritance_locator) end

      if k == "__mt" then
        for mt_k, mt_v in pairs(v) do obj_metatable[mt_k] = mt_v end
      end
    end

    -- Create our class type object
    local type_value = {}

    -- Creates a new instance of the class
    type_value.new = function(...)
      local args = {...}
      table.remove(args, 1)

      local t = {__type = type_value}
      setmetatable(t, obj_metatable)
      members.__init(t, unpack(args))

      return t
    end

    type_value._members = members

    setmetatable(type_value, {
      _class = true,
      __call = type_value.new,
    })

    return type_value
  end
end

local function new_class(...)
  local args = {...}

  -- Arg checking
  if #args == 0 then error("class takes at least one arg", 2) end
  for _, v in pairs(args) do
    if type(v) ~= "table" then error("class args must all be tables", 2) end
  end

  -- Check if we're creating a new base class or inheriting an existing one
  -- by seeing if args[1] has a metatable with _class = true
  local first_mt = getmetatable(args[1])
  if first_mt and first_mt._class then
    -- We're inheriting an existing class
    return new_inherited_class(args)
  else
    -- We're creating a new class
    return new_inherited_class({BaseType})(args[1])
  end
end

local class = {
  BaseType = BaseType,
}

setmetatable(class, {
  __call = function(...)
    local args = {...}
    table.remove(args, 1)
    return new_class(unpack(args))
  end,
})

---------------------------------------------------

local Fuckery
Fuckery = class {
  magic = function(self)
    print("You're cyuuuute!")
  end
}

local Vec2
Vec2 = class {
  __init = function(self, x, y)
    self.x = x
    self.y = y
  end,

  print_vec = function(self)
    print(string.format("(%f, %f)", self.x, self.y))
  end,

  reset_vec = function(self)
    self.x = 0
    self.y = 0
  end,

  __mt = {
    __add = function(self, other)
      return Vec2(self.x + other.x, self.y + other.y)
    end,

    __sub = function(self, other)
      return Vec2(self.x - other.x, self.y - other.y)
    end,

    __mul = function(self, other)
      return Vec2(self.x * other.x, self.y * other.y)
    end,

    __div = function(self, other)
      return Vec2(self.x / other.x, self.y / other.y)
    end,
  },
}

Vec2.ORIGIN = Vec2(0, 0)
Vec2.DELCIOUS_PIE = Vec2(3.14, 3.14)

local vec = Vec2(8, 4)
print(vec.x, vec.y)
vec.x = 2
vec:print_vec()
vec:reset_vec()
vec:print_vec()

-- Test metafunctions
local some_vec = Vec2(16, 2)
local other_vec = Vec2(-16, -4)
local added = some_vec + other_vec
added:print_vec()

local Vec3
Vec3 = class(Vec2, Fuckery) {
  __init = function(self, x, y, z)
    -- DON'T USE THE COLON SUGAR WITH SUPER `super:__init(x, y)`
    super.__init(self, x, y)
    self.z = z
  end,

  print_vec = function(self)
    print(string.format("(%f, %f, %f)", self.x, self.y, self.z))
  end,

  reset_vec = function(self)
    super.reset_vec(self)
    self.z = 0
  end,
}

local v3 = Vec3(1, 2, 3)
v3:print_vec()
v3:reset_vec()
v3:print_vec()

-- Test multiple inheritance
v3:magic()

-- Inherits metamethods
-- Albeit, this isn't really expected behavior from a Vec3 BUT IT'S A PROOF OF CONCEPT MMMK
local some_v3 = Vec3(4, 8)
local other_v3 = Vec3(2, 4)
local added_v3 = some_v3 + other_v3
added_v3:print_vec()
