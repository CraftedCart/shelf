local function class(name)
  return function(args)
    if type(args) == "string" then
      -- Derived class
      local parent = _G[args]
      return function(child_args)
        setmetatable(child_args, {
          __index = parent.base,
        })
        _G[name] = {
          base = child_args,
          new = function()
            local t = {}
            setmetatable(t, {
              __index = child_args
            })

            return t
          end
        }
      end

    else
      -- Base class
      _G[name] = {
        base = args,
        new = function()
          local t = {}
          setmetatable(t, {
            __index = args
          })

          return t
        end
      }
    end
  end
end

--------------------------------------------------

class "Vec3" {
  x = 0,
  y = 0,
  z = 0,

  set_y = function(self, y)
    self.y = y
  end,
}

local vec = Vec3.new()
vec.x = 4.2
vec:set_y(7)

print(vec.x, vec.y, vec.z)

--------------------------------------------------

class "Vec4" ("Vec3") {
  w = 0,

  log = function(self)
    print(self.x, self.y, self.z, self.w)
  end,
}

local v4 = Vec4.new()
v4.w = 2
v4.x = 9.2
v4:set_y(-8)
v4:log()

--------------------------------------------------

-- Multiple levels of inheritance
class "Vec5" ("Vec4") {
  v = 27,

  log = function(self)
    Vec4.base.log(self)
    print(self.v)
  end
}

local v5 = Vec5.new()
v5:set_y(3)
v5.x = 4
v5:log()

print(class)
