package craftedcart.lox.tool

import java.io.PrintWriter
import kotlin.system.exitProcess

fun main(args: Array<String>) {
    if (args.size != 1) {
        System.err.println("Usage: generate_ast <output directory>")
        exitProcess(64)
    }
    val outputDir = args[0]

    defineAst(outputDir, "Expr", listOf(
        "Binary   : Expr left, Token operator, Expr right",
        "Grouping : Expr expression",
        "Literal  : Any? value",
        "Unary    : Token operator, Expr right",
    ))
}

private fun defineAst(outputDir: String, baseName: String, types: List<String>) {
    val path = "$outputDir/$baseName.kt"
    val writer = PrintWriter(path, "UTF-8")

    writer.println("package craftedcart.lox.interpret")
    writer.println()
    writer.println("abstract class $baseName {")

    // Add the visitor interface
    writer.println("    interface Visitor<R> {")
    for (type in types) {
        val split = type.split(":")
        val className = split[0].trim()
        writer.println("        fun visit$className(expr: $className): R")
    }
    writer.println("    }")
    writer.println()

    // Add the inner classes
    for (type in types) {
        val split = type.split(":")
        val className = split[0].trim()
        writer.println(
            split[1]
                .trim()
                .split(",")
                .joinToString(", ", prefix = "    class $className(", postfix = "): $baseName() {") {
                    val fieldSplit = it.trim().split(" ")
                    val typeName = fieldSplit[0]
                    val name = fieldSplit[1]

                    "val $name: $typeName"
                }
        )

        // Add overridden accept method
        writer.println("        override fun <R> accept(visitor: Visitor<R>): R {")
        writer.println("            return visitor.visit$className(this)")
        writer.println("        }")

        writer.println("    }")
    }

    // Add the base accept() method
    writer.println()
    writer.println("    abstract fun <R> accept(visitor: Visitor<R>): R")

    writer.println("}")
    writer.close()
}

// inner class Binary(val left: Expr, val operator: Token, val right: Expr)
