package craftedcart.lox.interpret

class Scanner(private val source: String, private val reporter: IDiagnosticReporter) {
    companion object {
        private val keywords = mapOf(
            "and" to TokenType.AND,
            "class" to TokenType.CLASS,
            "else" to TokenType.ELSE,
            "false" to TokenType.FALSE,
            "for" to TokenType.FOR,
            "fun" to TokenType.FUN,
            "if" to TokenType.IF,
            "nil" to TokenType.NIL,
            "or" to TokenType.OR,
            "print" to TokenType.PRINT,
            "return" to TokenType.RETURN,
            "super" to TokenType.SUPER,
            "this" to TokenType.THIS,
            "true" to TokenType.TRUE,
            "var" to TokenType.VAR,
            "while" to TokenType.WHILE,
        )
    }

    private val tokens: MutableList<Token> = mutableListOf()

    /**
     * The start position of the currently scanning token
     */
    private var start = 0

    private var current = 0
    private var line = 1

    fun scanTokens(): List<Token> {
        while (!atEnd()) {
            // We're at the beginning of the next lexeme
            start = current
            scanToken()
        }

        tokens.add(Token(TokenType.EOF, "", null, line))
        return tokens
    }

    private fun scanToken() {
        val c = advance()
        when (c) {
            '(' -> addToken(TokenType.LEFT_PAREN)
            ')' -> addToken(TokenType.RIGHT_PAREN)
            '{' -> addToken(TokenType.LEFT_BRACE)
            '}' -> addToken(TokenType.RIGHT_BRACE)
            ',' -> addToken(TokenType.COMMA)
            '.' -> addToken(TokenType.DOT)
            '-' -> addToken(TokenType.MINUS)
            '+' -> addToken(TokenType.PLUS)
            ';' -> addToken(TokenType.SEMICOLON)
            '*' -> addToken(TokenType.STAR)
            '!' -> addToken(if (match('=')) TokenType.BANG_EQUAL else TokenType.BANG)
            '=' -> addToken(if (match('=')) TokenType.EQUAL_EQUAL else TokenType.EQUAL)
            '<' -> addToken(if (match('=')) TokenType.LESS_EQUAL else TokenType.LESS)
            '>' -> addToken(if (match('=')) TokenType.GREATER_EQUAL else TokenType.GREATER)
            '/' -> {
                // Comments begin with //
                if (match('/')) {
                    while (peek() != '\n' && !atEnd()) advance()
                } else {
                    addToken(TokenType.SLASH)
                }
            }

            // String
            '"' -> string()

            // Numbers
            in '0'..'9' -> number()

            // Skip over whitespace
            ' ' -> {}
            '\r' -> {}
            '\t' -> {}

            // Newline
            '\n' -> line++

            else -> {
                if (isAlpha(c)) {
                    identifier()
                } else {
                    // Wuh oh
                    reporter.error(line, "Unexpected character")
                }
            }
        }
    }

    private fun advance(): Char {
        return source[current++]
    }

    private fun addToken(type: TokenType) {
        addToken(type, null)
    }

    private fun addToken(type: TokenType, literal: Any?) {
        val text = source.substring(start, current)
        tokens.add(Token(type, text, literal, line))
    }

    private fun atEnd(): Boolean {
        return current >= source.length
    }

    /**
     * Peeks the next character and returns whether it matches `expected`
     */
    private fun match(expected: Char): Boolean {
        if (atEnd()) return false
        if (source[current] != expected) return false

        current++
        return true
    }

    private fun peek(): Char {
        if (atEnd()) return '\u0000'
        return source[current]
    }

    private fun peekNext(): Char {
        if (current + 1 >= source.length) return '\u0000'
        return source[current + 1]
    }

    private fun isAlpha(c: Char): Boolean {
        return c in 'a'..'z' || c in 'A'..'Z' || c == '_'
    }

    private fun isAlphanumeric(c: Char): Boolean {
        return isAlpha(c) || c in '0'..'9'
    }

    private fun string() {
        while (peek() != '"' && !atEnd()) {
            if (peek() == '\n') line++
            advance()
        }

        if (atEnd()) {
            reporter.error(line, "Unterminated string literal")
            return
        }

        // Advance past the closing "
        advance()

        val str = source.substring(start + 1, current - 1)
        addToken(TokenType.STRING, str)
    }

    private fun number() {
        while (peek() in '0'..'9') advance()

        // Look for a fractional part
        if (peek() == '.' && peekNext() in '0'..'9') {
            // Consume the '.'
            advance()

            while (peek() in '0'..'9') advance()
        }

        addToken(TokenType.NUMBER, source.substring(start, current).toDouble())
    }

    private fun identifier() {
        while (isAlphanumeric(peek())) advance()

        val text = source.substring(start, current)
        val type = keywords.getOrDefault(text, TokenType.IDENTIFIER)

        addToken(type)
    }
}