package craftedcart.lox.interpret

interface IDiagnosticReporter {
    fun error(line: Int, message: String)
}