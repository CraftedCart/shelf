package craftedcart.lox.interpret

import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Path
import kotlin.system.exitProcess

var hadError = false

fun main(args: Array<String>) {
//    when {
//        args.size > 1 -> {
//            println("Usage: jlox [script]")
//            exitProcess(64)
//        }
//        args.size == 1 -> {
//            runFile(Paths.get(args[0]))
//        }
//        else -> {
//            runPrompt()
//        }
//    }

    val expression: Expr = Expr.Binary(
        Expr.Unary(
            Token(TokenType.MINUS, "-", null, 1),
            Expr.Literal(123)
        ),
        Token(TokenType.STAR, "*", null, 1),
        Expr.Grouping(
            Expr.Literal(45.67)
        )
    )

    println(AstPrinter().print(expression))
}

fun runFile(path: Path) {
    val bytes = Files.readAllBytes(path)
    exec(String(bytes, Charset.defaultCharset()))

    if (hadError) exitProcess(65)
}

fun runPrompt() {
    while (true) {
        print("> ")
        val line = readLine() ?: break
        exec(line)
        hadError = false
    }
}

fun exec(source: String) {
    val reporter = object : IDiagnosticReporter {
        override fun error(line: Int, message: String) {
            reportError(line, message)
        }
    }

    val scanner = Scanner(source, reporter)
    val tokens = scanner.scanTokens()

    // For now, just print out tokens
    tokens.forEach {
        println(it)
    }
}

fun report(line: Int, where: String, message: String) {
    println("[line ${line}] Error ${where}: $message")
    hadError = true
}

fun reportError(line: Int, message: String) {
    report(line, "", message)
}