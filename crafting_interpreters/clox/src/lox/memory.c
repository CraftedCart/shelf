#include "lox/memory.h"
#include <stdlib.h>
#include <stdio.h>

void* lox_realloc(void* ptr, size_t old_size, size_t new_size) {
    if (new_size == 0) {
        free(ptr);
        return NULL;
    } else {
        void* result = realloc(ptr, new_size);

        // Check for allocation failure
        if (result == NULL) {
            fprintf(stderr, "Failed to allocate memory\n");
            exit(1);
        }

        return result;
    }
}
