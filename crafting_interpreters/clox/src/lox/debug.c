#include "lox/debug.h"
#include "lox/value.h"
#include "lox/object.h"
#include <stdio.h>

void lox_print_value(LoxValue value) {
    switch (value.type) {
        case LOX_VAL_TY_BOOL:
            if (value.as.boolean) {
                printf("true");
            } else {
                printf("false");
            }
            break;
        case LOX_VAL_TY_NIL:
            printf("nil");
            break;
        case LOX_VAL_TY_NUMBER:
            printf("%g", value.as.number);
            break;
        case LOX_VAL_TY_OBJ:
            switch (LOX_AS_OBJ(value)->type) {
                case LOX_OBJ_TY_STRING: {
                    LoxObjString* str = LOX_AS_STRING(value);
                    fwrite(str->chars, str->length, 1, stdout);
                    break;
                }
            }
            break;
    }
}

/** Disassemble a single-byte instruction */
static uint32_t disasm_simple_instruction(const char* name, uint32_t offset) {
    printf("%s\n", name);
    return offset + 1;
}

/** Disassemble an instruction that takes a constant index */
static uint32_t disasm_constant_instruction(const char* name, const LoxChunk* chunk, uint32_t offset) {
    uint8_t constant_idx = chunk->code[offset + 1];
    printf("%-22s %4d '", name, constant_idx);
    lox_print_value(chunk->constants.values[constant_idx]);
    printf("'\n");
    return offset + 2;
}

/** Disassemble an instruction that takes a byte arg */
static uint32_t disasm_byte_instruction(const char* name, const LoxChunk* chunk, uint32_t offset) {
    uint8_t arg = chunk->code[offset + 1];
    printf("%-22s %4d\n", name, arg);
    return offset + 2;
}

void lox_disassemble_chunk(const LoxChunk* chunk, const char* name) {
    printf("======== %s ========\n", name);

    // Print constants header
    printf(">> Constants <<\n");
    printf("Idx |Value\n");
    printf("----------\n");

    for (uint32_t i = 0; i < chunk->constants.count; i++) {
        printf("%04d '", i);
        lox_print_value(chunk->constants.values[i]);
        printf("'\n");
    }

    // Print code header
    printf("\n");
    printf(">> Code <<\n");
    printf("Offs|Line|OpCode                |Args\n");
    printf("-------------------------------------\n");

    for (uint32_t offset = 0; offset < chunk->count;) {
        offset = lox_disassemble_instruction(chunk, offset);
    }
}

uint32_t lox_disassemble_instruction(const LoxChunk* chunk, uint32_t offset) {
    printf("%04d ", offset);

    // Print line number
    uint32_t line_num = lox_chunk_offset_to_line(chunk, offset);
    if (offset > 0 && line_num == lox_chunk_offset_to_line(chunk, offset - 1)) { // Check for line continuation
        printf("   | ");
    } else {
        printf("%04d ", line_num);
    }

    // Print opcode and any args
    uint8_t opcode = chunk->code[offset];
    switch (opcode) {
        case LOX_OP_CONSTANT:
            return disasm_constant_instruction("LOX_OP_CONSTANT", chunk, offset);
        case LOX_OP_TRUE:
            return disasm_simple_instruction("LOX_OP_TRUE", offset);
        case LOX_OP_FALSE:
            return disasm_simple_instruction("LOX_OP_FALSE", offset);
        case LOX_OP_NIL:
            return disasm_simple_instruction("LOX_OP_NIL", offset);
        case LOX_OP_NEGATE:
            return disasm_simple_instruction("LOX_OP_NEGATE", offset);
        case LOX_OP_ADD:
            return disasm_simple_instruction("LOX_OP_ADD", offset);
        case LOX_OP_SUB:
            return disasm_simple_instruction("LOX_OP_SUB", offset);
        case LOX_OP_MUL:
            return disasm_simple_instruction("LOX_OP_MUL", offset);
        case LOX_OP_DIV:
            return disasm_simple_instruction("LOX_OP_DIV", offset);
        case LOX_OP_NOT:
            return disasm_simple_instruction("LOX_OP_NOT", offset);
        case LOX_OP_EQUAL:
            return disasm_simple_instruction("LOX_OP_EQUAL", offset);
        case LOX_OP_GREATER:
            return disasm_simple_instruction("LOX_OP_GREATER", offset);
        case LOX_OP_LESS:
            return disasm_simple_instruction("LOX_OP_LESS", offset);
        case LOX_OP_POP:
            return disasm_simple_instruction("LOX_OP_POP", offset);
        case LOX_OP_DEFINE_GLOBAL:
            return disasm_constant_instruction("LOX_OP_DEFINE_GLOBAL", chunk, offset);
        case LOX_OP_GET_GLOBAL:
            return disasm_constant_instruction("LOX_OP_GET_GLOBAL", chunk, offset);
        case LOX_OP_SET_GLOBAL:
            return disasm_constant_instruction("LOX_OP_SET_GLOBAL", chunk, offset);
        case LOX_OP_GET_LOCAL:
            return disasm_byte_instruction("LOX_OP_GET_LOCAL", chunk, offset);
        case LOX_OP_SET_LOCAL:
            return disasm_byte_instruction("LOX_OP_SET_LOCAL", chunk, offset);
        case LOX_OP_PRINT:
            return disasm_simple_instruction("LOX_OP_PRINT", offset);
        case LOX_OP_RETURN:
            return disasm_simple_instruction("LOX_OP_RETURN", offset);
        default:
            printf("Unknown opcode %d\n", opcode);
            return offset + 1;
    }
}
