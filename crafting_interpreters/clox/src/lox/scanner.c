#include "lox/scanner.h"
#include <string.h>

static bool is_at_end(LoxScanner* scanner) {
    return *scanner->current == '\0';
}

static LoxToken make_token(LoxScanner* scanner, LoxTokenType type) {
    LoxToken token = {
        .type = type,
        .start = scanner->start,
        .length = scanner->current - scanner->start,
        .line = scanner->line,
    };
    return token;
}

/**
 * Since `message` must outlive the token, you should only call this with messages with a static lifetime
 * (so..string literal constants)
 */
static LoxToken make_error_token(LoxScanner* scanner, const char* message) {
    LoxToken token = {
        .type = LOX_TOKEN_ERROR,
        .error_message = message,
        .length = strlen(message),
        .line = scanner->line,
    };
    return token;
}

static char advance(LoxScanner* scanner) {
    scanner->current++;
    return *(scanner->current - 1);
}

static char peek(LoxScanner* scanner) {
    return *scanner->current;
}

static char peek_next(LoxScanner* scanner) {
    if (is_at_end(scanner)) {
        return '\0';
    } else {
        return *(scanner->current + 1);
    }
}

static bool match(LoxScanner* scanner, char expected) {
    if (is_at_end(scanner)) return false;
    if (*scanner->current != expected) return false;

    scanner->current++; // Consume the matched character
    return true;
}

/** Also skips comments */
static void skip_whitespace(LoxScanner* scanner) {
    while (true) {
        char c = peek(scanner);
        switch (c) {
            case '\n':
                scanner->line++;
            case ' ':
            case '\r':
            case '\t':
                advance(scanner);
                break;

            // Skip comments
            case '/':
                if (peek_next(scanner) == '/') {
                    // A // comment does until the end of the line
                    while (peek(scanner) != '\n' && !is_at_end(scanner)) advance(scanner);
                } else {
                    return;
                }

            default:
                return;
        }
    }
}

static bool is_digit(char c) {
    return c >= '0' && c <= '9';
}

static bool is_alpha(char c) {
    return (c >= 'a' && c <= 'z') ||
        (c >= 'A' && c <= 'Z') ||
        (c == '_');
}

static LoxToken scan_string(LoxScanner* scanner) {
    char next_char = peek(scanner);
    while (next_char != '"' && !is_at_end(scanner)) {
        if (next_char == '\n') scanner->line++;

        advance(scanner);
        next_char = peek(scanner);
    }

    if (is_at_end(scanner)) {
        return make_error_token(scanner, "Unterminated string");
    }

    // Consume the closing quote
    advance(scanner);

    return make_token(scanner, LOX_TOKEN_STRING);
}

static LoxToken scan_number(LoxScanner* scanner) {
    while (is_digit(peek(scanner))) advance(scanner);

    // Look for a fractional part
    if (peek(scanner) == '.' && is_digit(peek_next(scanner))) {
        advance(scanner); // Consume the dot
        while (is_digit(peek(scanner))) advance(scanner);
    }

    return make_token(scanner, LOX_TOKEN_NUMBER);
}

static LoxTokenType check_keyword(
        LoxScanner* scanner,
        uint32_t start,
        uint32_t length,
        const char* rest,
        LoxTokenType type
        ) {
    if (
            scanner->current - scanner->start  == start + length && // Same length?
            memcmp(scanner->start + start, rest, length) == 0 // Same text?
            ) {
        return type;
    } else {
        return LOX_TOKEN_IDENTIFIER;
    }
}

static LoxTokenType word_type(LoxScanner* scanner) {
    switch (scanner->start[0]) {
        case 'a': return check_keyword(scanner, 1, 2, "nd", LOX_TOKEN_AND);
        case 'c': return check_keyword(scanner, 1, 4, "lass", LOX_TOKEN_CLASS);
        case 'e': return check_keyword(scanner, 1, 3, "else", LOX_TOKEN_ELSE);
        case 'i': return check_keyword(scanner, 1, 1, "f", LOX_TOKEN_IF);
        case 'n': return check_keyword(scanner, 1, 2, "il", LOX_TOKEN_NIL);
        case 'o': return check_keyword(scanner, 1, 1, "r", LOX_TOKEN_OR);
        case 'p': return check_keyword(scanner, 1, 4, "rint", LOX_TOKEN_PRINT);
        case 'r': return check_keyword(scanner, 1, 5, "eturn", LOX_TOKEN_RETURN);
        case 's': return check_keyword(scanner, 1, 4, "uper", LOX_TOKEN_SUPER);
        case 'v': return check_keyword(scanner, 1, 2, "ar", LOX_TOKEN_VAR);
        case 'w': return check_keyword(scanner, 1, 4, "hile", LOX_TOKEN_WHILE);
        case 'f':
            if (scanner->current - scanner->start > 1) { // Check if we have a 2nd letter
                switch (scanner->start[1]) {
                    case 'a': return check_keyword(scanner, 2, 3, "lse", LOX_TOKEN_FALSE);
                    case 'o': return check_keyword(scanner, 2, 1, "r", LOX_TOKEN_FOR);
                    case 'u': return check_keyword(scanner, 2, 1, "n", LOX_TOKEN_FUN);
                }
            }
            break;
        case 't':
            if (scanner->current - scanner->start > 1) { // Check if we have a 2nd letter
                switch (scanner->start[1]) {
                    case 'h': return check_keyword(scanner, 2, 2, "is", LOX_TOKEN_THIS);
                    case 'r': return check_keyword(scanner, 2, 2, "ue", LOX_TOKEN_TRUE);
                }
            }
            break;
    }

    return LOX_TOKEN_IDENTIFIER;
}

static LoxToken scan_ident_or_keyword(LoxScanner* scanner) {
    // Consume characters until the end of the identifier/keyword
    while (true) {
        char c = peek(scanner);
        if (!(is_alpha(c) || is_digit(c))) break;
        advance(scanner);
    }

    return make_token(scanner, word_type(scanner));
}

void lox_scanner_init(LoxScanner* scanner, const char* source) {
    scanner->start = source;
    scanner->current = source;
    scanner->line = 1;
}

LoxToken lox_scanner_scan(LoxScanner* scanner) {
    skip_whitespace(scanner);
    scanner->start = scanner->current;

    if (is_at_end(scanner)) {
        return make_token(scanner, LOX_TOKEN_EOF);
    }

    char c = advance(scanner);
    switch (c) {
        case '(': return make_token(scanner, LOX_TOKEN_LEFT_PAREN);
        case ')': return make_token(scanner, LOX_TOKEN_RIGHT_PAREN);
        case '{': return make_token(scanner, LOX_TOKEN_LEFT_BRACE);
        case '}': return make_token(scanner, LOX_TOKEN_RIGHT_BRACE);
        case ',': return make_token(scanner, LOX_TOKEN_COMMA);
        case '.': return make_token(scanner, LOX_TOKEN_DOT);
        case '-': return make_token(scanner, LOX_TOKEN_MINUS);
        case '+': return make_token(scanner, LOX_TOKEN_PLUS);
        case ';': return make_token(scanner, LOX_TOKEN_SEMICOLON);
        case '/': return make_token(scanner, LOX_TOKEN_SLASH);
        case '*': return make_token(scanner, LOX_TOKEN_STAR);
        case '!': return make_token(scanner, match(scanner, '=') ? LOX_TOKEN_BANG_EQUAL : LOX_TOKEN_BANG);
        case '=': return make_token(scanner, match(scanner, '=') ? LOX_TOKEN_EQUAL_EQUAL : LOX_TOKEN_EQUAL);
        case '>': return make_token(scanner, match(scanner, '=') ? LOX_TOKEN_GREATER_EQUAL : LOX_TOKEN_GREATER);
        case '<': return make_token(scanner, match(scanner, '=') ? LOX_TOKEN_LESS_EQUAL : LOX_TOKEN_LESS);
        case '"': return scan_string(scanner);
        default:
            if (is_digit(c)) {
                return scan_number(scanner);
            } else if (is_alpha(c)) {
                return scan_ident_or_keyword(scanner);
            }
    }

    return make_error_token(scanner, "Unexpected character");
}
