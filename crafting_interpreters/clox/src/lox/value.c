#include "lox/value.h"

LOX_DEFINE_DYN_ARRAY(LoxValue, lox_value_array)

bool lox_value_eq(LoxValue a, LoxValue b) {
    if (a.type != b.type) return false;

    switch (a.type) {
        case LOX_VAL_TY_BOOL: return LOX_AS_BOOL(a) == LOX_AS_BOOL(b);
        case LOX_VAL_TY_NIL: return true;
        case LOX_VAL_TY_NUMBER: return LOX_AS_NUMBER(a) == LOX_AS_NUMBER(b);
        case LOX_VAL_TY_OBJ: return LOX_AS_OBJ(a) == LOX_AS_OBJ(b);
    }
}
