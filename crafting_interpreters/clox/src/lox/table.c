#include "lox/table.h"
#include "lox/object.h"
#include "lox/memory.h"
#include <assert.h>
#include <string.h>

/** Grow the hash table when it's this percentage full */
#define TABLE_MAX_LOAD 0.75

void lox_table_init(LoxTable* table) {
    table->count = 0;
    table->capacity = 0;
    table->entries = NULL;
}

void lox_table_free(LoxTable* table) {
    LOX_FREE_ARRAY(LoxTableEntry, table->entries, table->capacity);
    lox_table_init(table);
}

static LoxTableEntry* find_entry(LoxTableEntry* entries, uint32_t capacity, LoxObjString* key) {
    assert(capacity > 0);

    // Ideally, we should find our entry at the index we just computed above
    // However in the case that multiple keys have the same hash, we'll probe linearly forward through the table to
    // try find an entry with the same key, or an empty entry
    // An empty entry simply means it isn't there, and it can be overwritten to insert with the same key
    //
    // This should never get into an infinite loop, thanks to our table's max load factor (we always leave some empty
    // space in the hash table)
    //
    // If the key isn't in the hash table, and we find a tombstone, we'll return the tombstone (so calling code can
    // choose to reuse it to populate it with a real key/value pair). If we don't find a tombstone, return the next
    // empty entry.
    uint32_t index = key->hash % capacity;
    LoxTableEntry* tombstone = NULL;
    while (true) {
        LoxTableEntry* entry = entries + index;
        if (entry->key == NULL) {
            if (LOX_IS_NIL(entry->value)) {
                // This entry is empty
                return tombstone != NULL ? tombstone : entry;
            } else {
                // We found a tombstone
                if (tombstone == NULL) tombstone = entry;
            }
        } else if (entry->key == key) { // Doing == on pointers to LoxObjStrings is fine - they're interned
            // We found the key already in the array
            return entry;
        }

        // Search the next entry
        // Wrap back round if we reach the end of the array
        index = (index + 1) % capacity;
    }
}

static void adjust_capacity(LoxTable* table, uint32_t capacity) {
    // Allocate and initialize a new array of entries
    LoxTableEntry* entries = LOX_ALLOC(LoxTableEntry, capacity);
    for (uint32_t i = 0; i < capacity; i++) {
        entries[i].key = NULL;
        entries[i].value = LOX_VAL_NIL;
    }

    // Rebuild the table by re-inserting everything into the new array of entries
    // Since we don't copy over tombstoned entries, we need to recount how big the table is
    table->count = 0;
    for (uint32_t i = 0; i < table->capacity; i++) {
        LoxTableEntry* src_entry = table->entries + i;
        if (src_entry->key == NULL) continue;

        LoxTableEntry* dest_entry = find_entry(entries, capacity, src_entry->key);
        dest_entry->key = src_entry->key;
        dest_entry->value = src_entry->value;
        table->count++;
    }

    // Free the old array
    LOX_FREE_ARRAY(LoxTableEntry, table->entries, table->capacity);

    // Let the table know about its new capacity
    table->entries = entries;
    table->capacity = capacity;
}

bool lox_table_set(LoxTable* table, LoxObjString* key, LoxValue value) {
    // See if the hash table is under load, and if so, grow it
    if (table->count + 1 > table->capacity * TABLE_MAX_LOAD) {
        uint32_t capacity = LOX_GROW_CAPACITY(table->capacity);
        adjust_capacity(table, capacity);
    }

    LoxTableEntry* entry = find_entry(table->entries, table->capacity, key);

    bool is_new_key = entry->key == NULL;
    if (is_new_key && LOX_IS_NIL(entry->value)) {
        // This is a new key in the table, and we're not overwriting a tombstoned entry
        // (Tombstoned entries were already counted earlier)
        table->count++;
    }

    entry->key = key;
    entry->value = value;

    return is_new_key;
}

bool lox_table_get(LoxTable* table, LoxObjString* key, LoxValue* out_value) {
    // Don't try and access a table if we have no memory allocated for it
    if (table->count == 0) return false;

    LoxTableEntry* entry = find_entry(table->entries, table->capacity, key);
    if (entry->key != NULL) {
        *out_value = entry->value;
        return true;
    } else {
        return false;
    }
}

bool lox_table_remove(LoxTable* table, LoxObjString* key) {
    if (table->count == 0) return false;

    // Find the entry
    LoxTableEntry* entry = find_entry(table->entries, table->capacity, key);
    if (entry->key == NULL) return false; // Key not in the table

    // Place a tombstone in the entry
    // Entries get marked as tombstone'd when they're removed, so we don't break the chain of values-with-the-same-hash
    entry->key = NULL;
    entry->value = LOX_VAL_BOOL(true);

    // We don't decrement the table's count here - we treat tombstone'd entries as contributing to the table's size
    // If we didn't do this, it'd be possible for the table to fill up entirely with tombstones - which could mean
    // trying to lookup/insert entries into the table would result in an infinite loop (find_entry expects some slack in
    // the table, always).

    return true;
}

void lox_table_add_all(LoxTable* from, LoxTable* to) {
    for (uint32_t i = 0; i < from->capacity; i++) {
        LoxTableEntry* from_entry = from->entries + i;
        if (from_entry->key != NULL) {
            lox_table_set(to, from_entry->key, from_entry->value);
        }
    }
}

LoxObjString* lox_table_find_string(LoxTable* table, const char* chars, uint32_t length, uint32_t hash) {
    if (table->count == 0) return NULL;

    uint32_t index = hash % table->capacity;
    while (true) {
        LoxTableEntry* entry = table->entries + index;
        if (entry->key == NULL) {
            // Stop if we find an empty, non-tombstone entry
            if (LOX_IS_NIL(entry->value)) return NULL;
        } else if (
                entry->key->length == length &&
                entry->key->hash == hash &&
                memcmp(chars, entry->key->chars, length) == 0
                ) {
            // Found a match
            return entry->key;
        }

        index = (index + 1) % table->capacity;
    }
}
