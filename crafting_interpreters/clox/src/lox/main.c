#include "lox/chunk.h"
#include "lox/vm.h"
#include "lox/debug.h"
#include "lox/common.h"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#define BENCHMARK(code) \
    do { \
        clock_t begin = clock(); \
        { \
            code \
        } \
        clock_t end = clock(); \
        double time_spent = (double) (end - begin) / CLOCKS_PER_SEC; \
        printf("Time taken: %g\n", time_spent); \
    } while (false)

/** You need to free the return value with `free()` */
static char* read_file(const char* path) {
    // TODO: What happens if we have a null byte in the middle of the file?

    FILE* file = fopen(path, "rb");
    if (file == NULL) {
        fprintf(stderr, "Could not open file \"%s\"\n", path);
        exit(74);
    }

    // Get file size
    if (fseek(file, 0L, SEEK_END) != 0) {
        fprintf(stderr, "Failed to seek to end of file \"%s\"\n", path);
        exit(74);
    }

    long file_size = ftell(file);
    if (file_size == -1L) {
        fprintf(stderr, "Failed to get size of file \"%s\"\n", path);
        exit(74);
    }

    rewind(file);
    if (ferror(file)) {
        fprintf(stderr, "Failed to rewind file \"%s\"\n", path);
        exit(74);
    }

    char* buffer = malloc(file_size + 1);
    if (buffer == NULL) {
        fprintf(stderr, "Not enough memory to read \"%s\"\n", path);
        exit(74);
    }

    size_t bytes_read = fread(buffer, sizeof(char), file_size, file);
    if (bytes_read != (size_t) file_size) {
        fprintf(stderr, "Failed to read entire file \"%s\"", path);
        exit(74);
    }

    buffer[bytes_read] = '\0'; // Null terminate the string

    fclose(file);
    return buffer;
}

static void repl(LoxVm* vm) {
    // TODO? Support lines longer than 1024 chars
    char line[1024];
    while (true) {
        printf("> ");
        if (fgets(line, sizeof(line), stdin) == NULL) {
            // (Probably) EOF
            printf("\n");
            break;
        }

        lox_vm_interpret(vm, line);
    }
}

static void run_file(LoxVm* vm, const char* path) {
    char* source = read_file(path);
    LoxInterpretResult result = lox_vm_interpret(vm, source);
    free(source);

    if (result == LOX_INTERPRET_COMPILE_ERROR) exit(65);
    if (result == LOX_INTERPRET_RUNTIME_ERROR) exit(70);
}

int main(int argc, const char* argv[]) {
    LoxVm vm;
    lox_vm_init(&vm);

    if (argc == 1) {
        repl(&vm);
    } else if (argc == 2) {
        run_file(&vm, argv[1]);
    } else {
        fprintf(stderr, "Usage: clox [path]\n");
    }

    lox_vm_free(&vm);

    return 0;
}
