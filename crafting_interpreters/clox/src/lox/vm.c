#include "lox/vm.h"
#include "lox/compiler.h"
#include "lox/object.h"
#include "lox/debug.h"
#include <assert.h>
#include <stdarg.h>
#include <string.h>
#include <stdio.h>

static void reset_stack(LoxVm* vm) {
    vm->stack_top = vm->stack;
}

static void runtime_error(LoxVm* vm, const char* format, ...) {
    // Print error
    va_list args;
    va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);
    fputs("\n", stderr);

    // Print line number
    // Since the VM's instruction pointer advances past the instruction before executing it, we want negate one from
    // here
    size_t instruction_offset = vm->ip - vm->chunk->code - 1;
    uint32_t line = lox_chunk_offset_to_line(vm->chunk, instruction_offset);
    fprintf(stderr, "[line %" PRIu32 "] in script\n", line);

    reset_stack(vm);
}

static bool is_falsey(LoxValue value) {
    if (LOX_IS_NIL(value)) {
        return true;
    } else if (LOX_IS_BOOL(value)) {
        return !LOX_AS_BOOL(value);
    } else {
        return true;
    }
}

static void free_object(LoxObj* obj) {
    switch (obj->type) {
        case LOX_OBJ_TY_STRING: {
            LoxObjString* str = (LoxObjString*) obj;
            LOX_FREE_ARRAY(char, str->chars, str->length + 1);
            LOX_FREE(LoxObjString, str);
        }

    }
}

static void free_objects(LoxVm* vm) {
    LoxObj* obj = vm->objects;
    while (obj != NULL) {
        LoxObj* next = obj->next;
        free_object(obj);
        obj = next;
    }
}

void lox_vm_init(LoxVm* vm) {
    reset_stack(vm);
    lox_table_init(&vm->globals);
    lox_table_init(&vm->strings);
    vm->objects = NULL;
}

void lox_vm_free(LoxVm* vm) {
    lox_table_free(&vm->strings);
    lox_table_free(&vm->globals);
    free_objects(vm);
}

void lox_vm_push(LoxVm* vm, LoxValue value) {
    assert(vm->stack_top <= vm->stack + LOX_STACK_MAX);

    *vm->stack_top = value;
    vm->stack_top++;
}

LoxValue lox_vm_pop(LoxVm* vm) {
    assert(vm->stack_top > vm->stack);

    vm->stack_top--;
    return *vm->stack_top;
}

LoxValue* lox_vm_peek(LoxVm* vm, uint32_t distance) {
    assert(vm->stack_top - distance > vm->stack);
    return (vm->stack_top - 1) - distance;
}

/** Pop the top two strings off the stack, concat them, push the result onto the stack */
static void concatenate_strings(LoxVm* vm) {
    LoxObjString* b = LOX_AS_STRING(lox_vm_pop(vm));
    LoxObjString* a = LOX_AS_STRING(lox_vm_pop(vm));

    uint32_t length = a->length + b->length;
    char* chars = LOX_ALLOC(char, length + 1);
    memcpy(chars, a->chars, a->length);
    memcpy(chars + a->length, b->chars, b->length);
    chars[length] = '\0';

    LoxObjString* result = lox_take_string(vm, chars, length);
    lox_vm_push(vm, LOX_VAL_OBJ(result));
}

static LoxInterpretResult run(LoxVm* vm) {
    #define READ_BYTE() (*vm->ip++)
    #define READ_CONSTANT() (vm->chunk->constants.values[READ_BYTE()])
    #define READ_STRING() LOX_AS_STRING(READ_CONSTANT())
    // TODO: Benchmark this?
    //       Would it be quicker to peek twice and store the results in local vars, then adjust the stack top ptr?
    //       Instead of peeking then popping?
    #define BINARY_OP(value_type, op) \
        do { \
            /* Check types */ \
            if (lox_vm_peek(vm, 0)->type != LOX_VAL_TY_NUMBER || lox_vm_peek(vm, 0)->type != LOX_VAL_TY_NUMBER) { \
                runtime_error(vm, "Operands must be numbers"); \
                return LOX_INTERPRET_RUNTIME_ERROR; \
            } \
            /* Perform arithmetic */ \
            LoxValue right = lox_vm_pop(vm); \
            LoxValue left = lox_vm_pop(vm); \
            LoxValue result = value_type(left.as.number op right.as.number); \
            lox_vm_push(vm, result); \
        } while (false)

    while (true) {
        #if LOX_DEBUG_TRACE_EXECUTION
            // Print stack
            printf("        | ");
            for (LoxValue* slot = vm->stack; slot < vm->stack_top; slot++) {
                printf("[");
                lox_print_value(*slot);
                printf("]");
            }
            printf("\n");

            // Print instruction
            lox_disassemble_instruction(vm->chunk, (uint32_t) (vm->ip - vm->chunk->code));
        #endif

        uint8_t opcode;
        switch (opcode = READ_BYTE()) {
            case LOX_OP_CONSTANT: {
                LoxValue constant = READ_CONSTANT();
                lox_vm_push(vm, constant);
                break;
            }
            case LOX_OP_TRUE: {
                lox_vm_push(vm, LOX_VAL_BOOL(true));
                break;
            }
            case LOX_OP_FALSE: {
                lox_vm_push(vm, LOX_VAL_BOOL(false));
                break;
            }
            case LOX_OP_EQUAL: {
                LoxValue b = lox_vm_pop(vm);
                LoxValue a = lox_vm_pop(vm);
                lox_vm_push(vm, LOX_VAL_BOOL(lox_value_eq(a, b)));
                break;
            }
            case LOX_OP_GREATER: {
                BINARY_OP(LOX_VAL_BOOL, >);
                break;
            }
            case LOX_OP_LESS: {
                BINARY_OP(LOX_VAL_BOOL, <);
                break;
            }
            case LOX_OP_NIL: {
                lox_vm_push(vm, LOX_VAL_NIL);
                break;
            }
            case LOX_OP_POP: {
                lox_vm_pop(vm);
                break;
            }
            case LOX_OP_DEFINE_GLOBAL: {
                LoxObjString* name = READ_STRING();

                // Peek the value, then pop
                // This ensures the VM can still find the value if a GC is triggered in the middle of adding it to the
                // hash table
                lox_table_set(&vm->globals, name, *lox_vm_peek(vm, 0));
                lox_vm_pop(vm);
                break;
            }
            case LOX_OP_GET_GLOBAL: {
                LoxObjString* name = READ_STRING();

                // Try read the global and push it onto the stack
                LoxValue value;
                if (!lox_table_get(&vm->globals, name, &value)) {
                    runtime_error(vm, "Undefined variable '%s'", name->chars);
                    return LOX_INTERPRET_RUNTIME_ERROR;
                }

                lox_vm_push(vm, value);
                break;
            }
            case LOX_OP_SET_GLOBAL: {
                LoxObjString* name = READ_STRING();

                // If trying to set a global resulted in a new global, remove it and throw a runtime error
                // (We remove it since otherwise it's effect would be visible in a REPL session, where we can continue
                // after runtime errors)
                if (lox_table_set(&vm->globals, name, *lox_vm_peek(vm, 0))) {
                    lox_table_remove(&vm->globals, name);
                    runtime_error(vm, "Undefined variable '%s'", name->chars);
                    return LOX_INTERPRET_RUNTIME_ERROR;
                }

                break;
            }
            case LOX_OP_GET_LOCAL: {
                uint8_t slot = READ_BYTE();
                lox_vm_push(vm, vm->stack[slot]);
                break;
            }
            case LOX_OP_SET_LOCAL: {
                uint8_t slot = READ_BYTE();
                vm->stack[slot] = *lox_vm_peek(vm, 0);
                break;
            }
            case LOX_OP_PRINT: {
                lox_print_value(lox_vm_pop(vm));
                printf("\n");
                break;
            }
            case LOX_OP_RETURN: {
                // Exit interpreter
                return LOX_INTERPRET_OK;
            }
            case LOX_OP_ADD: {
                LoxValue* b = lox_vm_peek(vm, 0);
                LoxValue* a = lox_vm_peek(vm, 1);
                if (LOX_IS_STRING(*a) && LOX_IS_STRING(*b)) {
                    concatenate_strings(vm);
                } else if (LOX_IS_NUMBER(*a) && LOX_IS_NUMBER(*b)) {
                    double b_num = LOX_AS_NUMBER(lox_vm_pop(vm));
                    double a_num = LOX_AS_NUMBER(lox_vm_pop(vm));
                    lox_vm_push(vm, LOX_VAL_NUMBER(a_num + b_num));
                } else {
                    runtime_error(vm, "Operands must be two numbers or two strings");
                    return LOX_INTERPRET_COMPILE_ERROR;
                }
                break;
            }
            case LOX_OP_SUB: {
                BINARY_OP(LOX_VAL_NUMBER, -);
                break;
            }
            case LOX_OP_MUL: {
                BINARY_OP(LOX_VAL_NUMBER, *);
                break;
            }
            case LOX_OP_DIV: {
                BINARY_OP(LOX_VAL_NUMBER, /);
                break;
            }
            case LOX_OP_NEGATE: {
                // LoxValue value = lox_vm_pop(vm);
                // value.val = -value.val;
                // lox_vm_push(vm, value);

                // It's more efficient to just negate the top value in-place, rather than pop it, negate, and push it
                LoxValue* value = lox_vm_peek(vm, 0);
                if (LOX_IS_NUMBER(*value)) {
                    runtime_error(vm, "Operand must be a number");
                    return LOX_INTERPRET_RUNTIME_ERROR;
                }

                double* num = &LOX_AS_NUMBER(*value);
                *num = -(*num);
                break;
            }
            case LOX_OP_NOT: {
                LoxValue value = lox_vm_pop(vm);
                lox_vm_push(vm, LOX_VAL_BOOL(is_falsey(value)));
                break;
            }
            default: {
                assert(false && "Bad opcode");
            }
        }
    }

    #undef BINARY_OP
    #undef READ_STRING
    #undef READ_CONSTANT
    #undef READ_BYTE
}

LoxInterpretResult lox_vm_interpret(LoxVm* vm, const char* source) {
    LoxChunk chunk;
    lox_chunk_init(&chunk);

    if (!lox_compile(vm, source, &chunk)) {
        lox_chunk_free(&chunk);
        return LOX_INTERPRET_COMPILE_ERROR;
    }

    vm->chunk = &chunk;
    vm->ip = vm->chunk->code;

    LoxInterpretResult result = run(vm);
    lox_chunk_free(&chunk);

    return result;
}
