#include "lox/chunk.h"
#include "lox/memory.h"
#include <assert.h>

LOX_DEFINE_DYN_ARRAY(LoxLineRle, lox_line_rle_array)

void lox_chunk_init(LoxChunk* chunk) {
    chunk->count = 0;
    chunk->capacity = 0;
    chunk->code = NULL;
    lox_line_rle_array_init(&chunk->lines);
    lox_value_array_init(&chunk->constants);
}

void lox_chunk_free(LoxChunk* chunk) {
    LOX_FREE_ARRAY(uint8_t, chunk->code, chunk->capacity);
    lox_line_rle_array_free(&chunk->lines);
    lox_value_array_free(&chunk->constants);
    lox_chunk_init(chunk);
}

void lox_chunk_code_push(LoxChunk* chunk, uint8_t byte, uint32_t line) {
    // Check if we need to reallocate the code array
    if (chunk->capacity < chunk->count + 1) {
        uint32_t old_capacity = chunk->capacity;
        chunk->capacity = LOX_GROW_CAPACITY(old_capacity);
        chunk->code = LOX_GROW_ARRAY(uint8_t, chunk->code, old_capacity, chunk->capacity);
    }

    chunk->code[chunk->count] = byte;

    // Add line
    if (chunk->lines.count == 0 || chunk->lines.values[chunk->lines.count - 1].line != line) {
        // Add a new line entry
        LoxLineRle line_data = {
            .line = line,
            .length = 1,
        };
        lox_line_rle_array_push(&chunk->lines, line_data);
    } else {
        // Extend the last line entry
        chunk->lines.values[chunk->lines.count - 1].length++;
    }

    chunk->count++;
}

uint32_t lox_chunk_constant_push_or_get(LoxChunk* chunk, LoxValue value) {
    // Check if we already have this constant
    for (uint32_t i = 0; i < chunk->constants.count; i++) {
        LoxValue stored_val = chunk->constants.values[i];
        if (lox_value_eq(stored_val, value)) {
            return i;
        }
    }

    // We don't have this constant already, add a new one
    lox_value_array_push(&chunk->constants, value);
    return chunk->constants.count - 1;
}

uint32_t lox_chunk_offset_to_line(const LoxChunk* chunk, uint32_t offset) {
    uint32_t current_offset = 0;
    for (uint32_t i = 0; i < chunk->lines.count; i++) {
        current_offset += chunk->lines.values[i].length;
        if (offset < current_offset) {
            return chunk->lines.values[i].line;
        }
    }

    // Not found - this shouldn't happen
    assert(false && "Line not found");
    return 0;
}
