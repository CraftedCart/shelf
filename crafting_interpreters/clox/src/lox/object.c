#include "lox/object.h"
#include "lox/memory.h"
#include "lox/table.h"
#include "lox/vm.h"
#include <string.h>

#define ALLOC_OBJ(vm, type, object_type) ((type*) allocate_object(vm, sizeof(type), object_type))

static LoxObj* allocate_object(LoxVm* vm, size_t size, LoxObjType type) {
    LoxObj* object = lox_realloc(NULL, 0, size);
    object->type = type;

    // Track the object so we can GC it
    object->next = vm->objects;
    vm->objects = object;

    return object;
}

static LoxObjString* allocate_string(LoxVm* vm, char* chars, uint32_t length, uint32_t hash) {
    LoxObjString* obj = ALLOC_OBJ(vm, LoxObjString, LOX_OBJ_TY_STRING);
    obj->length = length;
    obj->chars = chars;
    obj->hash = hash;

    // Intern the string
    lox_table_set(&vm->strings, obj, LOX_VAL_NIL);

    return obj;
}

/** FNV-1a non-cryptographic hashing algorithm */
static uint32_t hash_string(const char* chars, uint32_t length) {
    uint32_t hash = 2166136261u;
    for (uint32_t i = 0; i < length; i++) {
        hash ^= (uint8_t) chars[i];
        hash *= 16777619;
    }
    return hash;
}

LoxObjString* lox_copy_string(LoxVm* vm, const char* chars, uint32_t length) {
    uint32_t hash = hash_string(chars, length);

    // See if the string is already interned
    LoxObjString* interned = lox_table_find_string(&vm->strings, chars, length, hash);
    if (interned != NULL) return interned;

    // It's not interned, make a new string
    char* heap_chars = LOX_ALLOC(char, length + 1); // +1 for the null terminator
    memcpy(heap_chars, chars, length);
    heap_chars[length] = '\0'; // Null terminate
    return allocate_string(vm, heap_chars, length, hash);
}

LoxObjString* lox_take_string(LoxVm* vm, char* chars, uint32_t length) {
    uint32_t hash = hash_string(chars, length);

    // See if the string is already interned
    LoxObjString* interned = lox_table_find_string(&vm->strings, chars, length, hash);
    if (interned != NULL) {
        LOX_FREE_ARRAY(char, chars, length + 1); // +1 for the null terminator
        return interned;
    }

    return allocate_string(vm, chars, length, hash);
}
