import lldb
from typing import Optional


#  class SyntheticProvider_LoxObjString:
#      def __init__(self, valobj, internal_dict):
#          self.valobj = valobj

#      def num_children(self):
#          return 0

#      def get_child_index(self, name):
#          return 0

#      def get_child_at_index(self, name):
#          return 0

def value_for_enum_member(enum: lldb.SBType, name: str) -> Optional[int]:
    for member in enum.enum_members:
        if member.GetName() == name:
            return member.GetValueAsUnsigned()


def cast_to(obj: lldb.SBValue, type_name: str) -> lldb.SBValue:
    type = obj.GetTarget().FindFirstType(type_name)
    return obj.CreateChildAtOffset(None, 0, type)


def summary_LoxObjString(value, *rest):
    chars_addr = value.GetChildMemberWithName("chars").GetValueAsUnsigned()
    length = value.GetChildMemberWithName("length").GetValueAsUnsigned()

    if length == 0:
        return '""'

    error = lldb.SBError()
    chars = value.GetProcess().ReadMemory(chars_addr, length, error)
    if chars is not None:
        return '"%s"' % chars.decode("utf-8")
    else:
        return f"<couldn't read string>"


def summary_LoxObj(value, *rest):
    type = value.GetChildMemberWithName("type")

    type_num = type.GetValueAsUnsigned()
    if type_num == value_for_enum_member(type.type, "LOX_OBJ_TY_STRING"):
        return summary_LoxObjString(cast_to(value, "LoxObjString"))
    else:
        return "<unknown type>"


def summary_LoxValue(value, *rest):
    type = value.GetChildMemberWithName("type")

    type_num = type.GetValueAsUnsigned()
    if type_num == value_for_enum_member(type.type, "LOX_VAL_TY_BOOL"):
        return value.GetChildMemberWithName("as").GetChildMemberWithName("boolean")
    elif type_num == value_for_enum_member(type.type, "LOX_VAL_TY_NIL"):
        return "nil"
    if type_num == value_for_enum_member(type.type, "LOX_VAL_TY_NUMBER"):
        return value.GetChildMemberWithName("as").GetChildMemberWithName("number")
    if type_num == value_for_enum_member(type.type, "LOX_VAL_TY_OBJ"):
        return value.GetChildMemberWithName("as").GetChildMemberWithName("obj")
    else:
        return "<unknown type>"


def summary_LoxTableEntry(value, *rest):
    key = value.GetChildMemberWithName("key")
    value = value.GetChildMemberWithName("value")

    return f"[{summary_LoxObjString(key)}] = {value}"


def summary_LoxTable(value, *rest):
    entries = []
    for i in range(value.GetChildMemberWithName("capacity").GetValueAsUnsigned()):
        val = value.GetChildMemberWithName("entries").GetValueForExpressionPath(f"[{i}]")
        entries.append(str(val))

    return "\n    " + "\n    ".join(entries)


def __lldb_init_module(debugger, *rest):
    print("Registering LoxObjString")
    summary = lldb.SBTypeSummary.CreateWithFunctionName("lldb_printers.summary_LoxObjString")
    summary.SetOptions(lldb.eTypeOptionHideChildren)
    debugger.GetDefaultCategory().AddTypeSummary(lldb.SBTypeNameSpecifier("LoxObjString", False), summary)

    print("Registering LoxObj")
    summary = lldb.SBTypeSummary.CreateWithFunctionName("lldb_printers.summary_LoxObj")
    summary.SetOptions(lldb.eTypeOptionHideChildren)
    debugger.GetDefaultCategory().AddTypeSummary(lldb.SBTypeNameSpecifier("LoxObj", False), summary)

    print("Registering LoxValue")
    summary = lldb.SBTypeSummary.CreateWithFunctionName("lldb_printers.summary_LoxValue")
    summary.SetOptions(lldb.eTypeOptionHideChildren)
    debugger.GetDefaultCategory().AddTypeSummary(lldb.SBTypeNameSpecifier("LoxValue", False), summary)

    print("Registering LoxTableEntry")
    summary = lldb.SBTypeSummary.CreateWithFunctionName("lldb_printers.summary_LoxTableEntry")
    summary.SetOptions(lldb.eTypeOptionHideChildren)
    debugger.GetDefaultCategory().AddTypeSummary(lldb.SBTypeNameSpecifier("LoxTableEntry", False), summary)

    print("Registering LoxTable")
    summary = lldb.SBTypeSummary.CreateWithFunctionName("lldb_printers.summary_LoxTable")
    summary.SetOptions(lldb.eTypeOptionHideChildren)
    debugger.GetDefaultCategory().AddTypeSummary(lldb.SBTypeNameSpecifier("LoxTable", False), summary)
