#ifndef LOX_DEBUG_H
#define LOX_DEBUG_H

#include "lox/chunk.h"

void lox_print_value(LoxValue value);

/** Disassembles a chunk and prints it to stdout */
void lox_disassemble_chunk(const LoxChunk* chunk, const char* name);

/**
 * Disassembles an instruction and prints it to stdout
 *
 * @return The offset of the next instruction, if there is one
 */
uint32_t lox_disassemble_instruction(const LoxChunk* chunk, uint32_t offset);

#endif
