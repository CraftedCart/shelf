#ifndef LOX_COMPILER_H
#define LOX_COMPILER_H

#include "lox/vm.h"
#include "lox/chunk.h"

/**
 * Attempts to compile the source, and places the results in `out_chunk`. Returns whether compilation succeeded.
 *
 * Does not take ownership of `source`
 */
bool lox_compile(LoxVm* vm, const char* source, LoxChunk* out_chunk);

#endif
