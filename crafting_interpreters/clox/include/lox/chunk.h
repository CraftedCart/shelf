#ifndef LOX_CHUNK_H
#define LOX_CHUNK_H

#include "lox/value.h"
#include "lox/array.h"
#include "lox/common.h"

typedef enum {
    /**
     * Push a constant onto the stack
     *
     * Two byte instruction
     * - u8: Opcode
     * - u8: Constant index
     */
    LOX_OP_CONSTANT,

    /**
     * Push nil onto the stack
     *
     * One byte instruction
     * - u8: Opcode
     */
    LOX_OP_NIL,

    /**
     * Push true onto the stack
     *
     * One byte instruction
     * - u8: Opcode
     */
    LOX_OP_TRUE,

    /**
     * Push false onto the stack
     *
     * One byte instruction
     * - u8: Opcode
     */
    LOX_OP_FALSE,

    /**
     * Negates the value at the top of the stack
     *
     * Single byte instruction
     * - u8: Opcode
     */
    LOX_OP_NEGATE,

    /**
     * Pops two values and adds them
     *
     * Single byte instruction
     * - u8: Opcode
     */
    LOX_OP_ADD,

    /**
     * Pops two values and subtracts them
     *
     * Single byte instruction
     * - u8: Opcode
     */
    LOX_OP_SUB,

    /**
     * Pops two values and multiplies them
     *
     * Single byte instruction
     * - u8: Opcode
     */
    LOX_OP_MUL,

    /**
     * Pops two values and divides them
     *
     * Single byte instruction
     * - u8: Opcode
     */
    LOX_OP_DIV,

    /**
     * Inverts the value at the top of the stack (Turns truthy values into `false` and falsey values into `true`)
     *
     * Single byte instruction
     * - u8: Opcode
     */
    LOX_OP_NOT,

    /**
     * Pops the top two values off the stack, pushes a bool whether they're equal or not
     *
     * Single byte instruction
     * - u8: Opcode
     */
    LOX_OP_EQUAL,

    /**
     * Pops the top two values off the stack, pushes a bool whether a > b
     *
     * Single byte instruction
     * - u8: Opcode
     */
    LOX_OP_GREATER,

    /**
     * Pops the top two values off the stack, pushes a bool whether a < b
     *
     * Single byte instruction
     * - u8: Opcode
     */
    LOX_OP_LESS,

    /**
     * Pop the value at the top of the stack
     *
     * Single byte instruction
     * - u8: Opcode
     */
    LOX_OP_POP,

    /**
     * Pop the value at the top of the stack and define it in a global variable
     *
     * Two byte instruction
     * - u8: Opcode
     * - u8: Global name (constant index)
     */
    LOX_OP_DEFINE_GLOBAL,

    /**
     * Push the value of the given global onto the stack
     *
     * Two byte instruction
     * - u8: Opcode
     * - u8: Global name (constant index)
     */
    LOX_OP_GET_GLOBAL,

    /**
     * Pop the value of the given global onto the stack and assign it to the given global
     *
     * Two byte instruction
     * - u8: Opcode
     * - u8: Global name (constant index)
     */
    LOX_OP_SET_GLOBAL,

    /**
     * Push the value of the given local onto the stack
     *
     * Two byte instruction
     * - u8: Opcode
     * - u8: local name (constant index)
     */
    LOX_OP_GET_LOCAL,

    /**
     * Pop the value of the given local onto the stack and assign it to the given local
     *
     * Two byte instruction
     * - u8: Opcode
     * - u8: local name (constant index)
     */
    LOX_OP_SET_LOCAL,

    /**
     * Consume and print the value at the top of the stack
     *
     * Single byte instruction
     * - u8: Opcode
     */
    LOX_OP_PRINT,

    /**
     * Return from a function
     *
     * Single byte instruction
     * - u8: Opcode
     */
    LOX_OP_RETURN,
} LoxOpCode;

/**
 * Run-length encoded line entry, for debugging
 */
typedef struct {
    uint32_t line;

    /** Number of bytes in the `code` array that this line pertains to */
    uint32_t length;
} LoxLineRle;

LOX_DECLARE_DYN_ARRAY(LoxLineRle, lox_line_rle_array)

typedef struct {
    /** Number of values stored in `code` and `lines` */
    uint32_t count;

    /** Number of values allocated in `code` and `lines` */
    uint32_t capacity;

    /** Bytecode array */
    uint8_t* code;

    /** Line numbers for each byte in `code` */
    LoxLineRleArray lines;

    /** Constant values */
    LoxValueArray constants;
} LoxChunk;

/** Initialize an new chunk with no code */
void lox_chunk_init(LoxChunk* chunk);

/** Deallocate memory for a chunk and re-initialize it */
void lox_chunk_free(LoxChunk* chunk);

/** Add a byte to the chunk's code array */
void lox_chunk_code_push(LoxChunk* chunk, uint8_t byte, uint32_t line);

/**
 * Get the line number for a code offset
 *
 * This is not particularly fast
 */
uint32_t lox_chunk_offset_to_line(const LoxChunk* chunk, uint32_t offset);

/**
 * Add a constant to the chunk's constant array, or returns the index of an existing constant if `value` is already in
 * the constants array
 *
 * @return Index of the constant
 *
 * Note that only 255 constants are supported right now - you should check for if we exceed that number
 * (Since LOX_OP_CONSTANT only uses a single byte to determine which constant to load)
 */
uint32_t lox_chunk_constant_push_or_get(LoxChunk* chunk, LoxValue value);

#endif
