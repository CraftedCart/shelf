#ifndef LOX_COMMON_H
#define LOX_COMMON_H

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

// #if defined(__GNUC__) // GCC, Clang
//     #define LOX_FORCE_INLINE inline __attribute__((always_inline))
// #elif defined(_MSC_VER) // MSVC
//     #define LOX_FORCE_INLINE __forceinline
// #else
//     #define LOX_FORCE_INLINE inline
// #endif

#define LOX_DEBUG_TRACE_EXECUTION 1
#define LOX_DEBUG_PRINT_CODE 1

#endif
