#ifndef LOX_MEMORY_H
#define LOX_MEMORY_H

#include "lox/common.h"

/**
 * Calculate the new capacity for growing a dynamic array.
 *
 * Does not do any reallocations, just computes the new capacity value.
 *
 * @param capacity The old capacity
 *
 * @return The new capacity to use
 */
#define LOX_GROW_CAPACITY(capacity) \
    ((capacity) < 8 ? 8 : (capacity) * 2)

/**
 * Reallocates a dynamic array.
 *
 * @param type Type of object being stored
 * @param ptr Pointer to existing array data - this gets free'd
 * @param old_count Number of stored items in the existing array
 * @param new_count Number of items to allocate space for
 *
 * @return Newly allocated pointer
 */
#define LOX_GROW_ARRAY(type, ptr, old_count, new_count) \
    (type*) lox_realloc(ptr, sizeof(type) * old_count, sizeof(type) * new_count)


/**
 * Frees a dynamic array.
 *
 * @param type Type of object being stored
 * @param ptr Pointer to array
 * @param old_count Capacity of array
 */
#define LOX_FREE_ARRAY(type, ptr, old_count) \
    lox_realloc(ptr, sizeof(type) * (old_count), 0)

#define LOX_FREE(type, ptr) \
    lox_realloc(ptr, sizeof(type), 0)

/**
 * Handles allocation, reallocation, and freeing
 *
 * @param ptr If reallocating or freeing memory, the memory to realloc/free.
 *            Should be `NULL` when allocating.
 * @param old_size Bytes allocated in `ptr`.
 *                 If zero, and new_size is non-zero, we allocate memory.
 * @param new_size Bytes to allocate.
 *                 If zero, free memory at `ptr`.
 *
 * @return Pointer to new allocation, or `NULL` if freeing
 */
void* lox_realloc(void* ptr, size_t old_size, size_t new_size);

#define LOX_ALLOC(type, count) ((type*) lox_realloc(NULL, 0, sizeof(type) * count))

#endif
