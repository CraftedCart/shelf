#ifndef LOX_ARRAY_H
#define LOX_ARRAY_H

#include "lox/memory.h"
#include "lox/common.h"

#define LOX_DECLARE_DYN_ARRAY(type, func_prefix) \
    typedef struct { \
        uint32_t count; \
        uint32_t capacity; \
        type* values; \
    } type##Array; \
    \
    /** Initialize an new array with no values */ \
    void func_prefix##_init(type##Array* array); \
    \
    /** Deallocate memory for an array and re-initialize it */ \
    void func_prefix##_free(type##Array* array); \
    \
    /** Add a value to the array */ \
    void func_prefix##_push(type##Array* array, type value);

#define LOX_DEFINE_DYN_ARRAY(type, func_prefix) \
    void func_prefix##_init(type##Array* array) { \
        array->count = 0; \
        array->capacity = 0; \
        array->values = NULL; \
    } \
    \
    void func_prefix##_free(type##Array* array) { \
        LOX_FREE_ARRAY(type, array->values, array->capacity); \
        func_prefix##_init(array); \
    } \
    \
    void func_prefix##_push(type##Array* array, type value) { \
        /* Check if we need to reallocate the array */ \
        if (array->capacity < array->count + 1) { \
            uint32_t old_capacity = array->capacity; \
            array->capacity = LOX_GROW_CAPACITY(old_capacity); \
            array->values = LOX_GROW_ARRAY(type, array->values, old_capacity, array->capacity); \
        } \
        \
        array->values[array->count] = value; \
        array->count++; \
    }

#endif
