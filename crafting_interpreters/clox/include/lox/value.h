#ifndef LOX_VALUE_H
#define LOX_VALUE_H

#include "lox/array.h"
#include "lox/common.h"

typedef enum {
    LOX_VAL_TY_BOOL,
    LOX_VAL_TY_NIL,
    LOX_VAL_TY_NUMBER,
    LOX_VAL_TY_OBJ, ///< Objects are heap-allocated
} LoxValueType;

typedef struct LoxObj LoxObj;
typedef struct LoxObjString LoxObjString;

typedef struct {
    LoxValueType type;
    union {
        bool boolean;
        double number;
        LoxObj* obj;
    } as;
} LoxValue;

LOX_DECLARE_DYN_ARRAY(LoxValue, lox_value_array)

bool lox_value_eq(LoxValue a, LoxValue b);

#define LOX_VAL_BOOL(value) ((LoxValue) { .type = LOX_VAL_TY_BOOL, .as = { .boolean = value } })
#define LOX_VAL_NIL ((LoxValue) { .type = LOX_VAL_TY_NIL })
#define LOX_VAL_NUMBER(value) ((LoxValue) { .type = LOX_VAL_TY_NUMBER, .as = { .number = value } })
#define LOX_VAL_OBJ(value) ((LoxValue) { .type = LOX_VAL_TY_OBJ, .as = { .obj = (LoxObj*) (value) } })

#define LOX_IS_BOOL(value) ((value).type == LOX_VAL_TY_BOOL)
#define LOX_IS_NIL(value) ((value).type == LOX_VAL_TY_NIL)
#define LOX_IS_NUMBER(value) ((value).type == LOX_VAL_TY_NUMBER)
#define LOX_IS_OBJ(value) ((value).type == LOX_VAL_TY_OBJ)

#define LOX_AS_BOOL(value) ((value).as.boolean)
#define LOX_AS_NUMBER(value) ((value).as.number)
#define LOX_AS_OBJ(value) ((value).as.obj)

#endif
