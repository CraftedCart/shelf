#ifndef LOX_VM_H
#define LOX_VM_H

#include "lox/chunk.h"
#include "lox/table.h"
#include "lox/value.h"

#define LOX_STACK_MAX 256

typedef struct LoxVm {
    LoxChunk* chunk;

    /**
     * Instruction pointer
     *
     * Pointer to the next instruction to execute
     */
    uint8_t* ip;

    LoxValue stack[LOX_STACK_MAX];

    /**
     * Pointer to the element just past the top value in the stack
     *
     * It points to where the next value would be pushed onto the stack, or just past the end of the stack if the stack
     * is full.
     */
    LoxValue* stack_top;

    /** Global variables */
    LoxTable globals;

    /**
     * Interned strings
     *
     * We use this more like a hash set than a hash table - we don't use values and set them all to nil
     */
    LoxTable strings;

    /** Beginning of objects linked list */
    LoxObj* objects;
} LoxVm;

typedef enum {
    LOX_INTERPRET_OK,
    LOX_INTERPRET_COMPILE_ERROR,
    LOX_INTERPRET_RUNTIME_ERROR,
} LoxInterpretResult;

void lox_vm_init(LoxVm* vm);
void lox_vm_free(LoxVm* vm);

void lox_vm_push(LoxVm* vm, LoxValue value);
LoxValue lox_vm_pop(LoxVm* vm);
LoxValue* lox_vm_peek(LoxVm* vm, uint32_t distance);

LoxInterpretResult lox_vm_interpret(LoxVm* vm, const char* source);

#endif
