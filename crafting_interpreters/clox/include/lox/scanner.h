#ifndef LOX_SCANNER_H
#define LOX_SCANNER_H

#include "lox/common.h"

typedef struct {
    /** Beginning of the current lexeme being scanned */
    const char* start;

    /** Current character we're looking at */
    const char* current;

    uint32_t line;
} LoxScanner;

typedef enum {
    // Single-character tokens
    LOX_TOKEN_LEFT_PAREN,
    LOX_TOKEN_RIGHT_PAREN,
    LOX_TOKEN_LEFT_BRACE,
    LOX_TOKEN_RIGHT_BRACE,
    LOX_TOKEN_COMMA,
    LOX_TOKEN_DOT,
    LOX_TOKEN_MINUS,
    LOX_TOKEN_PLUS,
    LOX_TOKEN_SEMICOLON,
    LOX_TOKEN_SLASH,
    LOX_TOKEN_STAR,

    // One or two character tokens
    LOX_TOKEN_BANG,
    LOX_TOKEN_BANG_EQUAL,
    LOX_TOKEN_EQUAL,
    LOX_TOKEN_EQUAL_EQUAL,
    LOX_TOKEN_GREATER,
    LOX_TOKEN_GREATER_EQUAL,
    LOX_TOKEN_LESS,
    LOX_TOKEN_LESS_EQUAL,

    // Literals
    LOX_TOKEN_IDENTIFIER,
    LOX_TOKEN_STRING,
    LOX_TOKEN_NUMBER,

    // Keywords
    LOX_TOKEN_AND,
    LOX_TOKEN_CLASS,
    LOX_TOKEN_ELSE,
    LOX_TOKEN_FALSE,
    LOX_TOKEN_FOR,
    LOX_TOKEN_FUN,
    LOX_TOKEN_IF,
    LOX_TOKEN_NIL,
    LOX_TOKEN_OR,
    LOX_TOKEN_PRINT,
    LOX_TOKEN_RETURN,
    LOX_TOKEN_SUPER,
    LOX_TOKEN_THIS,
    LOX_TOKEN_TRUE,
    LOX_TOKEN_VAR,
    LOX_TOKEN_WHILE,

    LOX_TOKEN_ERROR,
    LOX_TOKEN_EOF,
} LoxTokenType;

typedef struct {
    LoxTokenType type;

    union {
        /** Pointer to the lexeme in the source string */
        const char* start;

        /** Only used if `type == LOX_TOKEN_ERROR` */
        const char* error_message;
    };

    /** Length of the lexeme */
    uint32_t length;

    uint32_t line;
} LoxToken;

/**
 * - Does not take ownership of `source`
 * - `source` must outlive all tokens scanned
 */
void lox_scanner_init(LoxScanner* scanner, const char* source);
LoxToken lox_scanner_scan(LoxScanner* scanner);

#endif

