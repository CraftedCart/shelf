#ifndef LOX_TABLE_H
#define LOX_TABLE_H

#include "lox/value.h"
#include "lox/common.h"

/**
 * A tombstoned entry is represented with a `NULL` key and a `LOX_VAL_BOOL(true)` value. Empty entries have a
 * `LOX_VAL_NIL` value.
 */
typedef struct {
    LoxObjString* key;
    LoxValue value;
} LoxTableEntry;

typedef struct {
    uint32_t count;
    uint32_t capacity;
    LoxTableEntry* entries;
} LoxTable;

void lox_table_init(LoxTable* table);
void lox_table_free(LoxTable* table);

/**
 * Adds the given key/value pair to the hash table. If `key` is already in the table, we replace its value.
 *
 * @return Whether the key was new or not (true if new, false if replacing an existing value)
 */
bool lox_table_set(LoxTable* table, LoxObjString* key, LoxValue value);

/**
 * `out_value` is set if the key exists
 *
 * @return Whether the key was found or not
 */
bool lox_table_get(LoxTable* table, LoxObjString* key, LoxValue* out_value);

/** @return Whether anything was removed */
bool lox_table_remove(LoxTable* table, LoxObjString* key);

/** Copy all entries from `from` to `to` */
void lox_table_add_all(LoxTable* from, LoxTable* to);

/** Find an existing string key - returns NULL if it's not in the table */
LoxObjString* lox_table_find_string(LoxTable* table, const char* chars, uint32_t length, uint32_t hash);

#endif
