#ifndef LOX_OBJECT_H
#define LOX_OBJECT_H

#include "lox/value.h"
#include "lox/common.h"

struct LoxVm;

typedef enum {
    LOX_OBJ_TY_STRING,
} LoxObjType;

struct LoxObj {
    LoxObjType type;
    struct LoxObj* next; ///< Linked list of objects
};

struct LoxObjString {
    LoxObj obj;
    uint32_t length;
    char* chars;

    /** Store the hash for a string here, so we don't have to recompute it every time we need to use a hash table */
    uint32_t hash;
};

LoxObjString* lox_copy_string(struct LoxVm* vm, const char* chars, uint32_t length);

/** Takes ownership of `chars` */
LoxObjString* lox_take_string(struct LoxVm* vm, char* chars, uint32_t length);

static inline bool lox_is_obj_type(LoxValue value, LoxObjType type) {
    return LOX_IS_OBJ(value) && LOX_AS_OBJ(value)->type == type;
}

#define LOX_IS_STRING(value) lox_is_obj_type(value, LOX_OBJ_TY_STRING)

#define LOX_AS_STRING(value) ((LoxObjString*) LOX_AS_OBJ(value))

#endif
