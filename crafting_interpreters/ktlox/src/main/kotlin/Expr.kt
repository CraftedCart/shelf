sealed class Expr {
    /** Assign to a variable within the environment */
    class Assign(val name: Token.Ident, val expr: Expr) : Expr()

    class Binary(val left: Expr, val operator: Token, val right: Expr) : Expr()
    class Logical(val left: Expr, val operator: Token, val right: Expr) : Expr()
    class Grouping(val expr: Expr) : Expr()
    class Literal(val value: Any?) : Expr()
    class Unary(val operator: Token, val right: Expr) : Expr()
    class Variable(val name: Token.Ident) : Expr()
    class Call(val callee: Expr, val paren: Token, val args: List<Expr>) : Expr() // Paren is stored so we know where to report runtime errors

    /** `fun(a, b) { return a + b; }` */
    class AnonymousFunction(val keyword: Token, val params: List<Token.Ident>, val body: List<Stmt>) : Expr() // Keyword "fun" is stored so we know where to report runtime errors (...maybe)

    /** Property access using a dot, like `something.something_else` */
    class Get(val obj: Expr, val name: Token.Ident) : Expr()
    class Set(val obj: Expr, val name: Token.Ident, val value: Expr) : Expr()

    class This(val keyword: Token.This) : Expr()
    class Super(val keyword: Token.Super, val method: Token.Ident) : Expr()
}