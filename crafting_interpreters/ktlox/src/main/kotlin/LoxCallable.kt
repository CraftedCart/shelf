interface LoxCallable {
    fun call(interpreter: Interpreter, args: List<Any?>): Any?
    fun arity(): Int
}