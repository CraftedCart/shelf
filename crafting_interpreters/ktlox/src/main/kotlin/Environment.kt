class Environment(val enclosing: Environment? = null) {
    private val values: MutableMap<String, Any?> = mutableMapOf()

    fun define(name: String, value: Any?) {
        // Don't check if the value already exists. This means we allow variable shadowing within the same scope.
        values[name] = value
    }

    fun assign(name: Token.Ident, value: Any?) {
        if (values.containsKey(name.ident)) {
            values[name.ident] = value
        } else if (enclosing != null) {
            enclosing.assign(name, value)
        } else {
            throw Interpreter.RuntimeError(name, "Attempt to assign to undefined variable '${name.ident}'")
        }
    }

    fun assignAt(distance: Int, name: Token.Ident, value: Any?) {
        ancestor(distance).values[name.ident] = value
    }

    fun get(name: Token.Ident): Any? {
        return values.getOrElse(name.ident) {
            if (enclosing != null) {
                enclosing.get(name)
            } else {
                throw Interpreter.RuntimeError(name, "Attempt to get undefined variable '${name.ident}'")
            }
        }
    }

    fun getAt(distance: Int, name: String): Any? = ancestor(distance).values[name]

    @OptIn(ExperimentalStdlibApi::class)
    fun ancestor(distance: Int): Environment {
        var env = this
        for (i in 0..<distance) {
            env = env.enclosing!!
        }

        return env
    }
}