class Resolver(val interpreter: Interpreter) {
    private enum class FunctionType {
        NONE,
        FUNCTION,
        METHOD,
        INITIALIZER,
    }

    private enum class ClassType {
        NONE,
        CLASS,
        SUBCLASS,
        MIXIN,
    }

    private class VarInfo(val definedAt: Token) {
        /**
         * False when vars have been declared, true when they've been defined
         * Eg: in `var a = 5;`, we declare it when figuring out `var a`, then define it after evaluating its initializer
         * expression (if it has one). Variables are still defined even if they have no initializer.
         */
        var isDefined = false

        /**
         * Is this variable ever read from?
         *
         * If not, we can emit an unused variable warning
         */
        var isRead = false
    }

    // Map keys are var names
    private val scopes: MutableList<MutableMap<String, VarInfo>> = mutableListOf()

    private var currentFunction = FunctionType.NONE
    private var currentClass = ClassType.NONE

    fun resolveStatements(stmts: List<Stmt>) {
        stmts.forEach { resolveStatement(it) }
    }

    private fun resolveStatement(stmt: Stmt) {
        when (stmt) {
            is Stmt.Block -> {
                beginScope()
                resolveStatements(stmt.stmts)
                endScope()
            }
            is Stmt.Break -> {}
            is Stmt.Expression -> resolveExpression(stmt.expr)
            is Stmt.Function -> {
                declare(stmt.name)
                define(stmt.name) // Define the name eagarly so we can do recursive functions
                resolveFunction(stmt.params, stmt.body, FunctionType.FUNCTION)
            }
            is Stmt.If -> {
                resolveExpression(stmt.condition)
                resolveStatement(stmt.thenBranch)
                if (stmt.elseBranch != null) resolveStatement(stmt.elseBranch)
            }
            is Stmt.Print -> resolveExpression(stmt.expr)
            is Stmt.Return -> {
                if (currentFunction == FunctionType.NONE) {
                    Diagnostics.error(stmt.keyword, "Cannot return from top-level code")
                }

                if (stmt.expr != null) {
                    if (currentFunction == FunctionType.INITIALIZER) {
                        Diagnostics.error(stmt.keyword, "Cannot return a value from initializer")
                    }

                    resolveExpression(stmt.expr)
                }
            }
            is Stmt.Var -> {
                // Split variable declarations up into declare and define steps, to account for stuff like...
                // ```
                // var a = "outer";
                // {
                //     var a = a;
                // }
                // ```
                //
                // We actually make this a compile error in Lox - I'm frankly alright with shadowing and get a little
                // irritated when languages/build scripts disallow it, and would rather have it work like Rust ;P
                declare(stmt.name)
                if (stmt.initializer != null) {
                    resolveExpression(stmt.initializer)
                }
                define(stmt.name)
            }
            is Stmt.While -> {
                resolveExpression(stmt.condition)
                resolveStatement(stmt.body)
            }
            is Stmt.Class -> {
                val enclosingClass = currentClass
                currentClass = ClassType.CLASS

                declare(stmt.name)
                define(stmt.name)

                if (stmt.superclass != null && stmt.superclass.name == stmt.name) {
                    Diagnostics.error(stmt.name, "A class cannot inherit from itself")
                }

                if (stmt.superclass != null) {
                    currentClass = ClassType.SUBCLASS
                    resolveExpression(stmt.superclass)

                    beginScope()

                    val superVar = VarInfo(stmt.name)
                    superVar.isDefined = true
                    // TODO: Always mark it as used to suppress warnings?
                    scopes.last()["super"] = superVar
                }

                resolveInstanceMethods(stmt.name, stmt.instanceMethods)

                if (stmt.superclass != null) endScope()

                currentClass = enclosingClass
            }
            is Stmt.Mixin -> {
                val enclosingClass = currentClass
                currentClass = ClassType.MIXIN

                declare(stmt.name)
                define(stmt.name)
                resolveInstanceMethods(stmt.name, stmt.instanceMethods)

                currentClass = enclosingClass
            }
        }
    }

    private fun resolveInstanceMethods(name: Token.Ident, instanceMethods: List<Stmt.Function>) {
        beginScope()

        val thisVar = VarInfo(name)
        thisVar.isDefined = true
        scopes.last()["this"] = thisVar

        for (method in instanceMethods) {
            val type = if (method.name.ident == "init") {
                FunctionType.INITIALIZER
            } else {
                FunctionType.METHOD
            }
            resolveFunction(method.params, method.body, type)
        }

        endScope()
    }

    private fun resolveExpression(expr: Expr) {
        when (expr) {
            is Expr.AnonymousFunction -> resolveFunction(expr.params, expr.body, FunctionType.FUNCTION)
            is Expr.Assign -> {
                resolveExpression(expr.expr) // Resolve the value we're assigning
                resolveLocal(expr, expr.name.ident) // Resolve what we're assigning to
            }
            is Expr.Binary -> {
                resolveExpression(expr.left)
                resolveExpression(expr.right)
            }
            is Expr.Call -> {
                resolveExpression(expr.callee)
                expr.args.forEach { resolveExpression(it) }
            }
            is Expr.Grouping -> resolveExpression(expr.expr)
            is Expr.Literal -> {}
            is Expr.Logical -> {
                resolveExpression(expr.left)
                resolveExpression(expr.right)
            }
            is Expr.Unary -> resolveExpression(expr.right)
            is Expr.Variable -> {
                if (scopes.isNotEmpty()) {
                    // Check for reading a variable in its own initializer, like `val a = a;`
                    val lastVarInfo = scopes.last().get(expr.name.ident)
                    if (lastVarInfo != null && !lastVarInfo.isDefined) {
                        Diagnostics.error(expr.name, "Cannot read local variable in its own initializer")
                    }
                }

                val scope = resolveLocal(expr, expr.name.ident)
                if (scope != null) {
                    scope[expr.name.ident]!!.isRead = true
                }
            }

            is Expr.Get -> resolveExpression(expr.obj)
            is Expr.Set -> {
                resolveExpression(expr.value)
                resolveExpression(expr.obj)
            }
            is Expr.This -> {
                if (currentClass == ClassType.NONE) {
                    Diagnostics.error(expr.keyword, "Cannot use 'this' outside of a class")
                } else {
                    val scope = resolveLocal(expr, "this")
                    scope!!["this"]!!.isRead = true
                }
            }
            is Expr.Super -> {
                if (currentClass == ClassType.NONE) {
                    Diagnostics.error(expr.keyword, "Cannot use 'super' outside of a class")
                } else if (currentClass != ClassType.SUBCLASS) {
                    Diagnostics.error(expr.keyword, "Cannot use 'super' in a class with no superclass")
                } else {
                    resolveLocal(expr, "super")
                }
            }
        }
    }

    /** Returns the scope it was resolved in */
    private fun resolveLocal(expr: Expr, name: String): MutableMap<String, VarInfo>? {
        for ((i, scope) in scopes.reversed().withIndex()) {
            if (scope.containsKey(name)) {
                interpreter.resolve(expr, i)
                return scope
            }
        }

        return null
    }

    private fun resolveFunction(params: List<Token.Ident>, body: List<Stmt>, type: FunctionType) {
        val enclosingFunctionType = currentFunction;
        currentFunction = type;

        beginScope()

        for (param in params) {
            declare(param)
            define(param)
        }

        resolveStatements(body)

        endScope()

        currentFunction = enclosingFunctionType
    }

    private fun beginScope() {
        scopes.add(mutableMapOf())
    }

    private fun endScope() {
        val scope = scopes.removeLast()
        for ((name, varInfo) in scope) {
            if (!varInfo.isRead) {
                Diagnostics.warn(varInfo.definedAt, "Unused variable $name")
            }
        }
    }

    private fun declare(name: Token.Ident) {
        if (scopes.isEmpty()) return

        val scope = scopes.last()
        if (scope.contains(name.ident)) {
            Diagnostics.error(name, "A variable named '${name.ident}' already exists in this scope")
        }
        scope[name.ident] = VarInfo(name)
    }

    private fun define(name: Token.Ident) {
        if (scopes.isEmpty()) return
        scopes.last()[name.ident]!!.isDefined = true
    }
}