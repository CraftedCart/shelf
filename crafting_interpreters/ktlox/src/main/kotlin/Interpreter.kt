class Interpreter {
    class InternalError(message: String) : RuntimeException(message)
    class RuntimeError(val token: Token, message: String) : RuntimeException(message)

    // Control-flow exceptions
    // We try and make throwing these cheapish: Don't add a stack trace or allow exception suppression
    class LoopBreak : RuntimeException(null, null, false, false)
    class FunctionReturn(val value: Any?) : RuntimeException(null, null, false, false)

    private val globalEnv = Environment()
    private var env: Environment = globalEnv
    private var locals: MutableMap<Expr, Int> = mutableMapOf()

    init {
        // Built-in functions
        globalEnv.define("clock", object : LoxCallable {
            override fun call(interpreter: Interpreter, args: List<Any?>): Any = System.currentTimeMillis().toDouble() / 1000.0
            override fun arity(): Int = 0
            override fun toString(): String = "<native fn clock>"
        })
        globalEnv.define("to_string", object : LoxCallable {
            override fun call(interpreter: Interpreter, args: List<Any?>): Any = stringify(args[0])
            override fun arity(): Int = 1
            override fun toString(): String = "<native fn to_string>"
        })
    }

    fun interpret(stmts: List<Stmt>) {
        try {
            stmts.forEach { executeStmt(it) }
        } catch (error: RuntimeError) {
            Diagnostics.runtimeError(error)
        }
    }

    fun resolve(expr: Expr, depth: Int) {
        locals[expr] = depth
    }

    private fun executeStmt(stmt: Stmt) {
        when (stmt) {
            is Stmt.Expression -> evaluateExpr(stmt.expr)
            is Stmt.Print -> println(stringify(evaluateExpr(stmt.expr)))
            is Stmt.Var -> env.define(stmt.name.ident, stmt.initializer?.let { evaluateExpr(it) })
            is Stmt.Function -> env.define(stmt.name.ident, LoxFunction(stmt, env, false))
            is Stmt.Block -> executeBlock(stmt.stmts, Environment(env))
            is Stmt.If -> if (isTruthy(evaluateExpr(stmt.condition))) {
                executeStmt(stmt.thenBranch)
            } else if (stmt.elseBranch != null) {
                executeStmt(stmt.elseBranch)
            }
            is Stmt.While -> try {
                while (isTruthy(evaluateExpr(stmt.condition))) {
                    executeStmt(stmt.body)
                }
            } catch (notError: LoopBreak) {
                // Break requested - we got out of the loop
            }
            is Stmt.Break -> throw LoopBreak()
            is Stmt.Return -> throw FunctionReturn(if (stmt.expr != null) { evaluateExpr(stmt.expr) } else { null })
            is Stmt.Class -> {
                env.define(stmt.name.ident, null)

                val superclass = if (stmt.superclass != null) {
                    // Check superclass is a class
                    val candidate = evaluateExpr(stmt.superclass)
                    if (candidate is LoxClass) {
                        candidate
                    } else {
                        throw RuntimeError(stmt.superclass.name, "Superclass must be a class")
                    }
                } else {
                    null
                }

                // Make env to resolve `super` within functions here
                if (superclass != null) {
                    env = Environment(env)
                    env.define("super", superclass)
                }

                // Create all methods in the class
                val instanceMethods = mutableMapOf<String, LoxFunction>()
                val classFunctions = mutableMapOf<String, LoxFunction>()
                for (method in stmt.instanceMethods) {
                    instanceMethods[method.name.ident] = LoxFunction(method, env, method.name.ident == "this")
                }
                for (method in stmt.classFunctions) {
                    classFunctions[method.name.ident] = LoxFunction(method, env, false)
                }

                if (superclass != null) {
                    env = env.enclosing!!
                }

                val cls = LoxClass(stmt.name.ident, superclass, instanceMethods, classFunctions)

                env.assign(stmt.name, cls)
            }
            is Stmt.Mixin -> {
                env.define(stmt.name.ident, null)

                // Create all methods in the mixin
                val instanceMethods = mutableMapOf<String, LoxFunction>()
                for (method in stmt.instanceMethods) {
                    instanceMethods[method.name.ident] = LoxFunction(method, env, method.name.ident == "this")
                }

                val mixin = LoxMixin(stmt.name.ident, instanceMethods)
                env.assign(stmt.name, mixin)
            }
        }
    }

    fun executeBlock(stmts: List<Stmt>, environment: Environment) {
        val prevEnv = env;
        try {
            env = environment
            stmts.forEach { executeStmt(it) }
        } finally {
            env = prevEnv
        }
    }

    fun evaluateExpr(expr: Expr): Any? {
        when (expr) {
            is Expr.Binary -> {
                // Evaluate left then right
                val left = evaluateExpr(expr.left)
                val right = evaluateExpr(expr.right)
                return when (expr.operator) {
                    is Token.Minus -> {
                        checkNumberOperands(expr.operator, left, right)
                        (left as Double) - (right as Double)
                    }
                    is Token.Slash -> {
                        checkNumberOperands(expr.operator, left, right)
                        (left as Double) / (right as Double)
                    }
                    is Token.Star -> {
                        checkNumberOperands(expr.operator, left, right)
                        (left as Double) * (right as Double)
                    }
                    is Token.Plus -> if (left is Double && right is Double) {
                        left + right
                    } else if (left is String && right is String) {
                        left + right
                    } else {
                        throw RuntimeError(expr.operator, "Operands must be two numbers or two strings")
                    }
                    is Token.Greater -> {
                        checkNumberOperands(expr.operator, left, right)
                        (left as Double) > (right as Double)
                    }
                    is Token.GreaterEqual -> {
                        checkNumberOperands(expr.operator, left, right)
                        (left as Double) >= (right as Double)
                    }
                    is Token.Less -> {
                        checkNumberOperands(expr.operator, left, right)
                        (left as Double) < (right as Double)
                    }
                    is Token.LessEqual -> {
                        checkNumberOperands(expr.operator, left, right)
                        (left as Double) <= (right as Double)
                    }
                    is Token.BangEqual -> !isEqual(left, right)
                    is Token.EqualEqual -> isEqual(left, right)
                    else -> throw InternalError("Unexpected operator ${expr.operator} to binary expression")
                }
            }
            is Expr.Logical -> {
                when (expr.operator) {
                    is Token.Or -> {
                        val left = evaluateExpr(expr.left)
                        return if (isTruthy(left)) {
                            left
                        } else {
                            evaluateExpr(expr.right)
                        }
                    }
                    is Token.And -> {
                        val left = evaluateExpr(expr.left)
                        return if (!isTruthy(left)) {
                            left
                        } else {
                            evaluateExpr(expr.right)
                        }
                    }
                    else -> throw InternalError("Unexpected operator ${expr.operator} to logical expression")
                }
            }
            is Expr.Grouping -> return evaluateExpr(expr.expr)
            is Expr.Literal -> return expr.value
            is Expr.Unary -> {
                val right = evaluateExpr(expr.right)
                return when (expr.operator) {
                    is Token.Minus -> -(right as Double)
                    is Token.Bang -> !isTruthy(right)
                    else -> throw InternalError("Unexpected operator ${expr.operator} to unary expression")
                }
            }

            is Expr.Variable -> return lookUpVariable(expr.name, expr)
            is Expr.Assign -> {
                val value = evaluateExpr(expr.expr)

                val distance = locals.get(expr)
                if (distance != null) {
                    env.assignAt(distance, expr.name, value)
                } else {
                    globalEnv.assign(expr.name, value)
                }

                return value
            }
            is Expr.Call -> {
                val callee = evaluateExpr(expr.callee)
                val args = expr.args.map { evaluateExpr(it) }

                when (callee) {
                    is LoxCallable -> {
                        val calleeArity = callee.arity()
                        if (args.size == calleeArity) {
                            return callee.call(this, args)
                        } else {
                            throw RuntimeError(expr.paren, "Expected $calleeArity arguments, found ${args.size} instead")
                        }
                    }
                    null -> throw RuntimeError(expr.paren, "Cannot call nil")
                    else -> throw RuntimeError(expr.paren, "Cannot call object of type ${callee.javaClass.simpleName}")
                }
            }
            is Expr.AnonymousFunction -> {
                return LoxAnonymousFunction(expr, env) // TODO: Not use env here
            }
            is Expr.Get -> {
                val obj = evaluateExpr(expr.obj)
                if (obj is LoxInstance) {
                    return obj.get(expr.name)
                } else {
                    throw RuntimeError(expr.name, "Only instances have properties")
                }
            }
            is Expr.Set -> {
                val obj = evaluateExpr(expr.obj)
                if (obj is LoxInstance) {
                    return obj.set(expr.name.ident, evaluateExpr(expr.value))
                } else {
                    throw RuntimeError(expr.name, "Only instances have fields")
                }
            }
            is Expr.This -> return lookUpVariable(expr.keyword, expr)
            is Expr.Super -> {
                val distance = locals[expr]!!
                val superclass = env.getAt(distance, "super") as LoxClass
                val inst = env.getAt(distance - 1, "this") as LoxInstance // "this" is always stored in the environment within the super environment
                val method = superclass.findMethod(expr.method.ident)
                if (method != null) {
                    return method.bind(inst)
                } else {
                    throw RuntimeError(expr.keyword, "Undefined method '${expr.method.ident}'")
                }
            }
        }
    }

    /** `nil` and `false` are falsey, everything else is truthy */
    private fun isTruthy(value: Any?): Boolean = when (value) {
        null -> false
        is Boolean -> value
        else -> true
    }

    private fun isEqual(left: Any?, right: Any?): Boolean =
        if (left == null && right == null) {
            true
        } else if (left == null) {
            false
        } else {
            left.equals(right)
        }

    private fun checkNumberOperand(operator: Token, operand: Any?) {
        if (operand !is Double) {
            throw RuntimeError(operator, "Operand must be a number")
        }
    }

    private fun checkNumberOperands(operator: Token, left: Any?, right: Any?) {
        if (left !is Double || right !is Double) {
            throw RuntimeError(operator, "Operands must be numbers")
        }
    }

    private fun lookUpVariable(name: Token.Ident, expr: Expr): Any? {
        val distance = locals.get(expr)
        return if (distance != null) {
            env.getAt(distance, name.ident)
        } else {
            globalEnv.get(name)
        }
    }

    // HACK
    private fun lookUpVariable(tok: Token.This, expr: Expr): Any? =
        lookUpVariable(Token.Ident(tok.line, "this"), expr)

    fun stringify(value: Any?): String = when (value) {
        null -> "nil"
        is Double -> {
            var text = value.toString()
            if (text.endsWith(".0")) {
                text = text.substring(0, text.length - 2)
            }

            text
        }
        else -> value.toString()
    }
}