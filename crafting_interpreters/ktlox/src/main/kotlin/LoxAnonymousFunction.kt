class LoxAnonymousFunction(val declaration: Expr.AnonymousFunction, val closure: Environment) : LoxCallable {
    override fun call(interpreter: Interpreter, args: List<Any?>): Any? {
        val env = Environment(closure)

        // Add args to the environment
        for ((i, arg) in args.withIndex()) {
            env.define(declaration.params[i].ident, arg)
        }

        // Run code
        try {
            interpreter.executeBlock(declaration.body, env)
        } catch (ex: Interpreter.FunctionReturn) {
            return ex.value
        }

        // No explicit return statement was run
        // Implicitly return nil
        return null
    }

    override fun arity(): Int = declaration.params.size

    override fun toString(): String = "<anonymous fn>"
}