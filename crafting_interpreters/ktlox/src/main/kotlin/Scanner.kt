private fun isAlpha(char: Char): Boolean = char in 'a'..'z' || char in 'A'..'Z' || char == '_'
private fun isAlphanumeric(char: Char): Boolean = isAlpha(char) || char in '0'..'9'

class Scanner(val source: String) {
    private val tokens: MutableList<Token> = mutableListOf()

    // Keep track of where we are
    private var start: Int = 0 // First char of the lexeme being scanned
    private var current: Int = 0 // Current char of the lexeme being scanned
    private var line: UInt = 1u

    fun scanTokens(): List<Token> {
        while (!isAtEnd()) {
            // We're at the beginning of the next lexeme
            start = current
            scanToken()
        }

        tokens.add(Token.Eof(line))
        return tokens
    }

    private fun scanToken() {
        when (val char = advance()) {
            '(' -> tokens.add(Token.LeftParen(line))
            ')' -> tokens.add(Token.RightParen(line))
            '{' -> tokens.add(Token.LeftBrace(line))
            '}' -> tokens.add(Token.RightBrace(line))
            ',' -> tokens.add(Token.Comma(line))
            '.' -> tokens.add(Token.Dot(line))
            '-' -> tokens.add(Token.Minus(line))
            '+' -> tokens.add(Token.Plus(line))
            ';' -> tokens.add(Token.Semicolon(line))
            '*' -> tokens.add(Token.Star(line))
            '!' -> if (match('=')) {
                tokens.add(Token.BangEqual(line))
            } else {
                tokens.add(Token.Bang(line))
            }
            '=' -> if (match('=')) {
                tokens.add(Token.EqualEqual(line))
            } else {
                tokens.add(Token.Equal(line))
            }
            '<' -> if (match('=')) {
                tokens.add(Token.LessEqual(line))
            } else {
                tokens.add(Token.Less(line))
            }
            '>' -> if (match('=')) {
                tokens.add(Token.GreaterEqual(line))
            } else {
                tokens.add(Token.Greater(line))
            }
            '/' -> if (match('/')) {
                // A comment beginning with `//` goes until the end of the line
                while (peek() != '\n' && !isAtEnd()) advance()
            } else if (match('*')) {
                multilineComment()
            } else {
                tokens.add(Token.Slash(line))
            }
            '"' -> string()
            in '0'..'9' -> number()

            // Whitespace
            ' ', '\r', '\t' -> {}
            '\n' -> line++

            else -> {
                if (isAlpha(char)) {
                    identifierOrKeyword()
                } else {
                    Diagnostics.error(line, "Unexpected character '$char'")
                }
            }
        }
    }

    /** Multiline comment with /* blah */ */
    private fun multilineComment() {
        var depth = 1

        while (!isAtEnd()) {
            if (peek() == '\n') { // If our comment has line breaks, increment line counter
                line++
            } else if (peek() == '/' && peekNext() == '*') { // Comment start
                depth++
            } else if (peek() == '*' && peekNext() == '/') {  // Comment terminator
                depth--
            }

            if (depth == 0) break

            advance()
        }

        if (isAtEnd()) {
            Diagnostics.error(line, "Unterminated multiline comment")
            return
        }

        // Consume the closing */
        advance()
        advance()
    }

    private fun string() {
        while (peek() != '"' && !isAtEnd()) {
            if (peek() == '\n') line++ // If our string has line breaks, increment line counter
            advance()
        }

        if (isAtEnd()) {
            Diagnostics.error(line, "Unterminated string")
            return
        }

        // Consume the closing quote mark
        advance()

        // Trim off the quote marks
        val str = source.substring(start + 1, current - 1)

        tokens.add(Token.Str(line, str))
    }

    private fun identifierOrKeyword() {
        while (isAlphanumeric(peek())) advance()

        val token = when (val ident = source.substring(start, current)) {
            "and" -> Token.And(line)
            "class" -> Token.Class(line)
            "else" -> Token.Else(line)
            "false" -> Token.False(line)
            "for" -> Token.For(line)
            "fun" -> Token.Fun(line)
            "if" -> Token.If(line)
            "nil" -> Token.Nil(line)
            "or" -> Token.Or(line)
            "print" -> Token.Print(line)
            "return" -> Token.Return(line)
            "super" -> Token.Super(line)
            "this" -> Token.This(line)
            "true" -> Token.True(line)
            "var" -> Token.Var(line)
            "while" -> Token.While(line)
            "break" -> Token.Break(line)
            "mixin" -> Token.Mixin(line)
            else -> Token.Ident(line, ident)
        }
        tokens.add(token)
    }

    private fun number() {
        // Consume all digits
        while (peek() in '0'..'9') advance()

        // Optional fractional part
        if (peek() == '.' && peekNext() in '0'..'9') {
            advance() // Consume the dot

            // Consume all fractional digits
            while (peek() in '0'..'9') advance()
        }

        tokens.add(Token.Num(line, source.substring(start, current).toDouble()))
    }

    /** Consume and return the next char */
    private fun advance(): Char = source[current++]

    /** Consume the next char if it matches `expected` */
    private fun match(expected: Char): Boolean {
        if (isAtEnd()) return false
        if (source[current] != expected) return false

        current++
        return true
    }

    private fun peek(): Char = if (isAtEnd()) { 0.toChar() } else { source[current] }
    private fun peekNext(): Char = if (current + 1 >= source.length) { 0.toChar() } else { source[current + 1] }

    private fun isAtEnd(): Boolean = current >= source.length
}