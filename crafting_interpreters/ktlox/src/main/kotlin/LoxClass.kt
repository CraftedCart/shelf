class LoxClass(val name: String, val superclass: LoxClass?, private val instanceMethods: MutableMap<String, LoxFunction>, classFunctions: Map<String, LoxFunction>) : LoxInstance(null), LoxCallable {
    init {
        set("mix", object : LoxCallable {
            override fun call(interpreter: Interpreter, args: List<Any?>): Any? {
                val mixin = args[0]
                if (mixin !is LoxMixin) {
                    val token = Token.Ident(0u, "???") // TODO: Not use a fake token
                    throw Interpreter.RuntimeError(token, "Expected argument to be a mixin")
                }

                mixin.instanceMethods.forEach {
                    instanceMethods[it.key] = it.value
                }

                return null
            }

            override fun arity(): Int = 1

        })

        classFunctions.forEach { set(it.key, it.value) }
    }

    override fun call(interpreter: Interpreter, args: List<Any?>): Any {
        val inst = LoxInstance(this)

        // See if we have an initializer function to call
        val initializer = findMethod("init")
        initializer?.bind(inst)?.call(interpreter, args)

        return inst
    }

    override fun arity(): Int {
        // See if we have an initializer function
        val initializer = findMethod("init")
        return initializer?.arity() ?: 0
    }

    fun findMethod(name: String): LoxFunction? = instanceMethods.getOrElse(name) { superclass?.findMethod(name) }

    override fun toString(): String = "<class $name>"
}