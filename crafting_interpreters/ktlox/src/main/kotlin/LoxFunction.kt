class LoxFunction(private val declaration: Stmt.Function, private val closure: Environment, private val isInitializer: Boolean) : LoxCallable {
    override fun call(interpreter: Interpreter, args: List<Any?>): Any? {
        val env = Environment(closure)

        // Add args to the environment
        for ((i, arg) in args.withIndex()) {
            env.define(declaration.params[i].ident, arg)
        }

        // Run code
        try {
            interpreter.executeBlock(declaration.body, env)
        } catch (ex: Interpreter.FunctionReturn) {
            return if (isInitializer) {
                closure.getAt(0, "this")
            } else {
                ex.value
            }
        }

        // Special case in the book - not entirely sure why for now
        if (isInitializer) return closure.getAt(0, "this")

        // No explicit return statement was run
        // Implicitly return nil
        return null
    }

    override fun arity(): Int = declaration.params.size

    fun bind(inst: LoxInstance): LoxFunction {
        val env = Environment(closure)
        env.define("this", inst)
        return LoxFunction(declaration, env, isInitializer)
    }

    override fun toString(): String = "<fn ${declaration.name.ident}>"
}