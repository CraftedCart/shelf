import java.lang.RuntimeException
import kotlin.reflect.KClass

class Parser(val tokens: List<Token>) {
    private class ParseError : RuntimeException()

    class Function(val params: List<Token.Ident>, val body: List<Stmt>)

    var current: Int = 0

    /**
     * Used to tracking when it's valid to have `break` statements
     *
     * TODO: Does this break if we try do something like this?
     * ```
     * var someFunc;
     * for (var i = 0; i < 10; i = i + 1) {
     *     fun thingy() {
     *         break;
     *     }
     *     someFunc = thingy;
     * }
     *
     * someFunc();
     * ```
     */
    var loopDepth: UInt = 0u

    fun parse(): List<Stmt>? {
        return try {
            val statements = mutableListOf<Stmt>()
            while (!isAtEnd()) {
                ruleDeclaration()?.let { statements.add(it) }
            }
            statements
        } catch (error: ParseError) {
            null
        }
    }

    private fun ruleDeclaration(): Stmt? {
        return try {
            if (match<Token.Var>()) {
                ruleDeclareVar()
            } else if (match<Token.Fun>()) {
                ruleDeclareNamedFunction("function")
            } else if (match<Token.Class>()) {
                ruleDeclareClass()
            } else if (match<Token.Mixin>()) {
                ruleDeclareMixin()
            } else {
                ruleStatement()
            }
        } catch (error: ParseError) {
            synchronize()
            null
        }
    }

    private fun ruleDeclareVar(): Stmt.Var {
        val name = consume<Token.Ident>("Expected variable name")
        val initExpr = if (match<Token.Equal>()) {
            ruleExpression()
        } else {
            null
        }
        consume<Token.Semicolon>("Expected ';' after variable declaration")
        return Stmt.Var(name, initExpr)
    }

    private fun ruleDeclareNamedFunction(kind: String): Stmt.Function {
        val name = consume<Token.Ident>("Expected $kind name")
        val func = ruleDeclareFunction(kind)
        return Stmt.Function(name, func.params, func.body)
    }

    private fun ruleDeclareAnonymousFunction(funKeyword: Token): Expr.AnonymousFunction {
        val func = ruleDeclareFunction("anonymous function")
        return Expr.AnonymousFunction(funKeyword, func.params, func.body)
    }

    /** @param kind Function or method */
    private fun ruleDeclareFunction(kind: String): Function {
        // Parse parameter list
        consume<Token.LeftParen>("Expected '(' after $kind name")
        val params = mutableListOf<Token.Ident>()
        if (!check<Token.RightParen>()) { // Check for empty parameter list
            do {
                if (params.size >= 255) {
                    error(peek(), "Can't have more than 255 parameters")
                }

                params.add(consume<Token.Ident>("Expected parameter name"))
            } while (match<Token.Comma>())
        }
        consume<Token.RightParen>("Expected ')' after $kind parameters")

        // Parse body
        consume<Token.LeftBrace>("Expected '{' before $kind body")
        val body = ruleBlock()

        return Function(params, body)
    }

    private fun ruleDeclareClass(): Stmt.Class {
        val name = consume<Token.Ident>("Expected class name")

        val superclass = if (match<Token.Less>()) {
            Expr.Variable(consume<Token.Ident>("Expected superclass name"))
        } else {
            null
        }

        consume<Token.LeftBrace>("Expected '{' before class body")

        // Parse methods
        val instanceMethods = mutableListOf<Stmt.Function>()
        val classFunctions = mutableListOf<Stmt.Function>()
        while (!check<Token.RightBrace>() && !isAtEnd()) {
            if (match<Token.Class>()) {
                classFunctions.add(ruleDeclareNamedFunction("class function"))
            } else {
                instanceMethods.add(ruleDeclareNamedFunction("method"))
            }
        }

        consume<Token.RightBrace>("Expected '}' after class body")
        return Stmt.Class(name, superclass, instanceMethods, classFunctions)
    }

    private fun ruleDeclareMixin(): Stmt.Mixin {
        val name = consume<Token.Ident>("Expected mixin name")
        consume<Token.LeftBrace>("Expected '{' after mixin name")

        // Parse methods
        val instanceMethods = mutableListOf<Stmt.Function>()
        while (!check<Token.RightBrace>() && !isAtEnd()) {
            instanceMethods.add(ruleDeclareNamedFunction("method"))
        }

        consume<Token.RightBrace>("Expected '}' after mixin body")
        return Stmt.Mixin(name, instanceMethods)
    }

    private fun ruleStatement(): Stmt =
        if (match<Token.If>()) {
            ruleStatementIf()
        } else if (match<Token.While>()) {
            ruleStatementWhile()
        } else if (match<Token.For>()) {
            ruleStatementFor()
        } else if (match<Token.Break>()) {
            ruleStatementBreak()
        } else if (match<Token.Return>()) {
            ruleStatementReturn()
        } else if (match<Token.Print>()) {
            ruleStatementPrint()
        } else if (match<Token.LeftBrace>()) {
            Stmt.Block(ruleBlock())
        } else {
            ruleStatementExpr()
        }

    private fun ruleStatementIf(): Stmt {
        consume<Token.LeftParen>("Expected '(' after 'if'")
        val condition = ruleExpression()
        consume<Token.RightParen>("Expected ')' after 'if' condition")

        val thenBranch = ruleStatement()
        var elseBranch: Stmt? = null
        if (match<Token.Else>()) {
            elseBranch = ruleStatement()
        }

        return Stmt.If(condition, thenBranch, elseBranch)
    }

    private fun ruleStatementWhile(): Stmt {
        consume<Token.LeftParen>("Expected '(' after 'while'")
        val condition = ruleExpression()
        consume<Token.RightParen>("Expected ')' after 'while' condition")

        loopDepth++
        val body = ruleStatement()
        loopDepth--

        return Stmt.While(condition, body)
    }

    /** For loops don't have their own nodes in the AST - they are desugared to while loops instead */
    private fun ruleStatementFor(): Stmt {
        consume<Token.LeftParen>("Expected '(' after 'for'")

        val initializer = if (match<Token.Semicolon>()) {
            null
        } else if (match<Token.Var>()) {
            ruleDeclareVar()
        } else {
            ruleStatementExpr()
        }

        var condition = if (check<Token.Semicolon>()) {
            null
        } else {
            ruleExpression()
        }
        consume<Token.Semicolon>("Expected ';' after 'for' loop condition")

        val increment = if (check<Token.RightParen>()) {
            null
        } else {
            ruleExpression()
        }
        consume<Token.RightParen>("Expected ')' after 'for' loop clauses")

        loopDepth++
        val body = ruleStatement()
        loopDepth--

        var out = body

        // If we have an increment, put it after the body
        if (increment != null) {
            out = Stmt.Block(listOf(body, Stmt.Expression(increment)))
        }

        // Make the while loop
        if (condition == null) condition = Expr.Literal(true) // Missing condition means always loop forever
        out = Stmt.While(condition, out)

        // If we have an initializer, wrap the whole thing up in a block that runs the initializer once before we enter
        // the loop
        if (initializer != null) {
            out = Stmt.Block(listOf(initializer, out))
        }

        return out
    }

    private fun ruleStatementBreak(): Stmt {
        if (loopDepth > 0u) {
            consume<Token.Semicolon>("Expected ';' after 'for'");
            return Stmt.Break
        } else {
            // TODO: Probably don't need to throw here and cause a synchronization?
            //       But I'm lazy and can't be bothered to make all these nullable
            //       (Maybe could make a no-op statement and return it here?)
            throw error(previous(), "Cannot use 'break' outside of a loop")
        }
    }

    private fun ruleStatementReturn(): Stmt {
        val keyword = previous()

        val expr = if (check<Token.Semicolon>()) {
            null
        } else {
            ruleExpression()
        }
        consume<Token.Semicolon>("Expected ';' after return value");

        return Stmt.Return(keyword, expr)
    }

    private fun ruleStatementPrint(): Stmt {
        // Already matched and consumed the "print" token - no need to consume it here
        val expr = ruleExpression()
        consume(Token.Semicolon::class, "Expected ';' after value")
        return Stmt.Print(expr)
    }

    private fun ruleBlock(): List<Stmt> {
        val stmts = mutableListOf<Stmt>()
        while (!check<Token.RightBrace>() && !isAtEnd()) {
            ruleDeclaration()?.let { stmts.add(it) }
        }
        consume<Token.RightBrace>("Expected '}' after block")
        return stmts
    }

    private fun ruleStatementExpr(): Stmt {
        val expr = ruleExpression()
        consume(Token.Semicolon::class, "Expected ';' after expression")
        return Stmt.Expression(expr)
    }

    private fun ruleExpression(): Expr = ruleAssignment()

    private fun ruleAssignment(): Expr {
        val expr = ruleLogicOr()

        // If we find an equals sign, assume everything on the left was either a variable, or a property getter
        // And rewrite the expression into assignment/set
        if (match<Token.Equal>()) {
            val equals = previous()
            val valueExpr = ruleAssignment()

            when (expr) {
                is Expr.Variable -> return Expr.Assign(expr.name, valueExpr)
                is Expr.Get -> return Expr.Set(expr.obj, expr.name, valueExpr)
                else -> {
                    // Report but don't throw the error, since we're not confused and don't need to go into panic-mode and
                    // synchronize
                    error(equals, "Invalid assignment target")
                }
            }
        }

        return expr
    }

    private fun ruleLogicOr(): Expr {
        var expr = ruleLogicAnd()

        while (match<Token.Or>()) {
            val or = previous()
            val right = ruleLogicAnd()
            expr = Expr.Logical(expr, or, right)
        }

        return expr
    }

    private fun ruleLogicAnd(): Expr {
        var expr = ruleEquality()

        while (match<Token.And>()) {
            val and = previous()
            val right = ruleEquality()
            expr = Expr.Logical(expr, and, right)
        }

        return expr
    }

    private fun ruleEquality(): Expr {
        var expr = ruleComparison()

        while (match(Token.BangEqual::class, Token.EqualEqual::class)) {
            val operator = previous()
            val right = ruleComparison()
            expr = Expr.Binary(expr, operator, right)
        }

        return expr
    }

    private fun ruleComparison(): Expr {
        var expr = ruleTerm()

        while (match(Token.Greater::class, Token.GreaterEqual::class, Token.Less::class, Token.LessEqual::class)) {
            val operator = previous()
            val right = ruleTerm()
            expr = Expr.Binary(expr, operator, right)
        }

        return expr
    }

    private fun ruleTerm(): Expr {
        var expr = ruleFactor()

        while (match(Token.Minus::class, Token.Plus::class)) {
            val operator = previous()
            val right = ruleFactor()
            expr = Expr.Binary(expr, operator, right)
        }

        return expr
    }

    private fun ruleFactor(): Expr {
        var expr = ruleUnary()

        while (match(Token.Slash::class, Token.Star::class)) {
            val operator = previous()
            val right = ruleUnary()
            expr = Expr.Binary(expr, operator, right)
        }

        return expr
    }

    private fun ruleUnary(): Expr {
        return if (match(Token.Bang::class, Token.Minus::class)) {
            val operator = previous()
            val right = ruleUnary()
            Expr.Unary(operator, right)
        } else {
            ruleCall()
        }
    }

    private fun ruleCall(): Expr {
        var expr = rulePrimary()

        while (true) {
            expr = if (match<Token.LeftParen>()) {
                finishCall(expr)
            } else if (match<Token.Dot>()) {
                val name = consume<Token.Ident>("Expected property name after '.'")
                Expr.Get(expr, name)
            } else {
                break;
            }
        }

        return expr
    }

    private fun finishCall(callee: Expr): Expr {
        val args = mutableListOf<Expr>()
        if (!check<Token.RightParen>()) {
            do {
                // While we don't really need an arg limit, we'll report an error here since the C interpreter will do
                // And we want to maintain compatability
                if (args.size >= 255) {
                    error(peek(), "Can't have more than 255 arguments")
                }

                args.add(ruleExpression())
            } while (match<Token.Comma>())
        }

        val paren = consume<Token.RightParen>("Expected ')' after function call arguments")
        return Expr.Call(callee, paren, args)
    }

    private fun rulePrimary(): Expr {
        if (match(Token.False::class)) {
            return Expr.Literal(false)
        } else if (match(Token.True::class)) {
            return Expr.Literal(true)
        } else if (match(Token.Nil::class)) {
            return Expr.Literal(null)
        } else if (match(Token.Num::class)) {
            return Expr.Literal((previous() as Token.Num).num)
        } else if (match(Token.Str::class)) {
            return Expr.Literal((previous() as Token.Str).str)
        } else if (match(Token.LeftParen::class)) {
            val expr = ruleExpression()
            consume(Token.RightParen::class, "Expected ')' after expression")
            return Expr.Grouping(expr)
        } else if (match(Token.Ident::class)) {
            return Expr.Variable(previous() as Token.Ident)
        } else if (match(Token.Fun::class)) {
            return ruleDeclareAnonymousFunction(previous())
        } else if (match(Token.This::class)) {
            return Expr.This(previous() as Token.This)
        } else if (match(Token.Super::class)) {
            val keyword = previous() as Token.Super;
            consume<Token.Dot>("Expected '.' after 'super'")
            val method = consume<Token.Ident>("Expected superclass method name");
            return Expr.Super(keyword, method)
        } else {
            throw error(peek(), "Expected expression, found ${peek()}")
        }
    }

    /** Returns whether the current token is of the given type. If so, consume it. */
    private fun match(vararg tokenTypes: KClass<out Token>): Boolean {
        for (type in tokenTypes) {
            if (check(type)) {
                advance()
                return true
            }
        }

        // No match
        return false
    }

    private inline fun <reified T: Token> match(): Boolean = match(T::class)

    /** Returns whether the current token is of the given type */
    private fun check(tokenType: KClass<out Token>): Boolean {
        if (isAtEnd()) return false

        return peek().javaClass == tokenType.java
    }

    private inline fun <reified T: Token> check(): Boolean = check(T::class)

    /** Consumes the next token if it's of the given token type, otherwise throws an error */
    private fun consume(tokenType: KClass<out Token>, message: String): Token {
        if (check(tokenType)) return advance()

        throw error(peek(), message);
    }

    private inline fun <reified T: Token> consume(message: String): T = consume(T::class, message) as T

    private fun error(token: Token, message: String): ParseError {
        Diagnostics.error(token, message)
        return ParseError()
    }

    private fun advance(): Token {
        if (!isAtEnd()) current++
        return previous()
    }

    private fun peek(): Token = tokens[current]
    private fun previous(): Token = tokens[current - 1]
    private fun isAtEnd(): Boolean = peek() is Token.Eof

    /**
     * When we encounter a syntax error, discard tokens until we get to a synchronization point where we think it's safe
     * to start parsing again.
     */
    private fun synchronize() {
        advance()

        while (!isAtEnd()) {
            if (previous() is Token.Semicolon) return
            when (peek()) {
                is Token.Class,
                is Token.Fun,
                is Token.Var,
                is Token.For,
                is Token.If,
                is Token.While,
                is Token.Print,
                is Token.Return -> return
                else -> {}
            }

            advance()
        }
    }
}