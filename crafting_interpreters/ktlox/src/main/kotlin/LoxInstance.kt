open class LoxInstance(private val cls: LoxClass?) {
    private val fields: MutableMap<String, Any?> = mutableMapOf()

    /** Fields shadow methods, since we look for fields before we look for methods */
    fun get(name: Token.Ident): Any? = fields.getOrElse(name.ident) {
        if (cls != null) {
            val method = cls.findMethod(name.ident)
            return method?.bind(this)
        }

        // Not found
        throw Interpreter.RuntimeError(name, "Undefined property '${name.ident}'")
    }

    fun set(name: String, value: Any?) {
        fields[name] = value
    }

    override fun toString(): String = if (cls != null) {
        "<${cls.name}>"
    } else {
        // Shouldn't be reachable, since the only place we make a LoxInstance with a null class is in LoxClass
        // and LoxClass overrides toString
        "<?>"
    }
}