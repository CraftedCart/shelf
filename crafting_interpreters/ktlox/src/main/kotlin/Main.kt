import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.system.exitProcess

fun main(args: Array<String>) {
    if (args.size > 1) {
        System.err.println("Usage: ktlox [script]")
        exitProcess(64)
    } else if (args.size == 1) {
        runFile(Paths.get(args[0]))
    } else {
        runPrompt()
    }
}

private fun runFile(path: Path) {
    val bytes = Files.readAllBytes(path)
    run(Interpreter(), String(bytes, Charset.defaultCharset()))

    // Indicate if we errored in our exit code
    if (Diagnostics.hadError) {
        exitProcess(65)
    } else if (Diagnostics.hadRuntimeError) {
        exitProcess(70)
    }
}

private fun runPrompt() {
    val interpreter = Interpreter()
    while (true) {
        print("> ")

        val line = readlnOrNull() ?: break
        run(interpreter, line)

        Diagnostics.hadError = false
    }
}

private fun run(interpreter: Interpreter, source: String) {
    val scanner = Scanner(source)
    val tokens = scanner.scanTokens()

    val parser = Parser(tokens)
    val stmts = parser.parse()

    if (Diagnostics.hadError) return

    val resolver = Resolver(interpreter)
    resolver.resolveStatements(stmts!!)

    if (Diagnostics.hadError) return

    interpreter.interpret(stmts)
}

object Diagnostics {
    var hadError = false
    var hadRuntimeError = false

    fun error(line: UInt, message: String) {
        reportError(line, "", message)
    }

    fun error(token: Token, message: String) {
        if (token is Token.Eof) {
            reportError(token.line, "at end", message)
        } else {
            // TODO: Storing the original user text would make prettier output instead of doing token.toString() here
            reportError(token.line, "at $token", message)
        }
    }

    fun warn(token: Token, message: String) {
        if (token is Token.Eof) {
            reportWarn(token.line, "at end", message)
        } else {
            // TODO: Storing the original user text would make prettier output instead of doing token.toString() here
            reportWarn(token.line, "at $token", message)
        }
    }

    fun runtimeError(error: Interpreter.RuntimeError) {
        System.err.println("${error.message}\n[line ${error.token.line}]")
        hadRuntimeError = true
    }

    fun reportError(line: UInt, where: String, message: String) {
        System.err.println("[line $line] Error $where: $message")
        hadError = true
    }

    fun reportWarn(line: UInt, where: String, message: String) {
        System.err.println("[line $line] Warning $where: $message")
    }
}