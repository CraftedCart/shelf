sealed class Token(val line: UInt) {
    class LeftParen(line: UInt) : Token(line)
    class RightParen(line: UInt) : Token(line)
    class LeftBrace(line: UInt) : Token(line)
    class RightBrace(line: UInt) : Token(line)
    class Comma(line: UInt) : Token(line)
    class Dot(line: UInt) : Token(line)
    class Minus(line: UInt) : Token(line)
    class Plus(line: UInt) : Token(line)
    class Semicolon(line: UInt) : Token(line)
    class Slash(line: UInt) : Token(line)
    class Star(line: UInt) : Token(line)

    class Bang(line: UInt) : Token(line)
    class BangEqual(line: UInt) : Token(line)
    class Equal(line: UInt) : Token(line)
    class EqualEqual(line: UInt) : Token(line)
    class Greater(line: UInt) : Token(line)
    class GreaterEqual(line: UInt) : Token(line)
    class Less(line: UInt) : Token(line)
    class LessEqual(line: UInt) : Token(line)

    // Literals
    class Ident(line: UInt, val ident: String) : Token(line) {
        override fun toString(): String {
            return "${this::class.simpleName}($ident)"
        }
    }
    class Str(line: UInt, val str: String) : Token(line) {
        override fun toString(): String {
            return "${this::class.simpleName}($str)"
        }
    }
    class Num(line: UInt, val num: Double) : Token(line) {
        override fun toString(): String {
            return "${this::class.simpleName}($num)"
        }
    }

    // Keywords
    class And(line: UInt) : Token(line)
    class Class(line: UInt) : Token(line)
    class Else(line: UInt) : Token(line)
    class False(line: UInt) : Token(line)
    class Fun(line: UInt) : Token(line)
    class For(line: UInt) : Token(line)
    class If(line: UInt) : Token(line)
    class Nil(line: UInt) : Token(line)
    class Or(line: UInt) : Token(line)
    class Print(line: UInt) : Token(line)
    class Return(line: UInt) : Token(line)
    class Super(line: UInt) : Token(line)
    class This(line: UInt) : Token(line)
    class True(line: UInt) : Token(line)
    class Var(line: UInt) : Token(line)
    class While(line: UInt) : Token(line)
    class Break(line: UInt) : Token(line) // Control flow challenge
    class Mixin(line: UInt) : Token(line) // Reuse functions across classes challenge

    class Eof(line: UInt) : Token(line)

    override fun toString(): String {
        return "${this::class.simpleName}"
    }
}