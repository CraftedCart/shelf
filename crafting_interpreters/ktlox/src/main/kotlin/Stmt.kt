sealed class Stmt {
    class Expression(val expr: Expr) : Stmt()
    class Print(val expr: Expr) : Stmt()
    class Var(val name: Token.Ident, val initializer: Expr?) : Stmt()
    class Block(val stmts: List<Stmt>) : Stmt()
    class If(val condition: Expr, val thenBranch: Stmt, val elseBranch: Stmt?) : Stmt()
    class While(val condition: Expr, val body: Stmt) : Stmt()
    object Break : Stmt()
    class Function(val name: Token.Ident, val params: List<Token.Ident>, val body: List<Stmt>) : Stmt()
    class Return(val keyword: Token, val expr: Expr?) : Stmt() // Keyword is here for runtime error reporting
    class Class(val name: Token.Ident, val superclass: Expr.Variable?, val instanceMethods: List<Function>, val classFunctions: List<Function>) : Stmt()
    class Mixin(val name: Token.Ident, val instanceMethods: List<Function>) : Stmt()
}