(defproject cljlox "0.1.0-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [orchestra "2021.01.01-1"]]
  :profiles {:dev {:dependencies [[org.clojure/test.check "0.9.0"]]}}
  :main cljlox.core
  :aot :all)
