;;;; Sum types

(ns cljlox.sum)

(defn- def-sum-constructors [current-ctors type-name variants]
  (if (some? variants)
    (let [variant-name (first (first variants))
          variant-args (rest (first variants))]
      (recur
        (cons
          `(defn
             ~(symbol (str type-name variant-name)) ; Ctor function name
             [~@variant-args] ; Ctor args
             (hash-map ~@(flatten (map #(list (keyword %) %) variant-args))))
          current-ctors)
        type-name
        (next variants)))
    current-ctors))

(defmacro def-sum [type-name & variants]
  `(do
     ;; Define something storing info about the sum type
     (def ~type-name (list ~@variants))

     ;; Define constructors for each variant
     ~@(def-sum-constructors '() type-name variants)))

(defmacro match [value & clauses]
  (when (some? clauses)
    (let [clause-variant (first (first clauses)) ; The sum-type variant we're looking for
          clause-pattern (second (first clauses)) ; Optional destructuring pattern
          clause-body (second clauses)] ; Code to run if this variant matches
      `(if (= (::variant ~value) ~clause-variant)
         ~(if (some? clause-pattern)
            `(let [~clause-pattern value] ~clause-body)
            clause-body)
         (match ~value ~@(next (next clauses)))))))

(comment
  (macroexpand
    '(def-sum token
       [:l-paren]
       [:r-paren]
       [:string value]))

  (use 'clojure.walk)
  (clojure.walk/macroexpand-all
    '(match mrowl
            [:l-paren] 1
            [:r-paren] 2
            [:string { value :value }] 3))

  (pst))
