(ns cljlox.scan
  (:require [clojure.spec.alpha :as s]
            [orchestra.core :refer [defn-spec]]
            [orchestra.spec.test :as st]))

(defn-spec string->tokens list?
  "Convert text into a list of tokens"
  [text string?]
  '())

(comment
  (st/instrument)
  (string->tokens "mrowl"))
