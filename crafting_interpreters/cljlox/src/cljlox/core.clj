(ns cljlox.core
  (:gen-class)
  (:require [cljlox.scan :as scan]
            [clojure.spec.alpha :as s]
            [orchestra.core :refer [defn-spec]]
            [orchestra.spec.test :as st]))

(defn- report-error
  "Show an error message to the user"
  [source line message]
  (binding [*out* *err*]
    (print source)
    (print ":")
    (print line)
    (print ": error: ")
    (println message)))


(s/def ::has-error boolean?)
(s/def ::run-result (s/keys :req-un [::has-error]))
(defn-spec run ::run-result ; TODO: Make private? (defn-spec doesn't have a defn-spec- variant)
  "Execute some Lox code"
  [text string?]
  (prn (scan/string->tokens text)) ; Just print tokens for now

  { :has-error false})

(defn- run-file [path]
  (run (slurp path)))

(defn- prompt-for-input
  "Prompt the user for input - returns the user's string, or nil on EOF"
  []
  (print "> ")
  (flush)
  (read-line))

(defn- run-prompt []
  (loop [input (prompt-for-input)]
    (when (some? input)
      (run input)
      (recur (prompt-for-input)))))

(defn -main [& args]
  (let [num-args (count args)]
    (cond
      (> num-args 1) (do
                       (println "Usage: cljlox [script]")
                       (System/exit 64))
      (= num-args 1) (run-file (first args))
      :else (run-prompt))))

(comment
  (st/instrument)
  (run "mrowl"))
