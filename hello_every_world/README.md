hello_every_world
=================

Wouldn't it be amusing if you could just get a hello-world program for whatever language you like, just from
StackOverflow? Well, that's what I thought anyway at some point in time.

```sh
$ ./run.py ruby
Taken from https://stackoverflow.com/questions/705729

puts "Hello World"
```

It... doesn't tend to work very well...

```sh
$ ./run.py python
Taken from https://stackoverflow.com/questions/50089690

python app.py

$ ./run.py c
Taken from https://stackoverflow.com/questions/5325326

        .file   "test.c"

$ ./run.py lua
Taken from https://stackoverflow.com/questions/45202329

lua C:\Users\James\Documents\my_lua\test0.lua
```

...maybe I should have pulled from [this question](https://codegolf.stackexchange.com/questions/55422/hello-world)
instead of all of SO, huh.
