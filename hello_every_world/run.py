#!/usr/bin/env python3

import urllib.request
from urllib.request import Request
import urllib.parse
import gzip
import json
import sys


def main():
    # /2.2/search?order=desc&max=1&sort=votes&intitle=ruby hello world&site=stackoverflow&filter=)E_e*yRSgs8K0-_k76x

    escaped_lang = urllib.parse.quote(" ".join(sys.argv[1:]))

    req = Request(
        "https://api.stackexchange.com/2.2/search?order=desc&sort=votes&intitle=" +
        escaped_lang +
        "%20hello%20world&site=stackoverflow&filter=)E_e*yRSgs8K0-_k76x"
        )

    contents = gzip.decompress(urllib.request.urlopen(req).read()).decode("utf-8")
    results = json.loads(contents)

    if len(results["items"]) == 0:
        print("No questions found")
        sys.exit(1)

    if len(results["items"][0]["answers"]) == 0:
        print("No answers to top question found")
        sys.exit(1)

    # print(results["items"][0]["question_id"])
    print("Taken from https://stackoverflow.com/questions/" + str(results["items"][0]["question_id"]))
    print()

    top_answer_md = ""
    top_answer_score = -99999999
    for answer in results["items"][0]["answers"]:
        if answer["score"] > top_answer_score:
            top_answer_score = answer["score"]
            top_answer_md = answer["body_markdown"]

    lines = top_answer_md.splitlines(False)

    was_previous_line_code = False
    for line in lines:
        if line.startswith("    ") and not line.isspace():
            was_previous_line_code = True
            print(line[4:])
        elif was_previous_line_code and not line.isspace():
            return

    if not was_previous_line_code:
        print("No code blocks found in top answer")


if __name__ == "__main__":
    main()
