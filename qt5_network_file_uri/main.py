#!/usr/bin/env python3

from PySide2.QtCore import QCoreApplication, QUrl
from PySide2.QtNetwork import QNetworkAccessManager, QNetworkRequest
import sys


app = None
req = None
reply = None


def main():
    global app
    global req
    global reply

    app = QCoreApplication(sys.argv)

    net = QNetworkAccessManager()

    req = QNetworkRequest(QUrl.fromLocalFile(__file__))
    reply = net.get(req)
    reply.finished.connect(on_finish)

    app.exec_()


def on_finish():
    print(f"Finished!! Error: {reply.error()}")
    print("Content")
    print("=======")
    print(str(reply.readAll()))

    app.quit()


if __name__ == "__main__":
    main()
