qt5\_network\_file\_uri
=======================

I wanted to see if `file://` URIs worked with `QNetworkAccessManager::get`. The answer: yes!
