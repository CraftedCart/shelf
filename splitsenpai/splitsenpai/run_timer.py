import asyncio
import time

from splitsenpai.wired import Wired


class RunTimer(Wired):
    def __init__(self):
        self.timer_start_point: float = None

    async def event_loop(self) -> None:
        while True:
            self.update_server()
            await asyncio.sleep(0.016)

    def start_timer(self) -> None:
        self.timer_start_point = time.time()

    def stop_timer(self) -> None:
        pass  # TODO

    def get_time_elapsed(self) -> float:
        if self.timer_start_point is None:
            return 0
        else:
            return time.time() - self.timer_start_point

    def update_server(self) -> None:
        self.wired_signal_emit(self)
