import json
import logging
import os
from typing import List

import websockets

import aiohttp_jinja2
import jinja2
from aiohttp import web

from splitsenpai.run_timer import RunTimer
from splitsenpai.webserver import views
from splitsenpai.webserver.websocket_connection import WebsocketConnection
from splitsenpai.wired import Wired

_logger = logging.getLogger(__name__)


class WebServer(Wired):
    def __init__(self):
        self.app = web.Application()
        self.runner = web.AppRunner(self.app)
        self.site = None

        self.websocket_connections: List[WebsocketConnection] = []

        # Setup jinja2 templates
        env = aiohttp_jinja2.setup(self.app, loader=jinja2.PackageLoader("splitsenpai.webserver", "templates"))

        # Setup static files
        self.app.router.add_static("/static/",
                                   path=os.path.dirname(__file__) + "/static",
                                   name="static")

        # Handle error pages (404, 500, etc.)
        error_middleware = self._error_pages({
            404: views.error_404,
            500: views.error_500,
            "generic": views.error_generic
        })

        self.app.middlewares.append(error_middleware)

        self.app.add_routes([
            web.get("/", views.index)
        ])

    @staticmethod
    def _error_pages(overrides):
        async def middleware(app, handler):
            async def middleware_handler(request):
                try:
                    response = await handler(request)
                    override = overrides.get(response.status)
                    if override is None:
                        return response
                    else:
                        return await override(request, response)
                except web.HTTPException as ex:
                    override = overrides.get(ex.status)
                    if override is None:
                        return await overrides["generic"](request, ex)
                    else:
                        return await override(request, ex)
                except Exception as ex:
                    override = overrides.get(500)
                    if override is None:
                        return await overrides["generic"](request, None)
                    else:
                        return await override(request, ex)

            return middleware_handler

        return middleware

    # Runs per client connection
    async def _websocket_server(self, websocket, path):
        conn = WebsocketConnection(websocket)
        conn.wired_connect_async(WebsocketConnection.on_recv_message, self.on_recv_message)
        self.websocket_connections.append(conn)
        try:
            await conn.event_loop()
        finally:
            self.websocket_connections.remove(conn)

    async def start_server(self):
        # Start HTTP server
        await self.runner.setup()
        self.site = web.TCPSite(self.runner, "0.0.0.0", 8080)
        await self.site.start()

        _logger.info("Web server is up and running at http://0.0.0.0:8080")

        # Start websocket server
        await websockets.serve(self._websocket_server, "0.0.0.0", 8765)

    def send_websocket_message(self, message: str):
        for conn in self.websocket_connections:
            conn.enqueue_message(message)

    async def on_recv_message(self, cmd: str):
        self.wired_signal_emit(cmd)

    async def recv_timer_update(self, timer: RunTimer):
        # Construct our message
        message = {
            "timer": {
                "elapsed": timer.get_time_elapsed()
            }
        }

        message_str = json.dumps(message)

        for conn in self.websocket_connections:
            await conn.enqueue_message(message_str)
