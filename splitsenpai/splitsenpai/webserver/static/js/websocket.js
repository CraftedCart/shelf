// splitsenpai namespace
(function(splitsenpai, undefined) {
    var socket = new WebSocket("ws://" + window.location.hostname + ":8765", "json");

    splitsenpai.setupWebsocket = function() {
        socket.onopen = function(event) {
            var notif = document.getElementById("websocket-connection-lost-notif");
            notif.style.display = "none";
        };

        socket.onmessage = function(event) {
//            console.log(event.data);

            parsed = JSON.parse(event.data);
            Object.keys(parsed).forEach(function(key) {
                value = parsed[key];

                if (key === "timer") {
                    onTimerUpdate(value);
                }
            });
        };

        socket.onclose = function(event) {
            console.log("Socket closed");

            var notif = document.getElementById("websocket-connection-lost-notif");
            notif.style.display = "block";

            // Retry for a connection after 5s
            setTimeout(function() {
                socket = new WebSocket("ws://" + window.location.hostname + ":8765", "json");
                return splitsenpai.setupWebsocket();
            }, 5000);
        };
    }

    splitsenpai.startTimer = function() {
        socket.send('{"commands": ["start_timer"]}');
    }

    function toHHMMSSMMM(seconds) {
        var secNum = seconds;
        var hours   = Math.floor(secNum / 3600);
        var minutes = Math.floor((secNum - (hours * 3600)) / 60);
        var seconds = secNum - (hours * 3600) - (minutes * 60);

        if (hours < 10) {hours = "0" + hours;}
        if (minutes < 10) {minutes = "0" + minutes;}
        if (seconds < 10) {seconds = "0" + seconds.toFixed(3);} else {seconds = seconds.toFixed(3);}
        return hours + ":" + minutes + ":" + seconds;
    }

    function onTimerUpdate(message) {
        var timerElement = document.getElementById("timer-elapsed");
        timerElement.innerHTML = toHHMMSSMMM(message.elapsed)
    }
}(window.splitsenpai = window.splitsenpai || {}));

splitsenpai.setupWebsocket();
