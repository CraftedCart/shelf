import logging

import aiohttp_jinja2
from aiohttp import web

_logger = logging.getLogger(__name__)


@aiohttp_jinja2.template("index.html")
async def index(request):
    pass


async def error_404(request, ex: web.HTTPException):
    # Log the exception
    _logger.exception("404 Page not found")

    return aiohttp_jinja2.render_template("404.html", request, {})


async def error_500(request, ex: Exception):
    # Log the exception
    _logger.exception("500 Internal server error")

    return aiohttp_jinja2.render_template("500.html", request, {})


async def error_generic(request, ex: Exception):
    if isinstance(ex, web.HTTPException):
        # Log the exception
        _logger.exception("{status} ???".format(status=ex.status))

        return aiohttp_jinja2.render_template("generic_error.html", request, {
            "status_code": ex.status
        })
    else:
        # Log the exception
        _logger.exception("500 Internal server error")

        return aiohttp_jinja2.render_template("generic_error.html", request, {
            "status_code": 500
        })
