import asyncio
import json

from websockets import WebSocketServerProtocol

from splitsenpai.wired import Wired


class WebsocketConnection(Wired):
    def __init__(self, websocket: WebSocketServerProtocol):
        self.websocket = websocket
        self.message_queue = asyncio.Queue()

    async def send_loop(self):
        while True:
            item = await self.message_queue.get()
            await self.websocket.send(item)

    async def recv_loop(self):
        while True:
            data = json.loads(await self.websocket.recv())
            if "commands" in data:
                for cmd in data["commands"]:
                    await self.on_recv_message(cmd)

    async def event_loop(self):
        consumer_task = asyncio.ensure_future(self.recv_loop())
        producer_task = asyncio.ensure_future(self.send_loop())
        done, pending = await asyncio.wait(
            [consumer_task, producer_task],
            return_when=asyncio.FIRST_COMPLETED,
        )
        for task in pending:
            task.cancel()

    async def enqueue_message(self, message: str):
        await self.message_queue.put(message)

    async def on_recv_message(self, cmd: str):
        self.wired_signal_emit(cmd)
