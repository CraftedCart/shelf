import logging
from typing import BinaryIO

_logger = logging.getLogger(__name__)


class LinuxMemoryAccessor:
    def __init__(self, pid: int):
        self.pid = pid
        self.emulator_memory_address = None
        self.memory_file: BinaryIO = None

    def get_memory_info(self):
        proc_maps = open("/proc/{}/maps".format(self.pid), "r")
        maps = proc_maps.readlines()
        proc_maps.close()

        for line in maps:
            split = line.split(" ")

            if "/dev/shm/dolphin-emu" in line or "/dev/shm/dolphinmem" in line:
                split_address = split[0].split("-")

                start_address = int(split_address[0], 16)
                end_address = int(split_address[1], 16)

                if end_address - start_address == 0x2000000:
                    # We found our address
                    _logger.info("Found emulator memory map: {}".format(line[:-1]))  # We remove the ending \n in "line"
                    self.emulator_memory_address = start_address
                    break

    def open_memory(self):
        """
        Open the memory file to be ready to read from
        """

        try:
            self.memory_file = open("/proc/{}/mem".format(self.pid), "rb")
        except PermissionError as e:
            _logger.error("###########################################################################################")
            _logger.error("#                                                                                         #")
            _logger.error("#     #      Failed to open process memory file - ensure that we have permission to do so #")
            _logger.error("#    # #     You can allow processes to attach to other processes by running              #")
            _logger.error("#   # | #                                                                                 #")
            _logger.error("#  #  |  #       echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope                     #")
            _logger.error("# #   .   #                                                                               #")
            _logger.error("# #########  Echo 1 afterwards to disable this                                            #")
            _logger.error("#                                                                                         #")
            _logger.error("###########################################################################################")

            raise e

    def close_memory(self):
        """
        Close the memory file
        """

        self.memory_file.close()
        self.memory_file = None

    def read_memory(self, offset: int, size: int):
        assert self.memory_file is not None

        self.memory_file.seek(self.emulator_memory_address + (offset & 0x7FFFFFFF))
        return self.memory_file.read(size)
