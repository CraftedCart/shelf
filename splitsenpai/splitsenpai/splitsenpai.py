import asyncio
import logging
import struct

from splitsenpai.memory.linux_memory_accessor import LinuxMemoryAccessor
from splitsenpai.run_timer import RunTimer
from splitsenpai.webserver.server import WebServer

memory_accessor = None


def main():
    global memory_accessor

    logger = logging.getLogger(__name__)
    logging.basicConfig(level=logging.INFO)

    logger.info("splitsenpai started")

    # REPLACE THIS NUMBER WITH THE `pidof dolphin-emu`
    memory_accessor = LinuxMemoryAccessor(29365)
    memory_accessor.get_memory_info()
    memory_accessor.open_memory()

    web_server = WebServer()

    timer = RunTimer()
    # timer.start_timer()

    def command_processor(cmd: str):
        if cmd == "start_timer":
            timer.start_timer()

    web_server.wired_connect(WebServer.on_recv_message, command_processor)

    timer.wired_connect_async(RunTimer.update_server, web_server.recv_timer_update)

    run_commands = [
        loop(),
        timer.event_loop(),
        web_server.start_server()
    ]

    asyncio.get_event_loop().run_until_complete(asyncio.wait(run_commands))
    asyncio.get_event_loop().run_forever()

    memory_accessor.close_memory()


async def loop():
    while True:
        timer_bytes = memory_accessor.read_memory(0x80000000, 0x1800000)
        timer = struct.unpack(">h", timer_bytes[0x553974:0x553976])[0]

        print(timer)
        await asyncio.sleep(1)
