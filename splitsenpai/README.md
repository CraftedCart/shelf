SplitSenpai
===========

The idea here was to make a speedrun timer/auto-splitter that could read a process' memory and split/stop the timer
automatically, as well as host a small web server so you could control it from a browser/show it on a stream overlay
with an OBS browser source.

There's... not much here to show - there's a timer you can start/stop at `localhost:8080`, and it can hook into
`dolphin-emu` on Linux and print out the timer value from Super Monkey Ball 2 in the console, buut that's it. Also if
you want to test this out, you'll need to modify `splitsenpai/splitsenpai.py` to give it the `dolphin-emu` process ID.
