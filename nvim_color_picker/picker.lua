local function hsv_to_rgb(hue, saturation, value)
  hue = hue % 360

  -- Returns the RGB equivalent of the given HSV-defined color
  -- (adapted from some code found around the web)

  -- If it's achromatic, just return the value
  if saturation == 0 then
    return value, value, value
  end

  -- Get the hue sector
  local hue_sector = math.floor( hue / 60 )
  local hue_sector_offset = ( hue / 60 ) - hue_sector

  local p = value * ( 1 - saturation )
  local q = value * ( 1 - saturation * hue_sector_offset )
  local t = value * ( 1 - saturation * ( 1 - hue_sector_offset ) )

  if hue_sector == 0 then
    return value, t, p
  elseif hue_sector == 1 then
    return q, value, p
  elseif hue_sector == 2 then
    return p, value, t
  elseif hue_sector == 3 then
    return p, q, value
  elseif hue_sector == 4 then
    return t, p, value
  elseif hue_sector == 5 then
    return value, p, q
  else
    error("Hue out of range")
  end
end

local function rgb_to_hsv(red, green, blue)
  -- Returns the HSV equivalent of the given RGB-defined color
  -- (adapted from some code found around the web)

  local hue, saturation, value

  local min_value = math.min( red, green, blue )
  local max_value = math.max( red, green, blue )

  value = max_value

  local value_delta = max_value - min_value

  -- If the color is not black
  if max_value ~= 0 then
    saturation = value_delta / max_value

  -- If the color is purely black
  else
    saturation = 0
    hue = -1
    return hue, saturation, value
  end

  if red == max_value then
    hue = ( green - blue ) / value_delta
  elseif green == max_value then
    hue = 2 + ( blue - red ) / value_delta
  else
    hue = 4 + ( red - green ) / value_delta
  end

  hue = hue * 60
  if hue < 0 then
    hue = hue + 360
  end

  return hue, saturation, value
end

local function clamp(val, min, max)
  return math.max(math.min(val, max), min)
end

local function rgb_to_hex(r, g, b)
  return string.format(
    "%02X%02X%02X",
    clamp(r * 255, 0, 255),
    clamp(g * 255, 0, 255),
    clamp(b * 255, 0, 255)
    )
end

local picker_buf
local picker_win

local picker_hl_ns

-- The █ char takes up 3 UTF-8 bytes
local BYTES_PER_CHAR = 3

-- 0 indexed
local HUE_BAR_LINE = 2
local SAT_BAR_LINE = 5
local VAL_BAR_LINE = 8

local HUE_ARROW_LINE = 1
local SAT_ARROW_LINE = 4
local VAL_ARROW_LINE = 7

local BUF_VAR_HUE = "color_picker_hue"
local BUF_VAR_SAT = "color_picker_sat"
local BUF_VAR_VAL = "color_picker_val"

local BUF_VAR_ACTIVE_ELEM = "color_picker_active_elem"

local BAR_LEFT_PAD = 5
local BAR_RIGHT_PAD = 1

local function get_var(var_name)
  local var = vim.api.nvim_buf_get_var(picker_buf, var_name)

  -- For some reason, setting a buffer-local decimal value then trying to retrieve it again gives me a table with a
  -- [false] and [true] key???
  if type(var) == "table" then
    return var[false]
  else
    return var
  end
end

local function make_highlights()
  local hue = get_var(BUF_VAR_HUE) * 360
  local sat = get_var(BUF_VAR_SAT)
  local val = get_var(BUF_VAR_VAL)

  local commands = {}

  for i = 0, 255 do
    local r, g, b = hsv_to_rgb(i / 255 * 360, sat, val)
    local hex = rgb_to_hex(r, g, b)
    table.insert(commands, "highlight ColorPickerHue" .. i .. " guifg=#" .. hex)

    r, g, b = hsv_to_rgb(hue, i / 255, val)
    hex = rgb_to_hex(r, g, b)
    table.insert(commands, "highlight ColorPickerSat" .. i .. " guifg=#" .. hex)

    r, g, b = hsv_to_rgb(hue, sat, i / 255)
    hex = rgb_to_hex(r, g, b)
    table.insert(commands, "highlight ColorPickerVal" .. i .. " guifg=#" .. hex)
  end

  vim.cmd(table.concat(commands, "|"))
end

local function colorize_bar(line_num, prefix)
  for i = 0, 255 do
    vim.api.nvim_buf_add_highlight(
      picker_buf,
      picker_hl_ns,
      prefix .. i,
      line_num,
      i * BYTES_PER_CHAR + BAR_LEFT_PAD,
      i * BYTES_PER_CHAR + BAR_LEFT_PAD + BYTES_PER_CHAR
      )
  end
end

local function update_arrow(arrow_line, value)
  vim.api.nvim_buf_set_lines(
    picker_buf,
    arrow_line,
    arrow_line + 1,
    true,
    {string.rep(" ", BAR_LEFT_PAD + math.floor(value * 254)) .. "V"}
    )
end

local function update_buf_content()
  update_arrow(HUE_ARROW_LINE, get_var(BUF_VAR_HUE))
  update_arrow(SAT_ARROW_LINE, get_var(BUF_VAR_SAT))
  update_arrow(VAL_ARROW_LINE, get_var(BUF_VAR_VAL))

  make_highlights()
end

local function set_buf_content()
  vim.api.nvim_buf_set_lines(
    picker_buf,
    0,
    0,
    true,
    {
      "",
      "",
      " Hue " .. string.rep("█", 255),
      "",
      "",
      " Sat " .. string.rep("█", 255),
      "",
      "",
      " Val " .. string.rep("█", 255),
    }
    )

  if picker_hl_ns == nil then
    picker_hl_ns = vim.api.nvim_create_namespace("color_picker")
  end

  -- Clear all highlights
  vim.api.nvim_buf_clear_namespace(picker_buf, picker_hl_ns, 0, -1)

  make_highlights()

  -- Hue
  colorize_bar(HUE_BAR_LINE, "ColorPickerHue")
  colorize_bar(SAT_BAR_LINE, "ColorPickerSat")
  colorize_bar(VAL_BAR_LINE, "ColorPickerVal")

  update_buf_content()
end

local function make_win()
  -- Make the buffer
  picker_buf = vim.api.nvim_create_buf(false, true)
  assert(picker_buf ~= 0)

  -- vim.api.nvim_buf_set_option(picker_buf, "modifiable", false)

  vim.api.nvim_buf_set_var(picker_buf, BUF_VAR_HUE, 0)
  vim.api.nvim_buf_set_var(picker_buf, BUF_VAR_SAT, 1)
  vim.api.nvim_buf_set_var(picker_buf, BUF_VAR_VAL, 1)

  vim.api.nvim_buf_set_var(picker_buf, BUF_VAR_ACTIVE_ELEM, "hue")

  set_buf_content()

  -- Make the floating window
  picker_win = vim.api.nvim_open_win(
    picker_buf,
    true,
    {
      style = "minimal",
      relative = "editor",
      row = 0,
      col = 0,
      width = 255 + BAR_LEFT_PAD + BAR_RIGHT_PAD,
      height = 10,
    }
    )
  assert(picker_win ~= 0)

  vim.api.nvim_win_set_option(picker_win, "winblend", 0)
end

local function main()
  make_win()

  vim.cmd("nnoremap <buffer> <silent> l :lua color_picker.increase(10 / 255)<CR>")
  vim.cmd("nnoremap <buffer> <silent> L :lua color_picker.increase(1 / 255)<CR>")
  vim.cmd("nnoremap <buffer> <silent> h :lua color_picker.increase(-10 / 255)<CR>")
  vim.cmd("nnoremap <buffer> <silent> H :lua color_picker.increase(-1 / 255)<CR>")

  vim.cmd("nnoremap <buffer> <silent> j :lua color_picker.next()<CR>")
  vim.cmd("nnoremap <buffer> <silent> k :lua color_picker.prev()<CR>")
end

-- GLOBALS --

color_picker = {}

function color_picker.increase(adj)
  local var

  local active = get_var(BUF_VAR_ACTIVE_ELEM)
  if active == "hue" then
    var = BUF_VAR_HUE
  elseif active == "sat" then
    var = BUF_VAR_SAT
  elseif active == "val" then
    var = BUF_VAR_VAL
  else
    error("Invalid active element!")
  end

  local value = get_var(var)
  value = clamp(value + adj, 0, 1)

  vim.api.nvim_buf_set_var(picker_buf, var, value)

  update_buf_content()
end

function color_picker.next()
  local active = get_var(BUF_VAR_ACTIVE_ELEM)

  if active == "hue" then
    active = "sat"
  elseif active == "sat" then
    active = "val"
  elseif active == "val" then
    active = "hue"
  else
    error("Invalid active element!")
  end

  vim.api.nvim_buf_set_var(picker_buf, BUF_VAR_ACTIVE_ELEM, active)
end

function color_picker.prev()
  local active = get_var(BUF_VAR_ACTIVE_ELEM)

  if active == "hue" then
    active = "val"
  elseif active == "sat" then
    active = "hue"
  elseif active == "val" then
    active = "sat"
  else
    error("Invalid active element!")
  end

  vim.api.nvim_buf_set_var(picker_buf, BUF_VAR_ACTIVE_ELEM, active)
end

main()
