#!/usr/bin/env python3

import time
import re
import requests


def follow(thefile):
    thefile.seek(0,2) # Go to the end of the file
    while True:
        line = thefile.readline()
        if not line:
            time.sleep(1) # Sleep briefly
            continue
        yield line


def main():
    rx = re.compile(".*DBG: Game Turn ([0-9]+).*")

    with open("civ5save/Logs/net_message_debug.log") as f:
        for line in follow(f):
            match = rx.match(line.strip())

            if match is not None:
                turn = match.group(1)
                print(f"It's now turn {turn}")

                data = {
                    "username": "Civilization 5",
                    "embeds": [{"title": f"Turn {turn} has begun"}],
                }

                requests.post(
                        "https://discord.com/api/webhooks/[REDACTED]/[REDACTED]",
                        json=data,
                        headers={"Content-Type": "application/json"}
                        )


if __name__ == "__main__":
    main()
