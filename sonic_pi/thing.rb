# frozen_string_literal: true

use_bpm 118

with_fx :reverb do
  live_loop :drums do
    sample :drum_heavy_kick
    sleep 1
  end
end

# use_synth :noise
# live_loop :ambient do
#   duration = 4
#   play 5, attack: duration, decay: 0, release: 0, amp: 0.5
#   sleep duration
# end

use_synth :dark_ambience
play chord(scale(:G, :minor)[0], :madd2, invert: -2)
sleep 0.5
play chord(scale(:G, :minor)[3], :madd2, invert: -2)
sleep 0.5
play chord(scale(:G, :minor)[5], :madd2, invert: -2)
# live_loop :idk do
# end
