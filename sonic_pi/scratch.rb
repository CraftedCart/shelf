# frozen_string_literal: true

use_synth :pulse
play 38
sleep 0.25
use_synth :dsaw
play 50
sleep 0.25
use_synth :prophet
play 62

play 60, attack: 0.1, decay: 0.2, sustain: 1, release: 0.5

sample :ambi_lunar_land
sample :ambi_drone

sample :ambi_lunar_land
sleep 1
play 48
sleep 0.5
play 36
sample :ambi_drone
sleep 1
play 36

sample :ambi_choir, rate: 0.5
sample :ambi_choir, rate: 0.4
sample :ambi_choir, rate: 0.6

# Crunchy!
sample :loop_amen, rate: 1.24
sample :loop_amen, rate: 1.25
sample :loop_amen, rate: 1.26

print sample_duration :loop_amen
sample :loop_amen, attack: 0.75, release: 0.75
sample :loop_amen, attack: 0.75, sustain: 0, release: 0.75
sample :loop_amen, start: 0.75
sample :loop_amen, finish: 0.5
sample :loop_amen, start: 0.5, finish: 0.7, rate: 0.2

sample :drum_cymbal_open
sample :drum_cymbal_open, attack: 0.01, sustain: 0.3, release: 0.1

loop do
  play rrand(50, 95)
  sleep 0.5
end

loop do
  sample :perc_bell, rate: rrand(0.125, 1.5)
  sleep rrand(0.2, 2)
end

use_synth :tb303
loop do
  play 50, release: 0.1, cutoff: rrand(60, 120)
  sleep 0.125
end

loop do
  play choose([60, 65, 72])
  sleep 0.125
end

live_loop :drums do
  sample :drum_heavy_kick
  sleep 0.5
  sample :drum_snare_hard
  sleep 0.5
end

loop do
  sample :loop_amen
  sleep sample_duration(:loop_amen)
end

sample :elec_plip

use_synth :prophet
play chord(:C4, :minor)
sleep 0.5
play chord(:As3, :minor)
sleep 0.5
play chord(:G3, :minor)

(1.0..2.0).step(0.1) do |rate|
  sample :loop_amen, rate: rate
  sleep sample_duration(:loop_amen, rate: rate)
end

s = play 50, release: 8
sleep 2
control s, note: 62

with_fx :reverb do
  with_fx :echo, phase: 0.125, decay: 8 do
    play 50
    sleep 0.5
    sample :elec_plip
    sleep 0.5
    play 62
  end
end

with_fx :slicer do
  play 50
  sleep 0.5
  sample :elec_plip
  sleep 0.5
  play 62
end

s = play 60, release: 5, note_slide: 1
control s, note: 65

with_fx :wobble, phase: 1, phase_slide: 5 do |e|
  use_synth :dsaw
  play 50, release: 5
  control e, phase: 0.025
end

play chord(:E4, :minor)
play chord(:E4, :minor, invert: 1)
play chord(:E4, :minor, invert: 2)
play chord(:E4, :minor, invert: -1)
play chord(:E4, :minor, invert: -2)
play chord(:E3, :minor, invert: -2, num_octaves: 3)
