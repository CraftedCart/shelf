"
`Counter` is a simple concrete class which supports incrementing and
decrementing.
Its API is
- `decrement` and `increment`
- `count`
Its creation message is `startAt:`
"
Class {
	#name : #Counter,
	#superclass : #Object,
	#instVars : [
		'count'
	],
	#category : #'CraftedCart-CounterTutorial'
}

{ #category : #accessing }
Counter class >> startingAt: val [
	"Create a new `Counter` initialized with a value"

	^ self new count: val.
]

{ #category : #accessing }
Counter >> count [
	^ count
]

{ #category : #accessing }
Counter >> count: val [
	count := val
]

{ #category : #operations }
Counter >> decrement [
	count := count - 1.
]

{ #category : #operations }
Counter >> increment [
	count := count + 1.
]

{ #category : #initialization }
Counter >> initialize [
	"Set the initial value of the `Counter` to 0"

	count := 0.
]

{ #category : #printing }
Counter >> printOn: stream [
	super printOn: stream.

	stream
		nextPutAll: ' with value ';
		print: count.
]
