"
This class contains tests for the `Counter` class
"
Class {
	#name : #CounterTest,
	#superclass : #TestCase,
	#category : #'CraftedCart-CounterTutorial'
}

{ #category : #tests }
CounterTest >> testCountIsSetAndRead [
	| counter |
	counter := Counter new.
	counter count: 7.
	self assert: counter count equals: 7.
]

{ #category : #tests }
CounterTest >> testDecrement [
	| counter |
	counter := Counter new.
	counter count: 0; decrement; decrement.
	self assert: counter count equals: -2.
]

{ #category : #tests }
CounterTest >> testIncrement [
	| counter |
	counter := Counter new.
	counter count: 0; increment; increment.
	self assert: counter count equals: 2.
]

{ #category : #tests }
CounterTest >> testInitialize [
	self assert: Counter new count equals: 0.
]

{ #category : #tests }
CounterTest >> testStartingAt [
	self assert: (Counter startingAt: 5) count equals: 5.
]
