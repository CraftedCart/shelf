#!/usr/bin/env python3

import os
import sys
from pathlib import Path
import hashlib
import xml.etree.ElementTree as ET
import xml.dom.minidom


SCHEMA = "{http://schemas.microsoft.com/developer/msbuild/2003}"


def main():
    tree = ET.parse(sys.argv[1])
    ET.register_namespace("", "http://schemas.microsoft.com/developer/msbuild/2003")
    root = tree.getroot()

    for item in root.findall(f"./{SCHEMA}PropertyGroup/{SCHEMA}ModActions"):
        item.clear()
    for item in root.findall(f"./{SCHEMA}PropertyGroup/{SCHEMA}ModContent"):
        item.clear()

    for item in root.findall(f"./{SCHEMA}ItemGroup/{SCHEMA}Content"):
        path = item.get("Include")

        if path.endswith(".xml") or path.endswith(".sql"):
            for mod_actions in root.findall(f"./{SCHEMA}PropertyGroup/{SCHEMA}ModActions"):
                action = ET.SubElement(mod_actions, "Action")
                set = ET.SubElement(action, "Set")
                set.text = "OnModActivated"
                type = ET.SubElement(action, "Type")
                type.text = "UpdateDatabase"
                filename = ET.SubElement(action, "FileName")
                filename.text = path.replace("\\", "/")

        elif path.endswith(".lua"):
            for mod_actions in root.findall(f"./{SCHEMA}PropertyGroup/{SCHEMA}ModContent"):
                content = ET.SubElement(mod_actions, "Content")
                type = ET.SubElement(content, "Type")
                type.text = "InGameUIAddin"
                name = ET.SubElement(content, "Name")
                name.text = os.path.basename(path.replace("\\", "/"))
                desc = ET.SubElement(content, "Description")
                desc.text = "Lua script"
                filename = ET.SubElement(content, "FileName")
                filename.text = path.replace("\\", "/")

    dom = xml.dom.minidom.parseString(ET.tostring(root, encoding="utf8", method="xml"))
    pretty_xml_as_string = dom.toprettyxml()

    with open("out.xml", "w") as f:
        f.write(pretty_xml_as_string)

    #  tree.write("out.xml")


if __name__ == "__main__":
    main()
