civ5\_autofill\_mod\_proj
=========================

Automatically fill in ModActions/ModContent in a civ5proj file, adding all .xml and .sql files as OnModActivated >
UpdateDatabase, and all .lua files as InGameUIAddins.
