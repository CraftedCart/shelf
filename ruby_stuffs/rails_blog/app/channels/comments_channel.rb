# frozen_string_literal: true

class CommentsChannel < ApplicationCable::Channel
  def self.broadcast(comment)
    broadcast_to(
      comment.article_id,
      comment: CommentsController.render(partial: "comments/comment", locals: { comment: comment })
    )
  end

  def subscribed
    # stream_from "some_channel"
    stream_for params[:article_id]
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
