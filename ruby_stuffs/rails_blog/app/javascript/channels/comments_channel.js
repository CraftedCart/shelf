import consumer from "./consumer"

if (gon.article_id) {
  console.log("Connecting!");
  consumer.subscriptions.create(
    {
      channel: "CommentsChannel",
      article_id: gon.article_id,
    },
    {
    connected() {
      // Called when the subscription is ready for use on the server
    },

    disconnected() {
      // Called when the subscription has been terminated by the server
    },

    received(data) {
      // Called when there's incoming data on the websocket for this channel
      let comments = document.getElementById("article-comments");
      comments.innerHTML = data.comment + comments.innerHTML;
    },
  });
}
