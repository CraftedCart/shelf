# frozen_string_literal: true

require "test_helper"

class CommentsMailerTest < ActionMailer::TestCase
  test "submitted" do
    mail = CommentsMailer.submitted comments(:one)
    assert_equal "You got a new blog comment!", mail.subject
    assert_equal ["owner@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match comments(:one).commenter, mail.body.encoded
    assert_match comments(:one).body, mail.body.encoded
  end

end
