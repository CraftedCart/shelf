class MakeStatusNonNullable < ActiveRecord::Migration[6.1]
  def change
    change_column_null :articles, :status, false, "public"
    change_column_null :comments, :status, false, "public"
  end
end
