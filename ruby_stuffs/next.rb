#!/usr/bin/env ruby
# frozen_string_literal: true

def do_stuff
  p(yield)
end

do_stuff { "hi" }
do_stuff { nil }
do_stuff { next }
do_stuff { next "hello" }
