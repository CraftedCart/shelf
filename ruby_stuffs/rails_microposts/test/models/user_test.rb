# frozen_string_literal: true

require "test_helper"

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(
      name: "Alice",
      email: "alice@example.com",
      password: "owo what's this?",
      password_confirmation: "owo what's this?",
    )
  end

  test "should be valid" do
    assert @user.valid?
  end

  test "empty name is invalid" do
    @user.name = ""
    assert_not @user.valid?
  end

  test "blank name is invalid" do
    @user.name = "     "
    assert_not @user.valid?
  end

  test "short name is invalid" do
    @user.name = "owo"
    assert_not @user.valid?
  end

  test "long name is invalid" do
    @user.name = "a" * 51
    assert_not @user.valid?
  end

  test "valid emails are valid" do
    emails = [
      "USER@foo.COM",
      "THE_US-ER@foo.bar.org",
      "first.last@foo.jp",
      "alice+bob@baz.cn",
      "simple@example.com",
      "very.common@example.com",
      "disposable.style.email.with+symbol@example.com",
      "other.email-with-hyphen@example.com",
      "fully-qualified-domain@example.com",
      "user.name+tag+sorting@example.com",
      "x@example.com",
      "example-indeed@strange-example.com",
      "admin@mailserver1",
      "example@s.example",
    ]

    emails.each do |email|
      @user.email = email
      assert @user.valid?, "#{email} should be valid"
    end
  end

  test "blank email is invalid" do
    @user.email = "\t\t"
    assert_not @user.valid?
  end

  test "invalid emails are invalid" do
    emails = [
      "user@example,com",
      "user_at_foo.org",
      "user.name@example.",
      "foo@bar_baz.com",
      "foo@bar+baz.com",
      "Abc.example.com",
      "A@b@c@example.com",
      'a"b(c)d,e:f;g<h>i[j\k]l@example.com',
      'just"not"right@example.com',
      'this is"not\allowed@example.com',
      'this\ still\"not\\allowed@example.com',
      "i_like_underscore@but_its_not_allowed_in_this_part.example.com",
      "foo@bar..com",
    ]

    emails.each do |email|
      @user.email = email
      assert_not @user.valid?, "#{email} should be invalid"
    end
  end

  test "long email is invalid" do
    @user.email = "#{"a" * 244}@example.com"
    assert_not @user.valid?
  end

  test "email addresses should be unique" do
    duplicate_user = @user.dup

    # Technically, only the domain part is case insensitive, but in practise the local part usually ends up being
    # case-insensitive too to avoid confusion
    duplicate_user.email.upcase!

    assert @user.save
    assert_not duplicate_user.valid?
  end

  test "email case is preserved" do
    @user.email = "Alice@EXAMPLE.com"
    assert @user.save
    @user.reload

    assert_equal @user.email, "Alice@EXAMPLE.com"
  end

  test "password cannot be empty" do
    @user.password = @user.password_confirmation = ""
    assert_not @user.valid?
  end

  test "password must be at least 8 chars long" do
    @user.password = @user.password_confirmation = "a" * 7
    assert_not @user.valid?

    @user.password = @user.password_confirmation = "a" * 8
    assert @user.valid?
  end
end
