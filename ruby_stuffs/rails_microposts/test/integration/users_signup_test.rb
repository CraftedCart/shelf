# frozen_string_literal: true

require "test_helper"

class UsersSignupTest < ActionDispatch::IntegrationTest
  test "invalid signup fails" do
    get signup_path
    assert_no_difference "User.count" do
      post users_path, params: {
        user: {
          name: "",
          email: "user@invalid",
          password: "foo",
          password_confirmation: "bar",
        },
      }
    end

    # A failed signup should re-render the new form, with errors
    assert_template "users/new"
    assert_select "div.error-explanation"
  end

  test "valid signup information" do
    get signup_path
    assert_difference "User.count" do
      post users_path, params: {
        user: {
          name: "Alice",
          email: "alice@alice.com",
          password: "alicealice",
          password_confirmation: "alicealice",
        },
      }
      follow_redirect!

      assert_template "users/show"
      assert_not_nil flash[:success]
      assert_nil flash[:danger]
    end
  end
end
