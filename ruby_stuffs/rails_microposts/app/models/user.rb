# frozen_string_literal: true

class User < ApplicationRecord
  has_secure_password

  validates :name,
    presence: true,
    length: { minimum: 4, maximum: 50 }

  validates :email,
    presence: true,
    length: { maximum: 255 },
    format: { with: URI::MailTo::EMAIL_REGEXP }, # Not a 100% compliant regex but good enough for most purposes
    uniqueness: { case_sensitive: false }

  validates :password,
    length: { minimum: 8 }
end
