# frozen_string_literal: true

module SessionsHelper
  # Logs in the given user
  def log_in(user)
    session[:user_id] = user.id
  end

  def current_user
    if !session[:user_id].nil?
      @current_user ||= User.find_by(id: session[:user_id])
    else
      nil
    end
  end

  def logged_in?
    !session[:user_id].nil?
  end
end
