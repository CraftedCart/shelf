# frozen_string_literal: true

# Enables the pgcrypto extension so we can use UUIDs in Postgres DBs
class EnableUuid < ActiveRecord::Migration[6.1]
  def change
    enable_extension "pgcrypto"
  end
end
