# Use UUIDs as the primary key type
Rails.application.config.generators do |g|
  g.orm :active_record, primary_key_type: :uuid
end
