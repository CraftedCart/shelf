# frozen_string_literal: true

class AddDefaultVotesToPollOptions < ActiveRecord::Migration[6.1]
  def change
    change_column_default :poll_options, :votes, from: nil, to: 0
    change_column_null :poll_options, :votes, false, 0
  end
end
