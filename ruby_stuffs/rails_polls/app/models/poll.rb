# frozen_string_literal: true

class Poll < ApplicationRecord
  has_many :poll_options, dependent: :destroy
  validates_associated :poll_options
  accepts_nested_attributes_for :poll_options

  validates :question, presence: true
end
