# frozen_string_literal: true

class PollOption < ApplicationRecord
  belongs_to :poll
  validates :option, presence: true
end
