# frozen_string_literal: true

class PollsController < ApplicationController
  def index
    @polls = Poll.all
  end

  def show
    @poll = Poll.find(params[:id])
  end

  def new
    @poll = Poll.new
    3.times do
      @poll.poll_options.build
    end
  end

  def create
    poll = Poll.new(poll_params)

    if poll.save
      redirect_to poll, notice: "Poll was successfully created."
    else
      render :new, status: :unprocessable_entity
    end
  end

  def vote
    option = PollOption.find(params[:option_id])
    option.votes += 1
    option.save!

    redirect_to poll_url(option.poll_id)
  end

  private

  def poll_params
    params.require(:poll).permit(:question, poll_options_attributes: [:option])
  end
end
