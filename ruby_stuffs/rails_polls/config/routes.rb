# frozen_string_literal: true

Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  # resources :polls
  get "/", to: "polls#index", as: "polls"
  post "/", to: "polls#create"

  get "/new", to: "polls#new", as: "new_poll"
  get "/:id", to: "polls#show", as: "poll"

  post "/vote/:option_id", to: "polls#vote", as: "vote_poll_option"
end
