use logwatcher::{LogWatcher, LogWatcherAction};
use regex::Regex;
use serde::Serialize;


#[derive(Serialize)]
struct Payload {
    username: String,
    embeds: Vec<Embed>,
}

#[derive(Serialize)]
struct Embed {
    title: String,
}

fn main() {
    let webhook_url = std::env::var("WEBHOOK_URL").expect("Could not get WEBHOOK_URL env var");
    let logs_dir = std::env::var("LOGS_DIR").expect("Could not get LOGS_DIR env var");

    let game_turn_re = Regex::new(r"DBG: Game Turn (\d+)").unwrap();

    let mut log_watcher = LogWatcher::register((logs_dir + "/net_message_debug.log").to_string())
        .expect("Failed to open net_message_debug.log");
    let client = reqwest::blocking::Client::new();

    log_watcher.watch(&mut |line| {
        if let Some(captures) = game_turn_re.captures(&line) {
            println!("It is turn {}", &captures[1]);

            let payload = Payload {
                username: "Civilization 5".to_owned(),
                embeds: vec![Embed {
                    title: format!("Turn {} has begun", &captures[1]),
                }],
            };

            if let Err(e) = client
                .post(&webhook_url)
                .header("Content-Type", "application/json")
                .body(serde_json::to_string(&payload).unwrap())
                .send()
            {
                eprintln!("Failed to send webhook: {}", e);
            }
        }

        LogWatcherAction::None
    });
}
