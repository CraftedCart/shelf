civ5_discord_bot_rust
=====================

A bot that watches for turn changes in `Logs/net_message_debug.log` and sends a message to a Discord channel. You must
have...some kind of message debug logging enabled in Civ 5's config.ini.
