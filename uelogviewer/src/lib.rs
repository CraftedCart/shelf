use std::error::Error;
use std::fs;

#[derive(Debug)]
struct LogEntry {
    date: String,
    num: String,
    category: String,
    message: String,
}

impl LogEntry {
    fn from_log_line(line: &str) -> LogEntry {
        // Example log line
        // [2019.01.31-11.39.37:363][296]LogModuleManager: Shutting down and abandoning module NetworkFile (10)

        let date: String = line.chars().skip(1).take(23).collect();
        let num: String = line.chars().skip(26).take(3).collect();
        let category: String = line.chars().skip(30).take_while(|c| {*c != ':'}).collect();
        let message: String = line.chars().skip(29).skip_while(|c| {*c != ':'}).skip(2).collect();

        LogEntry { date, num, category, message }
    }
}

pub fn view_log_file(filename: &str) -> Result<(), Box<dyn Error>> {
    let log_contents = fs::read_to_string(filename)?;
    let entries = parse_log_string(&log_contents);

    for entry in entries {
        if entry.category == "LogScript" { continue; }
        if entry.category == "LogScriptCore" { continue; }
        if entry.category == "LogParticles" { continue; }
        if entry.category == "LogSlate" { continue; }
        if entry.category == "LogBlueprintUserMessages" { continue; }
        if entry.category == "LogGameMode" { continue; }
        if entry.category == "LogLoad" { continue; }
        if entry.category == "LogWorld" { continue; }
        if entry.category == "LogUObjectGlobals" { continue; }
        if entry.category == "LogAIModule" { continue; }
        if entry.category == "LogInit" { continue; }

        println!("[{}] {}: {}", entry.date, entry.category, entry.message);
    }

    Ok(())
}

fn parse_log_string(log_contents: &str) -> Vec<LogEntry> {
    let mut entries = Vec::new();
    for line in log_contents.lines() {
        if !line.starts_with("[") { continue; }

        entries.push(LogEntry::from_log_line(line));
    }

    entries
}
