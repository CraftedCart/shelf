use std::env;

use uelogviewer;

fn main() {
    let mut args = env::args();
    args.next(); // Skip over the executable

    for arg in args {
        uelogviewer::view_log_file(&arg).unwrap_or_else(|err| {
            eprintln!("Error: {}", err);
        });
    }
}
