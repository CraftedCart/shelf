#include "Buffer.hpp"
#include <curses.h>

Buffer::Buffer(std::vector<std::string> lines) : lines(lines) {}

void Buffer::render() {
    for (std::string line : lines) {
        addstr((line + '\n').c_str());
    }
}

size_t Buffer::lineLength(unsigned int line) {
    return lines[line].length();
}

size_t Buffer::lineCount() {
    return lines.size();
}

Buffer Buffer::insert(char *c, unsigned long x, unsigned long y) {
    Buffer buffer = Buffer(std::vector<std::string>(lines)); //Clone the current buffer
    buffer.lines[y].insert(x, c);

    return buffer;
}

Buffer Buffer::remove(unsigned long x, unsigned long y) {
    Buffer buffer = Buffer(std::vector<std::string>(lines)); //Clone the current buffer
    buffer.lines[y].erase(buffer.lines[y].begin() + x);

    return buffer;
}

Buffer Buffer::splitLine(unsigned long x, unsigned long y) {
    Buffer buffer = Buffer(std::vector<std::string>(lines)); //Clone the current buffer
    std::string fullLine = lines[y];
    buffer.lines[y] = fullLine.substr(0, x);
    buffer.lines.insert(buffer.lines.begin() + y + 1, fullLine.substr(x));

    return buffer;
}

