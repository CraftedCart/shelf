#include "Cursor.hpp"
#include <algorithm>

Cursor::Cursor(int x, int y) :
    x(x),
    y(y)
{}

Cursor Cursor::up(Buffer &buffer) {
    return Cursor(x, y - 1).clamp(buffer);
}

Cursor Cursor::down(Buffer &buffer) {
    return Cursor(x, y + 1).clamp(buffer);
}

Cursor Cursor::left(Buffer &buffer) {
    return Cursor(x - 1, y).clamp(buffer);
}

Cursor Cursor::right(Buffer &buffer) {
    return Cursor(x + 1, y).clamp(buffer);
}

Cursor Cursor::toX(int newX) {
    return Cursor(newX, y);
}

Cursor Cursor::clamp(Buffer &buffer) {
    y = std::clamp(y, 0l, (long) buffer.lineCount() - 1);
    x = std::clamp(x, 0l, (long) buffer.lineLength(y));

    return Cursor(x, y);
}

