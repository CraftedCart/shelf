#include "Editor.hpp"
#include <curses.h>
#include <fstream>
#include <sstream>
#include <iostream>

#define ctrl(x) ((x) & 0x1f)

void onQuit() {
    endwin();
}

Editor::Editor() {
    std::ifstream t("foo.txt");
    std::stringstream strBuffer;
    strBuffer << t.rdbuf();

    std::vector<std::string> lines;
    std::string line;
    while (std::getline(strBuffer, line, '\n')) {
        lines.push_back(line);
    }

    buffer = Buffer(lines);
}

void Editor::run() {
    initscr();
    cbreak();
    noecho();
    keypad(stdscr, true);
    raw();
    std::atexit(onQuit);

    while (true) {
        render();
        handleInput();
    }
}

void Editor::render() {
    refresh();
    move(0, 0);
    buffer.render();

    unsigned int sizeX, sizeY;
    getmaxyx(stdscr, sizeY, sizeX);

    move(sizeY - 1, 0);
    printw("x: %d, y: %d - C-q to quit", cursor.x, cursor.y);

    move(cursor.y, cursor.x);
}

void Editor::handleInput() {
    int c = getch();
    switch (c) {
        case ctrl('q'):
            exit(0); break;
        case ctrl('u'):
            restoreSnapshot(); break;
        case KEY_LEFT:
            cursor = cursor.left(buffer); break;
        case KEY_RIGHT:
            cursor = cursor.right(buffer); break;
        case KEY_UP:
            cursor = cursor.up(buffer); break;
        case KEY_DOWN:
            cursor = cursor.down(buffer); break;
        case 127: //Backspace
            saveSnapshot();
            if (cursor.x > 0) {
                buffer = buffer.remove(cursor.x - 1, cursor.y);
                cursor = cursor.left(buffer);
            }
            break;
        case 10: //Return
            saveSnapshot();
            buffer = buffer.splitLine(cursor.x, cursor.y);
            cursor = cursor.down(buffer).toX(0);
            break;
        default:
            if (isascii(c) && isprint(c)) {
                saveSnapshot();
                buffer = buffer.insert((char*) &c, cursor.x, cursor.y);
                cursor = cursor.right(buffer);
            }
            break;
    }
}

void Editor::saveSnapshot() {
    history.push_back({buffer, cursor});
}

void Editor::restoreSnapshot() {
    if (history.size() < 1) return;

    HistorySnapshot snapshot = history[history.size() - 1];
    history.pop_back();

    buffer = snapshot.buffer;
    cursor = snapshot.cursor;
}

