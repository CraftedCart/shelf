#ifndef SCRATCHEDITOR_EDITOR_HPP
#define SCRATCHEDITOR_EDITOR_HPP

#include "Buffer.hpp"
#include "Cursor.hpp"

struct HistorySnapshot {
    Buffer buffer;
    Cursor cursor;
};

class Editor {
    protected:
        Buffer buffer;
        Cursor cursor;
        std::vector<HistorySnapshot> history;

    public:
        Editor();

        void run();

    protected:
        void render();
        void handleInput();

        void saveSnapshot();
        void restoreSnapshot();
};

#endif

