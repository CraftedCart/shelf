#ifndef SCRATCHEDITOR_CURSOR_HPP
#define SCRATCHEDITOR_CURSOR_HPP

#include "Buffer.hpp"

class Cursor {
    public:
        long x;
        long y;

    public:
        Cursor(int x = 0, int y = 0);

        Cursor up(Buffer &buffer);
        Cursor down(Buffer &buffer);
        Cursor left(Buffer &buffer);
        Cursor right(Buffer &buffer);
        Cursor toX(int newX);

        Cursor clamp(Buffer &buffer);
};

#endif

