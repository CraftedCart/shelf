#ifndef SCRATCHEDITOR_BUFFER_HPP
#define SCRATCHEDITOR_BUFFER_HPP

#include <vector>
#include <string>

class Buffer {
    protected:
        std::vector<std::string> lines;

    public:
        Buffer() = default;
        Buffer(std::vector<std::string> lines);

        void render();

        size_t lineLength(unsigned int line);
        size_t lineCount();

        Buffer insert(char *c, unsigned long x, unsigned long y);
        Buffer remove(unsigned long x, unsigned long y);
        Buffer splitLine(unsigned long x, unsigned long y);
};

#endif

