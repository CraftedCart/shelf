#!/usr/bin/env python3

import mido


LENGTH_PER_LINE = 24

# SONG_NAME = "prisimriver.mid"
# SONG_TICKS = 55000
# SPEED = 10

SONG_NAME = "bad_apple.mid"
SONG_TICKS = 65000
SPEED = 23

PLAYER = """
LINK 801

MARK LOOP
COPY F X
SWIZ X 0043 #TRI0
SWIZ X 0021 #SQR0

COPY F X
SWIZ X 0043 #SQR1

COPY 200 T
MARK WAIT_LOOP
SUBI T 1 T
TJMP WAIT_LOOP

NOOP

NOOP
SWIZ X 0021 #TRI0

COPY F X
SWIZ X 0043 #SQR0
SWIZ X 0021 #SQR1

COPY 200 T
MARK WAIT_LOOP2
SUBI T 1 T
TJMP WAIT_LOOP2

JUMP LOOP
"""

def main():
    mid = mido.MidiFile(SONG_NAME)

    # Horifically inefficient but it works so I can't be bothered to optimize it
    # Kinda a bit against the spirit of exapunks, huh

    sqr_notes_on = None
    tri_notes_on = None

    for track in mid.tracks:
        print("Processing track " + track.name)

        notes_on_each_tick = [[]]
        for tick in range(SONG_TICKS):
            notes_on = notes_on_each_tick[-1].copy()

            cur_tick = 0
            for msg in track:
                cur_tick += msg.time

                if cur_tick == tick:
                    if msg.type == "note_on":
                        notes_on.append(msg.note)
                    elif msg.type == "note_off":
                        notes_on.remove(msg.note)

            notes_on.sort()
            notes_on_each_tick.append(notes_on)

        if "SQR" in track.name:
            if sqr_notes_on is None:
                sqr_notes_on = notes_on_each_tick
            else:
                # Merge
                for i, tick_notes_on in enumerate(sqr_notes_on):
                    tick_notes_on.extend(notes_on_each_tick[i])
        elif "TRI" in track.name:
            tri_notes_on = notes_on_each_tick
        elif "NSE" in track.name:
            print(f"Ignoring noise track {track.name}")
        else:
            print(f"Ignoring track {track.name}")

    ######## MUNGE DATA AND WRITE OUT ########
    data = []
    i = 0
    for tick in range(0, SONG_TICKS, SPEED * 2):
        tick_data1 = ""
        tick_data2 = ""
        tick_data3 = ""

        tick_tri_notes_on = tri_notes_on[tick]
        if len(tick_tri_notes_on) == 0:
            tick_data1 += "00"
        else:
            tick_data1 += "%02d" % tick_tri_notes_on[i % len(tick_tri_notes_on)]

        tick_sqr_notes_on = sqr_notes_on[tick]
        if len(tick_sqr_notes_on) == 0:
            tick_data1 += "00"
            tick_data2 += "00"
        elif len(tick_sqr_notes_on) == 1:
            tick_data1 += "%02d" % tick_sqr_notes_on[0]
            tick_data2 += "00"
        else:
            tick_data1 += "%02d" % tick_sqr_notes_on[i % len(tick_sqr_notes_on)]
            tick_data2 += "%02d" % tick_sqr_notes_on[(i + 1) % len(tick_sqr_notes_on)]

        if len(tri_notes_on) > tick + SPEED:
            tick_tri_notes_on = tri_notes_on[tick + SPEED]
            if len(tick_tri_notes_on) == 0:
                tick_data2 += "00"
            else:
                tick_data2 += "%02d" % tick_tri_notes_on[i % len(tick_tri_notes_on)]

            tick_sqr_notes_on = sqr_notes_on[tick + SPEED]
            if len(tick_sqr_notes_on) == 0:
                tick_data3 += "00"
                tick_data3 += "00"
            elif len(tick_sqr_notes_on) == 1:
                tick_data3 += "%02d" % tick_sqr_notes_on[0]
                tick_data3 += "00"
            else:
                tick_data3 += "%02d" % tick_sqr_notes_on[i % len(tick_sqr_notes_on)]
                tick_data3 += "%02d" % tick_sqr_notes_on[(i + 1) % len(tick_sqr_notes_on)]

        data.append(tick_data1)
        data.append(tick_data2)
        data.append(tick_data3)

        i += 1

    # Write player
    with open(f"out/combined.asm", "w") as f:
        f.write(PLAYER)
        f.write(data_to_str(data))



# Consumes the data list
def data_to_str(data):
    data_as_str = ""
    while len(data) > 0:
        data_line = "DATA "

        while len(data) > 0:
            next_data = data.pop(0).lstrip("0")
            if next_data == "":
                next_data = "0"

            if len(data_line) + len(next_data) <= LENGTH_PER_LINE:
                data_line += next_data
                data_line += " "
            else:
                data.insert(0, next_data)

                # Remove trailing space
                data_line = data_line[:-1]
                data_as_str += "\n" + data_line
                break

    return data_as_str


if __name__ == "__main__":
    main()
