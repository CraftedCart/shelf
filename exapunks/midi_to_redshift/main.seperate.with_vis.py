#!/usr/bin/env python3

import mido


VALUES_PER_DATA_LINE = 6

# SONG_NAME = "prisimriver.mid"
# SONG_TICKS = 55000
# SPEED = 10

SONG_NAME = "bad_apple.mid"
SONG_TICKS = 70000
SPEED = 12

PLAYER = """
LINK 801

COPY 90 GY
COPY 100 GP
COPY 101 GP
COPY 102 GP
COPY 103 GP
COPY 104 GP
COPY 105 GP
COPY 106 GP
COPY 107 GP
COPY 108 GP
COPY 109 GP

MARK LOOP
COPY F X
COPY X #REGI
SUBI X 1 GX

COPY 195 T
MARK WAIT_LOOP
SUBI T 1 T
TJMP WAIT_LOOP

JUMP LOOP
"""

def main():
    mid = mido.MidiFile(SONG_NAME)

    # Horifically inefficient but it works so I can't be bothered to optimize it
    # Kinda a bit against the spirit of exapunks, huh

    sqr_notes_on = None
    tri_notes_on = None
    nse_notes_on = None

    for track in mid.tracks:
        print("Processing track " + track.name)

        notes_on_each_tick = [[]]
        for tick in range(SONG_TICKS):
            notes_on = notes_on_each_tick[-1].copy()

            cur_tick = 0
            for msg in track:
                cur_tick += msg.time

                if cur_tick == tick:
                    if msg.type == "note_on":
                        notes_on.append(msg.note)
                    elif msg.type == "note_off":
                        notes_on.remove(msg.note)

            notes_on.sort()
            notes_on_each_tick.append(notes_on)

        if "SQR" in track.name:
            if sqr_notes_on is None:
                sqr_notes_on = notes_on_each_tick
            else:
                # Merge
                for i, tick_notes_on in enumerate(sqr_notes_on):
                    tick_notes_on.extend(notes_on_each_tick[i])
        elif "TRI" in track.name:
            tri_notes_on = notes_on_each_tick
        elif "NSE" in track.name:
            nse_notes_on = notes_on_each_tick
        else:
            print(f"Ignoring track {track.name}")

    ######## WRITE TRI0 ########
    data = []
    for i, notes_on in enumerate(tri_notes_on[::SPEED]):
        if len(notes_on) == 0:
            data.append(0)
        else:
            data.append(notes_on[i % len(notes_on)])

    # Convert data ints -> data pseudo-instructions
    data_as_str = ""
    while len(data) > 0:
        data_as_str += "\nDATA "

        i = 0
        while len(data) > 0:
            data_as_str += str(data.pop(0))
            data_as_str += " "

            i += 1
            if i == VALUES_PER_DATA_LINE:
                break

    # Write player
    with open(f"out/TRI0.asm", "w") as f:
        f.write(PLAYER.replace("REGI", "TRI0"))
        f.write(data_as_str)

    ######## WRITE NSE0 ########
    if nse_notes_on is not None:
        data = []
        for i, notes_on in enumerate(nse_notes_on[::SPEED]):
            if len(notes_on) == 0:
                data.append(0)
            else:
                data.append(notes_on[i % len(notes_on)])

        # Convert data ints -> data pseudo-instructions
        data_as_str = ""
        while len(data) > 0:
            data_as_str += "\nDATA "

            i = 0
            while len(data) > 0:
                data_as_str += str(data.pop(0))
                data_as_str += " "

                i += 1
                if i == VALUES_PER_DATA_LINE:
                    break

        # Write player
        with open(f"out/NSE0.asm", "w") as f:
            f.write(PLAYER.replace("REGI", "NSE0"))
            f.write(data_as_str)

    ######## WRITE SQR0 ########
    data = []
    for i, notes_on in enumerate(sqr_notes_on[::SPEED]):
        if len(notes_on) == 0:
            data.append(0)
        else:
            data.append(notes_on[i % len(notes_on)])

    # Convert data ints -> data pseudo-instructions
    data_as_str = ""
    while len(data) > 0:
        data_as_str += "\nDATA "

        i = 0
        while len(data) > 0:
            data_as_str += str(data.pop(0))
            data_as_str += " "

            i += 1
            if i == VALUES_PER_DATA_LINE:
                break

    # Write player
    with open(f"out/SQR0.asm", "w") as f:
        f.write(PLAYER.replace("REGI", "SQR0"))
        f.write(data_as_str)

    ######## WRITE SQR1 ########
    data = []
    for i, notes_on in enumerate(sqr_notes_on[::SPEED]):
        if len(notes_on) < 2:
            data.append(0)
        else:
            data.append(notes_on[(i + 1) % len(notes_on)])

    # Convert data ints -> data pseudo-instructions
    data_as_str = ""
    while len(data) > 0:
        data_as_str += "\nDATA "

        i = 0
        while len(data) > 0:
            data_as_str += str(data.pop(0))
            data_as_str += " "

            i += 1
            if i == VALUES_PER_DATA_LINE:
                break

    # Write player
    with open(f"out/SQR1.asm", "w") as f:
        f.write(PLAYER.replace("REGI", "SQR1"))
        f.write(data_as_str)



def write_notes(notes, len):
    pass


if __name__ == "__main__":
    main()
