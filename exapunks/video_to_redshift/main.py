#!/usr/bin/env python3

import cv2
import numpy
import re


VIDEO = "bad_apple.webm"
SPEED = 17

RESOLUTION_PER_SPRITE_X = 10
RESOLUTION_PER_SPRITE_Y = 10

SPRITES_X = 4
SPRITES_Y = 4
NUM_SPRITES = SPRITES_X * SPRITES_Y

RESOLUTION_X = RESOLUTION_PER_SPRITE_X * SPRITES_X
RESOLUTION_Y = RESOLUTION_PER_SPRITE_Y * SPRITES_Y

LENGTH_PER_LINE = 24

PLAYER = """
COPY {X_POS} GX
COPY {Y_POS} GY
MARK LOOP

MARK FRAME_LOOP
COPY F X

; END OF FRAME?
TEST X = 999
TJMP NEXT_FRAME

; RLE?
TEST X = 800
FJMP NOT_RLE

; IT'S RLE

; INDEX IN X
COPY 0 X

MARK RLE_LOOP
COPY F T

; END OF FRAME?
TEST T = 999
TJMP NEXT_FRAME

SEEK -1
SWIZ F 0001 T
SEEK -1

FJMP COPY_OFF_RLE

; STORE COUNTER IN T
SWIZ F 0432 T

MARK COPY_ON_RLE_LOOP
FJMP RLE_LOOP

SWIZ X 4312 X
ADDI X 100 GP
SWIZ X 4312 X

ADDI X 1 X
SUBI T 1 T
JUMP COPY_ON_RLE_LOOP

MARK COPY_OFF_RLE

; STORE COUNTER IN T
SWIZ F 0432 T

MARK COPY_OFF_RLE_LOOP
FJMP RLE_LOOP

SWIZ X 4312 GP

ADDI X 1 X
SUBI T 1 T
JUMP COPY_OFF_RLE_LOOP

MARK NEXT_FRAME
@REP 4
WAIT
@END
VOID M
JUMP LOOP


MARK NOT_RLE
COPY X GP
MARK NOT_RLE_LOOP
COPY F X
; END OF FRAME?
TEST X = 999
TJMP NEXT_FRAME
COPY X GP
JUMP NOT_RLE_LOOP
"""

SIGNALER = f"""
MARK LOOP
@REP {NUM_SPRITES}
COPY 1 M
@END

@REP 22
WAIT
@END

COPY 160 T
MARK WAIT_LOOP
SUBI T 1 T
TJMP WAIT_LOOP

JUMP LOOP
"""


def main():
    prev_image = numpy.zeros((RESOLUTION_Y, RESOLUTION_X), numpy.int8)

    data_per_sprite = []
    for _ in range(SPRITES_X * SPRITES_Y):
        data_per_sprite.append([])

    vidcap = cv2.VideoCapture(VIDEO)
    success, image = vidcap.read()

    time_to_skip = 0

    i = 0
    while success:
        if time_to_skip <= 0:
            print(f"Frame {i}")

            # Convert to grayscale and downscale
            image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) # Assuming input image is BGR, not sure
            image = cv2.resize(image, (RESOLUTION_Y, RESOLUTION_X))
            # cv2.imwrite("out/frame%d.jpg" % i, image)

            largest_diff = 0
            for sprite_y in range(SPRITES_Y):
                for sprite_x in range(SPRITES_X):
                    y_start = sprite_y * RESOLUTION_PER_SPRITE_Y
                    y_end = y_start + RESOLUTION_PER_SPRITE_Y
                    x_start = sprite_x * RESOLUTION_PER_SPRITE_X
                    x_end = x_start + RESOLUTION_PER_SPRITE_X
                    sprite_data = data_per_sprite[sprite_y * SPRITES_X + sprite_x]

                    diff = diff_images(prev_image[y_start:y_end, x_start:x_end], image[y_start:y_end, x_start:x_end])
                    rle = rle_encode_image(image[y_start:y_end, x_start:x_end])

                    if len(diff) < len(rle):
                        sprite_data.extend(diff)
                    else:
                        sprite_data.extend(rle)

                    sprite_data.append("999") # 999 marks end of frame

            prev_image = image
            time_to_skip = SPEED
        else:
            time_to_skip -= 1

        success, image = vidcap.read()
        i += 1

    for sprite_y in range(SPRITES_Y):
        for sprite_x in range(SPRITES_X):
            with open(f"out/exa_{sprite_x}_{sprite_y}.asm", "w") as f:
                f.write(
                    re.sub(r"^;.*$", "", PLAYER, flags=re.MULTILINE)
                        .replace("{X_POS}", str(sprite_x * RESOLUTION_PER_SPRITE_Y))
                        .replace("{Y_POS}", str(sprite_y * RESOLUTION_PER_SPRITE_Y))
                        .replace("\n\n", "\n")
                        .replace("\n\n", "\n")
                        .replace("\n\n", "\n")
                )
                f.write(data_to_str(data_per_sprite[sprite_y * SPRITES_X + sprite_x]))

    with open(f"out/signaler.asm", "w") as f:
        f.write(SIGNALER)


# Consumes the data list
def data_to_str(data):
    data_as_str = ""
    while len(data) > 0:
        data_line = "DATA "

        while len(data) > 0:
            next_data = data.pop(0)
            if len(data_line) + len(next_data) <= LENGTH_PER_LINE:
                data_line += next_data
                data_line += " "
            else:
                data.insert(0, next_data)

                # Remove trailing space
                data_line = data_line[:-1]
                data_as_str += "\n" + data_line
                break

    return data_as_str


def rle_encode_image(image):
    out = ["800"] # 800 marks RLE data

    image = image.flatten()

    prev_pixel = None
    prev_len = None
    for i in range(len(image)):
        pixel = is_pixel_on(image[i])
        if pixel != prev_pixel:
            if prev_pixel is not None:
                out.append(f"{prev_len}{int(prev_pixel)}")

            prev_pixel = pixel
            prev_len = 1
        else:
            prev_len += 1

    out.append(f"{prev_len}{int(prev_pixel)}")

    return out


# Returns a list of strings to write to the Redshift GP register
def diff_images(a, b):
    out = []

    assert a.shape == b.shape

    height, width = a.shape
    assert height <= 10
    assert width <= 10

    for y in range(height):
        for x in range(width):
            if is_pixel_on(a[y, x]) and not is_pixel_on(b[y, x]):
                # Transition to dark
                string = f"{x}{y}".lstrip("0")
                if string == "":
                    string = "0"
                out.append(string)
            elif not is_pixel_on(a[y, x]) and is_pixel_on(b[y, x]):
                # Transition to light
                out.append(f"1{x}{y}")

    return out


def is_pixel_on(val):
    return val > 127


if __name__ == "__main__":
    main()
