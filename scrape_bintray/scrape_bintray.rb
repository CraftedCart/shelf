#!/usr/bin/env ruby

require "net/http"
require "open-uri"
require "json"
require "fileutils"

OUTPUT_DIR = "scraped"

def main
  if ARGV.length < 2
    puts "Too few arguments - usage: scrape_bintray.rb <subject> <repo>"
    exit 1
  end

  subject = ARGV[0]
  repo = ARGV[1]

  puts "Getting package list..."

  packages_str = Net::HTTP.get(URI("https://bintray.com/api/v1/repos/#{subject}/#{repo}/packages"))
  packages = JSON.parse(packages_str).map { |item| item["name"] }

  FileUtils.mkdir_p OUTPUT_DIR

  for package in packages
    puts "#{package} -> Getting file list..."

    files_str = Net::HTTP.get(URI("https://bintray.com/api/v1/packages/#{subject}/#{repo}/#{package}/files"))
    files = JSON.parse(files_str)

    # Write metadata to a file
    File.write(File.join(OUTPUT_DIR, "bintray_metadata_#{package}.json"), files_str)

    files.each_with_index do |file, idx|
      puts "#{package} -> [#{idx + 1} / #{files.length}] Saving #{file["path"]}"
      save_file(subject, repo, file["path"])
    end
  end
end

def save_file(subject, repo, file_path)
  out_path = File.join(OUTPUT_DIR, file_path)

  FileUtils.mkdir_p(File.dirname(out_path))

  File.open(out_path, "wb") do |saved_file|
    URI.open("https://dl.bintray.com/#{subject}/#{repo}/#{file_path}", "rb") do |read_file|
      saved_file.write(read_file.read)
    end
  end
end

if __FILE__ == $0
  main
end
