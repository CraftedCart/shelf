use serde::Deserialize;

#[derive(Deserialize, Debug)]
struct Thingy {
    a_string: String,
}

const SOURCE: &str = r##"
Thingy(
    a_string: r#"
        I'm just kinda curious if
        RON supports multiline
        strings here.
    "#,
)
"##;

fn main() {
    let thingy = ron::de::from_str::<Thingy>(SOURCE).unwrap();
    println!("{thingy:?}");
}
