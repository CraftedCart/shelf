use bevy::prelude::*;

use crate::score::ScoreResource;

#[derive(Component)]
struct TimeText;
#[derive(Component)]
struct ScoreText;

fn setup_ui(
    mut commands: Commands,
    asset_server: ResMut<AssetServer>,
    mut color_materials: ResMut<Assets<ColorMaterial>>,
) {
    let font = asset_server.load("FiraSans-Bold.ttf");
    let material = color_materials.add(Color::NONE.into());

    // Time text node
    commands
        .spawn_bundle(NodeBundle {
            style: Style {
                position_type: PositionType::Absolute,
                position: Rect {
                    left: Val::Px(10.0),
                    top: Val::Px(10.0),
                    ..Default::default()
                },
                ..Default::default()
            },
            material: material.clone(),
            ..Default::default()
        })
        .with_children(|parent| {
            parent
                .spawn_bundle(TextBundle {
                    text: Text::with_section(
                        "Time: 0.00",
                        TextStyle {
                            font: font.clone(),
                            font_size: 40.0,
                            color: Color::rgb(0.9, 0.9, 0.9),
                        },
                        TextAlignment::default(),
                    ),
                    ..Default::default()
                })
                .insert(TimeText);
        });

    // Score text node
    commands
        .spawn_bundle(NodeBundle {
            style: Style {
                position_type: PositionType::Absolute,
                position: Rect {
                    left: Val::Px(10.0),
                    bottom: Val::Px(10.0),
                    ..Default::default()
                },
                ..Default::default()
            },
            material,
            ..Default::default()
        })
        .with_children(|parent| {
            parent
                .spawn_bundle(TextBundle {
                    text: Text::with_section(
                        "Accuracy: 0.00%",
                        TextStyle {
                            font,
                            font_size: 40.0,
                            color: Color::rgb(0.9, 0.9, 0.9),
                        },
                        TextAlignment::default(),
                    ),
                    ..Default::default()
                })
                .insert(ScoreText);
        });
}

fn update_time_text(mut query: Query<&mut Text, With<TimeText>>, time: Res<Time>) {
    // Song starts 3 seconds after real time
    let secs = time.seconds_since_startup() - 3.0;

    // Don't do anything before the song starts
    if secs < 0.0 {
        return;
    }

    for mut text in query.iter_mut() {
        text.sections[0].value = format!("Time: {:.2}", secs);
    }
}

fn update_score_text(mut query: Query<&mut Text, With<ScoreText>>, score: Res<ScoreResource>) {
    for mut text in query.iter_mut() {
        text.sections[0].value = format!("Accuracy: {:.2}%", score.percent() * 100.0);
    }
}

pub struct UiPlugin;

impl Plugin for UiPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(setup_ui.system())
            .add_system(update_time_text.system())
            .add_system(update_score_text.system());
    }
}
