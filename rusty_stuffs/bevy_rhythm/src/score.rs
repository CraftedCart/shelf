#[derive(Default)]
pub struct ScoreResource {
    score: f32,
    max_score: f32,
}

impl ScoreResource {
    pub fn add_hit(&mut self, distance: f32) {
        self.score += crate::arrow::THRESHOLD - distance;
        self.max_score += crate::arrow::THRESHOLD;
    }

    pub fn add_miss(&mut self) {
        self.max_score += crate::arrow::THRESHOLD;
    }

    pub fn percent(&self) -> f32 {
        if self.max_score == 0.0 {
            0.0
        } else {
            self.score / self.max_score
        }
    }
}
