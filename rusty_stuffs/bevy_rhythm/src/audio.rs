use bevy::prelude::*;
use bevy_kira_audio::{Audio, AudioChannel};
use crate::song_config::SongConfig;

/// TODO: This isn't synced with gameplay ><
/// Waiting on <https://github.com/NiklasEi/bevy_kira_audio/issues/23> (Make playbacktime accessible to users)
fn start_song(audio: Res<Audio>, time: Res<Time>, config: Res<SongConfig>, channel: Res<SongChannel>) {
    let secs = time.seconds_since_startup();
    let secs_last = secs - time.delta_seconds_f64();

    if secs_last <= crate::START_DELAY && crate::START_DELAY <= secs {
        audio.set_volume_in_channel(0.1, &channel.0);
        audio.play_in_channel(config.audio.clone(), &channel.0);
    }
}

/// Song channel resource
struct SongChannel(AudioChannel);

impl Default for SongChannel {
    fn default() -> Self {
        SongChannel(AudioChannel::new("song-channel".to_owned()))
    }
}

pub struct AudioPlugin;

impl Plugin for AudioPlugin {
    fn build(&self, app: &mut App) {
        app
            .init_resource::<SongChannel>()
            .add_system(start_song.system());
    }
}
