use crate::{arrow_direction::ArrowDirection, score::ScoreResource, song_config::SongConfig};
use bevy::prelude::*;

/// Speed that an arrow moves at
pub const ARROW_SPEED: f32 = 800.0;

const ARROW_SPAWN_Y: f32 = -800.0;
const RECEPTOR_Y: f32 = 200.0;

/// Margin of error when hitting a note
pub const THRESHOLD: f32 = 80.0;

pub const SPAWN_DISTANCE_FROM_RECEPTOR: f32 = RECEPTOR_Y - ARROW_SPAWN_Y;

struct ArrowMaterialResource {
    arrow_4_mat: Handle<ColorMaterial>,
    arrow_8_mat: Handle<ColorMaterial>,
    arrow_12_mat: Handle<ColorMaterial>,
    arrow_16_mat: Handle<ColorMaterial>,
    arrow_other_mat: Handle<ColorMaterial>,

    receptor_mat: Handle<ColorMaterial>,

    error_marker_mat: Handle<ColorMaterial>,
    error_marker_zero_mat: Handle<ColorMaterial>,
}

impl FromWorld for ArrowMaterialResource {
    fn from_world(world: &mut World) -> Self {
        let asset_server = world.get_resource::<AssetServer>().unwrap();
        let arrow_handle = asset_server.load("arrow.png");
        let receptor_handle = asset_server.load("arrow_border.png");

        let mut materials = world.get_resource_mut::<Assets<ColorMaterial>>().unwrap();
        ArrowMaterialResource {
            arrow_4_mat: materials.add(ColorMaterial {
                texture: Some(arrow_handle.clone()),
                color: Color::rgb(1.0, 0.0, 0.0),
            }),
            arrow_8_mat: materials.add(ColorMaterial {
                texture: Some(arrow_handle.clone()),
                color: Color::rgb(0.0, 0.0, 1.0),
            }),
            arrow_12_mat: materials.add(ColorMaterial {
                texture: Some(arrow_handle.clone()),
                color: Color::rgb(0.67, 0.0, 1.0),
            }),
            arrow_16_mat: materials.add(ColorMaterial {
                texture: Some(arrow_handle.clone()),
                color: Color::rgb(1.0, 0.87, 0.0),
            }),
            arrow_other_mat: materials.add(ColorMaterial {
                texture: Some(arrow_handle),
                color: Color::rgb(0.5, 0.5, 0.5),
            }),
            receptor_mat: materials.add(receptor_handle.into()),

            error_marker_mat: materials.add(ColorMaterial {
                texture: None,
                color: Color::rgb(0.7, 0.7, 0.7),
            }),
            error_marker_zero_mat: materials.add(ColorMaterial {
                texture: None,
                color: Color::rgb(1.0, 1.0, 1.0),
            }),
        }
    }
}

#[derive(Clone, Copy, Default)]
struct HitError {
    time_hit: f64,
    error: f32,
}

struct ErrorMarkersResource {
    errors: [HitError; 100],
    next: usize,
}

impl Default for ErrorMarkersResource {
    fn default() -> Self {
        ErrorMarkersResource {
            errors: [HitError::default(); 100],
            next: 0,
        }
    }
}

/// Arrow component
#[derive(Component)]
struct Arrow {
    direction: ArrowDirection,
}

/// Receptor component
#[derive(Component)]
struct Receptor;

/// Error marker component
#[derive(Component)]
struct ErrorMarker(usize);

fn spawn_arrows(
    mut commands: Commands,
    mut song_config: ResMut<SongConfig>,
    materials: Res<ArrowMaterialResource>,
    time: Res<Time>,
) {
    let secs = time.seconds_since_startup() - crate::START_DELAY;
    let secs_last_frame = secs - time.delta_seconds_f64();

    // Counter of how many arrows we need to spawn and remove from the list
    let mut remove_counter = 0;
    for arrow in &song_config.arrows {
        if secs_last_frame < arrow.spawn_time && arrow.spawn_time < secs {
            remove_counter += 1;

            let material = match song_config.timing_point_at_time(arrow.note_time) {
                Some(timing_point) => {
                    let time_since_point = arrow.note_time - timing_point.start_time;
                    if time_since_point % timing_point.beat_length < 0.001 {
                        materials.arrow_4_mat.clone()
                    } else if time_since_point % (timing_point.beat_length * (4.0 / 8.0)) < 0.001 {
                        materials.arrow_8_mat.clone()
                    } else if time_since_point % (timing_point.beat_length * (4.0 / 12.0)) < 0.001 {
                        materials.arrow_12_mat.clone()
                    } else if time_since_point % (timing_point.beat_length * (4.0 / 16.0)) < 0.001 {
                        materials.arrow_16_mat.clone()
                    } else {
                        materials.arrow_other_mat.clone()
                    }
                }
                None => { materials.arrow_other_mat.clone() },
            };

            let mut transform =
                Transform::from_translation(Vec3::new(arrow.direction.x(), ARROW_SPAWN_Y, 1.0));
            transform.rotate(Quat::from_rotation_z(arrow.direction.rotation_rad()));

            commands
                .spawn_bundle(SpriteBundle {
                    material,
                    sprite: Sprite::new(Vec2::new(140.0, 140.0)),
                    transform,
                    ..Default::default()
                })
                .insert(Arrow {
                    direction: arrow.direction,
                });
        } else {
            break;
        }
    }

    // Remove the arrows we have spawned from the list
    for _ in 0..remove_counter {
        song_config.arrows.remove(0);
    }
}

fn move_arrows(mut query: Query<&mut Transform, With<Arrow>>, time: Res<Time>) {
    for mut transform in query.iter_mut() {
        transform.translation.y += time.delta_seconds() * ARROW_SPEED;
    }
}

fn despawn_arrows(
    query: Query<(Entity, &Transform, &Arrow)>,
    mut commands: Commands,
    key_input: Res<Input<KeyCode>>,
    mut score: ResMut<ScoreResource>,
    mut error_markers: ResMut<ErrorMarkersResource>,
    time: Res<Time>,
) {
    for (entity, transform, arrow) in query.iter() {
        let pos = transform.translation.y;

        // Check if it was hit in the timing window
        if (RECEPTOR_Y - THRESHOLD..=RECEPTOR_Y + THRESHOLD).contains(&pos)
            && arrow.direction.key_just_pressed(&key_input)
        {
            commands.entity(entity).despawn();
            score.add_hit((pos - RECEPTOR_Y).abs());

            // Update an error marker
            let error_next = error_markers.next;
            let mut error = &mut error_markers.errors[error_next];
            error.time_hit = time.seconds_since_startup();
            error.error = pos - RECEPTOR_Y;

            error_markers.next += 1;
            if error_markers.next >= error_markers.errors.len() {
                error_markers.next = 0;
            }
        }

        // Despawn arrows when they go off screen
        if pos > 800.0 {
            commands.entity(entity).despawn();
            score.add_miss();
        }
    }
}

fn setup_receptors(mut commands: Commands, materials: Res<ArrowMaterialResource>) {
    for dir in [
        ArrowDirection::Left,
        ArrowDirection::Down,
        ArrowDirection::Up,
        ArrowDirection::Right,
    ] {
        let mut transform = Transform::from_translation(Vec3::new(dir.x(), RECEPTOR_Y, 1.0));
        transform.rotate(Quat::from_rotation_z(dir.rotation_rad()));

        commands
            .spawn_bundle(SpriteBundle {
                material: materials.receptor_mat.clone(),
                sprite: Sprite::new(Vec2::new(140.0, 140.0)),
                transform,
                ..Default::default()
            })
            .insert(Receptor);
    }
}

fn setup_error_markers(mut commands: Commands, materials: Res<ArrowMaterialResource>) {
    for i in 0..100 {
        commands
            .spawn_bundle(SpriteBundle {
                material: materials.error_marker_mat.clone(),
                sprite: Sprite::new(Vec2::new(4.0, 32.0)),
                transform: Transform::from_xyz(0.0, 0.0, 4.0),
                ..Default::default()
            })
            .insert(ErrorMarker(i));
    }

    // The zero point
    commands
        .spawn_bundle(SpriteBundle {
            material: materials.error_marker_zero_mat.clone(),
            sprite: Sprite::new(Vec2::new(4.0, 32.0)),
            transform: Transform::from_xyz(0.0, 0.0, 5.0),
            ..Default::default()
        });
}

fn update_error_markers(
    mut query: Query<(&mut Transform, &ErrorMarker)>,
    error_markers: ResMut<ErrorMarkersResource>,
    time: Res<Time>,
) {
    for (mut trans, marker) in query.iter_mut() {
        let marker = error_markers.errors[marker.0];
        trans.translation.x = marker.error * 3.0;
        trans.scale.y = f32::max(0.0, 1.0 - (time.seconds_since_startup() - marker.time_hit) as f32);
    }
}

pub struct ArrowPlugin;
impl Plugin for ArrowPlugin {
    fn build(&self, app: &mut App) {
        app
            .init_resource::<ArrowMaterialResource>()
            .init_resource::<ErrorMarkersResource>()
            .add_startup_system(setup_receptors.system())
            .add_startup_system(setup_error_markers.system())
            .add_system(spawn_arrows.system())
            .add_system(despawn_arrows.system())
            .add_system(move_arrows.system())
            .add_system(update_error_markers.system());
    }
}
