mod arrow;
mod arrow_direction;
mod audio;
mod osu_parser;
mod score;
mod song_config;
mod ui;
mod shader;

use bevy::input::system::exit_on_esc_system;
use bevy::prelude::*;
use bevy_kira_audio::AudioPlugin;
use shader::background::BackgroundPlugin;

pub const START_DELAY: f64 = 5.0;

fn main() {
    App::new()
        .insert_resource(Msaa { samples: 4 })
        // .insert_resource(ClearColor(Color::rgb_u8(21, 21, 21)))
        .insert_resource(WindowDescriptor {
            title: "Bevy Rhythm".to_owned(),
            width: 1280.0,
            height: 720.0,
            vsync: false,
            resizable: true,
            ..Default::default()
        })
        .init_resource::<score::ScoreResource>()
        .add_plugins(DefaultPlugins)
        .add_plugin(AudioPlugin)
        .add_plugin(arrow::ArrowPlugin)
        .add_plugin(ui::UiPlugin)
        .add_plugin(audio::AudioPlugin)
        .add_plugin(BackgroundPlugin)
        .add_startup_system(setup.system())
        .add_system(exit_on_esc_system.system())
        .run();
}

fn setup(mut commands: Commands, asset_server: Res<AssetServer>) {
    let mut camera = OrthographicCameraBundle::new_2d();
    camera.transform = Transform::from_translation(Vec3::new(0.0, 0.0, 5.0));
    commands.spawn_bundle(camera);

    commands.spawn_bundle(UiCameraBundle::default());

    let config = osu_parser::parse(
        &*asset_server,
        &std::fs::read_to_string("/home/alice/.local/share/wineprefixes/osu/drive_c/users/alice/Local Settings/Application Data/osu!/Songs/205036 Alex Goot, Kurt Schneider, and Chrissy Costanza - Counting Stars (Ni/Alex Goot, Kurt Schneider, and Chrissy Costanza - Counting Stars (Nightcore Mix) (DrawdeX) [4K NM].osu").unwrap(),
    "/home/alice/.local/share/wineprefixes/osu/drive_c/users/alice/Local Settings/Application Data/osu!/Songs/205036 Alex Goot, Kurt Schneider, and Chrissy Costanza - Counting Stars (Ni/Nightcore - Counting Stars.mp3",
    ).unwrap();
    // let config = osu_parser::parse(
        // &*asset_server,
        // &std::fs::read_to_string("/home/alice/.local/share/wineprefixes/osu/drive_c/users/alice/Local Settings/Application Data/osu!/Songs/650415 MAZARE - Mazare Party/MAZARE - Mazare Party (Insp1r3) [4K Easy].osu").unwrap(),
    // "/home/alice/.local/share/wineprefixes/osu/drive_c/users/alice/Local Settings/Application Data/osu!/Songs/650415 MAZARE - Mazare Party/audio.mp3",
    // ).unwrap();
    commands.insert_resource(config);
}
