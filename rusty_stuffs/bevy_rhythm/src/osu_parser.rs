use bevy::{asset::AssetPath, prelude::*};

use crate::{arrow_direction::ArrowDirection, song_config::{ArrowTime, SongConfig, TimingPoint}};

#[derive(Debug)]
pub enum ParseError {
    MissingSection,
    MalformedData,
}

pub fn parse<'a, P: Into<AssetPath<'a>>>(asset_server: &AssetServer, content: &str, song_file: P) -> Result<SongConfig, ParseError> {
    let mut lines = content.lines();
    let mut line;

    // TODO: KLUDGE: This expects [TimingPoints] before [HitObjects]

    loop {
        match lines.next() {
            Some("[TimingPoints]") => break,
            Some(_) => continue,
            None => return Err(ParseError::MissingSection),
        };
    }

    let mut timing_points = Vec::new();
    loop {
        line = lines.next();
        let line = match line {
            Some(line) => line.trim(),
            None => break,
        };

        // If we run into a new section
        if line.starts_with('[') {
            break;
        }

        // Skip empty lines
        if line.is_empty() {
            continue;
        }

        let mut split = line.split(',');
        let time_ms = split.next().ok_or(ParseError::MalformedData)?;
        let beat_length_ms = split.next().ok_or(ParseError::MalformedData)?;
        split.next().ok_or(ParseError::MalformedData)?; // meter
        split.next().ok_or(ParseError::MalformedData)?; // sampleSet
        split.next().ok_or(ParseError::MalformedData)?; // sampleIndex
        split.next().ok_or(ParseError::MalformedData)?; // volume
        let uninherited = split.next().ok_or(ParseError::MalformedData)?;
        if uninherited == "0" {
            continue;
        }

        let start_time = time_ms
            .parse::<f64>()
            .or(Err(ParseError::MalformedData))? / 1000.0;
        let beat_length = beat_length_ms
            .parse::<f64>()
            .or(Err(ParseError::MalformedData))? / 1000.0;

        timing_points.push(TimingPoint {
            start_time,
            beat_length,
        });
    }

    loop {
        match line {
            Some("[HitObjects]") => break,
            Some(_) => line = lines.next(),
            None => return Err(ParseError::MissingSection),
        };
    }

    let mut arrows = Vec::new();
    loop {
        line = lines.next();
        let line = match line {
            Some(line) => line.trim(),
            None => break,
        };

        // If we run into a new section
        if line.starts_with('[') {
            break;
        }

        // Skip empty lines
        if line.is_empty() {
            continue;
        }

        let mut split = line.split(',');
        let x = split.next().ok_or(ParseError::MalformedData)?;
        split.next().ok_or(ParseError::MalformedData)?; // Y
        let time_ms = split.next().ok_or(ParseError::MalformedData)?;

        let x = x.parse::<f32>().or(Err(ParseError::MalformedData))?;
        let time_ms = time_ms
            .parse::<f64>()
            .or(Err(ParseError::MalformedData))?;

        let x = x / 512.0;
        let direction = if x < 0.25 {
            ArrowDirection::Left
        } else if x < 0.5 {
            ArrowDirection::Down
        } else if x < 0.75 {
            ArrowDirection::Up
        } else {
            ArrowDirection::Right
        };

        arrows.push(ArrowTime::new(time_ms / 1000.0, direction));
    }

    Ok(SongConfig {
        arrows,
        timing_points,
        audio: asset_server.load(song_file),
    })
}
