use bevy::{prelude::*, reflect::TypeUuid, render::{pipeline::{PipelineDescriptor, RenderPipeline}, render_graph, render_graph::{RenderGraph, RenderResourcesNode}, renderer::RenderResources, shader::{ShaderStage, ShaderStages}}, window::WindowResized};

use crate::song_config::SongConfig;

#[derive(Component)]
pub struct Background;

/// Inputs passed to shaders
#[derive(Component, RenderResources, TypeUuid)]
#[uuid = "0320b9b8-b3a3-4baa-8bfa-c94008177b17"]
pub struct ShaderInputs {
    time: f32,
    time_since_last_beat: f32,
}

impl Default for ShaderInputs {
    fn default() -> Self {
        ShaderInputs {
            time: 0.0,
            time_since_last_beat: 9999.0,
        }
    }
}

/// Updates time in ShaderInputs every frame
fn update_time(
    mut query: Query<&mut ShaderInputs>,
    time: Res<Time>,
    song_config: Res<SongConfig>,
) {
    let time = time.seconds_since_startup();
    let song_time = time - crate::START_DELAY;

    let time_since_last_beat = match song_config.timing_point_at_time(song_time) {
        Some(timing_point) => {
            let time_since_point = song_time - timing_point.start_time;
            time_since_point % timing_point.beat_length
        }
        None => { 9999.0 },
    };

    for mut node in query.iter_mut() {
        node.time = time as f32;
        node.time_since_last_beat = time_since_last_beat as f32;
    }
}


/// Adds ShaderInputs as an edge in the render graph
fn setup_render_graph(mut render_graph: ResMut<RenderGraph>) {
    render_graph.add_system_node("inputs", RenderResourcesNode::<ShaderInputs>::new(true));
    render_graph
        .add_node_edge("inputs", render_graph::base::node::MAIN_PASS)
        .unwrap();
}

fn setup_background(
    mut commands: Commands,
    mut pipelines: ResMut<Assets<PipelineDescriptor>>,
    mut shaders: ResMut<Assets<Shader>>,
    window: Res<WindowDescriptor>,
) {
    // Create a shader pipeline
    let pipeline_handle = pipelines.add(PipelineDescriptor::default_config(ShaderStages {
        vertex: shaders.add(Shader::from_glsl(
            ShaderStage::Vertex,
            include_str!("background.vert"),
        )),
        fragment: Some(shaders.add(Shader::from_glsl(
            ShaderStage::Fragment,
            include_str!("background.frag"),
        ))),
    }));

    commands
        .spawn_bundle(SpriteBundle {
            render_pipelines: RenderPipelines::from_pipelines(vec![RenderPipeline::new(pipeline_handle)]),
            transform: Transform::from_scale(Vec3::new(window.width, window.height, 1.0)),
            ..Default::default()
        })
        .insert(Background)
        .insert(ShaderInputs::default());
}

/// Resizes background when window is resized
fn update_background_size(
    mut event_reader: EventReader<WindowResized>,
    mut background: Query<(&mut Transform, With<Background>)>,
) {
    for event in event_reader.iter() {
        for (mut transform, _) in background.iter_mut() {
            transform.scale = Vec3::new(event.width, event.height, 1.0);
        }
    }
}

pub struct BackgroundPlugin;

impl Plugin for BackgroundPlugin {
    fn build(&self, app: &mut App) {
        app
            .add_asset::<ShaderInputs>()
            .add_startup_system(setup_render_graph.system())
            .add_system(update_time.system())
            .add_startup_system(setup_background.system())
            .add_system(update_background_size.system());
    }
}
