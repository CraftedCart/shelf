#version 450

layout(location = 0) in vec4 v_Position;
layout(location = 1) in vec2 v_Uv;

layout(location = 0) out vec4 o_Target;

layout(set = 2, binding = 0) uniform ShaderInputs_time { float time; };
layout(set = 2, binding = 1) uniform ShaderInputs_time_since_last_beat { float time_since_last_beat; };

float rand(vec2 c){
    return fract(sin(dot(c.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

#define PI 3.14159265358979323846

float noise(vec2 p, float freq ){
    float unit = 1.0/freq;
    vec2 ij = floor(p/unit);
    vec2 xy = mod(p,unit)/unit;
    //xy = 3.*xy*xy-2.*xy*xy*xy;
    xy = .5*(1.-cos(PI*xy));
    float a = rand((ij+vec2(0.,0.)));
    float b = rand((ij+vec2(1.,0.)));
    float c = rand((ij+vec2(0.,1.)));
    float d = rand((ij+vec2(1.,1.)));
    float x1 = mix(a, b, xy.x);
    float x2 = mix(c, d, xy.x);
    return mix(x1, x2, xy.y);
}

float perlin_noise(vec2 p, int res){
    float persistance = .5;
    float n = 0.;
    float normK = 0.;
    float f = 4.;
    float amp = 1.;
    int iCount = 0;
    for (int i = 0; i<50; i++){
        n+=amp*noise(p, f);
        f*=2.;
        normK+=amp;
        amp*=persistance;
        if (iCount == res) break;
        iCount++;
    }
    float nf = n/normK;
    return nf*nf*nf*nf;
}

void main() {
    vec3 col = vec3(
        perlin_noise((v_Uv                   + vec2(time * 0.1)) * 0.1, 20),
        perlin_noise((v_Uv + vec2(50.0, 0.0) - vec2(time * 0.1)) * 0.1, 20) * max(0.0, (2.0 - time_since_last_beat) / 2.0),
        perlin_noise((v_Uv + vec2(0.0, 50.0) + vec2(time * 0.1)) * 0.1, 20)
    ) * 0.5;

    // col += 0.01 * max(0.0, 0.5 - time_since_last_beat);

    o_Target = vec4(col, 1.0);
}
