use crate::arrow_direction::ArrowDirection;
use bevy::prelude::*;
use bevy_kira_audio::AudioSource;

#[derive(Clone, Copy)]
pub struct ArrowTime {
    pub spawn_time: f64,
    pub note_time: f64,
    pub direction: ArrowDirection,
}

pub struct TimingPoint {
    pub start_time: f64,
    pub beat_length: f64,
}

pub struct SongConfig {
    /// Must be sorted by spawn_time
    pub arrows: Vec<ArrowTime>,
    /// Must be sorted by start_time
    pub timing_points: Vec<TimingPoint>,
    pub audio: Handle<AudioSource>,
}

impl ArrowTime {
    pub fn new(note_time: f64, direction: ArrowDirection) -> ArrowTime {
        Self {
            spawn_time: note_time
                - (crate::arrow::SPAWN_DISTANCE_FROM_RECEPTOR / crate::arrow::ARROW_SPEED) as f64,
            note_time,
            direction,
        }
    }
}

impl SongConfig {
    pub fn timing_point_at_time(&self, time: f64) -> Option<&TimingPoint> {
        for point in self.timing_points.iter().rev() {
            if point.start_time <= time {
                return Some(point);
            }
        }

        None
    }
}
