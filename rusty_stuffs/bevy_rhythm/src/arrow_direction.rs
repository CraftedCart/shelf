use bevy::prelude::*;

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum ArrowDirection {
    Left,
    Down,
    Up,
    Right,
}

impl ArrowDirection {
    /// Checks if a key that corresponds to the given direction was just pressed
    pub fn key_just_pressed(&self, input: &Input<KeyCode>) -> bool {
        let key = match self {
            ArrowDirection::Left => KeyCode::D,
            ArrowDirection::Down => KeyCode::F,
            ArrowDirection::Up => KeyCode::J,
            ArrowDirection::Right => KeyCode::K,
        };

        input.just_pressed(key)
    }

    /// Returns the rotation (radians) for an arrow with the given direction
    pub fn rotation_rad(&self) -> f32 {
        match self {
            ArrowDirection::Left => std::f32::consts::PI,
            ArrowDirection::Down => -std::f32::consts::PI * 0.5,
            ArrowDirection::Up => std::f32::consts::PI * 0.5,
            ArrowDirection::Right => 0.0,
        }
    }

    /// Returns the x coordinate for an arrow with this direction
    pub fn x(&self) -> f32 {
        match self {
            ArrowDirection::Left => -150.0,
            ArrowDirection::Down => -50.0,
            ArrowDirection::Up => 50.0,
            ArrowDirection::Right => 150.0,
        }
    }
}
