use std::thread;
use std::time::Duration;
use std::sync::{mpsc, Mutex, Arc};

fn main() {
    println!("Hello, world!");

    let v = vec![1, 2, 3];

    let handle = thread::spawn(move || {
        for i in 0..10 {
            println!("Hello {} from spawned thread", i);
            thread::sleep(Duration::from_millis(1));
        }

        println!("Here's a vector: {:?}", v);
    });

    for i in 0..10 {
        println!("Hello {} from main thread", i);
        thread::sleep(Duration::from_millis(1));
    }

    handle.join().unwrap();

    //////// Channels ////////

    let (tx, rx) = mpsc::channel();
    let other_tx = tx.clone();

    thread::spawn(move || {
        let vals = vec![
            String::from("hi"),
            String::from("from"),
            String::from("the"),
            String::from("thread"),
        ];

        for val in vals {
            tx.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    thread::spawn(move || {
        let vals = vec![
            String::from("more"),
            String::from("messages"),
            String::from("for"),
            String::from("you"),
        ];

        for val in vals {
            other_tx.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    for msg in rx {
        println!("Got: {}", msg);
    }

    let counter = Arc::new(Mutex::new(0));
    let mut handles = vec![];

    for _ in 0..10 {
        let counter = Arc::clone(&counter);
        let handle = thread::spawn(move || {
            let mut num = counter.lock().unwrap();
            *num += 1;
        });
        handles.push(handle);
    }

    for h in handles {
        h.join().unwrap();
    }

    println!("Result: {}", *counter.lock().unwrap());
}
