use std::future::Future;
use std::pin::Pin;
use std::sync::{Arc, Mutex};
use std::task::{Context, Poll, Waker};
use std::time::Duration;

pub struct TimerFuture {
    shared: Arc<Mutex<TimerShared>>,
}

/// Shared state between the future and waiting thread
struct TimerShared {
    /// Whether the sleep timer has elapsed
    completed: bool,

    /// The waker for the task that `TimerFuture` is running on.
    /// The thread can use this after setting `completed = true` to tell `TimerFuture`'s task to
    /// wake up, see that `completed = true`, and move forward.
    waker: Option<Waker>,
}

impl TimerFuture {
    pub fn new(duration: Duration) -> Self {
        let shared = Arc::new(Mutex::new(TimerShared {
            completed: false,
            waker: None,
        }));

        // Start a thread to mark us as completed later
        let shared2 = shared.clone();
        std::thread::spawn(move || {
            std::thread::sleep(duration);
            let mut shared2 = shared2.lock().unwrap();
            shared2.completed = true;
            if let Some(waker) = shared2.waker.take() {
                waker.wake()
            }
        });

        TimerFuture { shared }
    }
}

impl Future for TimerFuture {
    type Output = ();

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let mut shared = self.shared.lock().unwrap();
        if shared.completed {
            Poll::Ready(())
        } else {
            shared.waker = Some(cx.waker().clone());
            Poll::Pending
        }
    }
}
