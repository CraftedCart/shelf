use std::time::Duration;
use crate::timer::TimerFuture;

mod timer;
mod executor;

fn main() {
    let (executor, spawner) = executor::new();

    spawner.spawn(async {
        println!("mrowl!");
        TimerFuture::new(Duration::from_secs_f32(2.5)).await;
        println!("mew!");
    });

    // Drop the spawner so that our executor knows it is finished and won't
    // receive more incoming tasks to run.
    drop(spawner);

    executor.run();
}
