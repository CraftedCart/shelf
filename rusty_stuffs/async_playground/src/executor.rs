use std::future::Future;
use std::sync::{Arc, mpsc, Mutex};
use std::sync::mpsc::{Receiver, SyncSender};
use std::task::Context;
use futures::future::BoxFuture;
use futures::FutureExt;
use futures::task::{ArcWake, waker_ref};

pub struct Executor {
    ready_queue: Receiver<Arc<Task>>,
}

#[derive(Clone)]
pub struct Spawner {
    task_sender: SyncSender<Arc<Task>>,
}

/// A future that can reschedule itself
struct Task {
    /// Mutex is not needed here since we only have one thread running tasks at once - this could be
    /// `UnsafeCell` instead.
    future: Mutex<Option<BoxFuture<'static, ()>>>,

    task_sender: SyncSender<Arc<Task>>,
}

pub fn new() -> (Executor, Spawner) {
    let (task_sender, ready_queue) = mpsc::sync_channel(10000);
    (Executor { ready_queue }, Spawner { task_sender })
}

impl Spawner {
    pub(crate) fn spawn(&self, future: impl Future<Output = ()> + 'static + Send) {
        let future = future.boxed();
        let task = Arc::new(Task {
            future: Mutex::new(Some(future)),
            task_sender: self.task_sender.clone(),
        });
        self.task_sender.send(task).expect("too many tasks queued");
    }
}

impl ArcWake for Task {
    fn wake_by_ref(arc_self: &Arc<Self>) {
        // Waking the task will throw it back into the ready queue
        arc_self.task_sender.send(arc_self.clone()).expect("too many tasks queued")
    }
}

impl Executor {
    pub(crate) fn run(&self) {
        while let Ok(task) = self.ready_queue.recv() {
            // Take the future and poll it if it's not yet complete (it is `Some`)
            let mut future_opt = task.future.lock().unwrap();
            if let Some(mut future) = future_opt.take() {
                let waker = waker_ref(&task);
                let mut context = Context::from_waker(&waker);
                if future.as_mut().poll(&mut context).is_pending() {
                    // We're not done with the future, put it back in the task to be re-run later
                    *future_opt = Some(future);
                }
            }
        }
    }
}
