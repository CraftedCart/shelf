use std::error::Error;
use std::fs;
use std::env;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn config_new_valid() {
        let args = vec![
            String::from("minigrep"),
            String::from("butter"),
            String::from("toast.txt"),
        ];
        let config = Config::new(&args);

        assert!(config.is_ok(), "Config failed to parse");
    }

    #[test]
    fn config_new_not_enough_args() {
        let args = vec![
            String::from("minigrep"),
            String::from("butter"),
        ];
        let config = Config::new(&args);

        assert!(config.is_err(), "Config with not enough args parsed without returning Err");
    }

    #[test]
    fn config_new_too_many_args() {
        let args = vec![
            String::from("minigrep"),
            String::from("butter"),
            String::from("toast.txt"),
            String::from("bread.txt"),
            String::from("-i"),
        ];
        let config = Config::new(&args);

        assert!(config.is_err(), "Config with too many args parsed without returning Err");
    }

    #[test]
    fn run_non_existent_file() {
        let config = Config {
            query: String::from("butter"),
            filename: String::from("the_bread_isnt_here.txt"),
            case_sensitive: true,
        };

        assert!(run(config).is_err(), "run() didn't return Err with a non-existent file");
    }

    #[test]
    fn one_result_case_sensitive() {
        let query = "duct";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.
Duct tape.";

        assert_eq!(
            vec!["safe, fast, productive."],
            search(query, contents)
        )
    }

    #[test]
    fn one_result_case_insensitive() {
        let query = "rUsT";
        let contents = "\
        Rust:
safe, fast, productive.
Pick three.
Trust me.";

        assert_eq!(
            vec!["Rust:", "Trust me."],
            search_case_insensitive(query, contents)
        )
    }

    #[test]
    fn two_results_case_sensitive() {
        let query = ".";
        let contents = "\
        Rust:
safe, fast, productive.
Pick three.";

        assert_eq!(
            vec!["safe, fast, productive.", "Pick three."],
            search(query, contents)
        )
    }
}

pub struct Config {
    pub query: String,
    pub filename: String,
    pub case_sensitive: bool,
}

impl Config {
    pub fn new(mut args: std::env::Args) -> Result<Config, &'static str> {
        args.next(); // Skip over the executable name

        let query = match args.next() {
            Some(arg) => arg,
            None => return Err("Didn't get a query string")
        };

        let filename = match args.next() {
            Some(arg) => arg,
            None => return Err("Didn't get a file name")
        };

        let case_sensitive = match args.next() {
            Some(arg) => {
                if arg == "-i" {
                    false
                } else {
                    env::var("CASE_INSENSITIVE").is_err()
                }
            },
            None => env::var("CASE_INSENSITIVE").is_err()
        };

        Ok(Config { query, filename, case_sensitive })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;

    let results = if config.case_sensitive {
        search(&config.query, &contents)
    } else {
        search_case_insensitive(&config.query, &contents)
    };

    for line in results {
        println!("{}", line);
    }

    Ok(())
}

fn search<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    contents.lines()
        .filter(|line| line.contains(query))
        .collect()
}

fn search_case_insensitive<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    let query = query.to_lowercase();

    contents.lines()
        .filter(|line| line.to_lowercase().contains(&query))
        .collect()
}
