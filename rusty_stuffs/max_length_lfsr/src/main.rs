mod lfsr;

use minifb::{Key, Window, WindowOptions};
use crate::lfsr::Lfsr;

const GRID_SIZE: usize = 512;

fn main() {
    let mut buffer: Vec<u32> = vec![0; GRID_SIZE * GRID_SIZE];

    let mut window = Window::new(
        "Max length LFSR - ESC to exit",
        GRID_SIZE,
        GRID_SIZE,
        WindowOptions::default(),
    )
        .unwrap_or_else(|e| {
            panic!("{}", e);
        });

    // Limit to max ~60 fps update rate
    window.limit_update_rate(Some(std::time::Duration::from_micros(16600)));

    // Tap numbers from http://users.ece.cmu.edu/~koopman/lfsr/
    let mut lfsr = Lfsr::new(1, 0x204D2);

    while window.is_open() && !window.is_key_down(Key::Escape) {
        for _ in 0..1024 {
            let next_val = lfsr.next() as usize;
            buffer[next_val] ^= 0xFFFFFFFF;

            // Special case for index 0, since an LFSR will never produce the value 0
            if next_val == 1 {
                buffer[0] ^= 0xFFFFFFFF;
            }
        }

        window
            .update_with_buffer(&buffer, GRID_SIZE, GRID_SIZE)
            .unwrap();
    }
}
