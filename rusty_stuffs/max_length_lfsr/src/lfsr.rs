pub struct Lfsr {
    val: u32,
    taps: u32,
}

impl Lfsr {
    pub fn new(initial_state: u32, taps: u32) -> Self {
        Self {
            val: initial_state,
            taps,
        }
    }

    pub fn next(&mut self) -> u32 {
        let first_bit = self.val & 1;
        self.val = self.val >> 1;

        // if first_bit != 0 {
        //     self.val ^= self.taps;
        // }

        // Abuse two's complement for less assembly and eliminating jump instructions
        // Negating +1 gives us -1, which is all ones in binary
        // Then, bitwise and it with our taps value
        let taps = self.taps & ((-(first_bit as i32)) as u32);
        self.val ^= taps;

        self.val
    }
}
