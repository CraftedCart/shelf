use std::{time::Instant, fs, path::PathBuf};
use rayon::prelude::*;

fn main() {
    write_sync();
    // write_async();
}

fn write_sync() {
    let data = make_data();
    let paths = make_paths();

    let start = Instant::now();
    for path in paths {
        fs::write(path, &data).unwrap();
    }
    let end = Instant::now();
    let diff = end - start;
    println!("Time taken: {diff:?}");
}

fn write_async() {
    let data = make_data();
    let paths = make_paths();

    let start = Instant::now();
    paths.into_par_iter().for_each(|path| {
        fs::write(path, &data).unwrap();
    });
    let end = Instant::now();
    let diff = end - start;
    println!("Time taken: {diff:?}");
}

/// Make 100k of some data
fn make_data() -> Vec<u8> {
    let mut data = Vec::with_capacity(100_000);
    for i in 0..100_000 {
        data.push((i % 256) as u8);
    }

    data
}

/// Make 160 paths to write to
fn make_paths() -> Vec<PathBuf> {
    let mut data = Vec::with_capacity(160);
    for i in 0..160 {
        data.push(std::env::current_dir().unwrap().join("temp").join(format!("{i}")));
    }

    data
}
