fn main() {
    let native_options = eframe::NativeOptions::default();
    eframe::run_native(
        "egui playground",
        native_options,
        Box::new(|cc| Box::new(PlaygroundApp::new(cc))),
    );
}

struct PlaygroundApp {}

impl PlaygroundApp {
    /// Called once before the first frame.
    pub fn new(_cc: &eframe::CreationContext<'_>) -> Self {
        PlaygroundApp {}
    }
}

impl eframe::App for PlaygroundApp {
    /// Called by the frame work to save state before shutdown.
    fn save(&mut self, _storage: &mut dyn eframe::Storage) {
    }

    /// Called each time the UI needs repainting, which may be many times per second.
    /// Put your widgets into a `SidePanel`, `TopPanel`, `CentralPanel`, `Window` or `Area`.
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::Window::new("Window").show(ctx, |ui| {
            egui::TopBottomPanel::top("menu_panel").resizable(false).show_inside(ui, |ui| {
                ui.label("Menu bar");
            });

            egui::SidePanel::left("left_side_panel").resizable(true).show_inside(ui, |ui| {
                egui::TopBottomPanel::top("tree_panel").resizable(true).show_inside(ui, |ui| {
                    ui.heading("Tree view");
                });

                ui.heading("Inspector view");
            });

            egui::CentralPanel::default().show_inside(ui, |ui| {
                ui.heading("3D view");
            });
        });
    }
}
