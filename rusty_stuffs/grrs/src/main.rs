use std::io;
use std::io::Write;
use std::fs::File;
use std::io::BufReader;
use std::io::BufWriter;
use std::path::PathBuf;

use structopt::StructOpt;
use failure::ResultExt;
use exitfailure::ExitFailure;

/// Search for a pattern in a file and display the lines that contain it.
#[derive(StructOpt)]
struct Cli {
    /// The pattern to look for
    pattern: String,

    /// The path to the file to read
    #[structopt(parse(from_os_str))]
    path: PathBuf,
}

fn main() -> Result<(), ExitFailure> {
    let args = Cli::from_args();

    let f = File::open(&args.path)
        .with_context(|_| format!("could not read file `{}`", &args.path.to_string_lossy()))?;
    let reader = BufReader::new(f);

    let stdout = io::stdout();
    let mut handle = BufWriter::new(stdout.lock());

    grrs::find_matches(reader, &args.pattern, |line| {
        writeln!(handle, "{}", line)?;
        Ok(())
    })?;

    Ok(())
}
