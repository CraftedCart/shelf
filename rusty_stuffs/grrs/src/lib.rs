use std::io::BufRead;

use exitfailure::ExitFailure;

#[test]
fn find_a_match() {
    let str = "Hello, world\nAlice is trans";

    let mut matches = Vec::new();
    find_matches(str.as_bytes(), "Hello", |line| {
        matches.push(String::from(line));
        Ok(())
    }).unwrap();

    assert_eq!(matches.len(), 1);
    assert_eq!(matches[0], "Hello, world");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

pub fn find_matches<F>(reader: impl BufRead, pattern: &str, mut line_callback: F) -> Result<(), ExitFailure>
where F : FnMut(&str) -> Result<(), ExitFailure> {
    for res in reader.lines() {
        let line;
        match res {
            Ok(content) => line = content,
            Err(_) => continue,
        }
        if line.contains(pattern) {
            line_callback(&line)?;
        }
    }

    Ok(())
}
