use ferris_says::say;
use std::collections::HashMap;
use std::io::{stdout, BufWriter};

#[derive(Debug)]
struct Rect {
    width: u32,
    height: u32,
}

impl Rect {
    fn new(width: u32, height: u32) -> Rect {
        Rect { width, height }
    }

    fn square(size: u32) -> Rect {
        Rect {
            width: size,
            height: size,
        }
    }

    fn area(&self) -> u32 {
        self.width * self.height
    }

    fn can_hold(&self, other: &Rect) -> bool {
        self.width > other.width && self.height > other.height
    }
}

fn main() {
    let stdout = stdout();
    let out = b"Hello fellow Rustaceans!";
    let width = 24;

    let mut writer = BufWriter::new(stdout.lock());
    say(out, width, &mut writer).unwrap();

    println!("Hello, world");

    let a = [1, 2, 3, 4, 5];
    let slice = &a[1..3];

    println!("Hey {}", slice[0]);
    println!("Hey {}", slice[1]);

    calc_area();
    enuuuuuuuuuuums();
    collections();
}

fn calc_area() {
    let rect1 = Rect::new(30, 50);
    let rect2 = Rect::new(10, 40);
    let rect3 = Rect::new(30, 50);
    let sq = Rect::square(20);

    println!("The area of {:?} is {}", &rect1, rect1.area());

    println!("Can rect1 hold rect2? {}", rect1.can_hold(&rect2));
    println!("Can rect1 hold rect3? {}", rect1.can_hold(&rect3));
    println!("Can rect1 hold sq? {}", rect1.can_hold(&sq));
}

enum IpAddr {
    V4(u8, u8, u8, u8),
    V6(String),
}

fn enuuuuuuuuuuums() {
    let home = IpAddr::V4(127, 0, 0, 1);
    let loopback = IpAddr::V6(String::from("::1"));

    route(home);
    route(loopback);

    // let some_number = Some(5);
    // let some_string = Some("a string");
    // let absent_number: Option<i32> = None;

    let x = 5;
    let y = Some(5);

    match y {
        Some(y) => println!("{}", x + y),
        None => println!("y is None!"),
    }
}

fn route(_ip_type: IpAddr) {}

fn collections() {
    let mut v: Vec<i32> = Vec::new();
    v.push(5);
    v.push(6);
    v.push(7);
    v.push(8);

    let mut v = vec![1, 2, 3, 4];
    for val in &v {
        println!("{}", val);
    }

    v.push(27);

    for val in &mut v {
        *val *= 4;
    }

    for val in &v {
        println!("{}", val);
    }

    let s1 = String::from("Hello");
    let s2 = String::from(", world!");

    let s3 = s1.clone() + &s2;
    println!("{}", s1);
    println!("{}", s3);

    let mut scores = HashMap::new();
    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Pink"), 50);

    if let Some(score) = scores.get("Pink") {
        println!("{}", score);
    } else {
        println!("Not a team!");
    }

    for (k, v) in &scores {
        println!("{}: {}", k, v);
    }

    println!("{:?}", &scores);

    let mut some_ints = vec![
        1, 1, 1, 2, 6, 3, 7, 3, 8, 3, 8, 5, 3, 6, 7, 4, 4, 4, 6, 3, 7, 3, 0, 9, 9, 0, 0, 0, 4, 3,
        9, 9, 9,
    ];
    some_ints.sort();
    println!("{:?}", some_ints);

    let mut total = 0;
    for val in &some_ints {
        total += val;
    }
    println!("Mean: {}", total as f32 / some_ints.len() as f32);

    println!("Median-ish: {}", some_ints[(some_ints.len() + 1) / 2 - 1]);

    let mut freq = HashMap::new();
    for val in some_ints {
        let count = freq.entry(val).or_insert(0);
        *count += 1;
    }

    let mut most = (0, 0);
    for (k, v) in &freq {
        if v > &most.1 {
            most = (*k, *v);
        }
    }

    println!("Mode: {} with {}", most.0, most.1);
}
