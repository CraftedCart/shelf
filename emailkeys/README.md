emailkeys
=========

A couple of scripts to generate and send out emails for distributing keys

## How to use

1: Replace keys.txt with a list of keys you want to email out

2: Replace recipients.csv with a CSV file with an `Email` column

3: Edit `genmails.py` and replace the example email template with your own

4: Run `genmails.py` to generate a bunch of emails to send - these will be placed in the `pending` directory

5: Edit `sendmails.py`, filling in your SMTP server and email address in the CONFIG section

6: Run `sendmails.py` to start sending emails

If the connection drops during sending emails, all should be fine to run `sendmails.py` again - it won't re-send already
sent emails.
