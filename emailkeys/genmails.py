#!/usr/bin/env python3

import csv
import os


def main():
    os.mkdir("pending")

    with open("keys.txt", "r") as f:
        keys = f.readlines()

    with open("usedkeys.txt", "w") as used_f:
        with open("recipients.csv", "r") as csv_f:
            reader = csv.DictReader(csv_f)

            keys_idx = 0
            for row in reader:
                key = keys[keys_idx][:-1]
                keys_idx += 1
                used_f.write(f"{row['Email']} - {key}\n")

                with open("pending/" + str(keys_idx) + " " + row["Email"].replace("@", " at "), "w") as f:

                    f.write(f"""{row["Email"]}
Subject: Hey, here's an email

Hey, you got something fancy

{key}

woah, isn't it amazing! Replace this text here with your email template.
""")


if __name__ == "__main__":
    main()
