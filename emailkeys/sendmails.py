#!/usr/bin/env python3

import smtplib, ssl, os, time


# CONFIG #
serv = "[SMTP SERVER HERE]"
port = 587
sender_email = "[YOUR EMAIL HERE]"
pending_dir = "pending"
done_dir = "done"
# END CONFIG #


def main():
    os.mkdir(done_dir)

    password = input("Type your password and press enter: ")

    # Create a secure SSL context
    context = ssl.create_default_context()

    with smtplib.SMTP(serv, port) as server:
        print("Encrypting")
        server.ehlo()  # Can be omitted
        server.starttls(context=context)  # Secure the connection
        server.ehlo()  # Can be omitted
        print("Logging in")
        server.login(sender_email, password)

        names = os.listdir(pending_dir)
        for name in names:
            with open(pending_dir + "/" + name) as f:
                email = f.readline()[:-1]
                content = f.read()

            print(f"Sending mail to {email}")
            server.sendmail(sender_email, email, content)
            os.rename(pending_dir + "/" + name, done_dir + "/" + name)

            print("Waiting 30s until sending the next one")
            time.sleep(30)


if __name__ == "__main__":
    main()
