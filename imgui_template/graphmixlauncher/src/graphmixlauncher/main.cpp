#include "graphmix/main.hpp"

int main(int argc, char *argv[]) {
    return GraphMix::launch(argc, argv);
}
