#ifndef GRAPHMIX_GRAPHMIX_RENDER_IMGUISDLBACKEND_HPP
#define GRAPHMIX_GRAPHMIX_RENDER_IMGUISDLBACKEND_HPP

#include "graphmix_export.h"
#include <imgui.h>

struct SDL_Window;
typedef union SDL_Event SDL_Event;

namespace GraphMix::Render::ImGuiSdlBackend {
    GRAPHMIX_EXPORT bool initForOpenGL(SDL_Window *window, void *sdlGlContext);
    GRAPHMIX_EXPORT bool initForVulkan(SDL_Window *window);
    GRAPHMIX_EXPORT bool initForD3D(SDL_Window *window);
    GRAPHMIX_EXPORT bool initForMetal(SDL_Window *window);
    GRAPHMIX_EXPORT void shutdown();
    GRAPHMIX_EXPORT void newFrame(SDL_Window *window);
    GRAPHMIX_EXPORT bool processEvent(const SDL_Event *event);
}

#endif
