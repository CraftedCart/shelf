#ifndef GRAPHMIX_GRAPHMIX_RENDER_RENDERER_HPP
#define GRAPHMIX_GRAPHMIX_RENDER_RENDERER_HPP

#include "graphmix_export.h"

namespace GraphMix::Render::Renderer {
    extern float deltaSeconds;

    GRAPHMIX_EXPORT bool init();
    GRAPHMIX_EXPORT void tearDown();
    GRAPHMIX_EXPORT void execMainLoop();
}

#endif
