#ifndef GRAPHMIX_GRAPHMIX_MAIN_HPP
#define GRAPHMIX_GRAPHMIX_MAIN_HPP

#include "graphmix_export.h"

namespace GraphMix {
    /**
     * Called from the launcher
     */
    GRAPHMIX_EXPORT int launch(int argc, char *argv[]);
}

#endif
