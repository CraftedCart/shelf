#ifndef GRAPHMIX_GRAPHMIX_APP_HPP
#define GRAPHMIX_GRAPHMIX_APP_HPP

#include "graphmix_export.h"

namespace GraphMix::App {
    GRAPHMIX_EXPORT int exec(int argc, char *argv[]);
    GRAPHMIX_EXPORT void onFrame();
}

#endif

