#include "graphmix/main.hpp"
#include "graphmix/App.hpp"

namespace GraphMix {
    int launch(int argc, char *argv[]) {
        return App::exec(argc, argv);
    }
}
