#include "graphmix/render/Renderer.hpp"
#include "graphmix/render/ImGuiSdlBackend.hpp"
#include "graphmix/render/ImGuiOpenGl3Backend.hpp"
#include "graphmix/App.hpp"
#include <SDL.h>
#include <imgui.h>
#include <GL/glew.h>
#include <iostream>
#include <algorithm>

namespace GraphMix::Render::Renderer {
    // Define externs
    float deltaSeconds = 0.0f;

    static SDL_Window* window;
    static SDL_GLContext glContext;

    bool init() {
        // Setup SDL
        // (Some versions of SDL before <2.0.10 appears to have performance/stalling issues on a minority of Windows systems,
        // depending on whether SDL_INIT_GAMECONTROLLER is enabled or disabled.. updating to latest version of SDL is recommended!)
        if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_GAMECONTROLLER) != 0) {
            std::cout << "SDL_Init error: " << SDL_GetError() << std::endl;
            return false;
        }

        // Decide GL+GLSL versions
#if defined(__APPLE__)
        // GL 3.2 Core + GLSL 150
        const char* glslVersionDirective = "#version 150";
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG); // Always required on Mac
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
#else
        // GL 3.0 + GLSL 130
        const char* glslVersionDirective = "#version 130";
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, 0);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
#endif

        // Create window with graphics context
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
        SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
        SDL_WindowFlags window_flags = (SDL_WindowFlags)(SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
        window = SDL_CreateWindow("Dear ImGui SDL2+OpenGL3 example", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280, 720, window_flags);
        glContext = SDL_GL_CreateContext(window);
        SDL_GL_MakeCurrent(window, glContext);
        SDL_GL_SetSwapInterval(0); // Disable vsync

        // Initialize OpenGL loader
        bool err = glewInit() != GLEW_OK;
        if (err) {
            std::cerr << "Failed to initialize OpenGL loader!" << std::endl;
            return false;
        }

        // Setup Dear ImGui context
        IMGUI_CHECKVERSION();
        ImGui::CreateContext();
        ImGuiIO& io = ImGui::GetIO();
        io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
        //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

        io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;

        // Setup Dear ImGui style
        ImGui::StyleColorsDark();
        //ImGui::StyleColorsClassic();

        // Setup Platform/Renderer backends
        Render::ImGuiSdlBackend::initForOpenGL(window, glContext);
        Render::ImGuiOpenGl3Backend::init(glslVersionDirective);

        // Load Fonts
        // - If no fonts are loaded, dear imgui will use the default font. You can also load multiple fonts and use ImGui::PushFont()/PopFont() to select them.
        // - AddFontFromFileTTF() will return the ImFont* so you can store it if you need to select the font among multiple.
        // - If the file cannot be loaded, the function will return NULL. Please handle those errors in your application (e.g. use an assertion, or display an error and quit).
        // - The fonts will be rasterized at a given size (w/ oversampling) and stored into a texture when calling ImFontAtlas::Build()/GetTexDataAsXXXX(), which ImGui_ImplXXXX_NewFrame below will call.
        // - Read 'docs/FONTS.md' for more instructions and details.
        // - Remember that in C/C++ if you want to include a backslash \ in a string literal you need to write a double backslash \\ !
        //io.Fonts->AddFontDefault();
        //io.Fonts->AddFontFromFileTTF("../../misc/fonts/Roboto-Medium.ttf", 16.0f);
        //io.Fonts->AddFontFromFileTTF("../../misc/fonts/Cousine-Regular.ttf", 15.0f);
        //io.Fonts->AddFontFromFileTTF("../../misc/fonts/DroidSans.ttf", 16.0f);
        //io.Fonts->AddFontFromFileTTF("../../misc/fonts/ProggyTiny.ttf", 10.0f);
        //ImFont* font = io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ArialUni.ttf", 18.0f, NULL, io.Fonts->GetGlyphRangesJapanese());
        //IM_ASSERT(font != NULL);

        return true;
    }

    void tearDown() {
        Render::ImGuiOpenGl3Backend::shutdown();
        Render::ImGuiSdlBackend::shutdown();
        ImGui::DestroyContext();

        SDL_GL_DeleteContext(glContext);
        SDL_DestroyWindow(window);
        SDL_Quit();
    }

    void execMainLoop() {
        ImGuiIO& io = ImGui::GetIO();

        // Since ImGui can take a few frames to fully settle, we want to keep rendering for a few frames after the last
        // SDL_Event came in
        float secondsSinceLastInteract = 0;

        Uint32 renderNextFrameEventType = SDL_RegisterEvents(1);

        Uint64 lastTime = 0;

        // Main loop
        bool done = false;
        while (!done) {
            SDL_Event event;
            SDL_WaitEvent(&event);

            bool hadUserInteract = false;
            do {
                Render::ImGuiSdlBackend::processEvent(&event);
                if (event.type == SDL_QUIT) {
                    done = true;
                } else if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE && event.window.windowID == SDL_GetWindowID(window)) {
                    done = true;
                }

                if (event.type != renderNextFrameEventType) {
                    hadUserInteract = true;
                }
            } while (SDL_PollEvent(&event));

            Uint64 nowTime = SDL_GetTicks();
            deltaSeconds = (nowTime - lastTime) / 1000.0f;
            lastTime = nowTime;

            if (hadUserInteract) {
                secondsSinceLastInteract = 0.0f;
            } else {
                secondsSinceLastInteract += deltaSeconds;
            }

            if (secondsSinceLastInteract < 0.5f) {
                SDL_Event ev;
                ev.type = renderNextFrameEventType;
                SDL_PushEvent(&ev);
            }

            // Start the Dear ImGui frame
            Render::ImGuiOpenGl3Backend::newFrame();
            Render::ImGuiSdlBackend::newFrame(window);
            ImGui::NewFrame();

            // Call back into the main application
            App::onFrame();

            // Rendering
            ImGui::Render();
            glViewport(0, 0, (int) io.DisplaySize.x, (int) io.DisplaySize.y);
            glClearColor(0.13f, 0.13f, 0.13f, 1.0f);
            glClear(GL_COLOR_BUFFER_BIT);
            Render::ImGuiOpenGl3Backend::renderDrawData(ImGui::GetDrawData());
            SDL_GL_SwapWindow(window);

            // Manually cap the framerate
            SDL_Delay(std::max(0.0f, (1000 / 120) - (deltaSeconds * 1000)));
        }
    }
}

