#include "imgui_export.h"

#define IMGUI_API IMGUI_EXPORT

// We use SDL2
#define imgui_disable_win32_default_clipboard_functions
#define imgui_disable_win32_default_ime_functions

// Define constructor and implicit cast operators to convert back<>forth between your math types and ImVec2/ImVec4
// This will be inlined as part of ImVec2 and ImVec4 class declarations

// #define IM_VEC2_CLASS_EXTRA \
    // ImVec2(const glm::vec2& f) { x = f.x; y = f.y; } \
    // operator glm::vec2() const { return glm::vec2(x, y); }

// #define IM_VEC4_CLASS_EXTRA \
    // ImVec4(const glm::vec4& f) { x = f.x; y = f.y; z = f.z; w = f.w; }  \
    // operator glm::vec4() const { return glm::vec4(x, y, z, w); }
