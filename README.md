The shelf
=========

A place for all my things that don't have a better place to go - various scripts, unfinished projects, etc. You might
have heard of something like this being called a "junk code" repo, or a "junk drawer". Oh and also some of my really old
stuff I really really don't like any more is here too, for funsies. ;3

I make no guarantees that anything here actually works, has any good documentation, or is any good in general ;3
