set -e

ca65 src/hello_world.asm
ca65 src/reset.asm
ld65 src/hello_world.o src/reset.o -C nes.cfg -o hello_world.nes
