.include "constants.inc"

.segment "CODE"

.import main

.export reset_handler
.proc reset_handler
  sei
  cld
  ldx #%00
  stx PPU_CTRL
  stx PPU_MASK

  ; Wait until the PPU is ready
vblankwait:
  bit PPU_STATUS
  bpl vblankwait
  jmp main
.endproc
