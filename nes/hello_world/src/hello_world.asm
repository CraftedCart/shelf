.include "constants.inc"
.include "header.inc"

.segment "CODE"

.import reset_handler

.proc irq_handler
  rti
.endproc

.proc nmi_handler
  rti
.endproc

.export main
.proc main
  lda #$fe
  sta $0003

  ; Reading PPU_STATUS resets the "address latch" for the PPU_ADDR
  ; This means the next write to PPU_ADDR is guaranteeded to be the high byte, followed by the low byte
  ldx PPU_STATUS

  ; Store $29 to $3f00 in PPU memory
  ; $3f00 is where palettes begin in PPU memory, specifically the 1st color in the 1st palette
  ; $14 is purple
  ldx #$3f
  stx PPU_ADDR
  ldx #$00
  stx PPU_ADDR
  lda #$14
  sta PPU_DATA

  ; Bitflags
  ; 0: Greyscale mode enable (0: normal color, 1: greyscale)
  ; 1: Left edge (8px) background enable (0: hide, 1: show)
  ; 2: Left edge (8px) foreground enable (0: hide, 1: show)
  ; 3: Background enable
  ; 4: Foreground enable
  ; 5: Emphasize red
  ; 6: Emphasize green
  ; 7: Emphasize blue
  lda #%00011110
  sta PPU_MASK

forever:
  jmp forever
.endproc

.segment "VECTORS"
.addr nmi_handler, reset_handler, irq_handler

.segment "CHR"
.res 8192
