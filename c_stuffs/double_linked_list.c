// https://craftinginterpreters.com/introduction.html
// Challenge 3: To get some practice with pointers, define a doubly linked list of heap-allocated strings. Write
//              functions to insert, find, and delete items from it. Test them.

#include <string.h>
#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>

typedef struct StringLink {
    char* string;
    struct StringLink* prev_link;
    struct StringLink* next_link;
} StringLink;

StringLink* StringLink_new(const char* string) {
    size_t len = strlen(string) + 1; // +1 for the null terminator
    char* new_string = malloc(len);
    memcpy(new_string, string, len);

    StringLink* new_link = malloc(sizeof(StringLink));
    new_link->string = new_string;
    new_link->prev_link = NULL;
    new_link->next_link = NULL;
    return new_link;
}

StringLink* StringLink_insert(StringLink* link, const char* string) {
    size_t len = strlen(string) + 1; // +1 for the null terminator
    char* new_string = malloc(len);
    memcpy(new_string, string, len);

    StringLink* new_link = malloc(sizeof(StringLink));
    new_link->string = new_string;
    new_link->prev_link = link;
    new_link->next_link = link->next_link;

    if (link->next_link != NULL) {
        link->next_link->prev_link = new_link;
    }
    link->next_link = new_link;

    return new_link;
}

void StringLink_remove(StringLink* link) {
    if (link->prev_link != NULL) {
        link->prev_link->next_link = link->next_link;
    }
    if (link->next_link != NULL) {
        link->next_link->prev_link = link->prev_link;
    }

    free(link->string);
    free(link);
}

StringLink* StringLink_find(StringLink* link, const char* string) {
    while (link != NULL) {
        if (strcmp(link->string, string) == 0) {
            return link;
        }

        link = link->next_link;
    }

    // Not found
    return NULL;
}

void StringLink_debug_print(StringLink* link) {
    printf(
            "0x%0.16" PRIX64 " | prev 0x%0.16" PRIX64 " | next 0x%0.16" PRIX64 " | %s\n",
            (uint64_t) link,
            (uint64_t) link->prev_link,
            (uint64_t) link->next_link,
            link->string
            );
}

void StringLink_walk(StringLink* link, void (*func)(StringLink*)) {
    while (link != NULL) {
        func(link);
        link = link->next_link;
    }
}

int main(int argc, char** argv) {
    StringLink* one = StringLink_new("hello 1");
    StringLink* two = StringLink_insert(one, "hello 2");
    StringLink* three = StringLink_insert(two, "hello 3");
    StringLink* four = StringLink_insert(two, "hello 4");
    StringLink* five = StringLink_insert(two, "hello 5");

    printf("Initial state\n");
    StringLink_walk(one, &StringLink_debug_print);

    StringLink_remove(two);

    printf("After removing\n");
    StringLink_walk(one, &StringLink_debug_print);

    printf("Find 4: %p\n", StringLink_find(one, "hello 4"));
}
