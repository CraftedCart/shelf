gemini\_view
============

A weekend project to make a simple Gemini browser. It's not particularly good, sometimes text rendering goes wrong, it
doesn't verify TLS certs haven't changed since first seeing them (TOFU), it renders whatever it receives, regardless of
status code (so inputs and redirects don't work), and it crashes on bad certs, but otherwise it works. ;3

You'll need a wide terminal, the text width is fixed at 120 cols, and then there's also an outliner sidebar.

`j`/`k` to scroll, `h`/`l` to go back/forward, shift + the indicated letters to follow a link.
