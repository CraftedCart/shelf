#!/usr/bin/env ruby
# frozen_string_literal: true

require "rubygems"
require "bundler/setup"

require "curses"
require "socket"
require "openssl"
require "uri"

LINK_CHARS = "ASDFQZWXECRVGTBHYNJUMKILOP".split("")

def wrap_line(content, cols)
  para = []
  i = 0
  while i < content.length
    j = i + cols

    j -= 1 while j != content.length && j > i + 1 && !(content[j] =~ /\s/)

    para << (i...j)
    i = j + 1
  end

  if para.length == 0
    [0...0]
  else
    para
  end
end

class GemLink
  attr_reader :text
  attr_reader :target
  attr_reader :key_sequence

  # text may be nil
  def initialize(text, target)
    @text = text
    @target = target
    @key_sequence = []
  end
end

class GemLine
  attr_reader :text
  attr_accessor :display_text
  attr_accessor :heading_level
  attr_accessor :link_id

  def initialize(text)
    @text = text
    @display_text = text

    @heading_level = 0
    @is_preformatted = false
    @is_quote = false
    @link_id = nil
  end

  def preformatted?
    @is_preformatted
  end

  def preformatted=(new)
    @is_preformatted = new
  end

  def quote?
    @is_quote
  end

  def quote=(new)
    @is_quote = new
  end
end

class WrappedLine
  attr_reader :line
  attr_reader :range

  def initialize(line, range)
    @line = line
    @range = range
  end
end

class GemText
  attr_reader :lines
  attr_reader :wrapped_lines
  attr_reader :links
  attr_reader :heading_lines

  def initialize(content)
    @lines = content.lines().map(&:chomp).map { |line| GemLine.new(line) }
    @wrapped_lines = []
    @links = []
    @heading_lines = []

    # Check for syntax and stuff
    preformatted_state = false
    next_link_id = 0
    @lines.each do |line|
      old_preformatted_state = preformatted_state
      if line.text.start_with?("```")
        preformatted_state = !preformatted_state
      end

      # We want to highlight the closing ``` as preformatted too
      line.preformatted = preformatted_state || old_preformatted_state

      # Check for headings
      3.times do |i|
        if line.text[i] == "#"
          line.heading_level += 1
        else
          break
        end
      end
      if line.heading_level > 0
        @heading_lines.push(line)
      end

      # Check for links
      if line.text.start_with?("=>")
        line.link_id = next_link_id
        next_link_id += 1

        positions = line.text.enum_for(:scan, /\S+/).map { Regexp.last_match.begin(0) }
        link_target = line.text[positions[1]...positions[2]].strip
        if positions.length > 2
          # There's custom link text
          link_text = line.text[positions[2]..]
          line.display_text = "#{link_text}    (#{link_target})"
          @links.push GemLink.new(link_text, link_target)
        else
          # There's no custom link text
          line.display_text = "#{link_target}"
          @links.push GemLink.new(nil, link_target)
        end
      end

      # Check if quote
      line.quote = line.text[0] == ">"
    end

    # Give key sequences to links
    if @links.length > 0
      chars_per_link = [1, Math.log(@links.length, LINK_CHARS.length).ceil].max
      @links.each_with_index do |link, link_id|
        thing = link_id % LINK_CHARS.length.pow(chars_per_link)
        chars_per_link.times do |i|
          mod = thing % LINK_CHARS.length
          thing = thing / LINK_CHARS.length

          link.key_sequence.prepend(LINK_CHARS[mod])
        end
      end
    end
  end

  def wrap_to(cols)
    @wrapped_lines = []
    ranges_per_line = @lines.map { |line| wrap_line(line.display_text, cols) }
    ranges_per_line.each_with_index do |actual_line_ranges, line|
      actual_line_ranges.each do |wrapped_line_range|
        @wrapped_lines.push WrappedLine.new(line, wrapped_line_range)
      end
    end
  end
end

def fetch_page(url)
  sock = TCPSocket.new(url.host, 1965)

  ssl_ctx = OpenSSL::SSL::SSLContext.new
  # ssl_ctx.set_params(verify_mode: OpenSSL::SSL::VERIFY_PEER)
  # ssl_ctx.ssl_version = :TLSv1_2
  ssl_sock = OpenSSL::SSL::SSLSocket.new(sock, ssl_ctx)

  ssl_sock.sync_close = true
  ssl_sock.connect

  # p ssl_sock.peer_cert.to_der

  # ssl_sock.print("gemini://gemini.circumlunar.space/docs/specification.gmi\r\n")
  ssl_sock.print("#{url.to_s}\r\n")

  content = ssl_sock.read

  sock.close

  content
end

class Page
  attr_reader :url
  attr_reader :content
  attr_accessor :scroll

  def initialize(url, content)
    @url = url
    @content = content
    @scroll = 0
  end
end

class History
  attr_reader :history_pos

  def initialize
    @history = []
    @history_pos = -1
  end

  def push(page)
    if @history.length > 0
      @history[@history_pos + 1..] = []
    end

    @history.push(page)
    @history_pos += 1
  end

  def current_page
    @history[@history_pos]
  end

  def back
    @history_pos -= 1
    @history[@history_pos]
  end

  def forward
    @history_pos += 1
    @history[@history_pos]
  end

  def length
    @history.length
  end
end

ADDRESS_BAR_PAIR = 1
ADDRESS_BAR_LOADING_PAIR = 2
HEADING_PAIR = 3
HEADING2_PAIR = 4
HEADING3_PAIR = 5
PREFORMATTED_PAIR = 6
LINK_PAIR = 7
LINK_URL_PAIR = 8
LINK_PROMPT_PAIR = 9
QUOTE_PAIR = 10

LINK_COLUMN_WIDTH = 5
OUTLINER_WIDTH = 100

def draw_content(win, gemtext, scroll, text_width)
  win_rows = win.maxy

  win.clear
  gemtext.wrapped_lines[scroll..].each_with_index do |wrapped_line, i|
    line = gemtext.lines[wrapped_line.line]
    link = nil
    extra_attrs = 0

    if line.preformatted?
      win.attrset(Curses.color_pair(PREFORMATTED_PAIR))
    elsif !line.link_id.nil?
      link = gemtext.links[line.link_id]
      is_candidate = @current_link_sequence == link.key_sequence.take(@current_link_sequence.length)
      extra_attrs = is_candidate ? 0 : Curses::A_DIM

      # Render the key sequence in the link column
      if wrapped_line.range.begin == 0
        win.setpos(i, 0)
        win.attrset(Curses.color_pair(LINK_PROMPT_PAIR) | extra_attrs)
        win.addstr(link.key_sequence.join)
      end

      win.attrset(Curses.color_pair(LINK_PAIR) | extra_attrs)
    elsif line.quote?
      win.attrset(Curses.color_pair(QUOTE_PAIR))
    elsif line.heading_level == 1
      win.attrset(Curses.color_pair(HEADING_PAIR) | Curses::A_UNDERLINE)
    elsif line.heading_level == 2
      win.attrset(Curses.color_pair(HEADING2_PAIR) | Curses::A_UNDERLINE)
    elsif line.heading_level == 3
      win.attrset(Curses.color_pair(HEADING3_PAIR) | Curses::A_UNDERLINE)
    else
      win.attrset(0)
    end

    win.setpos(i, LINK_COLUMN_WIDTH)

    # We render links a little differently
    # if line.link_id.nil?
      text = line.display_text[wrapped_line.range]
      # win.addstr(text.ljust(text_width, " "))

      # Do this instead of ljust to account for double width terminal characters
      win.addstr(text)
      win.addstr(" " * [text_width - win.curx + LINK_COLUMN_WIDTH, 0].max)
    # else
      # if link.text.nil?
        # win.addstr("#{link.target}")
      # else
        # win.addstr("#{link.text} ")
        # win.attrset(Curses.color_pair(LINK_URL_PAIR) | extra_attrs)
        # win.addstr("(#{link.target})")
      # end
    # end

    break if i + 1 == win_rows
  end
  win.refresh
end

def update_address_win(page)
  @address_win.bkgd(Curses.color_pair(ADDRESS_BAR_PAIR))

  @address_win.clear
  @address_win.setpos(0, 0)
  @address_win.addstr(page.url.to_s)

  @address_win.refresh
end

def update_outliner_win(page)
  @outliner_win.clear
  page.content.heading_lines.each_with_index do |line, i|
    if line.heading_level == 1
      @outliner_win.attrset(Curses.color_pair(HEADING_PAIR))
    elsif line.heading_level == 2
      @outliner_win.attrset(Curses.color_pair(HEADING2_PAIR))
    elsif line.heading_level == 3
      @outliner_win.attrset(Curses.color_pair(HEADING3_PAIR))
    end

    @outliner_win.setpos(i, 0)
    @outliner_win.addstr(line.text[..OUTLINER_WIDTH])
  end
  @outliner_win.refresh
end

def navigate_to(new_url)
  @current_url = new_url

  @address_win.bkgd(Curses.color_pair(ADDRESS_BAR_LOADING_PAIR))
  @address_win.clear
  @address_win.setpos(0, 0)
  @address_win.addstr(new_url.to_s)
  @address_win.refresh

  content = GemText.new(fetch_page(new_url))
  content.wrap_to(@text_width)
  @current_link_sequence = []

  @address_win.bkgd(Curses.color_pair(ADDRESS_BAR_PAIR))
  @address_win.refresh

  Page.new(new_url, content)
end

def main
  Curses.init_screen
  Curses.start_color
  Curses.use_default_colors
  Curses.crmode
  Curses.noecho
  Curses.curs_set(0) # Invisible cursor

  Curses.init_pair(ADDRESS_BAR_PAIR, Curses::COLOR_BLACK, Curses::COLOR_WHITE)
  Curses.init_pair(ADDRESS_BAR_LOADING_PAIR, Curses::COLOR_BLACK, Curses::COLOR_YELLOW)
  Curses.init_pair(HEADING_PAIR, Curses::COLOR_YELLOW, Curses::COLOR_BLACK)
  Curses.init_pair(HEADING2_PAIR, Curses::COLOR_MAGENTA, Curses::COLOR_BLACK)
  Curses.init_pair(HEADING3_PAIR, Curses::COLOR_RED, Curses::COLOR_BLACK)
  Curses.init_pair(PREFORMATTED_PAIR, Curses::COLOR_WHITE, Curses::COLOR_BLACK)
  Curses.init_pair(LINK_PAIR, Curses::COLOR_BLUE, -1)
  Curses.init_pair(LINK_URL_PAIR, Curses::COLOR_WHITE, -1)
  Curses.init_pair(LINK_PROMPT_PAIR, Curses::COLOR_YELLOW, -1)
  Curses.init_pair(QUOTE_PAIR, Curses::COLOR_GREEN, Curses::COLOR_BLACK)

  @text_width = 120
  # current_url = URI("gemini://gemini.circumlunar.space/docs/specification.gmi")
  # @current_url = URI("gemini://gemini.circumlunar.space/")
  @current_link_sequence = []

  @address_win = Curses::Window.new(1, 0, 0, 0)
  @address_win.bkgd(Curses.color_pair(ADDRESS_BAR_PAIR))

  content_win = Curses::Window.new(0, @text_width + LINK_COLUMN_WIDTH, 1, (Curses.cols / 2) - (@text_width / 2) - LINK_COLUMN_WIDTH)

  @outliner_win = Curses::Window.new(0, OUTLINER_WIDTH, 1, (Curses.cols / 2) + (@text_width / 2) + LINK_COLUMN_WIDTH)
  # @address_win.bkgd(Curses.color_pair(ADDRESS_BAR_PAIR))

  @history = History.new
  page = navigate_to URI("gemini://gemini.circumlunar.space/")
  # page = navigate_to URI("gemini://rawtext.club:1965/~sloum/spacewalk.gmi")
  # page = navigate_to URI("gemini://perplexing.space/")
  @history.push(page)
  update_outliner_win(page)

  loop do
    draw_content(content_win, @history.current_page.content, @history.current_page.scroll, @text_width)

    char = content_win.getch
    if char == "j"
      @history.current_page.scroll += 1
    elsif char == "k"
      @history.current_page.scroll -= 1
    elsif char == "h"
      if @history.history_pos > 0
        page = @history.back
        update_outliner_win(page)
        update_address_win(page)
      end
    elsif char == "l"
      if @history.history_pos < @history.length - 1
        page = @history.forward
        update_outliner_win(page)
        update_address_win(page)
      end

    # TODO: This doesn't work
    # elsif char == Curses::KEY_RESIZE
      # @address_win.resize(1, Curses.cols)
      # content_win.move(1, (Curses.cols / 2) - (@text_width / 2) - LINK_COLUMN_WIDTH)
      # content_win.resize(0, @text_width + LINK_COLUMN_WIDTH)
      # @outliner_win.move(1, (Curses.cols / 2) + (@text_width / 2) + LINK_COLUMN_WIDTH)
      # @outliner_win.resize(0, OUTLINER_WIDTH)

      # @address_win.refresh
      # @outliner_win.refresh
    else
      # Check if a link was pressed
      found_link_match = false
      test_pos = @current_link_sequence.length
      @history.current_page.content.links.each do |link|
        if link.key_sequence[test_pos] == char
          @current_link_sequence[test_pos] = char
          found_link_match = true

          @address_win.addstr(link.key_sequence.to_s)
          @address_win.refresh
          if link.key_sequence == @current_link_sequence
            # Link sequence pressed, navigate!
            # TODO: Check if this is a gemini URL

            # So the highlighted link updates
            draw_content(content_win, @history.current_page.content, @history.current_page.scroll, @text_width)

            # URI.join takes care of absolute vs relative URLs
            page = navigate_to URI.join(@history.current_page.url, link.target)
            @history.push(page)
            update_outliner_win(page)

            break
          end
        end
      end

      if !found_link_match
        @current_link_sequence = []
      end
    end

    # Clamp scrolling
    @history.current_page.scroll = @history.current_page.scroll.clamp(0, @history.current_page.content.wrapped_lines.length - 1)
  end
ensure
  Curses.close_screen
end

main if __FILE__ == $PROGRAM_NAME
