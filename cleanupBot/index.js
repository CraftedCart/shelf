const Discord = require('discord.js');

var client = new Discord.Client();

var count = 0;

client.on('ready', function() {
    console.log("Ready!");
});

client.on('message', message => {
    if (message.author.bot) return; //Ignore bots

    const prefix = "]";

    if (message.content.length <= prefix.length) return; //Otherwise we crash

    const args = splitArgs(message.content.slice(prefix.length).trim());
    const command = args.shift().toLowerCase();

    if (!message.content.startsWith(prefix)) return; //Ignore non-commands

    if (command === "cleanroles") {
        message.reply("Searching for roles with no members");

        var toClean = [];

        let roles = client.guilds.get("[GUILD ID GOES HERE]").roles;

        roles.every(function(role) {
            if (role.members.array().length == 0) {
                toClean.push(role);
            }

            return true;
        });

        message.reply("Found " + toClean.length + " roles with no members");

        toClean.every(function(toDel) {
            console.log("DEL: " + toDel.name);
            toDel.delete("unused");
            return true;
        });

        message.reply("poof!");
    } else {
        message.reply("Not a command.")
    }
});

function splitArgs(str) {
    //The parenthesis in the regex creates a captured group within the quotes
    var rgx = /[^\s"]+|"([^"]*)"/gi;
    var outArray = [];

    do {
        //Each call to exec returns the next regex match as an array
        var match = rgx.exec(str);
        if (match != null)
        {
            //Index 1 in the array is the captured group if it exists
            //Index 0 is the matched text, which we use if no captured group exists
            outArray.push(match[1] ? match[1] : match[0]);
        }
    } while (match != null);

    return outArray;
}

client.login('[DISCORD BOT TOKEN GOES HERE]');

