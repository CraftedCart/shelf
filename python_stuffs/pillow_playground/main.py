#!/usr/bin/env python3

from PIL import Image, ImageShow


if __name__ == "__main__":
    with Image.open("/path/to/something.png") as im:
        px = im.load()
        px[40, 40] = (255, 0, 0)

        ImageShow.show(im)
