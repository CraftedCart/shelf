#!/usr/bin/env python3

import docutils.parsers.rst
import docutils.utils
from docutils import nodes



def read_file(path):
    with open(path) as f:
        return f.read()


def main():
    input = read_file("test.rst")

    settings = docutils.frontend.OptionParser(components=(docutils.parsers.rst.Parser,)).get_default_values()
    document = docutils.utils.new_document("test.rst", settings)

    parser = docutils.parsers.rst.Parser()
    parser.parse(input, document)

    print("Whole doc")
    print("---------")
    print(document)
    print()

    print("Nodes")
    print("-----")
    for node in document:
        print(node)
        print(node.pformat())
    print()

    print("Recreation")
    print("----------")

    recreation = nodes.paragraph(text="Unlike ")
    recreation += nodes.literal(text="Image")
    recreation += nodes.Text(", the decal spins with the ball.")

    print(recreation)
    print(recreation.pformat())


if __name__ == "__main__":
    main()
