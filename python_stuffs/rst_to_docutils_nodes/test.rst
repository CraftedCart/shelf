Unlike ``Image``, the decal spins with the ball.

.. code-block:: basic

   With BallRelease.CreateBall
       .Color = &HFF00FF
   End With

.. list-table:: OwO
   :header-rows: 1

   * - Heading row 1, column 1
     - Heading row 1, column 2
     - Heading row 1, column 3
   * - Row 1, column 1
     -
     - Row 1, column 3
   * - Row 2, column 1
     - Row 2, column 2
     - Row 2, column 3

.. _thing:

Title
-----
Hyello

link to `Title`_

.. role:: test

Here's some :test:`inline markup` yeah
