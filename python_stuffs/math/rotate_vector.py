import numpy as np
import math


def coordinate_rotation_fast(v, phi):
    """
    https://github.com/ekiefl/pooltool/blob/7509bd796187e651fc5c597b4ad87fc17d2ffbd5/pooltool/utils/__init__.py#LL452C1-L470C31
    """

    # Build a 3x3 rotation matrix
    cos_phi = np.cos(phi)
    sin_phi = np.sin(phi)
    rotation = np.zeros((3, 3), np.float64)
    rotation[0, 0] = cos_phi
    rotation[0, 1] = -sin_phi
    rotation[1, 0] = sin_phi
    rotation[1, 1] = cos_phi
    rotation[2, 2] = 1

    return np.dot(rotation, v)


# rvw = Displacement (position), Velocity, Angular velocity
# Just copying the naming conventions from pooltool...I don't necessarily understand why they're used
# Probably some math-y convention
rvw = np.empty((3, 3), dtype=np.float64)
rvw[0, :] = [ 0, 10, 0]
rvw[1, :] = [ 0, 10, 0]
rvw[2, :] = [10,  0, 0]

rotated_rvw = coordinate_rotation_fast(rvw.T, math.radians(45)).T

np.set_printoptions(suppress=True)  # Don't use scientific notation
print(rotated_rvw)
