package craftedcart.alltheitems.api;

import craftedcart.alltheitems.gameobject.GameObject;
import craftedcart.alltheitems.tags.GameObjectTags;

public interface IAtiPlugin {
    /**
     * The first method called in a plugin, when it is loaded
     */
    default void initPlugin() {}

    /**
     * A plugin can add tags to an object, if they so desire
     *
     * @param obj The object to agg tags to tag
     * @param tags The tag container for the game object
     */
    default void processGameObjectTags(GameObject obj, GameObjectTags tags) {}
}
