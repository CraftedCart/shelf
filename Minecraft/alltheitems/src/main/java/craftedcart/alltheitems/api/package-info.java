@ParametersAreNonnullByDefault
@MethodsReturnNonnullByDefault

package craftedcart.alltheitems.api;

import mcp.MethodsReturnNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;
