package craftedcart.alltheitems;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.ProgressManager;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import org.apache.logging.log4j.Logger;

@Mod(modid = AllTheItems.MODID, useMetadata = true, clientSideOnly = true)
public class AllTheItems {
    public static final String MODID = "alltheitems";

    private static Logger logger;

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        logger = event.getModLog();
    }

    @EventHandler
    public void init(FMLInitializationEvent event) {
        // some example code
        logger.info("DIRT BLOCK >> {}", Blocks.DIRT.getRegistryName());

        int itemCount = ForgeRegistries.ITEMS.getEntries().size() + ForgeRegistries.BLOCKS.getEntries().size();
        ProgressManager.ProgressBar progressBar = ProgressManager.push("Registering items", itemCount);
        float averageTimePerStep = 0.0f;

        for (Item item : ForgeRegistries.ITEMS) {
            long nowTime = System.nanoTime();

            progressBar.step( String.format( "[%.2fs] %s", averageTimePerStep * (itemCount - progressBar.getStep()), item.getRegistryName()));

            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            float stepTime = (System.nanoTime() - nowTime) / 1000000000.0f;

            averageTimePerStep += (stepTime - averageTimePerStep) / progressBar.getStep();
        }

        for (Block block : ForgeRegistries.BLOCKS) {
            long nowTime = System.nanoTime();

            progressBar.step(String.format("[%.2fs] %s",averageTimePerStep * (itemCount - progressBar.getStep()), block.getRegistryName()));

            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            float stepTime = (System.nanoTime() - nowTime) / 1000000000.0f;

            averageTimePerStep += (stepTime - averageTimePerStep) / progressBar.getStep();
        }

        ProgressManager.pop(progressBar);
    }
}
