package craftedcart.alltheitems.tags;

public class Tag {
    private final String registryName;

    /**
     * Use getTagForRegistryName over this constructor to create tags
     */
    Tag(String registryName) {
        this.registryName = registryName;
    }

    public String getRegistryName() {
        return registryName;
    }
}
