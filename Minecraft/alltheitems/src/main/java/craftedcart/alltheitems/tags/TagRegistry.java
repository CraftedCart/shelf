package craftedcart.alltheitems.tags;

import java.util.HashMap;
import java.util.Map;

public class TagRegistry {
    private Map<String, Tag> registry = new HashMap<>();

    public Tag getTagForRegistryName(String registryName) {
        Tag tag = registry.get(registryName);

        if (tag == null) {
            tag = new Tag(registryName);
            addTag(tag);
        }

        return tag;
    }

    /**
     * Use getTagForRegistryName over addTag to create tags
     *
     * @param tag The tag to register
     */
    private void addTag(Tag tag) {
        registry.put(tag.getRegistryName(), tag);
    }
}
