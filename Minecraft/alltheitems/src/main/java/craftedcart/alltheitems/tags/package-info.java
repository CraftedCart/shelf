@ParametersAreNonnullByDefault
@MethodsReturnNonnullByDefault

package craftedcart.alltheitems.tags;

import mcp.MethodsReturnNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;
