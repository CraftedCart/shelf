AllTheItems
===========

I had an idea at some point to make a mod similar to Too Many Items/Not Enough Items/Just Enough Items, but with some
cool idea like mods being able to tag items so you could search by tag, and being able to see a whole tree of crafting
steps at once.

...yeah, this never really even began to get off the ground.
