GlassOS
=======

I used to enjoy making "operating systems" (graphical shells) for ComputerCraft back in ~2016, this is one such attempt.

The old git history for this project ;P
```
* 8859fdc (HEAD -> master, origin/master, origin/HEAD) Update Installer.lua
* 5328880 Update GlassOS.lua
* 676142a Rename Default.theme to Default.Gtheme
* b607f17 Fixed another spelling error in README.md
* 9aace2d Fixed spelling error
* b92ccb0 Update README.md
* f00c5f3 Added First Time Startup section to Readme.md
* 7a90d8c Added install instructions to README.md
* 12ca1ca Create Installer.lua
* 6d79fdb Removed DEV: Save default theme option
* 76c34aa Added login and basic user system to GlassOS.lua
* 52f76e7 Create Default.theme
* e5fa7ad Create ThemeSave.lua
* a308eb2 Update GlassOS.lua to get latest version
* fe392eb Create LICENSE
* b50eef1 Created GlassOS.lua - Still in development
* 4761b67 Initial commit
```

# ORIGINAL README DOWN BELOW

---

ComputerCraft - GlassOS
======================

An OS interface for Advanced ComputerCraft Computers to make it easy to use

####Type `pastebin get Rq8G6dVM install` followed by `install` to get it!
######Only works on Advanced Computers - Will overwrite `startup` file and, if `GlassOS` is present as a file, it will overwrite that also


#####First Time Startup
Upon first time startup, GlassOS will ask you to create a user account. Enter a username and password (Password can be left blank) to continue. You are then presented with a list of default built in apps
