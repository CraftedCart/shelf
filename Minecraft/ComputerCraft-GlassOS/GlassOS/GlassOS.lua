--GlassOS by CraftedCart
--Licensed under Creative Commons Attribution-ShareAlike 4.0 International

--Variables
w, h = term.getSize()
loggedInUser = ""
installDir = "/GlassOS"

theme = {}
theme["bg"] = colors.lightBlue
theme["menuBar"] = colors.green
theme["menuButtonsBG"] = colors.lime
theme["menuBarText"] = colors.black
theme["appButtons"] = colors.blue
theme["appButtonsText"] = colors.white
theme["dialogBG"] = colors.blue
theme["dialogText"] = colors.white
theme["dialogTextbox"] = colors.lightBlue
theme["dialogTextboxText"] = colors.black

apps = {
{"Paint","/rom/programs/color/paint","Enter Filename"},
{"Edit","/rom/programs/edit","Enter Filename"},
{"File: Delete","rom/programs/delete","Enter Filename or Folder to Delete"},
{"Adventure","/rom/programs/computer/adventure"},
{"System: Restart","/rom/programs/reboot"},
{"System: Shutdown","/rom/programs/shutdown"},
}

prefs = {["appPadding"] = 8,["Theme"] = "Default.Gtheme"}

showTime = true

--Functions
function updateMenuBarTime()
	while true do
		if showTime == true then
			local curX, curY = term.getCursorPos()
			term.setCursorPos(w - 8, 1)
			if loggedInUser == "" then
				term.setTextColor(colors.white)
				term.setBackgroundColor(colors.blue)
			else
				term.setTextColor(theme["menuBarText"])
				term.setBackgroundColor(theme["menuBar"])
			end
			term.write("        ")
			local formattedTime = textutils.formatTime(os.time())
			term.setCursorPos(w - string.len(formattedTime), 1)
			term.write(formattedTime)
			term.setCursorPos(curX, curY)
		end
		os.sleep(0.2)
	end
end

function drawDesktop()
	--Background Color
	term.setBackgroundColor(theme["bg"])
	term.clear()

	--Render buttons
	term.setBackgroundColor(theme["appButtons"])
	term.setTextColor(theme["appButtonsText"])
	for k, v in pairs(apps) do
		term.setCursorPos(prefs["appPadding"], k + 2)
		term.write(" " .. apps[k][1])
		for i = 1, w - (prefs["appPadding"] * 2) - string.len(apps[k][1]) - 1 do
			term.write(" ")
		end
		term.write("> ")
	end

	--Render menu button
	term.setCursorPos(2, 1)
	term.setBackgroundColor(theme["menuBar"])
	term.clearLine()

	term.setBackgroundColor(theme["menuButtonsBG"])
	term.setTextColor(theme["menuBarText"])
	term.write("Menu")
end

function appButtons()
	while true do
		--Detect mouse clicks
		e, btn, xPos, yPos = os.pullEvent()
		if e == "mouse_click" then
			if xPos >= 2 and xPos <= 5 and yPos == 1 then
				--Menu button clicked
				--TODO!
			elseif xPos >= prefs["appPadding"] and xPos <= w - prefs["appPadding"] and yPos >= 3 then
				--App button clicked
				if apps[yPos - 2] ~= nil then
					local appToLaunch = apps[yPos - 2][2]
					if apps[yPos - 2][3] then
						term.setCursorPos(2, 1)
						term.setBackgroundColor(theme["dialogBG"])
						term.setTextColor(theme["dialogText"])
						term.clearLine()
						term.write(apps[yPos - 2][1])
						term.setCursorPos(2, 2)
						term.clearLine()
						term.write(apps[yPos - 2][3])
						term.setCursorPos(1, 4)
						term.clearLine()
						term.setCursorPos(1, 3)
						term.setBackgroundColor(theme["dialogTextbox"])
						term.setTextColor(theme["dialogTextboxText"])
						term.clearLine()
						showTime = false
						txtIn = read()
						showTime = true
						appToLaunch = appToLaunch .. " " .. txtIn
					end
					return(appToLaunch)
				end
			end
		end
	end
end

function login()
	loggedInUser = ""

	--Get usernames and passwords file

	if fs.exists(installDir .. "/.userReg") and not fs.isDir(installDir .. "/.userReg") then
		userFile = fs.open(installDir .. "/.userReg", "r")
		userT = userFile.readAll()
		userFile.close()
		userT = textutils.unserialize(userT)
	else
		if fs.exists(installDir .. "/.userReg") then
			fs.delete(installDir .. "/.userReg")
		end
		--User Register file doesn't exist! - Create user
		term.setBackgroundColor(colors.gray)
		term.setTextColor(colors.black)
		term.clear()

		term.setCursorPos(1, 1)
		term.setBackgroundColor(colors.blue)
		term.clearLine()

		term.setBackgroundColor(colors.lightBlue)
		for i = 5, -3, -1 do
			term.setCursorPos(1, (h / 2) + i)
			term.clearLine()
		end

		term.setCursorPos(2, (h / 2) - 2)
		term.write("Welcome to GlassOS - Create a new user")
		term.setCursorPos(2, h / 2)
		term.write("Username")
		term.setCursorPos(2, (h / 2) + 3)
		term.write("Password")

		term.setBackgroundColor(colors.lightGray)
		term.setCursorPos(1, (h / 2) + 6)
		term.clearLine()
		term.setCursorPos(1, (h / 2) - 4)
		term.clearLine()

		term.setBackgroundColor(colors.blue)
		term.setCursorPos(1, (h / 2) + 4)
		term.clearLine()
		term.setCursorPos(1, (h / 2) + 1)
		term.clearLine()

		--Enter Username and Password
		term.setTextColor(colors.white)
		username = read()
		term.setCursorPos(1, (h / 2) + 4)
		password = read("*")

		userT = {}
		userT[username:lower()] = password
		userT = textutils.serialize(userT)

		userFile = fs.open(installDir .. "/.userReg", "w")
		userFile.write(userT)
		userFile.close()

		loggedInUser = username:lower()
	end

	while loggedInUser == "" do
		term.setBackgroundColor(colors.gray)
		term.setTextColor(colors.black)
		term.clear()

		term.setCursorPos(1, 1)
		term.setBackgroundColor(colors.blue)
		term.clearLine()

		term.setBackgroundColor(colors.lightBlue)
		for i = 5, -3, -1 do
			term.setCursorPos(1, (h / 2) + i)
			term.clearLine()
		end

		term.setCursorPos(2, (h / 2) - 2)
		term.write("Welcome to GlassOS - Login to continue")
		term.setCursorPos(2, h / 2)
		term.write("Username")
		term.setCursorPos(2, (h / 2) + 3)
		term.write("Password")

		term.setBackgroundColor(colors.lightGray)
		term.setCursorPos(1, (h / 2) + 6)
		term.clearLine()
		term.setCursorPos(1, (h / 2) - 4)
		term.clearLine()

		term.setBackgroundColor(colors.blue)
		term.setCursorPos(1, (h / 2) + 4)
		term.clearLine()
		term.setCursorPos(1, (h / 2) + 1)
		term.clearLine()

		--Enter Username and Password
		term.setTextColor(colors.white)
		username = read()
		term.setCursorPos(1, (h / 2) + 4)
		password = read("*")

		--User checking
		if userT[username:lower()] == nil or userT[username:lower()] ~= password then
			--Invalid username or password
			term.setBackgroundColor(colors.red)
			term.setCursorPos(2, (h / 2) - 2)
			term.setTextColor(colors.white)
			term.clearLine()
			term.write("Invalid Username or Password - Try again")
			os.sleep(2)
		else
			loggedInUser = username:lower()
		end
	end
end

function loadUserPrefs()
	--Read prefs file
	if fs.exists(installDir .. "/Users/" .. loggedInUser .. "/.prefs") then
		prefsFile = fs.open(installDir .. "/Users/" .. loggedInUser .. "/.prefs", "r")
		prefs = textutils.unserialize(prefsFile.readAll())
		prefsFile.close()
	else
		if not fs.exists(installDir .. "/Users") then fs.makeDir(installDir .. "/Users") end
		if not fs.exists(installDir .. "/Users/" .. loggedInUser) then fs.makeDir(installDir .. "/Users/" .. loggedInUser) end
		prefsFile = fs.open(installDir .. "/Users/" .. loggedInUser .. "/.prefs", "w")
		prefsFile.write(textutils.serialize(prefs))
		prefsFile.close()
	end

	--Read theme
	themeFile = fs.open(installDir .. "/Themes/" .. prefs["Theme"], "r")
	theme = textutils.unserialize(themeFile.readAll())
	themeFile.close()
end

--Main program
function main()
	drawDesktop()
	return(appButtons())
end

function getProgToRun()
	progToRun = main()
end

function runGlassOS()
	login()
	loadUserPrefs()
	getProgToRun()
end

os.sleep(5)
parallel.waitForAny(runGlassOS, updateMenuBarTime)

while true do
	parallel.waitForAny(getProgToRun, updateMenuBarTime)

	term.setCursorPos(1, 1)
	term.setBackgroundColor(colors.black)
	term.setTextColor(colors.white)
	term.clear()
	shell.run(progToRun)
	term.setCursorBlink(false)
end
