--Installer for GlassOS

function fileGetWrite(url, fileName, fileLocation)
	if http then
		fileURL = url
		print("Downloading " .. fileName)
		file = http.get(fileURL)
		print("Downloaded " .. fileName)
		if file then
			h = fs.open(fileLocation, "w")
			h.write(file.readAll())
			h.close()
			print("Wrote " .. fileName .. " to " .. fileLocation)
		else
			print("Seems like we couldn't find the file at " .. fileURL)
		end
	else
		print("Looks like you can't access the internet. Go download GlassOS manually from https://github.com/CraftedCart/ComputerCraft-GlassOS/")
	end
end

function makeFolder(location)
	if not fs.exists(location) then
		fs.makeDir(location)
		print("Created " .. location .. " directory")
	end
end

function install()
	term.setBackgroundColor(colors.black)
	term.setTextColor(colors.white)
	term.setCursorPos(1,1)
	term.clear()

	makeFolder("/GlassOS")
	makeFolder("/GlassOS/Themes")

	fileGetWrite("https://raw.githubusercontent.com/CraftedCart/ComputerCraft-GlassOS/master/GlassOS/GlassOS.lua", "GlassOS.lua", "/GlassOS/GlassOS.lua")
	fileGetWrite("https://raw.githubusercontent.com/CraftedCart/ComputerCraft-GlassOS/master/GlassOS/Themes/Default.Gtheme","Default.Gtheme", "/GlassOS/Themes/Default.Gtheme")

	print("Creating startup file")
	file = fs.open("/startup", "w")
	file.write("shell.run('/GlassOS/GlassOS.lua')")
	file.close()

	print("Done installing GlassOS!")
	for i = 5, 1, -1 do
		print("Restarting in " .. tostring(i) .. "...")
		os.sleep(1)
	end

	os.reboot()
end

if term.isColor() then
	--Can install!
	term.setBackgroundColor(colors.lightBlue)
	term.clear()
	term.setTextColor(colors.black)
	term.setCursorPos(2,2)
	term.write("Would you like to install GlassOS?")

	term.setCursorPos(2,4)
	term.setBackgroundColor(colors.blue)
	term.setTextColor(colors.white)
	term.clearLine()
	term.write("OK!")

	term.setCursorPos(2,6)
	term.clearLine()
	term.write("Cancel")

	while true do
		e, btn, xPos, yPos = os.pullEvent()
		if yPos == 4 then
			install()
			break
		elseif yPos == 6 then
			term.setBackgroundColor(colors.black)
			term.setCursorPos(1,1)
			term.clear()
			break
		end
	end
else
	--Can't install!
	term.clear()
	term.setCursorPos(1,1)
	print("GlassOS requires an advanced computer to run!")
end
