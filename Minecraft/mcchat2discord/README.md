McChat2Discord
==============

A small server mod for Minecraft 1.7.10 that replicates chat messages in Minecraft to a Discord channel. Make sure to
edit `src/main/java/craftedcart/mcchat2discord/McChat2Discord.java` to add in a Discord webhook URL.
