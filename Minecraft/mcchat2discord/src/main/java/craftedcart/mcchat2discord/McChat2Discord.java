package craftedcart.mcchat2discord;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.event.FMLServerStoppingEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.ServerChatEvent;

import javax.net.ssl.HttpsURLConnection;
import java.io.OutputStream;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Mod(modid = McChat2Discord.MODID, version = McChat2Discord.VERSION, acceptableRemoteVersions = "*")
public class McChat2Discord {
    public static final String MODID = "mcchat2discord";
    public static final String VERSION = "1.0";
    public static final String WEBHOOK_URL = "[YOUR DISCORD WEBHOOK URL HERE]";

    public ExecutorService executor;
    
    @EventHandler
    public void init(FMLInitializationEvent event) {
        if (FMLCommonHandler.instance().getSide() == Side.SERVER) {
            System.out.println("Registering mcchat2discord - running on game server");
            MinecraftForge.EVENT_BUS.register(this);
            executor = Executors.newFixedThreadPool(1);
        } else {
            System.out.println("Ignoring mcchat2discord - running on game client");
        }
    }

    @SubscribeEvent
    public void onServerChat(ServerChatEvent event) {
        executor.submit(new Request(event));
    }

    @EventHandler
    public void onServerStop(FMLServerStoppingEvent event) {
        if (FMLCommonHandler.instance().getSide() == Side.SERVER) {
            executor.shutdown();
        }
    }
}

class Request implements Runnable {
    private ServerChatEvent event;

    public Request(ServerChatEvent event) {
        this.event = event;
    }

    @Override
    public void run() {
        try {
            JsonObject json = new JsonObject();
            json.addProperty("username", String.format("[MC] %s", event.username));
            json.addProperty("content", event.message);
            String jsonStr = new GsonBuilder().create().toJson(json);

            URL url = new URL(McChat2Discord.WEBHOOK_URL);
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            connection.addRequestProperty("Content-Type", "application/json");
            connection.addRequestProperty("User-Agent", "McChat2Discord");
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");

            OutputStream stream = connection.getOutputStream();
            stream.write(jsonStr.getBytes());
            stream.flush();
            stream.close();

            connection.getInputStream().close(); //I'm not sure why but it doesn't work without getting the InputStream
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
