package io.github.craftedcart.cargo.component;

import io.github.craftedcart.cargo.gui.DefaultUITheme;
import io.github.craftedcart.cargo.gui.GuiRecipesOverlay;
import io.github.craftedcart.fluidui.component.Component;
import io.github.craftedcart.fluidui.theme.UITheme;
import io.github.craftedcart.fluidui.util.UIColor;
import io.github.craftedcart.fluidui.util.UIUtils;
import io.github.craftedcart.fluiduiminecraftadditions.component.ItemStackDisplay;
import io.github.craftedcart.fluiduiminecraftadditions.util.TooltipHelper;
import net.minecraft.item.ItemStack;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * @author CraftedCart
 * Created on 28/07/2016 (DD/MM/YYYY)
 */
public class RecipeItemStack extends ItemStackDisplay {

    @NotNull public UIColor filledColor = UIColor.transparent();
    @NotNull public UIColor emptyColor = UIColor.transparent();
    public boolean filled = true;

    @Nullable public GuiRecipesOverlay guiRecipesOverlay;

    public RecipeItemStack() {
        init();
        postInit();
    }

    public void init() {
        if (parentComponent != null) {
            setTheme(parentComponent.theme);
        }
    }

    @Override
    public void setTheme(@NotNull UITheme theme) {
        super.setTheme(theme);

        if (theme instanceof DefaultUITheme) {
            DefaultUITheme dTheme = (DefaultUITheme) theme;
            filledColor = dTheme.recipeItemStackFilledColor;
            emptyColor = dTheme.recipeItemStackEmptyColor;
        }
    }

    @Override
    public void componentDraw() {
        if (filled) {
            UIUtils.drawQuad(topLeftPx, bottomRightPx, filledColor);
        } else {
            UIUtils.drawQuad(topLeftPx, bottomRightPx, emptyColor);
        }

        super.componentDraw();
    }

    @Override
    public void setParentComponent(@Nullable Component parentComponent) {
        super.setParentComponent(parentComponent);

        if (parentComponent != null) {
            setTheme(parentComponent.theme);
        }
    }

    public void setFilledColor(@NotNull UIColor filledColor) {
        this.filledColor = filledColor;
    }

    public void setEmptyColor(@NotNull UIColor emptyColor) {
        this.emptyColor = emptyColor;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public void setGuiRecipesOverlay(@Nullable GuiRecipesOverlay guiRecipesOverlay) {
        this.guiRecipesOverlay = guiRecipesOverlay;
    }

    @Override
    public void setItemStack(ItemStack itemStack) {
        super.setItemStack(itemStack);

        if (itemStack == null) {
            customTooltip = null;
        } else {
            customTooltip = TooltipHelper.generateItemStackCustomTooltip(itemStack, theme);
        }
    }

}
