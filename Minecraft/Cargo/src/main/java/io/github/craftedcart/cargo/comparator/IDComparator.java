package io.github.craftedcart.cargo.comparator;

import io.github.craftedcart.cargo.item.ItemData;

import java.util.Comparator;

/**
 * @author CraftedCart
 * Created on 21/06/2016 (DD/MM/YYYY)
 */
public class IDComparator implements Comparator<ItemData> {

    @Override
    public int compare(ItemData o1, ItemData o2) {
        if (o1.getItemID() < o2.getItemID()) {
            return -1;
        } else if (o1.getItemID() > o2.getItemID()) {
            return 1;
        } else { //Same ID - Check metadata
            if (o1.getItemMetadata() < o2.getItemMetadata()) {
                return -1;
            } else if (o1.getItemMetadata() > o2.getItemMetadata()) {
                return 1;
            } else {
                return 0;
            }
        }
    }

}
