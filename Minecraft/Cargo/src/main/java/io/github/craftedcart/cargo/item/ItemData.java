package io.github.craftedcart.cargo.item;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;

import java.util.Objects;

/**
 * @author CraftedCart
 * Created on 20/06/2016 (DD/MM/YYYY)
 */
public class ItemData {

    public ItemStack itemStack;

    public ItemData(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

    public String getUniqueID() {

        int metadata = itemStack.getMetadata();

        NBTTagCompound serializedNbt = itemStack.serializeNBT();
        StringBuilder uniqueID = new StringBuilder(serializedNbt.getString("id"));

        uniqueID.append(':').append(metadata);

        NBTTagCompound nbtTagCompound = serializedNbt.getCompoundTag("tag");
        if (serializedNbt.hasKey("ForgeCaps")) {
            if (nbtTagCompound == null) {
                nbtTagCompound = new NBTTagCompound();
            }
            nbtTagCompound.setTag("ForgeCaps", serializedNbt.getCompoundTag("ForgeCaps"));
        }

        if (nbtTagCompound != null && !nbtTagCompound.hasNoTags()) {
            uniqueID.append(':').append(nbtTagCompound);
        }

        return uniqueID.toString();
    }

    public int getItemID() {
        return Item.getIdFromItem(itemStack.getItem());
    }

    public String getItemStringID() {
        ResourceLocation itemLoc = Item.REGISTRY.getNameForObject(itemStack.getItem());
        if (itemLoc != null) {
            return itemLoc.toString();
        } else {
            return "null"; //TODO: Maybe clean this up (IE: Don't return "null")
        }
    }

    public int getItemMetadata() {
        return itemStack.getMetadata();
    }

    public String getDisplayName() {
        return itemStack.getDisplayName();
    }

    public int getMaxStackSize() {
        return itemStack.getMaxStackSize();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof ItemData && Objects.equals(getUniqueID(), ((ItemData) obj).getUniqueID());
    }

    @Override
    public int hashCode() {
        return getUniqueID().hashCode();
    }

}
