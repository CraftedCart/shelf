package io.github.craftedcart.cargo.proxy;

import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

/**
 * Created by CraftedCart on 17/11/2015 (DD/MM/YYYY)
 */
abstract class CommonProxy implements IProxy {

    public abstract void getConfig(FMLPreInitializationEvent e);

    public abstract void preInit();
    public abstract void postInit();

}
