package io.github.craftedcart.cargo.reference;

/**
 * @author CraftedCart
 * Created on 19/06/2016 (DD/MM/YYYY)
 */
public final class Reference {

    public static final String MOD_ID = "cargo";
    public static final String MOD_NAME = "Cargo";
    public static final String VERSION = "1.0.0-Minecraft1.9.4";
    public static final String CLIENT_PROXY_CLASS = "io.github.craftedcart.cargo.proxy.ClientProxy";
    public static final String SERVER_PROXY_CLASS = "io.github.craftedcart.cargo.proxy.ServerProxy";

}
