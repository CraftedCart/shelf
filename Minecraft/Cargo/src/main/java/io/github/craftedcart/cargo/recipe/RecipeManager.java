package io.github.craftedcart.cargo.recipe;

import io.github.craftedcart.cargo.item.ItemData;
import io.github.craftedcart.cargo.util.LogHelper;
import io.github.craftedcart.fluidui.uiaction.UIAction;
import net.minecraft.item.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author CraftedCart
 * Created on 26/07/2016 (DD/MM/YYYY)
 */
public class RecipeManager {

    private static List<AbstractRecipeCategory> recipeCategories = new ArrayList<>();

    public static void addRecipeCategory(AbstractRecipeCategory recipeCategory) {
        recipeCategories.add(recipeCategory);
    }
    public static void findRecipes(ItemData itemData, ActionRecipesFound doneCallback) {
        List<AbstractRecipeCategory> foundRecipes = new ArrayList<>();

        for (AbstractRecipeCategory category : recipeCategories) {
            AbstractRecipeCategory foundCategory = null;

            for (IRecipeItem recipeItem : category.getRecipes()) {
                if (recipeItem.getOutputItems() != null) {
                    for (ItemStack itemStack : recipeItem.getOutputItems()) {
                        if (itemData.itemStack.isItemEqualIgnoreDurability(itemStack)) {
                            if (foundCategory == null) {
                                foundCategory = new AbstractRecipeCategory() {
                                    @NotNull
                                    @Override
                                    public String getCategoryID() {
                                        return category.getCategoryID();
                                    }

                                    @NotNull
                                    @Override
                                    public String getCategoryLocalizedName() {
                                        return category.getCategoryLocalizedName();
                                    }
                                };
                            }
                            foundCategory.addRecipe(recipeItem);
                        }
                    }
                }
            }

            if (foundCategory != null) {
                foundRecipes.add(foundCategory);
            }
        }

        doneCallback.execute(foundRecipes);
    }

}
