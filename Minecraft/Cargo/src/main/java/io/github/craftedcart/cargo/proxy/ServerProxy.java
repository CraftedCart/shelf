package io.github.craftedcart.cargo.proxy;

import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

/**
 * Created by CraftedCart on 17/11/2015 (DD/MM/YYYY)
 */
public class ServerProxy extends CommonProxy {

    @Override
    public void getConfig(FMLPreInitializationEvent e) {
        //No-Op
    }

    @Override
    public void preInit() {
        //No-Op
    }

    @Override
    public void postInit() {
        //No-Op
    }

}
