package io.github.craftedcart.cargo.comparator;

import io.github.craftedcart.cargo.item.ItemData;

import java.text.Collator;
import java.util.Comparator;

/**
 * @author CraftedCart
 * Created on 21/06/2016 (DD/MM/YYYY)
 */
public class AlphabeticalByDisplayNameComparator implements Comparator<ItemData> {

    @Override
    public int compare(ItemData o1, ItemData o2) {
        Collator collator = Collator.getInstance();
        collator.setStrength(Collator.PRIMARY);

        int alphabeticalOrdering = collator.compare(o1.getDisplayName(), o2.getDisplayName());

        if (alphabeticalOrdering == 0) {
            if (o1.getItemMetadata() < o2.getItemMetadata()) {
                return -1;
            } else if (o1.getItemMetadata() > o2.getItemMetadata()) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return alphabeticalOrdering;
        }
    }

}
