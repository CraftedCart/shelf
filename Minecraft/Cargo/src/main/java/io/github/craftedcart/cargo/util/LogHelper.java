package io.github.craftedcart.cargo.util;

import io.github.craftedcart.cargo.reference.Reference;
import net.minecraftforge.fml.common.FMLLog;
import org.apache.logging.log4j.Level;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CraftedCart on 17/11/2015 (DD/MM/YYYY)
 */
public class LogHelper {

    private static List<String> logPrefixes = new ArrayList<>();

    private static void log(Level logLevel, Object object) {
        FMLLog.log(Reference.MOD_NAME, logLevel, getPrefixString() + String.valueOf(object));
    }

    public static void all(Object object) {
        log(Level.ALL, object);
    }

    public static void fatal(Object object) {
        log(Level.FATAL, object);
    }

    public static void error(Object object) {
        log(Level.ERROR, object);
    }

    public static void warn(Object object) {
        log(Level.WARN, object);
    }

    public static void info(Object object) {
        log(Level.INFO, object);
    }

    public static void debug(Object object) {
        log(Level.DEBUG, object);
    }

    public static void trace(Object object) {
        log(Level.TRACE, object);
    }

    public static void off(Object object) {
        log(Level.OFF, object);
    }

    public static void addLogPrefix(String prefix) {
        logPrefixes.add(prefix);
    }

    public static void removeLastLogPrefix() {
        logPrefixes.remove(logPrefixes.size() - 1);
    }

    public static void removeLogPrefix(String prefix) {
        logPrefixes.remove(prefix);
    }

    public static void removeAllLogPrefixes() {
        logPrefixes.clear();
    }

    public static List<String> getLogPrefixes() {
        return logPrefixes;
    }

    public static String getPrefixString() {
        if (logPrefixes.isEmpty()) {
            return "";
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            for (String prefix : logPrefixes) {
                sb.append(prefix);
                sb.append(" > ");
            }

            sb.setLength(sb.length() - 3); //Delete last 3 " > " characters
            sb.append("]: ");

            return sb.toString();
        }
    }

}