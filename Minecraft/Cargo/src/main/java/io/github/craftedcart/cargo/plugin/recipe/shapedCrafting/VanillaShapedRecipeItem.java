package io.github.craftedcart.cargo.plugin.recipe.shapedCrafting;

import io.github.craftedcart.cargo.gui.GuiRecipesOverlay;
import io.github.craftedcart.cargo.recipe.IRecipeItem;
import io.github.craftedcart.fluidui.component.Component;
import net.minecraft.item.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

/**
 * @author CraftedCart
 * Created on 26/07/2016 (DD/MM/YYYY)
 */
public class VanillaShapedRecipeItem implements IRecipeItem {

    @NotNull private ItemStack[] recipeItems;
    private int recipeWidth;
    private int recipeHeight;
    @NotNull private ItemStack outputItem;

    public VanillaShapedRecipeItem(@NotNull ItemStack[] recipeItems, int recipeWidth, int recipeHeight, @NotNull ItemStack outputItem) {
        this.recipeItems = recipeItems;
        this.recipeWidth = recipeWidth;
        this.recipeHeight = recipeHeight;
        this.outputItem = outputItem;
    }

    @NotNull
    @Override
    public Component getUIComponent(GuiRecipesOverlay guiRecipesOverlay) {
        return new VanillaShapedRecipeItemUI(guiRecipesOverlay, recipeItems, recipeWidth, recipeHeight, outputItem);
    }

    @NotNull
    @Override
    public ItemStack[] getInputItems() {
        return recipeItems;
    }

    @NotNull
    @Override
    public ItemStack[] getOutputItems() {
        return new ItemStack[]{outputItem};
    }

    @Override
    public String toString() {
        return String.format("%d x %d %s -> %s", recipeWidth, recipeHeight, Arrays.toString(recipeItems), outputItem.toString());
    }
}
