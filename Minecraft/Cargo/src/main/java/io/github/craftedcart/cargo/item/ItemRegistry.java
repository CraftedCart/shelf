package io.github.craftedcart.cargo.item;

import io.github.craftedcart.cargo.util.LogHelper;
import io.github.craftedcart.fluidui.component.Button;
import io.github.craftedcart.fluidui.component.Component;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.*;

/**
 * @author CraftedCart
 * Created on 20/06/2016 (DD/MM/YYYY)
 */
public class ItemRegistry {

    public static Set<ItemData> items;
    public static Map<ItemData, Component> itemComponentMap = new HashMap<>();

    public static void init() {
        items = getAllItemData();
    }

    private static Set<ItemData> getAllItemData() {
        Set<ItemData> allItemData = new HashSet<>();

        //<editor-fold desc="Get items from creative tabs">
        for (CreativeTabs creativeTab : CreativeTabs.CREATIVE_TAB_ARRAY) {
            List<ItemStack> creativeTabItemStacks = new ArrayList<>();
            try {
                creativeTab.displayAllRelevantItems(creativeTabItemStacks);
            } catch (RuntimeException | LinkageError e) {
                LogHelper.error(String.format("Error whilst trying to get items from a creative tab: %s (%s)",
                        creativeTab.getTabLabel(), creativeTab.getTranslatedTabLabel()));
                LogHelper.error(e);
            }

            for (ItemStack itemStack : creativeTabItemStacks) {
                if (itemStack == null) {
                    LogHelper.error("Null ItemStack while getting creative tab item data");
                } else {
                    addItemStack(itemStack, allItemData);
                }
            }
        }
        //</editor-fold>

        for (Item item : Item.REGISTRY) {
            if (item != null) {
                List<ItemStack> itemSubTypes = new ArrayList<>();
                item.getSubItems(item, null, itemSubTypes);

                for (ItemStack itemStack : itemSubTypes) {
                    if (itemStack != null) {
                        addItemStack(itemStack, allItemData);
                    }
                }
            }
        }

        return allItemData;
    }

    private static void addItemStack(ItemStack itemStack, Set<ItemData> itemDataSet) {
        ItemData itemData = new ItemData(itemStack);
        itemDataSet.add(itemData);
    }

    public static Set<ItemData> getItems() {
        return items;
    }
}
