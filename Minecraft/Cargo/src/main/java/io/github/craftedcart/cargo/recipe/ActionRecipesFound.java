package io.github.craftedcart.cargo.recipe;

import java.util.List;

/**
 * @author CraftedCart
 * Created on 27/07/2016 (DD/MM/YYYY)
 */
public interface ActionRecipesFound {
    void execute(List<AbstractRecipeCategory> foundRecipes);
}
