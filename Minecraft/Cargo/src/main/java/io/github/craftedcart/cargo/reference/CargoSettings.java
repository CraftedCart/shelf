package io.github.craftedcart.cargo.reference;

import net.minecraft.client.Minecraft;

import java.util.Objects;

/**
 * @author CraftedCart
 * Created on 02/07/2016 (DD/MM/YYYY)
 */
public class CargoSettings {

    public static double sidebarMargin = 64; //64px from the side of the GUI

    public static boolean regexSearch = false;

    public static String giveCommand = "/give {playerName} {itemID} {itemCount} {metadata}";

    public static String getGiveCommand(String itemID, int itemCount, int metadata) { //TODO: Give with NBT data
        return giveCommand
                .replace("{playerName}", Minecraft.getMinecraft().thePlayer.getName())
                .replace("{itemID}", itemID)
                .replace("{itemCount}", Objects.toString(itemCount))
                .replace("{metadata}", Objects.toString(metadata))
                ;
    }

}
