package io.github.craftedcart.cargo.plugin.recipe.shapedCrafting;

import io.github.craftedcart.cargo.component.RecipeItemStack;
import io.github.craftedcart.cargo.gui.GuiRecipesOverlay;
import io.github.craftedcart.cargo.util.LogHelper;
import io.github.craftedcart.fluidui.component.Panel;
import io.github.craftedcart.fluidui.util.UIColor;
import net.minecraft.item.ItemStack;

import java.util.Arrays;

/**
 * @author CraftedCart
 * Created on 27/07/2016 (DD/MM/YYYY)
 */
public class VanillaShapedRecipeItemUI extends Panel {

    private final RecipeItemStack craftingItemStack0 = new RecipeItemStack();
    private final RecipeItemStack craftingItemStack1 = new RecipeItemStack();
    private final RecipeItemStack craftingItemStack2 = new RecipeItemStack();
    private final RecipeItemStack craftingItemStack3 = new RecipeItemStack();
    private final RecipeItemStack craftingItemStack4 = new RecipeItemStack();
    private final RecipeItemStack craftingItemStack5 = new RecipeItemStack();
    private final RecipeItemStack craftingItemStack6 = new RecipeItemStack();
    private final RecipeItemStack craftingItemStack7 = new RecipeItemStack();
    private final RecipeItemStack craftingItemStack8 = new RecipeItemStack();

    public VanillaShapedRecipeItemUI(GuiRecipesOverlay guiRecipesOverlay, ItemStack[] recipeItems, int recipeWidth, int recipeHeight, ItemStack outputItems) {
        ItemStack[] recipeItems9 = new ItemStack[9];

        if (recipeWidth == 1) {
            for (int i = 0; i < 3; i++) {
                recipeItems9[i * 3] = recipeItems[i];

                if (i + 1 == recipeHeight) {
                    break;
                }
            }
        } else if (recipeWidth == 2) {
            for (int i = 0; i < 6; i++) {
                recipeItems9[i + (i > 1 ? 1 : 0) + (i > 3 ? 1 : 0)] = recipeItems[i];

                if (i + 1 == recipeHeight * 2) {
                    break;
                }
            }
        } else if (recipeWidth == 3) {
            for (int i = 0; i < 9; i++) {
                recipeItems9[i] = recipeItems[i];

                if (i + 1 == recipeHeight * 3) {
                    break;
                }
            }
        }

        LogHelper.info(Arrays.toString(recipeItems9)); //TODO

        setOnInitAction(() -> {
            setTopLeftPos(0, 0);
            setBottomRightPos(0, 112);
            setBackgroundColor(UIColor.transparent());

            //<editor-fold desc="Crafting Item Stacks">
            //<editor-fold desc="craftingItemStack0">
            craftingItemStack0.setOnInitAction(() -> {
                craftingItemStack0.setTopLeftPos(-168, -52);
                craftingItemStack0.setBottomRightPos(-136, -20);
                craftingItemStack0.setTopLeftAnchor(0.5, 0.5);
                craftingItemStack0.setBottomRightAnchor(0.5, 0.5);
                craftingItemStack0.setItemStack(recipeItems9[0]);
                craftingItemStack0.setGuiRecipesOverlay(guiRecipesOverlay);
                craftingItemStack0.setFilled(true);
            });
            addChildComponent("craftingItemStack0", craftingItemStack0);
            //</editor-fold>

            //<editor-fold desc="craftingItemStack1">
            craftingItemStack1.setOnInitAction(() -> {
                craftingItemStack1.setTopLeftPos(-132, -52);
                craftingItemStack1.setBottomRightPos(-100, -20);
                craftingItemStack1.setTopLeftAnchor(0.5, 0.5);
                craftingItemStack1.setBottomRightAnchor(0.5, 0.5);
                craftingItemStack1.setItemStack(recipeItems9[1]);
                craftingItemStack1.setGuiRecipesOverlay(guiRecipesOverlay);
                if (recipeWidth >= 2) {
                    craftingItemStack1.setFilled(true);
                } else {
                    craftingItemStack1.setFilled(false);
                }
            });
            addChildComponent("craftingItemStack1", craftingItemStack1);
            //</editor-fold>

            //<editor-fold desc="craftingItemStack2">
            craftingItemStack2.setOnInitAction(() -> {
                craftingItemStack2.setTopLeftPos(-96, -52);
                craftingItemStack2.setBottomRightPos(-64, -20);
                craftingItemStack2.setTopLeftAnchor(0.5, 0.5);
                craftingItemStack2.setBottomRightAnchor(0.5, 0.5);
                craftingItemStack2.setItemStack(recipeItems9[2]);
                craftingItemStack2.setGuiRecipesOverlay(guiRecipesOverlay);
                if (recipeWidth >= 3) {
                    craftingItemStack2.setFilled(true);
                } else {
                    craftingItemStack2.setFilled(false);
                }
            });
            addChildComponent("craftingItemStack2", craftingItemStack2);
            //</editor-fold>

            //<editor-fold desc="craftingItemStack3">
            craftingItemStack3.setOnInitAction(() -> {
                craftingItemStack3.setTopLeftPos(-168, -16);
                craftingItemStack3.setBottomRightPos(-136, 16);
                craftingItemStack3.setTopLeftAnchor(0.5, 0.5);
                craftingItemStack3.setBottomRightAnchor(0.5, 0.5);
                craftingItemStack3.setItemStack(recipeItems9[3]);
                craftingItemStack3.setGuiRecipesOverlay(guiRecipesOverlay);
                if (recipeHeight >= 2) {
                    craftingItemStack3.setFilled(true);
                } else {
                    craftingItemStack3.setFilled(false);
                }
            });
            addChildComponent("craftingItemStack3", craftingItemStack3);
            //</editor-fold>

            //<editor-fold desc="craftingItemStack4">
            craftingItemStack4.setOnInitAction(() -> {
                craftingItemStack4.setTopLeftPos(-132, -16);
                craftingItemStack4.setBottomRightPos(-100, 16);
                craftingItemStack4.setTopLeftAnchor(0.5, 0.5);
                craftingItemStack4.setBottomRightAnchor(0.5, 0.5);
                craftingItemStack4.setItemStack(recipeItems9[4]);
                craftingItemStack4.setGuiRecipesOverlay(guiRecipesOverlay);
                if (recipeHeight >= 2 && recipeWidth >= 2) {
                    craftingItemStack4.setFilled(true);
                } else {
                    craftingItemStack4.setFilled(false);
                }
            });
            addChildComponent("craftingItemStack4", craftingItemStack4);
            //</editor-fold>

            //<editor-fold desc="craftingItemStack5">
            craftingItemStack5.setOnInitAction(() -> {
                craftingItemStack5.setTopLeftPos(-96, -16);
                craftingItemStack5.setBottomRightPos(-64, 16);
                craftingItemStack5.setTopLeftAnchor(0.5, 0.5);
                craftingItemStack5.setBottomRightAnchor(0.5, 0.5);
                craftingItemStack5.setItemStack(recipeItems9[5]);
                craftingItemStack5.setGuiRecipesOverlay(guiRecipesOverlay);
                if (recipeHeight >= 2 && recipeWidth >= 3) {
                    craftingItemStack5.setFilled(true);
                } else {
                    craftingItemStack5.setFilled(false);
                }
            });
            addChildComponent("craftingItemStack5", craftingItemStack5);
            //</editor-fold>

            //<editor-fold desc="craftingItemStack6">
            craftingItemStack6.setOnInitAction(() -> {
                craftingItemStack6.setTopLeftPos(-168, 20);
                craftingItemStack6.setBottomRightPos(-136, 52);
                craftingItemStack6.setTopLeftAnchor(0.5, 0.5);
                craftingItemStack6.setBottomRightAnchor(0.5, 0.5);
                craftingItemStack6.setItemStack(recipeItems9[6]);
                craftingItemStack6.setGuiRecipesOverlay(guiRecipesOverlay);
                if (recipeHeight >= 3) {
                    craftingItemStack6.setFilled(true);
                } else {
                    craftingItemStack6.setFilled(false);
                }
            });
            addChildComponent("craftingItemStack6", craftingItemStack6);
            //</editor-fold>

            //<editor-fold desc="craftingItemStack7">
            craftingItemStack7.setOnInitAction(() -> {
                craftingItemStack7.setTopLeftPos(-132, 20);
                craftingItemStack7.setBottomRightPos(-100, 52);
                craftingItemStack7.setTopLeftAnchor(0.5, 0.5);
                craftingItemStack7.setBottomRightAnchor(0.5, 0.5);
                craftingItemStack7.setItemStack(recipeItems9[7]);
                craftingItemStack7.setGuiRecipesOverlay(guiRecipesOverlay);
                if (recipeHeight >= 3 && recipeWidth >= 2) {
                    craftingItemStack7.setFilled(true);
                } else {
                    craftingItemStack7.setFilled(false);
                }
            });
            addChildComponent("craftingItemStack7", craftingItemStack7);
            //</editor-fold>

            //<editor-fold desc="craftingItemStack8">
            craftingItemStack8.setOnInitAction(() -> {
                craftingItemStack8.setTopLeftPos(-96, 20);
                craftingItemStack8.setBottomRightPos(-64, 52);
                craftingItemStack8.setTopLeftAnchor(0.5, 0.5);
                craftingItemStack8.setBottomRightAnchor(0.5, 0.5);
                craftingItemStack8.setItemStack(recipeItems9[8]);
                craftingItemStack8.setGuiRecipesOverlay(guiRecipesOverlay);
                if (recipeHeight >= 3 && recipeWidth >= 3) {
                    craftingItemStack8.setFilled(true);
                } else {
                    craftingItemStack8.setFilled(false);
                }
            });
            addChildComponent("craftingItemStack8", craftingItemStack8);
            //</editor-fold>
            //</editor-fold>

        });
    }

}
