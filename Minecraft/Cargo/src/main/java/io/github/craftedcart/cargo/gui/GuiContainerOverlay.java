package io.github.craftedcart.cargo.gui;

import io.github.craftedcart.cargo.comparator.AlphabeticalByDisplayNameComparator;
import io.github.craftedcart.cargo.comparator.IDComparator;
import io.github.craftedcart.cargo.item.ItemData;
import io.github.craftedcart.cargo.item.ItemRegistry;
import io.github.craftedcart.cargo.reference.CargoSettings;
import io.github.craftedcart.cargo.util.StringManager;
import io.github.craftedcart.fluidui.FluidUIScreen;
import io.github.craftedcart.fluidui.component.*;
import io.github.craftedcart.fluidui.plugin.AbstractComponentPlugin;
import io.github.craftedcart.fluidui.uiaction.UIAction;
import io.github.craftedcart.fluidui.util.EnumHAlignment;
import io.github.craftedcart.fluidui.util.EnumImageScaling;
import io.github.craftedcart.fluidui.util.EnumVAlignment;
import io.github.craftedcart.fluidui.util.UIColor;
import io.github.craftedcart.fluiduiminecraftadditions.component.ItemStackDisplay;
import io.github.craftedcart.fluiduiminecraftadditions.util.TextureManager;
import io.github.craftedcart.fluiduiminecraftadditions.util.TooltipHelper;
import net.minecraft.client.Minecraft;

import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * @author CraftedCart
 * Created on 02/07/2016 (DD/MM/YYYY)
 */
public class GuiContainerOverlay extends FluidUIScreen {

    final Panel sidebarPanel = new Panel();
    private final ListBox itemsListBox = new ListBox();
    private final Panel bottomPanel = new Panel();
    final TextField searchField = new TextField();
    private final Button scrollUpButton = new Button();
    private final Button scrollDownButton = new Button();
    private final Label itemsLabel = new Label();

    //Search error panels
    private final Panel invalidRegexPanel = new Panel();
    private final Image invalidRegexImage = new Image();
    private final Label invalidRegexLabel = new Label();

    public static List<Component> quickButtons = new ArrayList<>();

    public GuiContainerOverlay() {
        setTheme(new DefaultUITheme());

        sidebarPanel.setOnInitAction(() -> {
            sidebarPanel.setTopLeftPos(-256, 0);
            sidebarPanel.setBottomRightPos(0, -24);
            sidebarPanel.setTopLeftAnchor(1, 0);
            sidebarPanel.setBottomRightAnchor(1, 1);
        });
        addChildComponent("sidebarPanel", sidebarPanel);

        itemsListBox.setOnInitAction(() -> {
            itemsListBox.setTopLeftPos(0, 0);
            itemsListBox.setBottomRightPos(0, 0);
            itemsListBox.setTopLeftAnchor(0, 0);
            itemsListBox.setBottomRightAnchor(1, 1);
            itemsListBox.setChildComponentSpacing(0);
        });
        sidebarPanel.addChildComponent("itemsListBox", itemsListBox);

        invalidRegexPanel.setOnInitAction(() -> {
            invalidRegexPanel.setTopLeftPos(0, 0);
            invalidRegexPanel.setBottomRightPos(0, 256);
            invalidRegexPanel.setBackgroundColor(UIColor.matRed());
            invalidRegexPanel.setVisible(false);
        });
        itemsListBox.addChildComponent("invalidRegexPanel", invalidRegexPanel);

        invalidRegexImage.setOnInitAction(() -> {
            invalidRegexImage.setTopLeftPos(24, 24);
            invalidRegexImage.setBottomRightPos(-24, -72);
            invalidRegexImage.setTopLeftAnchor(0, 0);
            invalidRegexImage.setBottomRightAnchor(1, 1);
            invalidRegexImage.setTexture(TextureManager.getTexture("error256"));
            invalidRegexImage.setColor(UIColor.matWhite());
            invalidRegexImage.setImageScaling(EnumImageScaling.fit);
        });
        invalidRegexPanel.addChildComponent("invalidRegexImage", invalidRegexImage);

        invalidRegexLabel.setOnInitAction(() -> {
            invalidRegexLabel.setTopLeftPos(24, -48);
            invalidRegexLabel.setBottomRightPos(0, 0);
            invalidRegexLabel.setTopLeftAnchor(0, 1);
            invalidRegexLabel.setBottomRightAnchor(1, 1);
            invalidRegexLabel.setVerticalAlign(EnumVAlignment.centre);
            invalidRegexLabel.setText(StringManager.translateToLocal("cargo:invalidRegex"));
        });
        invalidRegexPanel.addChildComponent("invalidRegexLabel", invalidRegexLabel);

        addItemsToList();

        bottomPanel.setOnInitAction(() -> {
            bottomPanel.setTopLeftPos(0, -24);
            bottomPanel.setBottomRightPos(0, 0);
            bottomPanel.setTopLeftAnchor(0, 1);
            bottomPanel.setBottomRightAnchor(1, 1);
        });
        addChildComponent("bottomPanel", bottomPanel);

        searchField.setOnInitAction(() -> {
            searchField.setTopLeftPos(-256, 0);
            searchField.setBottomRightPos(-48, 0);
            searchField.setTopLeftAnchor(1, 0);
            searchField.setBottomRightAnchor(1, 1);
            searchField.setPlaceholder(StringManager.translateToLocalFormatted("cargo:searchBarPlaceholderFormat", StringManager.translateToLocal("cargo:search"),
                    Minecraft.IS_RUNNING_ON_MAC ? StringManager.translateToLocal("cargo:searchMacShortcut") : StringManager.translateToLocal("cargo:searchOtherShortcut")));
        });
        searchField.setOnReturnAction(() -> searchField.setSelected(false));
        searchField.setOnValueChangedAction(() -> searchByString(searchField.value));
        bottomPanel.addChildComponent("searchField", searchField);

        scrollUpButton.setOnInitAction(() -> {
            scrollUpButton.setTopLeftPos(-48, 0);
            scrollUpButton.setBottomRightPos(-24, 0);
            scrollUpButton.setTopLeftAnchor(1, 0);
            scrollUpButton.setBottomRightAnchor(1, 1);
            scrollUpButton.setTexture(TextureManager.getTexture("upArrow24"));
        });
        scrollUpButton.setOnLMBAction(() -> itemsListBox.scrollOffset += itemsListBox.height);
        bottomPanel.addChildComponent("scrollUpButton", scrollUpButton);

        scrollDownButton.setOnInitAction(() -> {
            scrollDownButton.setTopLeftPos(-24, 0);
            scrollDownButton.setBottomRightPos(0, 0);
            scrollDownButton.setTopLeftAnchor(1, 0);
            scrollDownButton.setBottomRightAnchor(1, 1);
            scrollDownButton.setTexture(TextureManager.getTexture("downArrow24"));
        });
        scrollDownButton.setOnLMBAction(() -> itemsListBox.scrollOffset -= itemsListBox.height);
        bottomPanel.addChildComponent("scrollDownButton", scrollDownButton);

        itemsLabel.setOnInitAction(() -> {
            itemsLabel.setTopLeftPos(0, 0);
            itemsLabel.setBottomRightPos(0, 0);
            itemsLabel.setTopLeftAnchor(0, 0);
            itemsLabel.setBottomRightAnchor(1, 1);
            itemsLabel.setHorizontalAlign(EnumHAlignment.centre);
            itemsLabel.setVerticalAlign(EnumVAlignment.centre);
            itemsLabel.setText(StringManager.translateToLocalFormatted("cargo:itemCount", ItemRegistry.itemComponentMap.size()));
        });
        bottomPanel.addChildComponent("itemsLabel", itemsLabel);

        addQuickButtons(bottomPanel, quickButtons);
    }

    public void addItemsToList() {
        SortedSet<ItemData> allItemData = new TreeSet<>(new IDComparator());
        allItemData.addAll(ItemRegistry.getItems());
        for (ItemData itemData : allItemData) { //Loop through all items

            final Panel itemPanel = new Panel();
            itemPanel.setOnInitAction(() -> {
                itemPanel.setTopLeftPos(0, 0);
                itemPanel.setBottomRightPos(0, 24);
                itemPanel.setBackgroundColor(UIColor.transparent());
            });
            itemsListBox.addChildComponent(itemData.getUniqueID(), itemPanel);
            ItemRegistry.itemComponentMap.put(itemData, itemPanel);

            final Button itemButton = new Button();
            itemButton.setOnInitAction(() -> {
                itemButton.setTopLeftPos(0, 0);
                itemButton.setBottomRightPos(-48, 0);
                itemButton.setTopLeftAnchor(0, 0);
                itemButton.setBottomRightAnchor(1, 1);
                itemButton.setBackgroundIdleColor(UIColor.transparent());
                itemButton.setShowTooltipIfMouseOverChildComponent(true);
            });
            itemButton.addPlugin(new AbstractComponentPlugin() {
                @Override
                public void onPreDraw() {
                    if (itemButton.mouseOver) {
                        itemButton.setCustomTooltip(TooltipHelper.generateItemStackCustomTooltip(itemData.itemStack, new DefaultUITheme() {{
                            panelBackgroundColor = UIColor.matGrey800(0.9);
                        }}));
                    }
                }
            });
            itemButton.setOnLMBAction(() -> Minecraft.getMinecraft().thePlayer.sendChatMessage(
                    CargoSettings.getGiveCommand(itemData.getItemStringID(), itemData.getMaxStackSize(), itemData.getItemMetadata())));
            itemPanel.addChildComponent("itemButton", itemButton);

            final ItemStackDisplay itemStackDisplay = new ItemStackDisplay();
            itemStackDisplay.setOnInitAction(() -> {
                itemStackDisplay.setTopLeftPos(2, 0);
                itemStackDisplay.setBottomRightPos(26, 0);
                itemStackDisplay.setTopLeftAnchor(0, 0);
                itemStackDisplay.setBottomRightAnchor(0, 1);
                itemStackDisplay.setItemStack(itemData.itemStack);
            });
            itemButton.addChildComponent("itemStackDisplay", itemStackDisplay);

            final Label itemNameLabel = new Label();
            itemNameLabel.setOnInitAction(() -> {
                itemNameLabel.setTopLeftPos(28, 0);
                itemNameLabel.setBottomRightPos(0, 0);
                itemNameLabel.setTopLeftAnchor(0, 0);
                itemNameLabel.setBottomRightAnchor(1, 1);
                itemNameLabel.setVerticalAlign(EnumVAlignment.centre);
                itemNameLabel.setText(itemData.itemStack.getItem().getItemStackDisplayName(itemData.itemStack));
            });
            itemButton.addChildComponent("itemNameLabel", itemNameLabel);
            
            final Button itemRecipeButton = new Button();
            itemRecipeButton.setOnInitAction(() -> {
                itemRecipeButton.setTopLeftPos(-48, 0);
                itemRecipeButton.setBottomRightPos(-24, 0);
                itemRecipeButton.setTopLeftAnchor(1, 0);
                itemRecipeButton.setBottomRightAnchor(1, 1);
                itemRecipeButton.setBackgroundIdleColor(UIColor.matWhite());
                itemRecipeButton.setTexture(TextureManager.getTexture("recipe24"));
                itemRecipeButton.setTooltip(StringManager.translateToLocal("cargo:viewRecipes"));
                itemRecipeButton.setVisible(false);
            });
            itemRecipeButton.setOnLMBAction(() -> showRecipes(itemData));
            itemPanel.addChildComponent("itemRecipeButton", itemRecipeButton);

            final Button itemUsageButton = new Button();
            itemUsageButton.setOnInitAction(() -> {
                itemUsageButton.setTopLeftPos(-24, 0);
                itemUsageButton.setBottomRightPos(0, 0);
                itemUsageButton.setTopLeftAnchor(1, 0);
                itemUsageButton.setBottomRightAnchor(1, 1);
                itemUsageButton.setBackgroundIdleColor(UIColor.matWhite());
                itemUsageButton.setTexture(TextureManager.getTexture("usage24"));
                itemUsageButton.setTooltip(StringManager.translateToLocal("cargo:viewUsages"));
                itemUsageButton.setVisible(false);
            });
            itemUsageButton.setOnLMBAction(() -> showUsages(itemData));
            itemPanel.addChildComponent("itemUsageButton", itemUsageButton);

            itemPanel.addPlugin(new AbstractComponentPlugin() {
                @Override
                public void onPreDraw() {
                    if (itemPanel.mouseOver) {
                        itemRecipeButton.setVisible(true);
                        itemUsageButton.setVisible(true);
                    } else {
                        itemRecipeButton.setVisible(false);
                        itemUsageButton.setVisible(false);
                    }
                }
            });

        }
    }

    public void searchByString(String searchString) {
        int foundItems = 0;

        Pattern regexPattern = null;
        if (CargoSettings.regexSearch) {
            try {
                regexPattern = Pattern.compile("(?s).*" + searchString.toLowerCase() + ".*");
                invalidRegexPanel.setVisible(false);
            } catch (PatternSyntaxException e) { //Invalid RegEx
                for (Map.Entry<ItemData, Component> entry : ItemRegistry.itemComponentMap.entrySet()) {
                    ItemData itemData = entry.getKey();
                    Component component = entry.getValue();

                    component.setVisible(false);
                }

                invalidRegexPanel.setVisible(true);

                itemsListBox.scrollOffset = 0;
                itemsListBox.smoothedScrollOffset = 0;

                itemsListBox.reorganizeChildComponents();

                if (searchString.isEmpty()) {
                    itemsLabel.setText(StringManager.translateToLocalFormatted("cargo:itemCount", ItemRegistry.itemComponentMap.size()));
                } else {
                    itemsLabel.setText(StringManager.translateToLocalFormatted("cargo:itemsFoundCount", foundItems, ItemRegistry.itemComponentMap.size()));
                }

                return;
            }
        }

        for (Map.Entry<ItemData, Component> entry : ItemRegistry.itemComponentMap.entrySet()) {
            ItemData itemData = entry.getKey();
            Component component = entry.getValue();
            if (CargoSettings.regexSearch) { //Search with RegEx
                assert regexPattern != null;
                if (regexPattern.matcher(itemData.itemStack.getDisplayName().toLowerCase()).find()) {
                    component.setVisible(true);
                    foundItems++;
                } else {
                    component.setVisible(false);
                }
            } else { //Search without RegEx
                if (itemData.itemStack.getDisplayName().toLowerCase().contains(searchString.toLowerCase())) {
                    component.setVisible(true);
                    foundItems++;
                } else {
                    component.setVisible(false);
                }
            }
        }

        itemsListBox.scrollOffset = 0;
        itemsListBox.smoothedScrollOffset = 0;

        itemsListBox.reorganizeChildComponents();

        if (searchString.isEmpty()) {
            itemsLabel.setText(StringManager.translateToLocalFormatted("cargo:itemCount", ItemRegistry.itemComponentMap.size()));
        } else {
            itemsLabel.setText(StringManager.translateToLocalFormatted("cargo:itemsFoundCount", foundItems, ItemRegistry.itemComponentMap.size()));
        }
    }

    private static void toggleRegexSearch() {
        CargoSettings.regexSearch = !CargoSettings.regexSearch;

        if (!CargoSettings.regexSearch) {
            GuiOverlayHandler.containerOverlay.invalidRegexPanel.setVisible(false);
        }

        GuiOverlayHandler.containerOverlay.searchByString(GuiOverlayHandler.containerOverlay.searchField.value);
    }

    private void showRecipes(ItemData itemData) {
        GuiRecipesOverlay overlay = new GuiRecipesOverlay();
        overlay.getRecipes(itemData);
        setOverlayUiScreen(overlay);
    }

    private void showUsages(ItemData itemData) {
        GuiRecipesOverlay overlay = new GuiRecipesOverlay();
        overlay.getUsages(itemData);
        setOverlayUiScreen(overlay);
    }

    public static void registerQuickButton(Component component) {
        quickButtons.add(component);
    }

    private void addQuickButtons(Component parentComponent, List<Component> quickButtons) {

        class LoopI {
            private int i = 0;
        }
        LoopI loopI = new LoopI();

        for (Component component : quickButtons) {

            final UIAction prevInitAction = component.onInitAction;
            component.setOnInitAction(() -> {
                if (prevInitAction != null) {
                    prevInitAction.execute();
                }
                component.setTopLeftPos(loopI.i * 24, 0);
                component.setBottomRightPos(loopI.i * 24 + 24, 0);
                component.setTopLeftAnchor(0, 0);
                component.setBottomRightAnchor(0, 1);
            });

            parentComponent.addChildComponent(component);
            loopI.i++;
        }
    }

    public static void registerCargoQuickButtons() {
        //<editor-fold desc="Settings Button">
        final Button settingsButton = new Button();

        settingsButton.setOnInitAction(() -> {
            settingsButton.setTexture(TextureManager.getTexture("settings24"));
            settingsButton.setTooltip(StringManager.translateToLocal("cargo:settings"));
        });
        settingsButton.setName("settingsButton");
        settingsButton.setOnLMBAction(() -> GuiOverlayHandler.containerOverlay.setOverlayUiScreen(new GuiSettingsOverlay()));
        registerQuickButton(settingsButton);
        //</editor-fold>

        //<editor-fold desc="Regex Button">
        final CheckBox regexButton = new CheckBox();

        regexButton.setOnInitAction(() -> {
            regexButton.setTexture(TextureManager.getTexture("regex24"));
            regexButton.setTooltip(StringManager.translateToLocal("cargo:useRegexSearch"));
        });
        regexButton.setName("regexButton");
        regexButton.setOnLMBAction(GuiContainerOverlay::toggleRegexSearch);
        registerQuickButton(regexButton);
        //</editor-fold>
    }

}
