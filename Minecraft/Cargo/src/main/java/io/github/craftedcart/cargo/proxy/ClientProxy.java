package io.github.craftedcart.cargo.proxy;

import io.github.craftedcart.cargo.gui.GuiContainerOverlay;
import io.github.craftedcart.cargo.gui.GuiOverlayHandler;
import io.github.craftedcart.cargo.item.ItemRegistry;
import io.github.craftedcart.cargo.plugin.recipe.shapedCrafting.VanillaShapedCraftingCategory;
import io.github.craftedcart.cargo.util.LogHelper;
import io.github.craftedcart.fluidui.FontCache;
import io.github.craftedcart.fluiduiminecraftadditions.util.TextureManager;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.newdawn.slick.SlickException;

import java.awt.*;
import java.io.IOException;

/**
 * Created by CraftedCart on 17/11/2015 (DD/MM/YYYY)
 */
public class ClientProxy extends CommonProxy {

    @Override
    public void getConfig(FMLPreInitializationEvent e) {
        //No-Op (Yet?)
    }

    @Override
    public void preInit() {
        LogHelper.addLogPrefix("Client");

        try {
            LogHelper.info("Registering AWT Font \"Roboto-Regular\"");

            FontCache.registerAWTFont("Roboto-Regular",
                    Minecraft.getMinecraft().getResourceManager().getResource(new ResourceLocation("cargo:font/Roboto-Regular.ttf")).getInputStream(),
                    Font.TRUETYPE_FONT);

        } catch (IOException | FontFormatException e) {
            LogHelper.error("Error when registering AWT Font \"Roboto-Regular\"");
            LogHelper.error(e);
        }

        try {
            LogHelper.info("Registering Unicode Font for \"Roboto-Regular\" 16px");
            FontCache.registerUnicodeFont("Roboto-Regular", 16);

            LogHelper.info("Registering Unicode Font for \"Roboto-Regular\" 24px");
            FontCache.registerUnicodeFont("Roboto-Regular", 24);

        } catch (SlickException | IOException | FontFormatException e) {
            LogHelper.error("Error when registering Unicode Fonts \"Roboto-Regular\"");
            LogHelper.error(e);
        }

        LogHelper.info("Registering textures");
        TextureManager.registerTexture("upArrow24", new ResourceLocation("cargo:fluidUITextures/upArrow24.png"));
        TextureManager.registerTexture("downArrow24", new ResourceLocation("cargo:fluidUITextures/downArrow24.png"));
        TextureManager.registerTexture("settings24", new ResourceLocation("cargo:fluidUITextures/settings24.png"));
        TextureManager.registerTexture("regex24", new ResourceLocation("cargo:fluidUITextures/regex24.png"));
        TextureManager.registerTexture("error256", new ResourceLocation("cargo:fluidUITextures/error256.png"));
        TextureManager.registerTexture("recipe24", new ResourceLocation("cargo:fluidUITextures/recipe24.png"));
        TextureManager.registerTexture("usage24", new ResourceLocation("cargo:fluidUITextures/usage24.png"));

        LogHelper.info("Registering quick buttons");
        GuiContainerOverlay.registerCargoQuickButtons();

        LogHelper.removeLastLogPrefix();
    }

    @Override
    public void postInit() {
        LogHelper.addLogPrefix("Client");

        LogHelper.info("Initializing ItemRegistry (Getting all items)");
        ItemRegistry.init();

        LogHelper.info("Initializing VanillaShapedCraftingCategory");
        VanillaShapedCraftingCategory.init();

        LogHelper.info("Initializing GuiOverlayHandler");
        GuiOverlayHandler.init();

        LogHelper.info("Registering GuiOverlayHandler with the EVENT_BUS");
        MinecraftForge.EVENT_BUS.register(new GuiOverlayHandler());

        LogHelper.removeLastLogPrefix();
    }

}
