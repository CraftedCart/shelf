package io.github.craftedcart.cargo.gui;

import io.github.craftedcart.fluidui.FluidUIScreen;
import io.github.craftedcart.fluidui.component.ListBox;
import io.github.craftedcart.fluidui.component.Panel;
import io.github.craftedcart.fluidui.plugin.PluginSmoothAnimateAnchor;
import io.github.craftedcart.fluidui.plugin.PluginSmoothAnimatePanelBackgroundColor;
import io.github.craftedcart.fluidui.util.UIColor;

/**
 * @author CraftedCart
 * Created on 04/07/2016 (DD/MM/YYYY)
 */
public class GuiSettingsOverlay extends FluidUIScreen {

    public final Panel screenBackgroundPanel = new Panel();
    public final ListBox settingsListBox = new ListBox();

    public GuiSettingsOverlay() {
        setTheme(new DefaultUITheme());

        screenBackgroundPanel.setOnInitAction(() -> {
            screenBackgroundPanel.setTopLeftPos(0, 0);
            screenBackgroundPanel.setBottomRightPos(0, 0);
            screenBackgroundPanel.setTopLeftAnchor(0, 0);
            screenBackgroundPanel.setBottomRightAnchor(1, 1);
            screenBackgroundPanel.setBackgroundColor(UIColor.pureBlack(0));

            PluginSmoothAnimatePanelBackgroundColor screenBgColorAnim = new PluginSmoothAnimatePanelBackgroundColor();
            screenBgColorAnim.setTargetBackgroundColor(UIColor.pureBlack(0.75));
            screenBackgroundPanel.addPlugin(screenBgColorAnim);
            screenBackgroundPanel.setOnLMBAction(() -> {
                if (parentComponent instanceof FluidUIScreen) {
                    ((FluidUIScreen) parentComponent).setOverlayUiScreen(null);
                }
            });

            settingsListBox.setOnInitAction(() -> {
                settingsListBox.setTopLeftPos(0, 0);
                settingsListBox.setBottomRightPos(512, 0);
                settingsListBox.setTopLeftAnchor(0, 1);
                settingsListBox.setBottomRightAnchor(0, 1);
                settingsListBox.setBackgroundColor(UIColor.matWhite());

                PluginSmoothAnimateAnchor settingsListBoxAnimAnchor = new PluginSmoothAnimateAnchor();
                settingsListBoxAnimAnchor.setTargetTopLeftAnchor(0, 0);
                settingsListBox.addPlugin(settingsListBoxAnimAnchor);
            });
            screenBackgroundPanel.addChildComponent("settingsListBox", settingsListBox);
        });
        addChildComponent("screenBackroundPanel", screenBackgroundPanel);

    }

}
