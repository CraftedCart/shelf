package io.github.craftedcart.cargo.gui;

import io.github.craftedcart.fluidui.FontCache;
import io.github.craftedcart.fluidui.theme.UITheme;
import io.github.craftedcart.fluidui.util.UIColor;

/**
 * @author CraftedCart
 * Created on 05/03/2016 (DD/MM/YYYY)
 */
public class DefaultUITheme extends UITheme {

    public UIColor recipeItemStackFilledColor = UIColor.matGrey400();
    public UIColor recipeItemStackEmptyColor = UIColor.matGrey300();

    public DefaultUITheme() {
        labelFont = FontCache.getUnicodeFont("Roboto-Regular", 16);
        tooltipFont = FontCache.getUnicodeFont("Roboto-Regular", 16);
        tooltipColor = UIColor.matWhite();
        tooltipTextColor = UIColor.matGrey900();
        scrollbarThickness = 4;
        scrollbarBG = UIColor.matGrey900();
        scrollbarFG = UIColor.matBlue();
        listBoxBackgroundColor = UIColor.transparent();
        labelTextColor = UIColor.matWhite();
        checkBoxUncheckedColor = UIColor.matGrey300();
    }

}
