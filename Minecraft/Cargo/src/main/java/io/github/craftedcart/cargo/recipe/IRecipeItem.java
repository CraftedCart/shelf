package io.github.craftedcart.cargo.recipe;

import io.github.craftedcart.cargo.gui.GuiRecipesOverlay;
import io.github.craftedcart.fluidui.component.Component;
import net.minecraft.item.ItemStack;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * @author CraftedCart
 * Created on 26/07/2016 (DD/MM/YYYY)
 */
public interface IRecipeItem {

    /**
     * @return The UI component to display to the user per recipe
     */
    @NotNull
    Component getUIComponent(GuiRecipesOverlay guiRecipesOverlay);

    /**
     * @return The items used to craft the recipe
     */
    @Nullable
    ItemStack[] getInputItems();

    /**
     * @return The items gotten from crafting the recipe
     */
    @Nullable
    ItemStack[] getOutputItems();

}
