package io.github.craftedcart.cargo.plugin.recipe.shapedCrafting;

import io.github.craftedcart.cargo.recipe.AbstractRecipeCategory;
import io.github.craftedcart.cargo.recipe.RecipeManager;
import io.github.craftedcart.cargo.util.LogHelper;
import io.github.craftedcart.cargo.util.StringManager;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.ShapedRecipes;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * @author CraftedCart
 * Created on 26/07/2016 (DD/MM/YYYY)
 */
public class VanillaShapedCraftingCategory extends AbstractRecipeCategory {

    public static void init() {
        VanillaShapedCraftingCategory category = new VanillaShapedCraftingCategory();

        List<IRecipe> recipeList = new ArrayList<>(CraftingManager.getInstance().getRecipeList());

        for (IRecipe recipe : recipeList) {
            if (recipe instanceof ShapedRecipes) {
                ShapedRecipes shapedRecipe = (ShapedRecipes) recipe;

                if (shapedRecipe.getRecipeOutput() != null) {
                    VanillaShapedRecipeItem recipeItem = new VanillaShapedRecipeItem(shapedRecipe.recipeItems,
                            shapedRecipe.recipeWidth, shapedRecipe.recipeHeight, shapedRecipe.getRecipeOutput());

                    category.addRecipe(recipeItem);

                    LogHelper.info(recipeItem);
                }
            }
        }

        RecipeManager.addRecipeCategory(category);
    }

    @NotNull
    @Override
    public String getCategoryID() {
        return "vanillaCrafting";
    }

    @NotNull
    @Override
    public String getCategoryLocalizedName() {
        return StringManager.translateToLocal("cargo:vanillaShapedCrafting");
    }

}
