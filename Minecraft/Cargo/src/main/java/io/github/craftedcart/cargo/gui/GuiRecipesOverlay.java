package io.github.craftedcart.cargo.gui;

import io.github.craftedcart.cargo.item.ItemData;
import io.github.craftedcart.cargo.recipe.AbstractRecipeCategory;
import io.github.craftedcart.cargo.recipe.IRecipeItem;
import io.github.craftedcart.cargo.recipe.RecipeManager;
import io.github.craftedcart.cargo.recipe.RecipesUIElements;
import io.github.craftedcart.cargo.util.StringManager;
import io.github.craftedcart.fluidui.FluidUIScreen;
import io.github.craftedcart.fluidui.FontCache;
import io.github.craftedcart.fluidui.component.*;
import io.github.craftedcart.fluidui.plugin.PluginSmoothAnimateAnchor;
import io.github.craftedcart.fluidui.plugin.PluginSmoothAnimatePanelBackgroundColor;
import io.github.craftedcart.fluidui.util.EnumHAlignment;
import io.github.craftedcart.fluidui.util.EnumVAlignment;
import io.github.craftedcart.fluidui.util.UIColor;
import io.github.craftedcart.fluiduiminecraftadditions.util.TextureManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author CraftedCart
 * Created on 26/07/2016 (DD/MM/YYYY)
 */
public class GuiRecipesOverlay extends FluidUIScreen {

    private final Panel screenBackgroundPanel = new Panel();
    private final Panel recipesPanel = new Panel();
    private final Label recipesTitleLabel = new Label();
    private final GradientPanel recipesHeadingShadow = new GradientPanel();
    private final HorizontalListBox recipesBreadcrumbsListBox = new HorizontalListBox();
    private final GradientPanel recipesBreadcrumbsShadow = new GradientPanel();
    private final Panel recipesCategoriesPanel = new Panel();
    private final GradientPanel recipesCategoriesShadow = new GradientPanel();
    private final Panel workspacePanel = new Panel();

    private final Image noRecipesFoundImage = new Image();
    private final Label noRecipesFoundLabel = new Label();

    private final List<RecipesUIElements> breadcrumbsElements = new ArrayList<>();

    public GuiRecipesOverlay() {
        setTheme(new DefaultUITheme());

        //<editor-fold desc="Screen Background Panel">
        screenBackgroundPanel.setOnInitAction(() -> {
            screenBackgroundPanel.setTopLeftPos(0, 0);
            screenBackgroundPanel.setBottomRightPos(0, 0);
            screenBackgroundPanel.setTopLeftAnchor(0, 0);
            screenBackgroundPanel.setBottomRightAnchor(1, 1);
            screenBackgroundPanel.setBackgroundColor(UIColor.pureBlack(0));

            PluginSmoothAnimatePanelBackgroundColor screenBgColorAnim = new PluginSmoothAnimatePanelBackgroundColor();
            screenBgColorAnim.setTargetBackgroundColor(UIColor.pureBlack(0.75));
            screenBackgroundPanel.addPlugin(screenBgColorAnim);
            screenBackgroundPanel.setOnLMBAction(() -> {
                if (parentComponent instanceof FluidUIScreen) {
                    ((FluidUIScreen) parentComponent).setOverlayUiScreen(null);
                }
            });

            //<editor-fold desc="Recipes Panel">
            recipesPanel.setOnInitAction(() -> {
                recipesPanel.setTopLeftPos(24, 48);
                recipesPanel.setBottomRightPos(-24, 0);
                recipesPanel.setTopLeftAnchor(0, 1);
                recipesPanel.setBottomRightAnchor(1, 2);
                recipesPanel.setBackgroundColor(UIColor.matWhite());

                PluginSmoothAnimateAnchor settingsListBoxAnimAnchor = new PluginSmoothAnimateAnchor();
                settingsListBoxAnimAnchor.setTargetTopLeftAnchor(0, 0);
                settingsListBoxAnimAnchor.setTargetBottomRightAnchor(1, 1);
                recipesPanel.addPlugin(settingsListBoxAnimAnchor);

                //<editor-fold desc="Recipes Title Panel">
                recipesTitleLabel.setOnInitAction(() -> {
                    recipesTitleLabel.setTopLeftPos(72, 0);
                    recipesTitleLabel.setBottomRightPos(0, 72);
                    recipesTitleLabel.setTopLeftAnchor(0, 0);
                    recipesTitleLabel.setBottomRightAnchor(1, 0);
                    recipesTitleLabel.setVerticalAlign(EnumVAlignment.centre);
                    recipesTitleLabel.setFont(FontCache.getUnicodeFont("Roboto-Regular", 24));
                    recipesTitleLabel.setTextColor(UIColor.matGrey900());
                });
                recipesPanel.addChildComponent("recipesTitleLabel", recipesTitleLabel);
                //</editor-fold>

                //<editor-fold desc="Recipes Breadcrumbs List Box">
                recipesBreadcrumbsListBox.setOnInitAction(() -> {
                    recipesBreadcrumbsListBox.setTopLeftPos(0, 72);
                    recipesBreadcrumbsListBox.setBottomRightPos(0, 100);
                    recipesBreadcrumbsListBox.setTopLeftAnchor(0, 0);
                    recipesBreadcrumbsListBox.setBottomRightAnchor(1, 0);
                    recipesBreadcrumbsListBox.setBackgroundColor(UIColor.matWhite());
                });
                recipesPanel.addChildComponent("recipesBreadcrumbsListBox", recipesBreadcrumbsListBox);
                //</editor-fold>

                //<editor-fold desc="Recipes Heading Shadow">
                recipesHeadingShadow.setOnInitAction(() -> {
                    recipesHeadingShadow.setTopLeftPos(0, 72);
                    recipesHeadingShadow.setBottomRightPos(0, 76);
                    recipesHeadingShadow.setTopLeftAnchor(0, 0);
                    recipesHeadingShadow.setBottomRightAnchor(1, 0);
                });
                recipesPanel.addChildComponent("recipesHeadingShadow", recipesHeadingShadow);
                //</editor-fold>

                //<editor-fold desc="Recipes Categories Panel">
                recipesCategoriesPanel.setOnInitAction(() -> {
                    recipesCategoriesPanel.setTopLeftPos(0, 100);
                    recipesCategoriesPanel.setBottomRightPos(0, 124);
                    recipesCategoriesPanel.setTopLeftAnchor(0, 0);
                    recipesCategoriesPanel.setBottomRightAnchor(1, 0);
                    recipesCategoriesPanel.setBackgroundColor(UIColor.matWhite());
                });
                recipesPanel.addChildComponent("recipesCategoriesPanel", recipesCategoriesPanel);
                //</editor-fold>

                //<editor-fold desc="Recipes Breadcrumbs Shadow">
                recipesBreadcrumbsShadow.setOnInitAction(() -> {
                    recipesBreadcrumbsShadow.setTopLeftPos(0, 100);
                    recipesBreadcrumbsShadow.setBottomRightPos(0, 104);
                    recipesBreadcrumbsShadow.setTopLeftAnchor(0, 0);
                    recipesBreadcrumbsShadow.setBottomRightAnchor(1, 0);
                });
                recipesPanel.addChildComponent("recipesBreadcrumbsShadow", recipesBreadcrumbsShadow);
                //</editor-fold>

                workspacePanel.setOnInitAction(() -> {
                    workspacePanel.setTopLeftPos(0, 124);
                    workspacePanel.setBottomRightPos(0, 0);
                    workspacePanel.setTopLeftAnchor(0, 0);
                    workspacePanel.setBottomRightAnchor(1, 1);
                    workspacePanel.setBackgroundColor(UIColor.matWhite());
                });
                recipesPanel.addChildComponent("workspacePanel", workspacePanel);

                //<editor-fold desc="Recipes Categories Shadow">
                recipesCategoriesShadow.setOnInitAction(() -> {
                    recipesCategoriesShadow.setTopLeftPos(0, 124);
                    recipesCategoriesShadow.setBottomRightPos(0, 128);
                    recipesCategoriesShadow.setTopLeftAnchor(0, 0);
                    recipesCategoriesShadow.setBottomRightAnchor(1, 0);
                });
                recipesPanel.addChildComponent("recipesCategoriesShadow", recipesCategoriesShadow);
                //</editor-fold>

                noRecipesFoundImage.setOnInitAction(() -> {
                    noRecipesFoundImage.setTopLeftPos(-128, -128);
                    noRecipesFoundImage.setBottomRightPos(128, 128);
                    noRecipesFoundImage.setTopLeftAnchor(0.5, 0.5);
                    noRecipesFoundImage.setBottomRightAnchor(0.5, 0.5);
                    noRecipesFoundImage.setTexture(TextureManager.getTexture("error256"));
                    noRecipesFoundImage.setColor(UIColor.matGrey900());
                    noRecipesFoundImage.setVisible(false);
                });
                workspacePanel.addChildComponent("noRecipesFoundImage", noRecipesFoundImage);

                noRecipesFoundLabel.setOnInitAction(() -> {
                    noRecipesFoundLabel.setTopLeftPos(-128, 132);
                    noRecipesFoundLabel.setBottomRightPos(128, 156);
                    noRecipesFoundLabel.setTopLeftAnchor(0.5, 0.5);
                    noRecipesFoundLabel.setBottomRightAnchor(0.5, 0.5);
                    noRecipesFoundLabel.setVerticalAlign(EnumVAlignment.centre);
                    noRecipesFoundLabel.setHorizontalAlign(EnumHAlignment.centre);
                    noRecipesFoundLabel.setText(StringManager.translateToLocal("cargo:noRecipesFound"));
                    noRecipesFoundLabel.setTextColor(UIColor.matGrey900());
                    noRecipesFoundLabel.setVisible(false);
                });
                workspacePanel.addChildComponent("noRecipesFoundLabel", noRecipesFoundLabel);
            });
            screenBackgroundPanel.addChildComponent("recipesPanel", recipesPanel);
            //</editor-fold>
        });
        addChildComponent("screenBackroundPanel", screenBackgroundPanel);
        //</editor-fold>

    }

    private RecipesUIElements getCurrentBreadcrumbElement() {
        return breadcrumbsElements.get(breadcrumbsElements.size() - 1);
    }

    public void getRecipes(ItemData itemData) {
        recipesTitleLabel.setText(StringManager.translateToLocal("cargo:recipes")); //Set the title to say recipes
        addBreadcrumb(itemData, RecipeType.RECIPE); //Add a breadcrumb item
        RecipeManager.findRecipes(itemData, foundRecipes -> {
            for (AbstractRecipeCategory category : foundRecipes) { //Get all categories for found recipes
                getCurrentBreadcrumbElement().categories.add(category); //Add the category to the category list
            }
        });

        if (!getCurrentBreadcrumbElement().categories.isEmpty()) {
            for (AbstractRecipeCategory category : getCurrentBreadcrumbElement().categories) {
                final ListBox categoryListBox = new ListBox();
                categoryListBox.setOnInitAction(() -> {
                    categoryListBox.setTopLeftPos(0, 0);
                    categoryListBox.setBottomRightPos(0, 0);
                    categoryListBox.setTopLeftAnchor(0, 0);
                    categoryListBox.setBottomRightAnchor(1, 1);
                    categoryListBox.setBackgroundColor(UIColor.transparent());
                });
                workspacePanel.addChildComponent(category.getCategoryID(), categoryListBox);

                int i = 0;
                for (IRecipeItem recipeItem : category.getRecipes()) {
                    categoryListBox.addChildComponent(Objects.toString(i), recipeItem.getUIComponent(this));
                    i++;
                }
            }
        } else {
            noRecipesFoundImage.setVisible(true);
            noRecipesFoundLabel.setVisible(true);
        }
    }

    public void getUsages(ItemData itemData) {
        recipesTitleLabel.setText(StringManager.translateToLocal("cargo:usages")); //Set the title to say usages
        addBreadcrumb(itemData, RecipeType.USAGE); //Add a breadcrumb item
    }

    private Component getBreadcrumbsItemComponent(ItemData itemData, RecipeType type) {

        final Panel breadcrumbsPanel = new Panel();
        breadcrumbsPanel.setOnInitAction(() -> {
            breadcrumbsPanel.setTopLeftPos(0, 0);
            double totalWidth = 34 + new DefaultUITheme().labelFont.getWidth(itemData.itemStack.getItem().getItemStackDisplayName(itemData.itemStack));
            breadcrumbsPanel.setBottomRightPos(totalWidth, 0);
            breadcrumbsPanel.setBackgroundColor(UIColor.transparent());

            Image typeImage = new Image();
            typeImage.setOnInitAction(() -> {
                typeImage.setTopLeftPos(4, 0);
                typeImage.setBottomRightPos(28, 0);
                typeImage.setTopLeftAnchor(0, 0);
                typeImage.setBottomRightAnchor(0, 1);
                typeImage.setColor(UIColor.matGrey900());

                if (type == RecipeType.RECIPE) {
                    typeImage.setTexture(TextureManager.getTexture("recipe24"));
                } else if (type == RecipeType.USAGE) {
                    typeImage.setTexture(TextureManager.getTexture("usage24"));
                }
            });
            breadcrumbsPanel.addChildComponent("typeImage", typeImage);

            Label itemNameLabel = new Label();
            itemNameLabel.setOnInitAction(() -> {
                itemNameLabel.setTopLeftPos(32, 0);
                itemNameLabel.setBottomRightPos(0, 0);
                itemNameLabel.setTopLeftAnchor(0, 0);
                itemNameLabel.setBottomRightAnchor(1, 1);
                itemNameLabel.setVerticalAlign(EnumVAlignment.centre);
                itemNameLabel.setTextColor(UIColor.matGrey900());
                itemNameLabel.setText(itemData.itemStack.getItem().getItemStackDisplayName(itemData.itemStack));
            });
            breadcrumbsPanel.addChildComponent("itemNameLabel", itemNameLabel);
        });

        return breadcrumbsPanel;

    }

    private void addBreadcrumb(String id, Component component) {
        recipesBreadcrumbsListBox.addChildComponent(id, component);
        breadcrumbsElements.add(new RecipesUIElements());
    }

    private void addBreadcrumb(ItemData itemData, RecipeType type) {
        addBreadcrumb(itemData.getUniqueID(), getBreadcrumbsItemComponent(itemData, type));
    }

    private enum RecipeType {
        RECIPE, USAGE
    }

}
