package io.github.craftedcart.cargo;

import io.github.craftedcart.cargo.proxy.IProxy;
import io.github.craftedcart.cargo.reference.Reference;
import io.github.craftedcart.cargo.util.LogHelper;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

import java.awt.*;
import java.io.IOException;

/**
 * @author CraftedCart
 * Created on 19/06/2016
 */
@Mod(modid = Reference.MOD_ID, name = Reference.MOD_NAME, version = Reference.VERSION, dependencies = "after:FluidUIMinecraftAdditions")
public class ModCargo {

    @Mod.Instance
    public static ModCargo instance;

    @SidedProxy(clientSide = Reference.CLIENT_PROXY_CLASS, serverSide = Reference.SERVER_PROXY_CLASS)
    private static IProxy proxy;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) throws IOException {
        LogHelper.addLogPrefix("Pre-Init");

        LogHelper.info("Started...");
        long startTime = System.nanoTime();

        //proxy.getConfig(event);

        LogHelper.info("Initializing Proxied stuff");
        proxy.preInit();

        long endTime = System.nanoTime();
        long duration = endTime - startTime;
        LogHelper.info(String.format("Finished in %06.3f s (%08.2f ms / %d ns)", duration / 1000000000f, duration / 1000000f, duration));

        LogHelper.removeAllLogPrefixes();
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) throws IOException {
        LogHelper.addLogPrefix("Post-Init");

        LogHelper.info("Started...");
        long startTime = System.nanoTime();

        //proxy.getConfig(event);

        LogHelper.info("Initializing Proxied stuff");
        proxy.postInit();

        long endTime = System.nanoTime();
        long duration = endTime - startTime;
        LogHelper.info(String.format("Finished in %06.3f s (%08.2f ms / %d ns)", duration / 1000000000f, duration / 1000000f, duration));

        LogHelper.removeAllLogPrefixes();
    }

}
