package io.github.craftedcart.cargo.proxy;

import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

/**
 * Created by CraftedCart on 17/11/2015 (DD/MM/YYYY)
 */
public interface IProxy {

    void getConfig(FMLPreInitializationEvent e);
    void preInit();
    void postInit();

}
