package io.github.craftedcart.cargo.recipe;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

/**
 * @author CraftedCart
 * Created on 26/07/2016 (DD/MM/YYYY)
 */
public abstract class AbstractRecipeCategory {

    private ArrayList<IRecipeItem> recipes = new ArrayList<>();

    /**
     * @return A unique ID per category
     */
    @NotNull
    public abstract String getCategoryID();

    /**
     * @return A localized name for the category (Eg: Should return "Shaped Crafting", not "cargo:vanillaShapedCrafting")
     */
    @NotNull
    public abstract String getCategoryLocalizedName();

    /**
     * @param item The recipe item to be added to this category
     */
    public void addRecipe(IRecipeItem item) {
        recipes.add(item);
    }

    /**
     * @param item The recipe item to be removed from this category
     */
    public void removeRecipe(IRecipeItem item) {
        recipes.remove(item);
    }

    /**
     * @return A duplicate of the recipes ArrayList
     */
    @NotNull
    public ArrayList<IRecipeItem> getRecipes() {
        return new ArrayList<>(recipes);
    }

}
