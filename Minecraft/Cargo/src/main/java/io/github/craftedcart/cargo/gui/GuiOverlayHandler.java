package io.github.craftedcart.cargo.gui;

import io.github.craftedcart.cargo.reference.CargoSettings;
import io.github.craftedcart.cargo.reference.Reference;
import io.github.craftedcart.cargo.util.LogHelper;
import io.github.craftedcart.fluidui.util.PosXY;
import io.github.craftedcart.fluidui.util.UIUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraftforge.client.event.GuiScreenEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

import java.util.ArrayList;
import java.util.List;

/**
 * @author CraftedCart
 * Created on 02/07/2016 (DD/MM/YYYY)
 */
public class GuiOverlayHandler {

    public static GuiContainerOverlay containerOverlay;

    public static List<Integer> nextKeyBuffer = new ArrayList<>();
    public static List<Boolean> nextKeyStateBuffer = new ArrayList<>();
    public static List<Character> nextKeyCharBuffer = new ArrayList<>();

    public static List<Integer> nextButtonBuffer = new ArrayList<>();
    public static List<Boolean> nextButtonStateBuffer = new ArrayList<>();
    public static List<PosXY> nextButtonPosBuffer = new ArrayList<>();

    public static void init() {
        containerOverlay = new GuiContainerOverlay();
    }

    @SubscribeEvent
    public void tickEvent(TickEvent.RenderTickEvent e) {
        if (e.phase == TickEvent.Phase.END) {
            UIUtils.calcStuff();
        }
    }

    @SubscribeEvent
    public void drawScreenEvent(GuiScreenEvent.DrawScreenEvent.Post e) {
        drawOverlay();
    }

    public void drawOverlay() {
        Minecraft mc = Minecraft.getMinecraft();

        if (mc.currentScreen instanceof GuiContainer) { //A container GUI is open
            drawContainerOverlay(mc, (GuiContainer) mc.currentScreen);
        }
    }

    public void drawContainerOverlay(Minecraft mc, GuiContainer gui) {
        //For hot swapping code - Not for production
//        containerOverlay = new GuiContainerOverlay();

        ScaledResolution sr = new ScaledResolution(mc);
        double guiScale = sr.getScaledWidth() / (double) Display.getWidth();

        containerOverlay.sidebarPanel.setTopLeftPos(
                -(gui.guiLeft / guiScale - CargoSettings.sidebarMargin),
                0);

        containerOverlay.searchField.setTopLeftPos(
                -(gui.guiLeft / guiScale - CargoSettings.sidebarMargin),
                0);

        GL11.glPushMatrix();
        UIUtils.setup(false);
        GL11.glDisable(GL11.GL_DEPTH_TEST);
        GlStateManager.disableLighting();

        //<editor-fold desc="Keyboard">
        while (nextKeyBuffer.size() > 0) {
            if (nextKeyStateBuffer.get(0)) {
                containerOverlay.onKey(nextKeyBuffer.get(0), nextKeyCharBuffer.get(0));
            }

            nextKeyBuffer.remove(0);
            nextKeyStateBuffer.remove(0);
            nextKeyCharBuffer.remove(0);
        }
        //</editor-fold>

        //<editor-fold desc="Mouse">
        while (nextButtonBuffer.size() > 0) {
            if (nextButtonStateBuffer.get(0)) {
                containerOverlay.onClick(nextButtonBuffer.get(0), nextButtonPosBuffer.get(0));
            }

            nextButtonBuffer.remove(0);
            nextButtonStateBuffer.remove(0);
            nextButtonPosBuffer.remove(0);
        }
        //</editor-fold>

        containerOverlay.draw();

        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GlStateManager.enableLighting();
        GL11.glPopMatrix();
    }

    @SubscribeEvent
    public void onGuiKeyboardEvent(GuiScreenEvent.KeyboardInputEvent.Pre event) {
        if (containerOverlay.searchField.isSelected) {
            event.setCanceled(true);

            if (Keyboard.getEventKey() == Keyboard.KEY_ESCAPE && Keyboard.getEventKeyState()) { //If Esc was pressed
                containerOverlay.searchField.setSelected(false);
            }

            nextKeyBuffer.add(Keyboard.getEventKey());
            nextKeyStateBuffer.add(Keyboard.getEventKeyState());
            nextKeyCharBuffer.add(Keyboard.getEventCharacter());
        } else if (((Minecraft.IS_RUNNING_ON_MAC && (Keyboard.isKeyDown(Keyboard.KEY_LMETA) || Keyboard.isKeyDown(Keyboard.KEY_RMETA))) ||
                !Minecraft.IS_RUNNING_ON_MAC && (Keyboard.isKeyDown(Keyboard.KEY_LCONTROL) || Keyboard.isKeyDown(Keyboard.KEY_RCONTROL))) &&
                containerOverlay.overlayUiScreen == null) {
            if (Keyboard.getEventKey() == Keyboard.KEY_F) {
                event.setCanceled(true);
                containerOverlay.searchField.setSelected(true);
            }
        } else if (containerOverlay.overlayUiScreen != null) {
            event.setCanceled(true);

            if (Keyboard.getEventKey() == Keyboard.KEY_ESCAPE && Keyboard.getEventKeyState()) { //If Esc was pressed
                containerOverlay.setOverlayUiScreen(null);
            }
        }
    }

    @SubscribeEvent
    public void onGuiMouseEvent(GuiScreenEvent.MouseInputEvent.Pre event) {
        nextButtonBuffer.add(Mouse.getEventButton());
        nextButtonStateBuffer.add(Mouse.getEventButtonState());
        nextButtonPosBuffer.add(new PosXY(Mouse.getX(), Mouse.getY()));

        if (containerOverlay.overlayUiScreen != null) { //Cancel the click event if an overlay is present
            event.setCanceled(true);
        }
    }

}
