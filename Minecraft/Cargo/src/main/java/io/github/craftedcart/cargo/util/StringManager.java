package io.github.craftedcart.cargo.util;

import net.minecraft.util.text.translation.I18n;

import java.util.IllegalFormatException;

/**
 * @author CraftedCart
 * Created on 02/07/2016 (DD/MM/YYYY)
 */
public class StringManager {

    public static String translateToLocal(String key) {
        if (I18n.canTranslate(key)) {
            return I18n.translateToLocal(key);
        } else {
            return I18n.translateToFallback(key);
        }
    }

    public static String translateToLocalFormatted(String format, Object... args) {
        String s = translateToLocal(format);
        try {
            return String.format(s, args);
        } catch (IllegalFormatException e) {
            LogHelper.error("Format error: " + s);
            LogHelper.error(e);
            return "FORMAT ERROR: " + s;
        }
    }

}
