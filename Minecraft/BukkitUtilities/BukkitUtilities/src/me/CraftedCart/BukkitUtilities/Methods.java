package me.CraftedCart.BukkitUtilities;

import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Methods {
	
	public static void buHelpCmd(CommandSender sender){
		//BU COMMAND CODE PLAYER
		Player player = (Player) sender;
		
		player.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "-->BukkitUtilities Help<--");
		player.sendMessage(ChatColor.AQUA + "/getTime|/clock " + ChatColor.BLUE + "Gets the current ingame time");
		player.sendMessage(ChatColor.AQUA + "/randInt [Integer Min] [Integer Max]" + ChatColor.BLUE + "Generates a random number between the Min and Max value");
		//END OF BU COMMAND CODE PLAYER
	}

	public static void getTimeCmd(CommandSender sender) {
		//GetTime COMMAND CODE
		Player player = (Player) sender;
		String formattedTime = parseTime(player.getWorld().getTime());
		
		player.sendMessage(ChatColor.AQUA + "The current ingame time is: " + ChatColor.GREEN + formattedTime);
	}
	
	public static void getRandInt(CommandSender sender, Integer num1, Integer num2) {
		//GetRandInt COMMAND CODE
		Player player = (Player) sender;
		Random rnd = new Random();
		Integer randNum = rnd.nextInt(num2 - num1) + num1;
		
		player.sendMessage(ChatColor.AQUA + "Chose : " + ChatColor.GREEN + randNum + " from " + num1 + " - " + num2);
	}
	////////////////////////////////////////////////
	
	public static Integer strToInt(String s){
		try {
			Integer num= Integer.parseInt(s);
			return num;
		} catch(NumberFormatException e) {
			return null;
		}
	}
	
	public static String parseTime(long time)
    {
        long gameTime = time;
        long hours = gameTime / 1000 + 6;
        long minutes = (gameTime % 1000) * 60 / 1000; 
        String ampm = "AM";
        if (hours >= 12)
        {
            hours -= 12; ampm = "PM"; 
        }
 
        if (hours >= 12)
        {
            hours -= 12; ampm = "AM"; 
        }
 
        if (hours == 0) hours = 12;
 
        String mm = "0" + minutes; 
        mm = mm.substring(mm.length() - 2, mm.length());
 
        return hours + ":" + mm + " " + ampm;
    }
}
