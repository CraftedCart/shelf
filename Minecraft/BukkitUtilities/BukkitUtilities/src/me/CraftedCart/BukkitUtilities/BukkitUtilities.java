package me.CraftedCart.BukkitUtilities;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class BukkitUtilities extends JavaPlugin {
	
	
	@Override
	public void onEnable() {
		getLogger().info("BukkitUtilities by CraftedCart has beed sucsesfully enabled");
	}
	
	@Override
	public void onDisable() {
		getLogger().info("BukkitUtilities by CraftedCart has beed sucsesfully disabled");
	}
	
	//RECIEVE COMMANDS
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		//BU COMMAND
		if (cmd.getName().equalsIgnoreCase("bu")){
			if (sender instanceof Player){
				Methods.buHelpCmd(sender);
			} else {
				getLogger().info("This command can only be run by a player");
			}
		}
		
		//GETTIME COMMAND
		if (cmd.getName().equalsIgnoreCase("gettime") || cmd.getName().equalsIgnoreCase("clock")){
			if (sender instanceof Player){
				if (args.length != 0){
					sender.sendMessage(ChatColor.RED + "Too many arguments");
					return false;
				}
					Methods.getTimeCmd(sender);
			} else {
				getLogger().info("This command can only be run by a player");
			}
		}
		
		//RANDINT COMMAND
				if (cmd.getName().equalsIgnoreCase("randint")){
					if (sender instanceof Player){
						if (args.length != 2){
							sender.sendMessage(ChatColor.RED + "Invalid Arguments");
							return false;
						}
							Integer num1 = Methods.strToInt(args[0]);
							Integer num2 = Methods.strToInt(args[1]);
							//
							if (num1 == null || num2 == null){
								sender.sendMessage(ChatColor.RED + "Invalid Arguments");
								return false;
							}
							//
							Methods.getRandInt(sender, num1, num2);
					} else {
						getLogger().info("This command can only be run by a player");
					}
				}
		return true;
	}
}
