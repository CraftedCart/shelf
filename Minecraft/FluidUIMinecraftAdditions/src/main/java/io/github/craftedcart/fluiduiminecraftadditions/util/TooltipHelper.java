package io.github.craftedcart.fluiduiminecraftadditions.util;

import io.github.craftedcart.fluidui.component.Label;
import io.github.craftedcart.fluidui.component.Panel;
import io.github.craftedcart.fluidui.theme.UITheme;
import io.github.craftedcart.fluidui.util.EnumVAlignment;
import io.github.craftedcart.fluidui.util.UIColor;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author CraftedCart
 * Created on 28/07/2016 (DD/MM/YYYY)
 */
public class TooltipHelper {

    public static final List<UIColor> COLOR_INDEX_TO_UI_COLOR = new ArrayList<UIColor>() {{
        add(UIColor.matGrey900()); //0: Black
        add(UIColor.matIndigo()); //1: Dark Blue
        add(UIColor.matGreen()); //2: Dark Green
        add(UIColor.matTeal()); //3: Dark Aqua
        add(UIColor.matRed900()); //4: Dark Red
        add(UIColor.matPurple()); //5: Dark Purple
        add(UIColor.matOrange()); //6: Gold
        add(UIColor.matGrey()); //7: Grey
        add(UIColor.matGrey400()); //8: Light Grey
        add(UIColor.matBlue()); //9: Blue
        add(UIColor.matLightGreen()); //A: Green
        add(UIColor.matCyan()); //B: Aqua
        add(UIColor.matRed()); //C: Red
        add(UIColor.matPurple300()); //D: Light Purple
        add(UIColor.matYellow()); //E: Yellow
        add(UIColor.matWhite()); //F: White
    }};

    public static Panel generateItemStackCustomTooltip(@NotNull ItemStack itemStack, UITheme theme) {
        List<String> tooltipList = itemStack.getTooltip(Minecraft.getMinecraft().thePlayer, Minecraft.getMinecraft().gameSettings.advancedItemTooltips);

        Panel tooltipComponent = new Panel();
        tooltipComponent.setOnInitAction(() -> {
            tooltipComponent.setTheme(theme);

            class LambdaStuff {
                private int i = 0;
                private double longestWidth = 0;
            }
            LambdaStuff lambdaStuff = new LambdaStuff();

            for (String item : tooltipList) {
//                Label label = new Label();
//                label.setOnInitAction(() -> {
//                    label.setTopLeftPos(4, 4 + lambdaStuff.i * 24);
//                    label.setBottomRightPos(-4, 4 + (lambdaStuff.i + 1) * 24);
//                    label.setVerticalAlign(EnumVAlignment.centre);
//                    label.setText(item);
//
//                    if (lambdaStuff.i == 0) { //This is the first line, so it's the title
//                        label.setTextColor(COLOR_INDEX_TO_UI_COLOR.get(itemStack.getRarity().rarityColor.getColorIndex()));
//                    }
//
//                    assert label.font != null;
//                    int stringWidth = label.font.getWidth(removeFormattingCodes(item));
//                    if (stringWidth > lambdaStuff.longestWidth) {
//                        lambdaStuff.longestWidth = stringWidth;
//                    }
//                });
//                tooltipComponent.addChildComponent("label" + Objects.toString(lambdaStuff.i), label);

                List<Label> labels = processFormatting(item, theme, 0, 0, null, 0);

                Panel panel = new Panel();
                panel.setOnInitAction(() -> {
                    panel.setTopLeftPos(4, 4 + lambdaStuff.i * 24);
                    panel.setBottomRightPos(-4, (lambdaStuff.i + 1) * 24);
                    panel.setTopLeftAnchor(0, 0);
                    panel.setBottomRightAnchor(1, 0);
                    panel.setBackgroundColor(UIColor.transparent());
                });
                tooltipComponent.addChildComponent(Objects.toString(lambdaStuff.i), panel);

                int i = 0;
                double lineWidth = 0;
                for (Label label : labels) {
                    panel.addChildComponent(Objects.toString(i), label);
                    label.init();
                    label.preDraw();

                    lineWidth += label.width;
                    i++;
                }

                if (lineWidth > lambdaStuff.longestWidth) {
                    lambdaStuff.longestWidth = lineWidth;
                }

                lambdaStuff.i++;
            }

            tooltipComponent.setTopLeftPos(0, 0);
            tooltipComponent.setBottomRightPos(lambdaStuff.longestWidth + 8, tooltipList.size() * 24 + 8);
        });

        return tooltipComponent;
    }

    public static List<Label> processFormatting(@NotNull String inString, @NotNull UITheme theme, int textColorIndex, int defaultTextColorIndex,
                                                @Nullable List<Label> prevList, double prevWidth) {
        List<Label> labelList;
        if (prevList != null) {
            labelList = prevList;
        } else {
            labelList = new ArrayList<>();
        }

        final String string = inString //Remove unsupported formats //TODO Unsupported formats
                .replace("§k", "")
                .replace("§l", "")
                .replace("§m", "")
                .replace("§n", "")
                .replace("§o", "")
                ;

        if (string.isEmpty()) {
            return labelList;
        }

        //<editor-fold desc="§0">
        int index0 = string.indexOf("§0");
        if (index0 != -1) {
            Label label = new Label();
            double nextWidth = prevWidth + theme.labelFont.getWidth(string.substring(0, index0));

            label.setOnInitAction(() -> {
                label.setTheme(theme);
                label.setTopLeftPos(prevWidth, 0);
                label.setBottomRightPos(nextWidth, 0);
                label.setTopLeftAnchor(0, 0);
                label.setBottomRightAnchor(0, 1);
                label.setText(string.substring(0, index0));
                label.setTextColor(COLOR_INDEX_TO_UI_COLOR.get(textColorIndex));
            });

            return processFormatting(string.substring(index0 + 2), theme, 0, defaultTextColorIndex, labelList, nextWidth);
        }
        //</editor-fold>

        //<editor-fold desc="§1">
        int index1 = string.indexOf("§1");
        if (index1 != -1) {
            Label label = new Label();
            double nextWidth = prevWidth + theme.labelFont.getWidth(string.substring(0, index1));

            label.setOnInitAction(() -> {
                label.setTheme(theme);
                label.setTopLeftPos(prevWidth, 0);
                label.setBottomRightPos(nextWidth, 0);
                label.setTopLeftAnchor(0, 0);
                label.setBottomRightAnchor(0, 1);
                label.setText(string.substring(0, index1));
                label.setTextColor(COLOR_INDEX_TO_UI_COLOR.get(textColorIndex));
            });

            return processFormatting(string.substring(index1 + 2), theme, 1, defaultTextColorIndex, labelList, nextWidth);
        }
        //</editor-fold>

        //<editor-fold desc="§2">
        int index2 = string.indexOf("§2");
        if (index2 != -1) {
            Label label = new Label();
            double nextWidth = prevWidth + theme.labelFont.getWidth(string.substring(0, index2));

            label.setOnInitAction(() -> {
                label.setTheme(theme);
                label.setTopLeftPos(prevWidth, 0);
                label.setBottomRightPos(nextWidth, 0);
                label.setTopLeftAnchor(0, 0);
                label.setBottomRightAnchor(0, 1);
                label.setText(string.substring(0, index2));
                label.setTextColor(COLOR_INDEX_TO_UI_COLOR.get(textColorIndex));
            });

            return processFormatting(string.substring(index1 + 2), theme, 2, defaultTextColorIndex, labelList, nextWidth);
        }
        //</editor-fold>

        //<editor-fold desc="§3">
        int index3 = string.indexOf("§3");
        if (index3 != -1) {
            Label label = new Label();
            double nextWidth = prevWidth + theme.labelFont.getWidth(string.substring(0, index3));

            label.setOnInitAction(() -> {
                label.setTheme(theme);
                label.setTopLeftPos(prevWidth, 0);
                label.setBottomRightPos(nextWidth, 0);
                label.setTopLeftAnchor(0, 0);
                label.setBottomRightAnchor(0, 1);
                label.setText(string.substring(0, index3));
                label.setTextColor(COLOR_INDEX_TO_UI_COLOR.get(textColorIndex));
            });

            return processFormatting(string.substring(index3 + 2), theme, 3, defaultTextColorIndex, labelList, nextWidth);
        }
        //</editor-fold>

        //<editor-fold desc="§4">
        int index4 = string.indexOf("§4");
        if (index4 != -1) {
            Label label = new Label();
            double nextWidth = prevWidth + theme.labelFont.getWidth(string.substring(0, index4));

            label.setOnInitAction(() -> {
                label.setTheme(theme);
                label.setTopLeftPos(prevWidth, 0);
                label.setBottomRightPos(nextWidth, 0);
                label.setTopLeftAnchor(0, 0);
                label.setBottomRightAnchor(0, 1);
                label.setText(string.substring(0, index4));
                label.setTextColor(COLOR_INDEX_TO_UI_COLOR.get(textColorIndex));
            });

            return processFormatting(string.substring(index4 + 2), theme, 4, defaultTextColorIndex, labelList, nextWidth);
        }
        //</editor-fold>

        //<editor-fold desc="§5">
        int index5 = string.indexOf("§5");
        if (index5 != -1) {
            Label label = new Label();
            double nextWidth = prevWidth + theme.labelFont.getWidth(string.substring(0, index5));

            label.setOnInitAction(() -> {
                label.setTheme(theme);
                label.setTopLeftPos(prevWidth, 0);
                label.setBottomRightPos(nextWidth, 0);
                label.setTopLeftAnchor(0, 0);
                label.setBottomRightAnchor(0, 1);
                label.setText(string.substring(0, index5));
                label.setTextColor(COLOR_INDEX_TO_UI_COLOR.get(textColorIndex));
            });

            return processFormatting(string.substring(index5 + 2), theme, 5, defaultTextColorIndex, labelList, nextWidth);
        }
        //</editor-fold>

        //<editor-fold desc="§6">
        int index6 = string.indexOf("§6");
        if (index6 != -1) {
            Label label = new Label();
            double nextWidth = prevWidth + theme.labelFont.getWidth(string.substring(0, index6));

            label.setOnInitAction(() -> {
                label.setTheme(theme);
                label.setTopLeftPos(prevWidth, 0);
                label.setBottomRightPos(nextWidth, 0);
                label.setTopLeftAnchor(0, 0);
                label.setBottomRightAnchor(0, 1);
                label.setText(string.substring(0, index6));
                label.setTextColor(COLOR_INDEX_TO_UI_COLOR.get(textColorIndex));
            });

            return processFormatting(string.substring(index6 + 2), theme, 6, defaultTextColorIndex, labelList, nextWidth);
        }
        //</editor-fold>

        //<editor-fold desc="§7">
        int index7 = string.indexOf("§7");
        if (index7 != -1) {
            Label label = new Label();
            double nextWidth = prevWidth + theme.labelFont.getWidth(string.substring(0, index7));

            label.setOnInitAction(() -> {
                label.setTheme(theme);
                label.setTopLeftPos(prevWidth, 0);
                label.setBottomRightPos(nextWidth, 0);
                label.setTopLeftAnchor(0, 0);
                label.setBottomRightAnchor(0, 1);
                label.setText(string.substring(0, index7));
                label.setTextColor(COLOR_INDEX_TO_UI_COLOR.get(textColorIndex));
            });

            return processFormatting(string.substring(index7 + 2), theme, 7, defaultTextColorIndex, labelList, nextWidth);
        }
        //</editor-fold>

        //<editor-fold desc="§8">
        int index8 = string.indexOf("§8");
        if (index8 != -1) {
            Label label = new Label();
            double nextWidth = prevWidth + theme.labelFont.getWidth(string.substring(0, index8));

            label.setOnInitAction(() -> {
                label.setTheme(theme);
                label.setTopLeftPos(prevWidth, 0);
                label.setBottomRightPos(nextWidth, 0);
                label.setTopLeftAnchor(0, 0);
                label.setBottomRightAnchor(0, 1);
                label.setText(string.substring(0, index8));
                label.setTextColor(COLOR_INDEX_TO_UI_COLOR.get(textColorIndex));
            });

            return processFormatting(string.substring(index8 + 2), theme, 8, defaultTextColorIndex, labelList, nextWidth);
        }
        //</editor-fold>

        //<editor-fold desc="§9">
        int index9 = string.indexOf("§9");
        if (index9 != -1) {
            Label label = new Label();
            double nextWidth = prevWidth + theme.labelFont.getWidth(string.substring(0, index9));

            label.setOnInitAction(() -> {
                label.setTheme(theme);
                label.setTopLeftPos(prevWidth, 0);
                label.setBottomRightPos(nextWidth, 0);
                label.setTopLeftAnchor(0, 0);
                label.setBottomRightAnchor(0, 1);
                label.setText(string.substring(0, index9));
                label.setTextColor(COLOR_INDEX_TO_UI_COLOR.get(textColorIndex));
            });

            return processFormatting(string.substring(index9 + 2), theme, 9, defaultTextColorIndex, labelList, nextWidth);
        }
        //</editor-fold>

        //<editor-fold desc="§a">
        int indexa = string.indexOf("§a");
        if (indexa != -1) {
            Label label = new Label();
            double nextWidth = prevWidth + theme.labelFont.getWidth(string.substring(0, indexa));

            label.setOnInitAction(() -> {
                label.setTheme(theme);
                label.setTopLeftPos(prevWidth, 0);
                label.setBottomRightPos(nextWidth, 0);
                label.setTopLeftAnchor(0, 0);
                label.setBottomRightAnchor(0, 1);
                label.setText(string.substring(0, indexa));
                label.setTextColor(COLOR_INDEX_TO_UI_COLOR.get(textColorIndex));
            });

            return processFormatting(string.substring(indexa + 2), theme, 10, defaultTextColorIndex, labelList, nextWidth);
        }
        //</editor-fold>

        //<editor-fold desc="§b">
        int indexb = string.indexOf("§b");
        if (indexb != -1) {
            Label label = new Label();
            double nextWidth = prevWidth + theme.labelFont.getWidth(string.substring(0, indexb));

            label.setOnInitAction(() -> {
                label.setTheme(theme);
                label.setTopLeftPos(prevWidth, 0);
                label.setBottomRightPos(nextWidth, 0);
                label.setTopLeftAnchor(0, 0);
                label.setBottomRightAnchor(0, 1);
                label.setText(string.substring(0, indexb));
                label.setTextColor(COLOR_INDEX_TO_UI_COLOR.get(textColorIndex));
            });

            return processFormatting(string.substring(indexb + 2), theme, 11, defaultTextColorIndex, labelList, nextWidth);
        }
        //</editor-fold>

        //<editor-fold desc="§c">
        int indexc = string.indexOf("§c");
        if (indexc != -1) {
            Label label = new Label();
            double nextWidth = prevWidth + theme.labelFont.getWidth(string.substring(0, indexc));

            label.setOnInitAction(() -> {
                label.setTheme(theme);
                label.setTopLeftPos(prevWidth, 0);
                label.setBottomRightPos(nextWidth, 0);
                label.setTopLeftAnchor(0, 0);
                label.setBottomRightAnchor(0, 1);
                label.setText(string.substring(0, indexc));
                label.setTextColor(COLOR_INDEX_TO_UI_COLOR.get(textColorIndex));
            });

            return processFormatting(string.substring(indexc + 2), theme, 13, defaultTextColorIndex, labelList, nextWidth);
        }
        //</editor-fold>

        //<editor-fold desc="§d">
        int indexd = string.indexOf("§d");
        if (indexd != -1) {
            Label label = new Label();
            double nextWidth = prevWidth + theme.labelFont.getWidth(string.substring(0, indexd));

            label.setOnInitAction(() -> {
                label.setTheme(theme);
                label.setTopLeftPos(prevWidth, 0);
                label.setBottomRightPos(nextWidth, 0);
                label.setTopLeftAnchor(0, 0);
                label.setBottomRightAnchor(0, 1);
                label.setText(string.substring(0, indexd));
                label.setTextColor(COLOR_INDEX_TO_UI_COLOR.get(textColorIndex));
            });

            return processFormatting(string.substring(indexd + 2), theme, 14, defaultTextColorIndex, labelList, nextWidth);
        }
        //</editor-fold>

        //<editor-fold desc="§e">
        int indexe = string.indexOf("§e");
        if (indexe != -1) {
            Label label = new Label();
            double nextWidth = prevWidth + theme.labelFont.getWidth(string.substring(0, indexe));

            label.setOnInitAction(() -> {
                label.setTheme(theme);
                label.setTopLeftPos(prevWidth, 0);
                label.setBottomRightPos(nextWidth, 0);
                label.setTopLeftAnchor(0, 0);
                label.setBottomRightAnchor(0, 1);
                label.setText(string.substring(0, indexe));
                label.setTextColor(COLOR_INDEX_TO_UI_COLOR.get(textColorIndex));
            });

            return processFormatting(string.substring(indexe + 2), theme, 15, defaultTextColorIndex, labelList, nextWidth);
        }
        //</editor-fold>

        //<editor-fold desc="§f">
        int indexf = string.indexOf("§f");
        if (indexf != -1) {
            Label label = new Label();
            double nextWidth = prevWidth + theme.labelFont.getWidth(string.substring(0, indexf));

            label.setOnInitAction(() -> {
                label.setTheme(theme);
                label.setTopLeftPos(prevWidth, 0);
                label.setBottomRightPos(nextWidth, 0);
                label.setTopLeftAnchor(0, 0);
                label.setBottomRightAnchor(0, 1);
                label.setText(string.substring(0, indexf));
                label.setTextColor(COLOR_INDEX_TO_UI_COLOR.get(textColorIndex));
            });

            return processFormatting(string.substring(indexf + 2), theme, 16, defaultTextColorIndex, labelList, nextWidth);
        }
        //</editor-fold>

        //<editor-fold desc="§r">
        int indexr = string.indexOf("§r");
        if (indexr != -1) {
            Label label = new Label();
            double nextWidth = prevWidth + theme.labelFont.getWidth(string.substring(0, indexr));

            label.setOnInitAction(() -> {
                label.setTheme(theme);
                label.setTopLeftPos(prevWidth, 0);
                label.setBottomRightPos(nextWidth, 0);
                label.setTopLeftAnchor(0, 0);
                label.setBottomRightAnchor(0, 1);
                label.setText(string.substring(0, indexr));
                label.setTextColor(COLOR_INDEX_TO_UI_COLOR.get(textColorIndex));
            });

            return processFormatting(string.substring(indexr + 2), theme, defaultTextColorIndex, defaultTextColorIndex, labelList, nextWidth);
        }
        //</editor-fold>

        return labelList;
    }

    public static String removeFormattingCodes(String inputString) {
        return inputString
                .replace("§0", "")
                .replace("§1", "")
                .replace("§2", "")
                .replace("§3", "")
                .replace("§4", "")
                .replace("§5", "")
                .replace("§6", "")
                .replace("§7", "")
                .replace("§8", "")
                .replace("§9", "")
                .replace("§a", "")
                .replace("§b", "")
                .replace("§c", "")
                .replace("§d", "")
                .replace("§e", "")
                .replace("§f", "")
                .replace("§k", "")
                .replace("§l", "")
                .replace("§m", "")
                .replace("§n", "")
                .replace("§o", "")
                .replace("§r", "")
                ;
    }

}
