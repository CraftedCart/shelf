package io.github.craftedcart.fluiduiminecraftadditions.component;

import io.github.craftedcart.fluidui.component.Component;
import io.github.craftedcart.fluidui.util.EnumXY;
import io.github.craftedcart.fluidui.util.UIUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.ForgeHooksClient;
import org.lwjgl.opengl.GL11;

/**
 * @author CraftedCart
 * Created on 27/07/2016 (DD/MM/YYYY)
 */
public class ItemStackDisplay extends Component {

    public ItemStack itemStack;

    public ItemStackDisplay() {
        postInit();
    }

    public void draw() {
        preDraw();
        componentDraw();
        postDraw();
    }

    public void componentDraw() {
        if (itemStack != null) {
            double scale = 0.0625 * Math.min(width, height); //0.0165 == 1 / 16

            GL11.glEnable(GL11.GL_TEXTURE_2D);
            GL11.glPushAttrib(GL11.GL_ALL_ATTRIB_BITS);

            RenderItem renderItem = Minecraft.getMinecraft().getRenderItem();
            TextureManager textureManager = Minecraft.getMinecraft().getTextureManager();

            RenderHelper.enableGUIStandardItemLighting();
            //Binding the missing texture causes it to work? Wha? Well, I guess I won't complain...
            textureManager.bindTexture(TextureMap.LOCATION_MISSING_TEXTURE);

            GL11.glPushMatrix();
            GL11.glScaled(scale, scale, scale);
            GL11.glEnable(GL11.GL_DEPTH_TEST);

            //TODO: Item layering seems to be wrong (Eg: The bottom part of stairs is rendered on top of the top part)

            renderItem.renderItemAndEffectIntoGUI(itemStack, (int) (topLeftPx.x / scale), (int) (topLeftPx.y / scale));
            renderItem.renderItemOverlays(Minecraft.getMinecraft().fontRendererObj, itemStack, (int) (topLeftPx.x / scale), (int) (topLeftPx.y / scale));

            GL11.glPopMatrix();
            GL11.glPopAttrib();

            GL11.glDisable(GL11.GL_DEPTH_TEST);
            GL11.glDisable(GL11.GL_TEXTURE_2D);
        }
    }

    public void setItemStack(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

}
