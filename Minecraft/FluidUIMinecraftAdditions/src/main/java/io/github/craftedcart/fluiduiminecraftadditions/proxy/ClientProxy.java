package io.github.craftedcart.fluiduiminecraftadditions.proxy;

import io.github.craftedcart.fluiduiminecraftadditions.util.AdditionsLogHelper;
import io.github.craftedcart.fluiduiminecraftadditions.util.TextureManager;
import net.minecraftforge.common.MinecraftForge;

/**
 * Created by CraftedCart on 17/11/2015 (DD/MM/YYYY)
 */
public class ClientProxy extends CommonProxy {

    @Override
    public void preInit() {
        AdditionsLogHelper.addLogPrefix("Client");

        AdditionsLogHelper.info("Registering TextureManager with the EVENT_BUS");
        MinecraftForge.EVENT_BUS.register(new TextureManager());

        AdditionsLogHelper.removeLastLogPrefix();
    }

}
