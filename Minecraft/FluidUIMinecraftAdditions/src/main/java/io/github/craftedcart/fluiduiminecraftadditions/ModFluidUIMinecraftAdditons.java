package io.github.craftedcart.fluiduiminecraftadditions;

import io.github.craftedcart.fluiduiminecraftadditions.proxy.IProxy;
import io.github.craftedcart.fluiduiminecraftadditions.reference.Reference;
import io.github.craftedcart.fluiduiminecraftadditions.util.AdditionsLogHelper;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

import java.io.IOException;

/**
 * @author CraftedCart
 * Created on 19/06/2016
 */
@Mod(modid = Reference.MOD_ID, name = Reference.MOD_NAME, version = Reference.VERSION)
public class ModFluidUIMinecraftAdditons {

    @Mod.Instance
    public static ModFluidUIMinecraftAdditons instance;

    @SidedProxy(clientSide = Reference.CLIENT_PROXY_CLASS, serverSide = Reference.SERVER_PROXY_CLASS)
    private static IProxy proxy;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) throws IOException {
        AdditionsLogHelper.addLogPrefix("Pre-Init");

        AdditionsLogHelper.info("Started...");
        long startTime = System.nanoTime();

        AdditionsLogHelper.info("Initializing Proxied stuff");
        proxy.preInit();

        long endTime = System.nanoTime();
        long duration = endTime - startTime;
        AdditionsLogHelper.info(String.format("Finished in %06.3f s (%08.2f ms / %d ns)", duration / 1000000000f, duration / 1000000f, duration));

        AdditionsLogHelper.removeAllLogPrefixes();
    }

}
