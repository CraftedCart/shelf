package io.github.craftedcart.fluiduiminecraftadditions.proxy;

/**
 * Created by CraftedCart on 17/11/2015 (DD/MM/YYYY)
 */
abstract class CommonProxy implements IProxy {

    public abstract void preInit();

}
