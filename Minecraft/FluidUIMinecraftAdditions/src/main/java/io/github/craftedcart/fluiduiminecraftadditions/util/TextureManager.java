package io.github.craftedcart.fluiduiminecraftadditions.util;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * @author CraftedCart
 * Created on 03/07/2016 (DD/MM/YYYY)
 */
public class TextureManager {

    public static Map<String, ResourceLocation> textureLocationsMap = new HashMap<>();
    public static Map<String, Texture> textureMap = new HashMap<>();

    public static void loadTexture(String name, InputStream inputStream) {
        try {
            textureMap.put(name, TextureLoader.getTexture("PNG", inputStream));
        } catch (IOException e) {
            AdditionsLogHelper.error(String.format("Error while loading texture \"%s\"", name));
            AdditionsLogHelper.error(e);
        }

        try {
            inputStream.close();
        } catch (IOException e) {
            AdditionsLogHelper.error(String.format("Error while closing the InputStream for texture \"%s\"", name));
            AdditionsLogHelper.error(e);
        }
    }

    public static void registerTexture(String name, ResourceLocation resourceLocation) {
         textureLocationsMap.put(name, resourceLocation);
    }

    public static Texture getTexture(String name) {
        return textureMap.get(name);
    }

    @SubscribeEvent
    public void onTextureStitchDone(TextureStitchEvent.Post event) {
        AdditionsLogHelper.addLogPrefix("Texture Loading");

        AdditionsLogHelper.info("Started...");

        AdditionsLogHelper.info(String.format("Unloading %d FluidUI Textures", textureMap.size()));
        long startTime = System.nanoTime();

        for (Map.Entry<String, Texture> entry : textureMap.entrySet()) {
            entry.getValue().release();
        }
        textureMap.clear();

        AdditionsLogHelper.info(String.format("Loading %d FluidUI Textures", textureLocationsMap.size()));
        for (Map.Entry<String, ResourceLocation> entry : textureLocationsMap.entrySet()) {
            String name = entry.getKey();
            ResourceLocation resourceLocation = entry.getValue();

            try {
                loadTexture(name, Minecraft.getMinecraft().getResourceManager().getResource(resourceLocation).getInputStream());
            } catch (IOException e) {
                AdditionsLogHelper.error(String.format("Error while getting the resource for texture \"%s\"", name));
                AdditionsLogHelper.error(e);
            }
        }

        long endTime = System.nanoTime();
        long duration = endTime - startTime;
        AdditionsLogHelper.info(String.format("Finished in %06.3f s (%08.2f ms / %d ns)", duration / 1000000000f, duration / 1000000f, duration));

        AdditionsLogHelper.removeAllLogPrefixes();
    }

}
