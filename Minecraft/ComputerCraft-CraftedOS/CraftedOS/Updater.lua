--CraftedOS Updater V2
local optionChose = false

term.setBackgroundColour(colours.blue)
term.clear()
term.setCursorPos(2, 2)
term.setTextColour(colours.white)
term.write("Welcome to the CraftedOS Updater")
term.setCursorPos(2, 3)
term.setTextColour(colours.white)
term.write("Do you want to update CraftedOS?")

term.setCursorPos(4, 16)
term.setBackgroundColour(colours.red)
term.clearLine()
term.write("Update")

term.setCursorPos(4, 18)
term.setBackgroundColour(colours.red)
term.clearLine()
term.write("Cancel")

while not optionChose do
	local event, button, X, Y = os.pullEvent("mouse_click")

	if Y == 16 then
		optionChose = true
		--UPDATE
		term.setBackgroundColour(colours.blue)	
		term.clear()
		term.setTextColour(colours.white)
		term.setCursorPos(2, 2)
		term.write("Connecting to the internet...")
		netfileOS = http.get("http://pastebin.com/raw.php?i=QXgwEg6N")
		netfileUpdater = http.get("http://pastebin.com/raw.php?i=JjvJp8DQ")
		if netfileOS and netfileUpdater then
			term.setCursorPos(2, 3)
			term.write("Found Files")
			term.setCursorPos(2, 4)
			term.write("Writing Files")
			f = fs.open("CraftedOS/CraftedOS.lua", "w")
			f.write(netfileOS.readAll())
			netfileOS.close()
			f.close()
			f = fs.open("CraftedOS/Updater", "w")
			f.write(netfileUpdater.readAll())
			netfileUpdater.close()
			f.close()
			term.setCursorPos(2, 5)
			term.write("Finished Updating")
			term.setTextColour(colours.lime)
			term.setCursorPos(2, 7)
			term.write("Restarting in 3...")
			os.sleep(1)
			term.setCursorPos(2, 7)
			term.write("Restarting in 2...")
			os.sleep(1)
			term.setCursorPos(2, 7)
			term.write("Restarting in 1...")
			os.sleep(1)
			term.setCursorPos(2, 7)
			term.write("--Now restarting--")
			os.reboot()
		else
			term.setCursorPos(2, 3)
			term.write("Failed to update - No internet connection?")
			term.setTextColour(colours.lime)
			term.setCursorPos(2, 5)
			term.write("Restarting in 3...")
			os.sleep(1)
			term.setCursorPos(2, 5)
			term.write("Restarting in 2...")
			os.sleep(1)
			term.setCursorPos(2, 5)
			term.write("Restarting in 1...")
			os.sleep(1)
			term.setCursorPos(2, 5)
			term.write("--Now restarting--")
			os.reboot()
		end
		--END UPDATE

	elseif Y == 18 then
		optionChose = true
		--CANCEL
		term.setBackgroundColour(colours.blue)
		term.clear()
		term.setCursorPos(2, 2)
		term.write("Update Cancelled - Now restarting...")
		term.setTextColour(colours.lime)
		term.setCursorPos(2, 4)
		term.write("Restarting in 3...")
		os.sleep(1)
		term.setCursorPos(2, 4)
		term.write("Restarting in 2...")
		os.sleep(1)
		term.setCursorPos(2, 4)
		term.write("Restarting in 1...")
		os.sleep(1)
		term.setCursorPos(2, 4)
		term.write("--Now restarting--")
		os.reboot()
	end
end
