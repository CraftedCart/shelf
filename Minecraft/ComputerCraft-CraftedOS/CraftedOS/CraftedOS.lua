--LOAD APIs
os.loadAPI("CraftedOS/OSFiles/OS/API/readFile")
os.loadAPI("CraftedOS/OSFiles/OS/API/advIO")
--END LOAD APIs

--VARIABLES
	--Startup Logo Variables
startupBGColour = tonumber(readFile.readLineFile("CraftedOS/OSFiles/OS/Config/OSConfig.cfg", 17))
startupBGColourLine = tonumber(readFile.readLineFile("CraftedOS/OSFiles/OS/Config/OSConfig.cfg", 19))
startupTXTColour = tonumber(readFile.readLineFile("CraftedOS/OSFiles/OS/Config/OSConfig.cfg", 21))
starupLogoTime = tonumber(readFile.readLineFile("CraftedOS/OSFiles/OS/Config/OSConfig.cfg", 23))
showStartup = readFile.readLineFile("CraftedOS/OSFiles/OS/Config/OSConfig.cfg", 39)

	--Login Variables
loginBGColour = tonumber(readFile.readLineFile("CraftedOS/OSFiles/OS/Config/OSConfig.cfg", 28))
loginBGColourUser = tonumber(readFile.readLineFile("CraftedOS/OSFiles/OS/Config/OSConfig.cfg", 30))
loginTXTColour = tonumber(readFile.readLineFile("CraftedOS/OSFiles/OS/Config/OSConfig.cfg", 32))
loginTXTBoxColour = tonumber(readFile.readLineFile("CraftedOS/OSFiles/OS/Config/OSConfig.cfg", 34))
autoLogin = readFile.readLineFile("CraftedOS/OSFiles/OS/Config/OSConfig.cfg", 41)
loginLen = 30
loginTitleColour = colours.grey
loginHintColour = colours.purple

username1 = readFile.readLineFile("CraftedOS/OSFiles/OS/Config/OSConfig.cfg", 3)
username2 = readFile.readLineFile("CraftedOS/OSFiles/OS/Config/OSConfig.cfg", 7)
username3 = readFile.readLineFile("CraftedOS/OSFiles/OS/Config/OSConfig.cfg", 11)
	--User
desktopColour = 8
desktopMenuBarColour = 256
desktopMenuBtnColour = 128

	--OTHER
osVersion = "0.5.1"
loggedIn = 0
--END VARIABLES

--FUNCTIONS

--DRAW DESKTOP

function initShutdown()
	os.shutdown()
end

function initReboot()
	os.reboot()
end

function drawDesktop()
	term.setBackgroundColour(desktopColour)
	term.clear()
	term.setCursorPos(2, 1)
	term.setBackgroundColour(desktopMenuBarColour)
	term.clearLine()
	term.setBackgroundColour(desktopMenuBtnColour)
	term.write(" Menu ")
end

function startupLogo()
	term.setBackgroundColour(startupBGColour)
	term.clear()
	term.setCursorPos(41, 18)
	term.setBackgroundColour(startupBGColourLine)
	term.clearLine()
	term.setTextColour(startupTXTColour)
	term.write("CraftedOS")
	os.sleep(starupLogoTime)
end

function startupLogoError()
	term.setBackgroundColour(startupBGColour)
	term.clear()
	term.setCursorPos(41, 18)
	term.setBackgroundColour(startupBGColourLine)
	term.clearLine()
	term.setTextColour(startupTXTColour)
	term.write("CraftedOS")
	term.setCursorPos(2, 2)
	term.setBackgroundColour(startupBGColourLine)
	term.clearLine()
	term.setTextColour(startupTXTColour)
	term.write("Failed to read Config (ShowStartup)")
	term.setCursorPos(2, 3)
	term.setBackgroundColour(startupBGColourLine)
	term.clearLine()
	term.setTextColour(startupTXTColour)
	term.write("Using Default Setting")
	os.sleep(starupLogoTime)
end
	
function loginFail(user, passAst)

end

function getPass(usr)
	term.setBackgroundColour(loginBGColour)
	term.clear()
	term.setBackgroundColour(colours.white) --------TEMP
	term.setCursorPos(32, 5)
	term.write("                  ")
	term.setCursorPos(32, 6)
	term.write("         Password ")
	term.setCursorPos(32, 7)
	term.setBackgroundColour(colours.white) --------TEMP
	term.write(" ")
	term.setBackgroundColour(colours.grey) --------TEMP
	term.write("                ")
	term.setBackgroundColour(colours.white) --------TEMP
	term.write(" ")
	term.setCursorPos(32, 8)
	term.write("                  ")
	term.setCursorPos(33, 7)
	term.setTextColour(colours.white) --------TEMP
	advIO.advRead("*", nil, 16)
end

function login()
	term.setBackgroundColour(loginBGColour)
	term.clear()
	
	term.setCursorPos(2, 1)
	term.setBackgroundColour(loginTitleColour)
	term.clearLine()
	term.setBackgroundColour(colours.black)
	term.setTextColour(colours.white)
	term.write(" Shutdown ")
	
	term.setCursorPos(13, 1)
	term.setBackgroundColour(colours.black)
	term.setTextColour(colours.white)
	term.write("  Restart ")
	
	term.setCursorPos(2, 3)
	term.setBackgroundColour(loginBGColourUser)
	
	userN1Space = ""
	for i = string.len(username1), 18 do
		userN1Space = userN1Space .. " "
	end
	
	term.write("                    ")
	term.setCursorPos(2, 4)
	term.write(" " .. username1 .. userN1Space)
	term.setCursorPos(2, 5)
	term.write("                    ")
	
	
	term.setCursorPos(2, 7)
	term.setBackgroundColour(loginBGColourUser)
	
	userN2Space = ""
	for i = string.len(username2), 18 do
		userN2Space = userN2Space .. " "
	end
	
	term.write("                    ")
	term.setCursorPos(2, 8)
	term.write(" " .. username2 .. userN2Space)
	term.setCursorPos(2, 9)
	term.write("                    ")
	
	
	term.setCursorPos(2, 11)
	term.setBackgroundColour(loginBGColourUser)
	
	userN3Space = ""
	for i = string.len(username3), 18 do
		userN3Space = userN3Space .. " "
	end
	
	term.write("                    ")
	term.setCursorPos(2, 12)
	term.write(" " .. username3 .. userN3Space)
	term.setCursorPos(2, 13)
	term.write("                    ")
	
	term.setCursorPos(2, 18)
	term.setBackgroundColour(loginHintColour)
	term.clearLine()
	term.setTextColour(loginTXTColour)
	term.write("Select a user to login to")
	
	optionChose = false
	while optionChose == false do
		local event, button, X, Y = os.pullEventRaw()
		if event == "mouse_click" and button == 1 then
			if X >= 2 and X <= 11 and Y == 1 then
				--Shutdown
				optionChose = true
				initShutdown()
			elseif X >= 13 and X <= 22 and Y == 1 then
				--Reboot
				optionChose = true
				initReboot()
			elseif X >= 2 and X <= 20 and Y >= 3 and Y <=5 then
				--RequestPass for User1
				optionChose = true
				getPass(1)
			end
		end
	end
end
--END FUNCTIONS

--OS FUNCTION
function osMain()
	if showStartup == "true" then
		startupLogo()
	elseif showStartup == "false" then
		--Do Nothing
	else
		startupLogoError()
	end
	
	if autoLogin == "" or autoLogin == "0" then
		login()
	else
		loggedIn = tonumber(autoLogin)
	end
	
	desktopColour = tonumber(readFile.readLineFile("CraftedOS/OSFiles/OS/Config/User/User" .. loggedIn .. "Config.cfg", 2))
	desktopMenuBarColour = tonumber(readFile.readLineFile("CraftedOS/OSFiles/OS/Config/User/User" .. loggedIn .. "Config.cfg", 4))
	desktopMenuBtnColour = tonumber(readFile.readLineFile("CraftedOS/OSFiles/OS/Config/User/User" .. loggedIn .. "Config.cfg", 6))
	
	drawDesktop()
end
--END OS FUNCTION

ok, err = pcall(osMain)
if not ok then
	if term.isColour() then
		term.setBackgroundColour(colours.grey)
		term.setTextColour(colours.white)
		term.clear()
		term.setCursorPos(2, 2)
		term.write("CraftedOS has Crashed - " .. textutils.formatTime(os.time(), false))
		term.setCursorPos(2, 3)
		term.write(err)
		term.setCursorPos(2, 4)
		term.write("[       ] Logging Crash")
		f = fs.open("CraftedOS/OSFiles/OS/CrashLog.txt", "a")
		term.setCursorPos(2, 4)
		term.write("[=      ]")
		f.writeLine("====================")
		term.setCursorPos(2, 4)
		term.write("[==     ]")
		f.writeLine(textutils.formatTime(os.time(), false))
		term.setCursorPos(2, 4)
		term.write("[===    ]")
		f.writeLine(err)
		term.setCursorPos(2, 4)
		term.write("[====   ]")
		f.writeLine("====================")
		term.setCursorPos(2, 4)
		term.write("[=====  ]")
		f.writeLine("")
		term.setCursorPos(2, 4)
		term.write("[====== ]")
		f.close()
		term.setCursorPos(2, 4)
		term.write("[=======]")
		term.setCursorPos(2, 5)
		term.write("Logged crash to CraftedOS/OSFiles/OS/CrashLog.txt")
		term.setCursorPos(1, 7)
	else
		term.clear()
		term.setCursorPos(2, 2)
		term.write("CraftedOS has Crashed")
		term.setCursorPos(2, 3)
		term.write(err)
	end
end
