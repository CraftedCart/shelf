CraftedOS
=========

ComputerCraft was a fun mod to mess around in - something I used to enjoy was making graphical shells and calling them
"operating systems"... yeah >.<

This used to have its own git history

```
* e2ed3a2 (HEAD -> master, origin/master, origin/HEAD) Rename Updater to Updater.lua
* 11df9dc Rename CraftedOS to CraftedOS.lua
* df37611 Updates CraftedOS.lua
* f9f95c7 Updated Startup. Currently has bug
* a101b8e Fixed Multiple Points Error
* 718f899 Cleared CrashLog and added Default Configs
* 6d47937 Updated to newest Version - v0.5.1
* 18c6437 Update README.md
* 2281369 (tag: v0.05-InDev) Update README.md
* a024abd Update README.md
* 4063d29 Update README.md
* 6905592 Update README.md
* 8c584fd Update README.md
* 8a2eba9 Update README.md
* 58a8461 Rename README.html to README.md
* 239c94e Update and rename README.md to README.html
* d5911b8 Update README.md
* d3b41aa Create UserDefConfig.cfg
* ea414ef Create OSConfig.cfg
* 416d604 Add readFile API from pastebin
* 962fa6f Delete .gitignore
* 4512706 Update README.md
* b379228 Update README.md
* d00537b Create CraftedOS using pastebin code
* a71e70e Create directory with gitignore
* addcfe0 Added Code Coming soon
* 2d6db51 Added more <br>s
* 24dc412 Added <br>
* 8173b1b Updated README
* fedc044 Initial commit
```

...yeah I wasn't very good with version control in 2016 either

# ORIGINAL README BELOW

---

# CraftedOS
### The ComputerCraft Project to Replace the Shell

Please report bugs and ideas to the GitHub issue tracker. Please select the version under the milestones drop down
