#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>
#include <regex>
#include <iostream>

using namespace boost::program_options;
using namespace boost::filesystem;

#define MAX_MACRO_REPLACEMENTS 128

void printHelp(options_description &desc);
void preprocessFile(std::string filepath);
void checkMacro(std::string origStr, std::string str, unsigned int lineNumber, std::string filepath, std::vector<std::pair<std::regex, std::string>> &macros);

int main(int argc, char *argv[]) {
    options_description desc{"Options"};

    try {
        desc.add_options()
            ("help,h", "Help screen")
            ("file", value<std::vector<std::string>>()->multitoken()->required(), "File to preprocess");

        positional_options_description pos_desc;
        pos_desc.add("file", -1);

        command_line_parser parser{argc, argv};
        parser.options(desc).positional(pos_desc).allow_unregistered();
        parsed_options parsed_options = parser.run();

        variables_map vm;
        store(parsed_options, vm);
        notify(vm);

        if (vm.count("help")) {
            printHelp(desc);
        } else if (vm.count("file")) {
            std::vector<std::string> files = (vm["file"].as<std::vector<std::string>>());

            for (std::string filepath : files) {
                preprocessFile(filepath);
            }
        }
    } catch (const required_option &ex) {
        std::cerr << ex.what() << std::endl << std::endl;
        printHelp(desc);
    } catch (const error &ex) {
        std::cerr << ex.what() << std::endl;
    }

    return 0;
}

void printHelp(options_description &desc) {
    std::cout << "Usage: bspp [options] file..." << std::endl;
    std::cout << desc << std::endl;
}

void preprocessFile(std::string filepath) {
    std::vector<std::pair<std::regex, std::string>> macros;

    ifstream file(filepath);
    std::string str;

    bool inMultilineComment = false;

    unsigned int lineNumber = 1;
    while(std::getline(file, str)) {
        //Used so that we don't toggle multiline again on the 2nd comment stripping pass
        bool toggledMultilineAlready = false;

        //Process all regex macros
        std::string outStr = str; //Output string, after processig all macros

        //Strip multi line comments
        if (outStr == "     ") {
            //Begin/end a multiline
            inMultilineComment = !inMultilineComment;
            toggledMultilineAlready = true;
        }

        if (inMultilineComment) {
            lineNumber++;
            continue;
        }

        //Strip single line comments
        //TODO: Not strip 5 spaces if in string
        static std::regex commentsRx("     .*");
        outStr = std::regex_replace(outStr, commentsRx, "", std::regex_constants::format_sed);

        bool repeatReplace = true;
        unsigned int replacements = 0;

        while (repeatReplace) {
            std::string newOutStr = outStr;

            for (const auto &rx : macros) {
                newOutStr = std::regex_replace(newOutStr, rx.first, rx.second, std::regex_constants::format_sed);
            }

            if (newOutStr != outStr) {
                outStr = newOutStr;
                replacements++;

                repeatReplace = replacements < MAX_MACRO_REPLACEMENTS;

                if (!repeatReplace) {
                    //Too many replacements
                    std::cout << filepath << ":" << lineNumber << ": Too many repeat macro replacements" << std::endl;
                    std::cout << "             " << str << std::endl;
                    std::cout << "Expanded to: " << outStr << std::endl;
                    std::cout << std::endl;
                    exit(EXIT_FAILURE);
                }
            } else {
                repeatReplace = false;
            }
        }

        checkMacro(str, outStr, lineNumber, filepath, macros);

        //Strip comments again
        //Strip multi line comments
        if (!toggledMultilineAlready && outStr == "     ") {
            //Begin/end a multiline
            inMultilineComment = !inMultilineComment;
            toggledMultilineAlready = true;
        }

        if (inMultilineComment) {
            lineNumber++;
            continue;
        }

        //Strip single line comments
        //TODO: Not strip 5 spaces if in string
        outStr = std::regex_replace(outStr, commentsRx, "", std::regex_constants::format_sed);

        //Strp empty lines and lines that begin with #
        if (
                boost::starts_with(outStr, "#") ||
                outStr == ""
           ) {
            lineNumber++;
            continue;
        }

        std::cout << outStr << std::endl;

        lineNumber++;
    }
}

void checkMacro(std::string origStr, std::string str, unsigned int lineNumber, std::string filepath, std::vector<std::pair<std::regex, std::string>> &macros) {
        if (boost::starts_with(str, "#define")) {
            //Find the search and replacement parts of the string
            unsigned int searchStart = 0;
            unsigned int searchEnd = 0;
            unsigned int replaceStart = 0;
            unsigned int replaceEnd = 0;

            bool skipNext = false;
            unsigned int i = 0;
            std::string regexStr = str.substr(8);
            for (const char &c : regexStr) {
                if (skipNext) {
                    skipNext = false;
                    i++;
                    continue;
                }

                if (c == '\\') {
                    skipNext = true;
                    i++;
                    continue;
                }

                if (c == '/') {
                    if (searchStart == 0) {
                        searchStart = i + 1;
                    } else if (replaceStart == 0) {
                        searchEnd = i - 1;
                        replaceStart = i + 1;
                    } else if (replaceEnd == 0) {
                        replaceEnd = i - 1;
                    } else {
                        //Too many forward slashes!
                        std::cout << filepath << ":" << lineNumber << ": Invalid #define replacement regex - too many forward slashes" << std::endl;
                        std::cout << "             " << origStr << std::endl;
                        std::cout << "Expanded to: " << str << std::endl;
                        std::cout << std::endl;
                        exit(EXIT_FAILURE);
                    }
                }

                i++;
            }

            //If any of the search/replace values are zero, throw an error
            //TODO: Actually throw an exception
            if (
                    searchStart == 0 ||
                    searchEnd == 0 ||
                    replaceStart == 0 ||
                    replaceEnd == 0
               ) {
                std::cout << filepath << ":" << lineNumber << ": Invalid #define replacement regex" << std::endl;
                std::cout << "             " << origStr << std::endl;
                std::cout << "Expanded to: " << str << std::endl;
                std::cout << std::endl;
                exit(EXIT_FAILURE);
            }

            std::regex rx(regexStr.substr(searchStart, searchEnd - searchStart + 1));
            std::string replace = regexStr.substr(replaceStart, replaceEnd - replaceStart + 1);

            //Replace escaped chars
            boost::algorithm::replace_all(replace, "\\n", "\n");
            boost::algorithm::replace_all(replace, "\\r", "\r");
            boost::algorithm::replace_all(replace, "\\t", "\t");
            boost::algorithm::replace_all(replace, "\\ ", " ");
            boost::algorithm::replace_all(replace, "\\/", "/");
            boost::algorithm::replace_all(replace, "\\\\", "\\");

            macros.push_back(std::pair<std::regex, std::string>(rx, replace));
        }
}

